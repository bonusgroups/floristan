<?php

namespace App\Facades;

use App\Services\FranchiseeService;
use Illuminate\Support\Facades\URL;

/**
 * Class UrlDependCityFacade
 * @package App\Facades
 */
class UrlDependCityFacade extends URL
{
    /**
     * @param string $name
     * @param array $parameters
     * @param bool $absolute
     * @return string
     * @throws \Exception
     */
    public static function route($name, $parameters = [], $absolute = true)
    {
        /** @var FranchiseeService $franchiseeService */
        $franchiseeService = app(FranchiseeService::class);

        if ($franchiseeService->isCurrentFranchiseeDetected()) {
            $parameters = array_merge(['city' => $franchiseeService->getCurrentFranchisee()->city_domain], $parameters);
            return parent::route($name, $parameters, $absolute);
        }

        return parent::route($name, $parameters, $absolute);
    }
}
