<?php

namespace App;

use App\Models\Banner;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManager;

class BannerImage extends Model
{

    protected $fillable = [
        'file_name',
        'banner_id',
    ];


    public function banner()
    {
        return $this->belongsTo(Banner::class);
    }


    public function getUrl()
    {
        return url($this->getPath());
    }


    public function getFileName()
    {
        return $this->file_name;
    }


    public function getBasePath()
    {
        return 'storage/banner_images';
    }


    public function getPath()
    {
        return $this->getBasePath() . '/' . $this->getFileName();
    }

}
