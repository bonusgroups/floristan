<?php

// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);


  error_reporting(0);
  
$user = array(
    'USER_LOGIN'=>'crmfloristan@mail.ru',
    'USER_HASH'=>'a717ddb74ec8f348cf12a988f551643f01ad73fa'
);

$subdomain = 'floristan116';


#Формируем ссылку для запроса
$link = 'https://'.$subdomain.'.amocrm.ru/private/api/auth.php?type=json';

$curl = set_curl($link, http_build_query($user));
$out = curl_exec($curl);
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);

$code=(int)$code;
$errors=array(
    301=>'Moved permanently',
    400=>'Bad request',
    401=>'Unauthorized',
    403=>'Forbidden',
    404=>'Not found',
    500=>'Internal server error',
    502=>'Bad gateway',
    503=>'Service unavailable'
);
try
{
#Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
    if($code!=200 && $code!=204)
        throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
}
catch(Exception $E)
{
    print_r('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
    $no_errors=false;
}

curl_close($curl); #Завершаем сеанс cURL

$Response = json_decode($out,true);
$Response = $Response['response'];

//получить данные по привязанному контакту


$link = 'https://'.$subdomain.'.amocrm.ru/private/api/v2/json/accounts/current';

$curl = set_curl_read($link);
$out = curl_exec($curl);
$code = curl_getinfo($curl,CURLINFO_HTTP_CODE);
curl_close($curl); #Завершаем сеанс cURL

$Response = json_decode($out,true);
$Response = $Response['response'];


//получаем данные из амо о полях
$rows_fields_birthday = array();
$Response_custom_fields_contacts = $Response['account']['custom_fields']['contacts'];

foreach ($Response_custom_fields_contacts as $Response_temp) {

    if (str_pos_new("День рождения", $Response_temp['name'])) {
        $rows_fields_birthday[] = $Response_temp['id'];
    }

}

//print_r($rows_fields_birthday);





$num = 500;
$page = 0;
$kk = 0;
$kk2 = 0;

$kk3 = 0;
$kk4 = 0;


//считывание всех сделок из Амо
do {

    $link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/contacts/list?full=Y&limit_rows='.$num.'&limit_offset='.($page*$num).'';
    $page++;
    $curl = set_curl_read($link);

    $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную

    $Response = json_decode($out,true);

    $Response = $Response['response']['contacts'];
    $count = count($Response);

    foreach ($Response as $Response_row) {

        // print_r($Response_row);

        $clients_email  = '';
        $clients_phone  = '';
        $clients_name = $Response_row['name'];

        $Response_item = $Response_row['custom_fields'];


        $custom_fields = array();

        //считывание дополнительных полей в удобный массив
        foreach ($Response_row['custom_fields'] as $custom_fields_item) {
            $custom_fields[$custom_fields_item['id']] = $custom_fields_item['values'][0]['value'];
        }

        $leads[$kk] = [
            'id'=>$Response_row['id'],
            'name'=>$Response_row['name'],
            'birthday'=>[
            ],
            'responsible_user_id'=>$Response_row['responsible_user_id'],
        ];

        foreach ($rows_fields_birthday as $row_fields_birthday) {
            $leads[$kk]['birthday'][$row_fields_birthday] = $custom_fields[$row_fields_birthday];
        }

        $kk++;
    }

//} while ($page<= 57);
} while ($count!= 0);

//echo $kk;
//print_r(  $leads);


foreach ($leads as $contact) {

    //Обнуляем переменные
    $flag_task = false;
    $flag_birthday = false;

    foreach ($contact['birthday'] as $birthday_num => $birthday_date) {
        $flag_task[$birthday_num] = false;
        // $flag_birthday[$birthday_num] = false;
    }

    //Проходим по всем ДР, ищем где есть совпадение 3 дня
    foreach ($contact['birthday'] as $birthday_num => $birthday_date) {

        $contact['birthday_new'][$birthday_num] = '';
        $contact['birthday_day'][$birthday_num] = '';
        $contact['birthday_task'][$birthday_num] = '';

        if ($birthday_date!='') {
            $flag_birthday = true;
            $birthday_new_date = date('d.m.'.date('Y'), strtotime($birthday_date));

            $birthday_count_day = round((strtotime($birthday_new_date)-strtotime(date('d.m.Y')))/24/60/60);


            // if ($contact['birthday1_task']=='') {
            if ($birthday_count_day==2) {
                // echo  $birthday_count_day.'<br/>';
                $flag_task = true;
                $contact['birthday_date'] = $birthday_date;
                $contact['birthday_task']='1';
            }
            // }

        }

        # code...
    }




    if ($contact['birthday_task']=='1') {
        // print_r($contact);
        $contacts[] = $contact;


    }


}
//ставим задачи



foreach ($contacts as $contact) {
    // print_r( $contact); echo "<br/>\n\r";

    echo '#'.$contact['id'].'. '.$contact['name'].' - '.date('d.m.Y', strtotime($contact['birthday_date']));
    echo "<br/>\n\r";

    $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/tasks/set';
    $contacts['request']['tasks']['add'] = array(
        array(

            "element_id" => $contact['id'],
            "element_type" => 1,
            'task_type' => 1,
            'text' => 'Связаться с клиентом по случаю дня рождения или знаменательной даты ('.date('d.m.Y', strtotime($contact['birthday_date'])).')!',
            'created_at'=>time(),
            'updated_at'=>time(),
            'responsible_user_id' => $contact['responsible_user_id'],
            'created_by' =>  $contact['responsible_user_id'],
            'complete_till_at' => strtotime(date("Y-m-d 23:59",time())),

        )
    );

    $curl = set_curl_task($link, json_encode($contacts));
    $out = curl_exec($curl);
    $Response = json_decode($out, true);


    // //var_dump($Response);
    // $Response = $Response['response']['tasks']['add'];

}

//меняем контакт, ставим флаг, что задачу поставили

//



// echo '<table border="1">';
// echo '<tr>';
// foreach ($contacts[0] as $key => $value) {
//     echo '<td>'.$key.'</td>';
// }
// echo '</tr>';


// foreach ($contacts as $key => $values) {
//   echo '<tr>';
//   foreach ($values as $value) {

//    //if ($key=='datatime') {$value = date('d.n.y',$value); }
//    echo '<td>'.$value.'</td>';
//  }
//  echo '</tr>';
// }
// echo '</table>';


function str_pos_new($find, $string) {
    $pos = strpos($string, $find);
    if ($pos === false) {
        $result = false;
    } else {
        $result = true;
    }
    return $result;
}


function set_curl($link,$data){
    $curl=curl_init();
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$link);
    curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
    curl_setopt($curl,CURLOPT_POST, true);
    curl_setopt($curl,CURLOPT_POSTFIELDS, $data);
    //curl_setopt($curl,CURLOPT_HTTPHEADER,array('IF-MODIFIED-SINCE: Tue, 01 Aug 2017 00:00:00'));
    //curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
    return $curl;
}

function set_curl_read($link){
    $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$link);
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__

    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);

    $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
    $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
    return $curl;
}


function set_curl_task($link,$data){
    $curl=curl_init();
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$link);
    curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
    curl_setopt($curl,CURLOPT_POST, true);
    curl_setopt($curl,CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
    return $curl;
}

if (!function_exists('get_var')) {
    function get_var($var) {
        if (isset($_REQUEST[$var])) {$result = $_REQUEST[$var]; } else { $result = ''; }
        return $result;
    }
}


?>