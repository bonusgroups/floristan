<?php

if (!function_exists('city_domain')) {
    /**
     * @param string $city Domain's part with city
     * @return string
     * @throws Exception
     */
    function city_domain($city)
    {
        if (!env('APP_BASE_DOMAIN')) {
            throw new Exception('Please, set APP_BASE_DOMAIN in .env');
        }

        return $city . '.' . env('APP_BASE_DOMAIN');
    }
}

if (!function_exists('routeWithCity')) {
    /**
     * @param string|array $name
     * @param array $arguments
     * @param bool $absolute
     * @return string
     */
    function routeWithCity($name, $arguments = [], $absolute = true)
    {
        $arguments = array_merge($arguments, ['city' => request('city')]);
        return route($name, $arguments, $absolute);
    }
}

if (!function_exists('lang')) {
    /**
     * @param $key
     * @return mixed
     */
    function lang($key, $arguments = [])
    {
        return \Illuminate\Support\Facades\Lang::get('all.' . $key, $arguments);
    }
}

if (!function_exists('field')) {
    function field($model, $field)
    {
        $field = $field . "_" . App::getLocale();

        if (!$model) return null;

        return $model->{$field} ?: null;
    }
}

if (!function_exists('get_cart_size')) {
    function get_cart_size($format = true)
    {
        $size = Session::get('cart_size', 0);
        return $format ? number_format($size, 0, '.', ' ') : $size;
    }
}

if (!function_exists('fix_number_format')) {
    function price_format($price, $add = ' ₽')
    {
        return number_format($price, 2, '.', ' ') . $add;
    }
}

if (!function_exists("price_mul")) {
    function price_mul($price, \App\Models\Bouquet $bouquet = null)
    {
        if ($bouquet and $bouquet->isAdditional()) {
            $mul = \App\Services\SettingService::shared()->get("mul_price_additional", 250) / 100;
        } else {
            $mul = \App\Services\SettingService::shared()->get("mul_price", 250) / 100;
        }

        return $price * $mul;
    }
}

if (!function_exists('size_to_text')) {
    function size_to_text($size)
    {
        switch ($size) {
            case \App\Models\Bouquet::SIZE_SMALL:
                return 'Маленький';
            case \App\Models\Bouquet::SIZE_MIDDLE:
                return 'Средний';
            case \App\Models\Bouquet::SIZE_BIG:
                return 'Большой';
        }
    }
}


function current_franchisee()
{
    return (new \App\Services\FranchiseeService())->getCurrentFranchisee();
}