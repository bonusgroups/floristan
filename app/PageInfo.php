<?php

namespace App;

use App\Models\Franchisee;
use Illuminate\Database\Eloquent\Model;

class PageInfo extends Model
{

    public $fillable = [
        'path',
        'title',
        'meta_keywords',
        'meta_description'
    ];


    public static function getByPath($path)
    {
        return self::where('path', $path)->first();
    }


    public function getTitle()
    {
        return $this->replaceTokens($this->title);
    }


    public function getMetaDescription()
    {
        return $this->replaceTokens($this->meta_description);
    }


    public function getMetaKeywords()
    {
        return $this->replaceTokens($this->meta_keywords);
    }


    public function replaceTokens($text)
    {
        foreach ($this->getTokens() as $token => $value) {
            $text = str_replace($token, $value, $text);
        }

        return $text;
    }


    public function getTokens()
    {
        if ($this->tokens) return $this->tokens;

        $franchisee = Franchisee::getCurrent();

        $tokens = [
            '[city]' => $franchisee->city_ru,
            '[city_prepositional]' => $franchisee->prepositional_name ?: $franchisee->city_ru
        ];

        $this->tokens = $tokens;

        return $tokens;
    }


    public static function getCanonical()
    {
        $city = request()->get('city');

        if ($city) {
            $url = url(request()->path());

            if ($query = request()->except(['city'])) {
                $url .= '?' . http_build_query($query);
            }

            return $url;
        }

        return null;
    }

}
