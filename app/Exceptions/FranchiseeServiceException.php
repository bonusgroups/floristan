<?php

namespace App\Exceptions;

use Exception;

/**
 * Class FranchiseeServiceException
 * @package App\Exceptions
 */
class FranchiseeServiceException extends \Exception
{

    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

}
