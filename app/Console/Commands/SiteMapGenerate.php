<?php

namespace App\Console\Commands;

use App\Models\Bouquet;
use App\Models\BouquetCategory;
use App\Models\Franchisee;
use App\Services\FranchiseeService;
use Illuminate\Console\Command;
use samdark\sitemap\Sitemap;

class SiteMapGenerate extends Command
{

    public $baseUrl = null;


    /**
     * @return null
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param null $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = trim($baseUrl, '/');
    }


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function getUrl($path)
    {
        $path = trim($path, '/');
        $url = $this->getBaseUrl() . '/' . $path;

        return trim($url, '/');
    }


    public function handle()
    {
        foreach (Franchisee::all() as $item) {
            $domain = $item->domain;

            $this->setBaseUrl('https://' . $domain);

            $sitemap = new Sitemap('public/storage/sitemap/' . $domain . '.xml');

            $sitemap->addItem($this->getUrl('/'));
            $sitemap->addItem($this->getUrl('catalog/all'));
            $sitemap->addItem($this->getUrl('delivery'));
            $sitemap->addItem($this->getUrl('reviews'));
            $sitemap->addItem($this->getUrl('about'));
            $sitemap->addItem($this->getUrl('contacts'));

            foreach (BouquetCategory::query()->get(['id', 'slug']) as $category) {
                $sitemap->addItem($this->getUrl('catalog/' . $category->slug));
            }

            foreach (Bouquet::query()->get(['id']) as $bouquet) {
                $sitemap->addItem($this->getUrl('catalog/product/' . $bouquet->id));
            }

            $sitemap->write();
        }
    }
}
