<?php

namespace App\Console\Commands;

use App\Models\AdminUser;
use Hash;
use Illuminate\Console\Command;

/**
 * Class MakeAdmin
 * @package App\Console\Commands
 */
class MakeAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create user for administration panel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->ask('Email');

        if (AdminUser::whereEmail($email)->exists()) {
            $this->error('Пользователь с таким email уже существует');

            return;
        }

        $password = $this->ask('Пароль');
        $passwordConfirmation = $this->ask('Подтверждение пароля');

        if ($password != $passwordConfirmation) {
            $this->error('Пароли не совпадают');
        }

        $name = $this->ask('Имя');

        AdminUser::create([
            'email' => $email,
            'name' => $name,
            'password' => Hash::make($password),
        ]);

        $this->info("Пользователь {$email} успешно создан");

        return;
    }
}
