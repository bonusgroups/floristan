<?php

namespace App\Console\Commands;

use App\Models\AdminUser;
use Illuminate\Console\Command;

class ChangePsw extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:password';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = AdminUser::get();
        foreach($users as $i=>$user) {
            $this->line($i.': '.$user->email);
        }

        $res = $this->ask("User number");
        if(is_null($res)) {
            return;
        }

        if(!isset($users[$res])) {
            return;
        }

        $user = $users[$i];
        $user->email = $this->ask("email", $user->email);
        $psw = $this->ask("password");
        if(!is_null($psw) && !empty($psw)) {
            $user->password = \Hash::make($psw);
        }
        $user->save();
    }
}
