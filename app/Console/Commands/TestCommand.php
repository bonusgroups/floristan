<?php

namespace App\Console\Commands;

use App\Classes\AmoCrm;
use App\Http\Controllers\Site\CheckoutController;
use App\Models\Delivery;
use App\Models\Flower;
use App\Models\FlowerPrice;
use App\Models\Franchisee;
use App\Models\Order;
use App\Services\YaKassaService;
use Curl\Curl;
use DB;
use Exception;
use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Illuminate\Console\Command;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'site:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);

        $spreadsheetId = '1vwz_iKqI3ffMH4rwjBgCzrhxZEf5D4DEgILomN4-FJ8';

        $count = count($service->spreadsheets_values->get($spreadsheetId, 'A:A')->getValues());
        $rowNumber = $count + 1;

        $values = [
            ['test3', 'test2']
        ];

        $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

        $response = $service->spreadsheets_values->update($spreadsheetId, 'A' . $rowNumber, $body, ['valueInputOption' => 'USER_ENTERED']);

        dd(
            $response
        );

    }


    public function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Google Sheets API PHP Quickstart');
        $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
        $client->setAuthConfig('credentials.json');
        $client->setAccessType('offline');
//        $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = 'token.json';

        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }


}
