<?php

namespace App\Providers;

use App\Models\Franchisee;
use App\Models\Order;
use App\Services\FranchiseeService;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(FranchiseeService::class, function () {
            return new FranchiseeService();
        });

        if (!$this->app->runningInConsole()) {
            $this->shareCurrentFranchiseeInViews();
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }

    private function shareCurrentFranchiseeInViews()
    {
        \View::creator('*', function (View $view) {
            $view->with('currentFranchisee', Franchisee::getCurrent() ?: Franchisee::getDefaultFranchisee());
        });
    }
}
