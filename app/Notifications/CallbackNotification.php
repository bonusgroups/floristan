<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CallbackNotification extends Notification
{
    use Queueable;
    /**
     * @var string
     */
    private $phone = '';


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($phone)
    {
        //
        $this->phone = $phone;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return
            (new MailMessage)
                ->subject('Обратный звонок')
                ->bcc('info@floristan.ru')
                ->bcc('floristan116@mail.ru')
                ->greeting('Обратный зовонок!')
                ->line('Номер телефона: '.$this->phone);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
