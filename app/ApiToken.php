<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ApiToken extends Model
{

    public function isExpired()
    {
        return Carbon::now()->addMonth(-1)->gt($this->created_at);
    }

}
