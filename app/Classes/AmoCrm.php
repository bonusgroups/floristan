<?php namespace App\Classes;


use Exception;

Class AmoCrm
{

    function send_amo($data_amo)
    {
        $GAId = array_key_exists('_ga', $_COOKIE) ? $_COOKIE['_ga'] : "неизвестно";

        if (trim($data_amo['name']) == '') {
            $data_amo['name'] = 'Имя не указано';
        }
        if (trim($data_amo['phone']) == '') {
            $data_amo['phone'] = '';
        }
        if (trim($data_amo['email']) == '') {
            $data_amo['email'] = '';
        }

        $user = array(
            'USER_LOGIN' => 'crmfloristan@mail.ru',
            'USER_HASH' => 'a717ddb74ec8f348cf12a988f551643f01ad73fa'
        );

        $subdomain = 'floristan116';

        $manager_id = '2939704';
        $status_id = '22759504';

        $data_id = array(
            'phone' => '458295',
            'email' => '458297',
            'delivery_date' => '463607',
            'contacts' => '463397',
            'delivery_address' => '463323',
            'delivery_time' => '463297',
            'delivery_type' => '463273',
            'dopinfo' => '463301',
            'delivery_date2' => '463297',
            'pay_type' => '463291',
            'price' => '461665',
            //'roistat'=>'',
        );

        if ($data_amo['delivery_address'] != '') {
            $data_amo['delivery_type'] = '824287'; //курьером
        } else {
            $data_amo['delivery_type'] = '824285'; //самовывоз
        }

        $tags = $data_amo['tags'];

        $comment = "";
        $comment .= "Дата и время заявки: " . date('d.n.Y H:i:s') . "\n";
        if ($data_amo['name'] != '') {
            $comment .= "ФИО: " . $data_amo['name'] . "\n";
        }
        if ($data_amo['phone'] != '') {
            $comment .= "Телефон: " . $data_amo['phone'] . "\n";
        }
        if ($data_amo['email'] != '') {
            $comment .= "Электронный адрес: " . $data_amo['email'] . "\n";
        }
        // if ($data_amo['param']!='') { $comment .="Сайт: ".$data_amo['param']."\n";}
        // if ($data_amo['utm']!='') { $comment .="".$data_amo['utm']."\n";}
        // if ($data_amo['ip']!='') { $comment .="ip: ".$data_amo['ip']."\n";}

        $amo_comment = '';
        $data_amo['summ'] = 0;

        if ($data_amo['zakaz']) {
            $data_amo['count'] = count($data_amo['zakaz']);
            foreach ($data_amo['zakaz'] as $row_zakaz_item) {
                $amo_comment .= $row_zakaz_item['name'] . " " . $row_zakaz_item['razmer'] . " " . $row_zakaz_item['price'] . " руб.\n\r";
                $amo_comment .= $row_zakaz_item['link'] . "\n\r";
                $data_amo['summ'] += $row_zakaz_item['price'];
            }
            $amo_comment .= "Сумма заказа: " . $data_amo['summ'] . " руб.\n\r";
        }

        if ($data_amo['text_otkrytki'] != '') {
            $amo_comment .= "...\n\rТекст открытки: " . $data_amo['text_otkrytki'] . "\n\r...\n\r";
        }

        if ($data_amo['delivery_address'] != '') {
            $amo_comment .= "Адрес доставки: " . $data_amo['delivery_address'] . "\n\r";
        }

        if ($data_amo['delivery_date'] != '') {
            $amo_comment .= "Дата доставки: " . $data_amo['delivery_date'] . "\n\r";
        }
        if ($data_amo['delivery_time'] != '') {
            $amo_comment .= "Время доставки: " . $data_amo['delivery_time'] . "\n\r";
        }

        if ($data_amo['contact_phone'] != '') {
            $amo_comment .= "...\n\rТелефон получателя: " . $data_amo['contact_phone'] . "\n\r";
        }

        if ($data_amo['contact_name'] != '') {
            $amo_comment .= "Имя получателя: " . $data_amo['contact_name'] . "\n\r";
        }


        $data_amo['comment'] = $amo_comment;


        if ($data_amo['comment'] != '') {
            $comment .= "...\n\rДетали заказа: \n" . $data_amo['comment'] . "\n";
        }

        #Формируем ссылку для запроса

        $link = 'https://' . $subdomain . '.amocrm.ru/private/api/auth.php?type=json';

        $curl = $this->set_curl($link, http_build_query($user));
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl); #Завершаем сеанс cURL

        $Response = json_decode($out, true);
        $Response = $Response['response'];

        //ищем контакт

        $phone = $data_amo['phone'];
        $email = $data_amo['email'];

        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace('-', '', $phone);
        $phone = str_replace(' ', '', $phone);

        $data_amo['phone'] = $phone;

        $phone = substr($phone, 2);

        if (strlen($phone) > 6) {

            $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/contacts/list?query=' . $phone;
            $curl = $this->set_curl_read($link);

            $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $code = (int)$code;
            $errors = array(
                301 => 'Moved permanently',
                400 => 'Bad request',
                401 => 'Unauthorized',
                403 => 'Forbidden',
                404 => 'Not found',
                500 => 'Internal server error',
                502 => 'Bad gateway',
                503 => 'Service unavailable'
            );
            try {
                #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
                if ($code != 200 && $code != 204)
                    throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
            } catch (Exception $E) {
                echo('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
            }

            /**
             * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
             * нам придётся перевести ответ в формат, понятный PHP
             */
            $Response = json_decode($out, true);
            $Response = $Response['response']['contacts'];

            $count_response_phone = count($Response);

            /* если что-то нашли */
            $links_ids_all_phone = array();

            if ($count_response_phone > 0) {

                $current_manager_phone['id'] = $Response[0]['responsible_user_id'];
                $current_client_phone = $Response[0]['id'];
                $output_link_phone = '';
                $Response_link_phone = $Response[0]['linked_leads_id'];

                foreach ($Response_link_phone as $v) {

                    $output_link_phone .= $v . ', ';
                    $links_ids_all_phone[] = $v;

                }

                // echo ('Количество найденных результатов: '.count($count_response).'<br/>');
                // echo ('ID клиента: '.$current_client .'<br/>');
                // echo ('Клиента ведет менеджер: '.$current_manager['id'] .'<br/>');
                // echo ('Сделки контакта: '.$output_link .'<br/>');

                $search_phone = 'Yes';
                $repeat_phone = '[повтор]';
            } else {
                $search_phone = 'No';
                $current_manager_phone['id'] = $manager_id;
                $repeat_phone = '';
            }
        } else {

            $count_response_phone = 0;
            $search_phone = 'No';
            $current_manager_phone['id'] = $manager_id;
            $repeat_phone = '';
        }

        //поиск по email

        if (strlen($email) > 6) {
            $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/contacts/list?query=' . $email;
            $curl = $this->set_curl_read($link);

            $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $code = (int)$code;
            $errors = array(
                301 => 'Moved permanently',
                400 => 'Bad request',
                401 => 'Unauthorized',
                403 => 'Forbidden',
                404 => 'Not found',
                500 => 'Internal server error',
                502 => 'Bad gateway',
                503 => 'Service unavailable'
            );
            try {
                #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
                if ($code != 200 && $code != 204)
                    throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
            } catch (Exception $E) {
                echo('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
            }

            /**
             * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
             * нам придётся перевести ответ в формат, понятный PHP
             */
            $Response = json_decode($out, true);
            $Response = $Response['response']['contacts'];
            $count_response_email = count($Response);


            /* если что-то нашли */
            $links_ids_all_email = array();

            if ($count_response_email > 0) {

                $current_manager_email['id'] = $Response[0]['responsible_user_id'];
                $current_client_email = $Response[0]['id'];
                $output_link_email = '';
                $Response_link_email = $Response[0]['linked_leads_id'];

                foreach ($Response_link_email as $v) {

                    $output_link_email .= $v . ', ';
                    $links_ids_all_email[] = $v;

                }


                // echo ('Количество найденных результатов: '.count($count_response).'<br/>');
                // echo ('ID клиента: '.$current_client .'<br/>');
                // echo ('Клиента ведет менеджер: '.$current_manager['id'] .'<br/>');
                // echo ('Сделки контакта: '.$output_link .'<br/>');


                $search_email = 'Yes';
                $repeat_email = '[повтор]';
            } else {
                $search_email = 'No';
                $current_manager_email['id'] = $manager_id;
                $repeat_email = '';
            }

        } else {
            $count_response_email = 0;
            $search_email = 'No';
            $current_manager_email['id'] = $manager_id;
            $repeat_email = '';
        }

        //сохраняем переменные. Приоритет данным по email

        if ($count_response_email > 0) {
            $search = $search_email;
            $repeat = $repeat_email;
            $links_ids_all = $links_ids_all_email;

            $current_manager['id'] = $current_manager_email['id'];
            $current_client = $current_client_email;
            $Response_link = $Response_link_email;

            $count_response = $count_response_email;
        } else {
            if ($count_response_phone > 0) {
                $search = $search_phone;
                $repeat = $repeat_phone;
                $links_ids_all = $links_ids_all_phone;

                $current_manager['id'] = $current_manager_phone['id'];
                $current_client = $current_client_phone;
                $Response_link = $Response_link_phone;

                $count_response = $count_response_phone;
            } else {
                $search = 'No';
                $current_manager['id'] = $manager_id;
                $repeat = '';
                $count_response = '0';
            }
        }

        // сделка

        $leads['request']['leads']['add'] = array(
            array(
                'name' => $data_amo['title'] . ' - ' . $data_amo['name'] . ' ' . $repeat,
                'date_create' => time(),
                'tags' => $tags,
                'price' => $data_amo['summ'],
                'status_id' => $status_id,
                'responsible_user_id' => $current_manager['id'],
                'custom_fields' => array(
                    array('id' => $data_id['delivery_address'],
                        'values' => array(
                            array(
                                'value' => $data_amo['delivery_address']
                            )
                        )
                    ),
                    array('id' => $data_id['delivery_date'],
                        'values' => array(
                            array(
                                'value' => $data_amo['delivery_date']
                            )
                        )
                    ),
                    array('id' => $data_id['delivery_type'],
                        'values' => array(
                            array(
                                'value' => $data_amo['delivery_type']
                            )
                        )
                    ),
                    array('id' => $data_id['contacts'],
                        'values' => array(
                            array(
                                'value' => $data_amo['contact_phone'] . ' ' . $data_amo['contact_name']
                            )
                        )
                    ),
                    // array('id'=>$data_id['delivery_time'],
                    // 'values' => array(
                    //   array(
                    //     'value'=> $data_amo['delivery_time']
                    //     )
                    //   )
                    // ),
                    array('id' => $data_id['dopinfo'],
                        'values' => array(
                            array(
                                'value' => $data_amo['delivery_address'] . ' - ' . $data_amo['delivery_date'] . ' в ' . $data_amo['delivery_time']
                            )
                        )
                    ),
                    array('id' => $data_id['delivery_date2'],
                        'values' => array(
                            array(
                                'value' => $data_amo['delivery_date']
                            )
                        )
                    ),
                    array('id' => $data_id['pay_type'],
                        'values' => array(
                            array(
                                'value' => $data_amo['pay_type']
                            )
                        )
                    ),
                    //  array('id'=>'293299',
                    //   'values' => array(
                    //     array(
                    //       'value'=> 589035
                    //       )
                    //     )
                    // ),
                )
            )
        );

        //     'delivery_date2'=>'463297',
        //     'pay_type'=>'463291',

        //print_r($leads['request']['leads']['add']);

        $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/leads/set';
        $curl = $this->set_curl($link, json_encode($leads));

        $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $code = (int)$code;
        $errors = array(
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        );
        try {
            #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
            if ($code != 200 && $code != 204)
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        } catch (Exception $E) {
            echo('Ошибка создания лида: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }

        /**
         * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
         * нам придётся перевести ответ в формат, понятный PHP
         */
        $Response = json_decode($out, true);
        //print_r($out);

        $Response = $Response['response']['leads']['add'];

        //$output='ID добавленных сделок:'.PHP_EOL;
        $links_ids = array();

        foreach ($Response as $v) {
            if (is_array($v)) {
                //$output.=$v['id'].PHP_EOL;
                $links_ids[] = $v['id'];
            }
        }

        // контакт

        if ($count_response > 0) {
            $links_ids_all_out = array_merge($links_ids, $links_ids_all);

            //обновляем контакт
            $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/contacts/set';
            $contacts['request']['contacts']['update'] = array(
                array(
                    'id' => $current_client,
                    'last_modified' => time(),
                    'linked_leads_id' => $links_ids_all_out,


                )
            );

            $curl = $this->set_curl($link, json_encode($contacts));

            $out = curl_exec($curl);
            $Response = json_decode($out, true);
            $Response = $Response['response']['contacts']['update'];

            //$output='ID добавленных контактов:'.PHP_EOL;
            foreach ($Response as $v) {
                if (is_array($v)) {
                    // $output.=$v['id'].PHP_EOL;

                }
            }

            // var_dump($output);
        } else {
            //контакт создаем

            $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/contacts/set';

            $contacts['request']['contacts']['add'] = array(
                array(
                    'name' => $data_amo['name'],
                    'linked_leads_id' => $links_ids,
                    'tags' => $tags,
                    'date_create' => time(),
                    'responsible_user_id' => $current_manager['id'],
                    'custom_fields' => array(
                        // array('id'=>$data_id['roistat'],
                        // 'values' => array(
                        //   array(
                        //     'value'=> $data_amo['roistat']
                        //     )
                        //   )
                        // ),
                    )
                )
            );

            if ($data_amo['phone'] != '') {

                $contacts['request']['contacts']['add'][0]['custom_fields'][] = array(
                    'id' => $data_id['phone'],
                    'values' => array(
                        array('value' => $data_amo['phone'],
                            'enum' => "MOB"
                        )
                    )
                );
            }

            if ($data_amo['email'] != '') {
                $contacts['request']['contacts']['add'][0]['custom_fields'][] = array(
                    'id' => $data_id['email'],
                    'values' => array(
                        array('value' => $data_amo['email'],
                            'enum' => "WORK"
                        )
                    )
                );
            }
        }

        $curl = $this->set_curl($link, json_encode($contacts));

        $out = curl_exec($curl);
        $Response = json_decode($out, true);

        //$Response=$Response['response']['contacts']['add'];

        //$output='ID добавленных контактов:'.PHP_EOL;
                //var_dump($output);


        //задача не нужна
        // $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/tasks/set';
        // $contacts['request']['tasks']['add'] = array(
        //    array(

        //        "element_id" => $links_ids[0],
        //        'responsible_user_id'=>$current_manager['id'],
        //        "element_type" => 2,
        //        'task_type' => 1,
        //        'text' => 'Ответить на запрос клиента: '.$data_amo['name'],
        //        'complete_till' => strtotime(date("Y-m-d",time())." 23:59")
        //        )
        //    );

        // $curl = set_curl($link, json_encode($contacts));

        // $out = curl_exec($curl);
        // $Response = json_decode($out, true);
        //      //var_dump($Response);

        // $Response = $Response['response']['tasks']['add'];

        $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/notes/set';
        $contacts['request']['notes']['add'] = array(
            array(

                "element_id" => $links_ids[0],
                "element_type" => 2,
                'note_type' => 4,
                'text' => "" . $tags . ":\n" . $comment . ''
            )
        );

        $curl = $this->set_curl($link, json_encode($contacts));

        $out = curl_exec($curl);
        $Response = json_decode($out, true);
        //var_dump($Response);
        $Response = $Response['response']['notes']['add'];
    }


    function get_ip()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }


    function set_curl($link, $data)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        return $curl;
    }


    function set_curl_read($link)
    {
        $curl = curl_init();
        #Сохраняем дескриптор сеанса cURL
        #Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        return $curl;
    }

}
