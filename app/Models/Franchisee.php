<?php

namespace App\Models;

use App\Services\FranchiseeService;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notifiable;

/**
 * Class Franchisee
 * @package App\Models
 */
class Franchisee extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'city_ru',
        'city_en',
        'domain',
        'email',
        'phone',
        'address_ru',
        'address_en',
        'opening_hours',
        'commission_for_bouquets',
        'requisites',
        'password',
        "social_vk",
        "social_fb",
        "social_ig",
        "social_wa",
        "social_tg",
        "social_vb",
        'meta_yandex_verification',
        'meta_google_verification',
        'prepositional_name'
    ];

    protected $dates = ["total_date"];

    protected $hidden = [
        'password',
        'remember_token',
    ];


    public static function findByDomain($host)
    {
        return self::where('domain', $host)->first();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }

    /**
     * @return null|string
     */
    public function getCityDomainAttribute()
    {
        $explodedDomain = explode('.', $this->domain);

        if (!empty($explodedDomain)) {
            return $explodedDomain[0];
        }

        return null;
    }

    /**
     * AdminUser constructor.
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

//        ResetPassword::toMailUsing(function (Franchisee $user, $token) {
//            return (new MailMessage)
//                ->greeting('Здравствуйте!')
//                ->line('Вы получили это письмо потому, что на сайте Floristan была запущена процедура сброса пароля.')
//                ->action(
//                    'Сбросить пароль',
//                    \URL::route('franchisee.password.reset', ['token' => $token, 'email' => $user->email])
//                )
//                ->line('Если не вы запустили данную процедуру, то игнорируйте это письмо');
//        });
    }

    public function setPasswordAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function prices()
    {
        return $this->hasMany(FranchiseePrice::class, 'id');
    }

    public function feedbacks()
    {
        return $this->hasMany(Feedbacks::class);
    }

    public function logs()
    {
        return $this->hasMany(FranchiseeLog::class);
    }

    public function makeLog($date, $sum)
    {
        \DB::beginTransaction();
        $log = new FranchiseeLog();
        $log->type = "out";
        $log->balance = $this->balance;
        $log->append = $sum;
        $log->franchisee_id = $this->id;
        $log->created_at = $date;
        $log->updated_at = $date;
        $log->save();
        $this->balance -= $sum;
        if ($sum > 0) {
            $this->total_payment += $sum;
        }
        $this->total_date = $date;
        $this->save();
        \DB::commit();
    }


    public function getCity()
    {
        return field($this, 'city');
    }


    public static function getDefaultFranchisee()
    {
        return Franchisee::whereIn('domain', [
            'kazan.floristan.ru',
            'kazan.new.floristan.ru',
            'kazan.new.floristan.test',
            'kazan.floristan.test',
            'dev.floristan.ru',
            'floristan.ru',
            'floristan.test'
        ])->first();
    }


    public static function getDefaultFranchiseeId()
    {
        $franchisee = self::getDefaultFranchisee();

        return $franchisee ? $franchisee->id : null;
    }


    public function getTotalCash()
    {
        $total = Order::query()
            ->where('franchisee_id', $this->id)
            ->where('status', [Order::STATUS_COMPLETED])
            ->sum('total_value');

        $our = Order::query()
            ->where('franchisee_id', $this->id)
            ->where('status', [Order::STATUS_COMPLETED])
            ->sum('our_value');

        return $total - $our;
    }


    public function getTotalPayment()
    {
        return $this
            ->logs()
            ->where('append', '<', 0)
            ->get(['append'])
            ->map(function ($value) {
                return (float)($value['append'] * -1);
            })
            ->sum();
    }


    public static function getCurrent()
    {
        return app(FranchiseeService::class)->getCurrentfranchisee();
    }


    public function getPhoneWithFormat()
    {
        $phone = preg_replace("/[^0-9]/", '', $this->phone);

        $part1 = mb_substr($phone, 0, 1);
        $part2 = mb_substr($phone, 1, 3);
        $part3 = mb_substr($phone, 4, 3);
        $part4 = mb_substr($phone, 7, 2);
        $part5 = mb_substr($phone, 9, 2);

        if ($part1 == 7) {
            $part1 = '+7';
        }

        return "{$part1} ({$part2}) {$part3}-{$part4}-{$part5}";
    }


    public function getCode()
    {
        $baseDomain = env('APP_BASE_DOMAIN');
        $domain = $this->domain;

        return trim(explode($baseDomain, $domain)[0], '.');
    }

}
