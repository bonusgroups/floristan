<?php

namespace App\Models;

use App\Services\BouquetVoteService;
use App\Services\FranchiseeService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Swis\LaravelFulltext\Indexable;

/**
 * Class Bouquet
 * @package App\Models
 * @property Collection $flowers
 */
class Bouquet extends Model
{
    use Indexable;

    protected $indexContentColumns = ['description_ru', 'description_en'];
    protected $indexTitleColumns = ['name_ru', 'name_en', 'reasons.name_ru', 'reasons.name_en', 'flowers.name_ru', 'flowers.name_en', 'categories.name_ru', 'categories.name_en'];

    const SIZE_MIDDLE = 'size_middle';
    const SIZE_BIG = 'size_big';
    const SIZE_SMALL = 'size_small';

    protected $fillable = [
        'name_ru',
        'name_en',
        'is_active',
        'is_new',
        'hit',
        'discount',
        'franchisee_id',
        'description_ru',
        'description_en',
        'text_ru',
        'text_en',
        'is_additional'
    ];


    public function votes()
    {
        return $this->hasMany(BouquetVote::class);
    }


    public function colors()
    {
        return $this->belongsToMany(BouquetColor::class);
    }

    public static function getSizesArray()
    {
        return [
            'Маленький' => self::SIZE_SMALL,
            'Средний' => self::SIZE_MIDDLE,
            'Большой' => self::SIZE_BIG
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(BouquetImage::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reasons()
    {
        return $this->belongsToMany(BouquetReason::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function flowers()
    {
        return $this->belongsToMany(Flower::class)
            ->withPivot(['count', 'size', 'id']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function franchisee()
    {
        return $this->belongsTo(Franchisee::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(BouquetCategory::class);
    }

    public function franchiseePrice()
    {
        return $this->hasMany(BouquetsFranchiseePrice::class, "id");
    }

    public function priceBetween($bouquetSize)
    {
        $prices = $this->franchiseePrice()->where("size", $bouquetSize)->get();

        return $prices->min('price') . "-" . $prices->max('price');
    }

    public function priceBetweenWithFormat($bouquetSize)
    {

        $prices = $this->franchiseePrice()->where("size", $bouquetSize)->get();

        return number_format(price_mul($prices->min('price')), 0, '.', ' ') . " - " . number_format(price_mul($prices->max('price')), 2, '.', ' ');
    }

    /**
     * @return float
     */
    public function price($bouquetSize, $franchise_id = null, $price_mul = true)
    {
        $sum = 0;

        $franchisee = new FranchiseeService($franchise_id ? Franchisee::find($franchise_id) : null);

        if ($franchisee->isCurrentFranchiseeDetected() && is_null($franchise_id)) {
            $franchise_id = $franchisee->getCurrentFranchisee()->id;
        }

        $defaultFranchiseeId = Franchisee::getDefaultFranchiseeId();

        foreach ($this->flowers()->wherePivot('size', $bouquetSize)->get() as $flower) {
            $flowerPrice = $flower->prices()->where("franchisee_id", $franchise_id)->first() ?: $flower->prices()->where("franchisee_id", $defaultFranchiseeId)->first();

            if (!$flowerPrice) continue;

            $sum += $flowerPrice->price * $flower->pivot->count;
        }

        return $price_mul ? price_mul($sum, $this) : $sum;
    }


    /**
     * @param float $bouquetSize
     * @return float|int
     */
    public function priceWithDiscount($bouquetSize, $franchise_id = null)
    {
        $price = $this->price($bouquetSize, $franchise_id);

        return $price * ((100 - $this->discount) / 100);
    }

    /**
     * @param float $bouquetSize
     * @return float|int
     */
    public function ballsFromPrice($bouquetSize)
    {
        $price = $this->priceWithDiscount($bouquetSize);

        return round($price * 0.1);
    }

    /**
     * @return float
     */
    public function priceWithFormat($bouquetSize)
    {
        return number_format($this->price($bouquetSize), 0, '.', ' ');
    }

    /**
     * @param float $bouquetSize
     * @return float|int
     */
    public function priceWithDiscountWithFormat($bouquetSize)
    {
        return number_format($this->priceWithDiscount($bouquetSize), 0, '.', ' ');
    }


    public function getSizes()
    {
        return [
            self::SIZE_SMALL,
            self::SIZE_MIDDLE,
            self::SIZE_BIG
        ];
    }


    public function getMinimalPrice()
    {
        foreach ($this->getSizes() as $size) {
            if ($price = $this->price($size)) return $price;
        }

        return 0;
    }


    public function getMinimalPriceSize()
    {
        $prices = array_filter([
            self::SIZE_SMALL => $this->price(self::SIZE_SMALL),
            self::SIZE_MIDDLE => $this->price(self::SIZE_MIDDLE),
            self::SIZE_BIG => $this->price(self::SIZE_BIG)
        ], function ($item) {
            return $item > 0;
        });

        asort($prices);

        return sizeof($prices) ? collect($prices)->keys()->first() : null;
    }

    /**
     * @return string
     */
    public function getMinimalPriceWithFormat()
    {
        return number_format($this->getMinimalPrice(), 0, '.', ' ');
    }


    public function getMinimalPriceWithDiscount()
    {
        foreach ($this->getSizes() as $size) {
            if ($price = $this->priceWithDiscount($size)) return $price;
        }

        return 0;
    }

    /**
     * @return string
     */
    public function getMinimalPriceWithDiscountWithFormat()
    {
        return number_format($this->getMinimalPriceWithDiscount(), 0, '.', ' ');
    }

    /**
     * @param string $sizeType
     * @return string
     */
    public function consistAsString($sizeType)
    {
        $flowers = $this->flowers->where('pivot.size', $sizeType);
        $consist = '';

        /** @var Flower $flower */
        foreach ($flowers as $flower) {
            $consist .= $flower->name_ru . ' ' . $flower->pivot->count . ' шт, ';
        }


        return $consist;
    }

    public function getFirstAvailbleSize()
    {
        $price = $this->priceWithDiscount(self::SIZE_SMALL);

        if ($price > 0) {
            return self::SIZE_SMALL;
        }

        $price = $this->priceWithDiscount(self::SIZE_MIDDLE);

        if ($price > 0) {
            return self::SIZE_MIDDLE;
        }

        $price = $this->priceWithDiscount(self::SIZE_BIG);

        if ($price > 0) {
            return self::SIZE_BIG;
        }

        return self::SIZE_SMALL;
    }


    public function getName()
    {
        return field($this, 'name');
    }

    public static function getAllColors()
    {
        return BouquetColor::query()->get();
    }


    public function scopeIsActive($query)
    {
        return $query->where('is_active', 1);
    }


    public function scopeIsBouquet($query)
    {
        return $query->where('is_additional', 0);
    }


    public function scopeIsAdditional($query)
    {
        return $query->where('is_additional', 1);
    }


    public function isBouquet()
    {
        return !(bool)$this->is_additional;
    }


    public function isAdditional()
    {
        return (bool)$this->is_additional;
    }


    public function getPriceMul()
    {
        if ($this->isAdditional()) {
            return \App\Services\SettingService::shared()->get("mul_price_additional", 250) / 100;
        }

        return \App\Services\SettingService::shared()->get("mul_price", 250) / 100;
    }


    public static function getSizeNameByCode($code)
    {
        $data = [
            self::SIZE_SMALL => 'Маленький',
            self::SIZE_MIDDLE => 'Средний',
            self::SIZE_BIG => 'Большой'
        ];

        if (isset($data[$code])) return $data[$code];

        return null;
    }


    public function getRatingAverage()
    {
        $service = new BouquetVoteService();
        $service->setBouquet($this);
        $service->setIp(request()->ip());

        return $service->getAverage();
    }


    public function getRatingValue()
    {
        $service = new BouquetVoteService();
        $service->setBouquet($this);
        $service->setIp(request()->ip());

        return $service->getValue();
    }


    public function getVotesCount()
    {
        return $this->votes()->count();
    }


    public function getCurrentVote()
    {
        $service = new BouquetVoteService();
        $service->setBouquet($this);
        $service->setIp(request()->ip());

        return $service->getValue();
    }

}
