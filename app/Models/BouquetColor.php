<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BouquetColor
 * @package App\Models
 */
class BouquetColor extends Model
{
    protected $fillable = [
        'name',
    ];

    public $timestamps = false;

    public function bouquets()
    {
        return $this->belongsToMany(Bouquet::class);
    }


    public static function getByHex($value)
    {
        if (!$value) return null;

        return self::where('hex_value', $value)->first();
    }
}
