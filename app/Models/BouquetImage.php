<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

/**
 * Class BouquetImage
 * @package App\Models
 */
class BouquetImage extends Model
{
    protected $fillable = [
        'file_name',
        'bouquet_id',
    ];

    /**
     * @return string
     */
    public function getUrlAttribute()
    {
        return \Storage::disk('bouquet_images')->url($this->file_name);
    }

    /**
     * @return string
     */
    public function getPreviewAttribute()
    {
        return \Storage::disk('bouquet_images')->url("preview_".$this->file_name);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bouquet()
    {
        return $this->belongsTo(Bouquet::class);
    }

    /**
     * @param Bouquet $bouquet
     * @param UploadedFile $file
     * @return BouquetImage|Model
     */
    public static function store(Bouquet $bouquet, UploadedFile $file)
    {
        $fileName = Str::random(7) . '.' . $file->clientExtension();

        \Storage::disk('bouquet_images')->put($fileName, file_get_contents($file->path()));

        return BouquetImage::create([
            'file_name' => $fileName,
            'bouquet_id' => $bouquet->getKey(),
        ]);
    }
}
