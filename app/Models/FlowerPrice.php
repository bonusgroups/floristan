<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FlowerPrice
 * @package App\Models
 */
class FlowerPrice extends Model
{
    protected $fillable = [
        'flower_id',
        'franchisee_id',
        'price',
    ];

    public $timestamps = false;


    public function flower()
    {
        return $this->belongsTo(Flower::class);
    }
}
