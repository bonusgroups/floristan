<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Flower
 * @package App\Models
 */
class Flower extends Model
{
    protected $fillable = [
        'name_ru',
        'name_en',
        'is_additional',
        'hide_in_filters'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(FlowerPrice::class);
    }

    public function bouquets()
    {
        return $this->belongsToMany(Bouquet::class);
    }


    public function scopeIsNotHideInFilters($query)
    {
        $query->where('show_in_filters', 1);
    }

}
