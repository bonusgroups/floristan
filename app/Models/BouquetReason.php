<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BouquetReason
 * @package App\Models
 */
class BouquetReason extends Model
{
    protected $fillable = [
        'name_ru',
        'name_en',
    ];

    public $timestamps = false;

    public function bouquets() {
        return $this->belongsToMany(Bouquet::class);
    }
}
