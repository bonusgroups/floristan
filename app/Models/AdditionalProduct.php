<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AdditionalProduct
 * @package App\Models
 */
class AdditionalProduct extends Model
{
    protected $fillable = [
        'name_ru',
        'name_en',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(AdditionalProductPrice::class);
    }
}
