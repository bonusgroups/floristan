<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 21.05.2018
 * Time: 16:08
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BouquetsFranchiseePrice extends Model
{

    public function bouquet() {
        return $this->belongsTo(Bouquet::class);
    }
}