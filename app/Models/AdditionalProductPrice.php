<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 17.05.2018
 * Time: 10:20
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AdditionalProductPrice extends Model
{
    protected $fillable = [
        'additional_product_id',
        'franchisee_id',
        'price',
    ];

    public $timestamps = false;
}