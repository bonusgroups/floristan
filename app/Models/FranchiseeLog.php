<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 28.05.2018
 * Time: 12:57
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class FranchiseeLog extends Model
{
    public function franchisee() {
        return $this->belongsTo(Franchisee::class);
    }
}