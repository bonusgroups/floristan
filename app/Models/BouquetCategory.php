<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class BouquetCategory
 * @package App\Models
 */
class BouquetCategory extends Model
{
    protected $fillable = [
        'name_ru',
        'name_en',
        'slug',
        'title',
        'description',
        'h1',
        'text',
        'published',
        'parent_id',
        'seo_title'
    ];


    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }


    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }


    public static function getFromUrl()
    {
        $slug = request()->route()->parameter('slug');

        return self::where('slug', $slug)->first();
    }


    public function getPageUrl()
    {
        return url('catalog', $this->slug);
    }


    public function scopePublished($query)
    {
        return $query->where('published', true);
    }


    public function scopeOnlyRoot($query)
    {
        return $query->where(function ($query) {
            $query->whereNull('parent_id');
            $query->orWhere('parent_id', 0);
        });
    }


    public static function getParentOptions()
    {
        return self::query()
            ->published()
            ->onlyRoot()
            ->orderBy('sort_order', 'asc')
            ->pluck('name_ru', 'id')
            ->toArray();
    }


    public function prepareSlug()
    {
        $this->slug = Str::slug($this->name_ru);
    }


    public function hasSlug()
    {
        return (bool)$this->slug;
    }


    public static function boot()
    {
        parent::boot();

        self::updating(function (self $model) {
            if (!$model->hasSlug()) {
                $model->prepareSlug();
            }
        });
    }

}
