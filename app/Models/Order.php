<?php

namespace App\Models;

use App\Classes\AmoCrm;
use App\Services\SettingService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    const STATUS_NEW = 0;
    const STATUS_PARTNER = 1;
    const STATUS_CANCEL = 2;
    const STATUS_COMPLETED = 3;

    const PAY_CASH = 0;
    const PAY_ONLINE = 1;
    const PAY_CARD = 2;


    protected $guarded = array();

    public $fillable = [
        "franchisee_id",
        "delivery_id",
        "postcard_text",
        "delivery_date",
        "delivery_time",
        "status",
        "total_value",
        "our_value",
        "partner_value",
        "points_value",
        "comment",
        "client_name",
        "client_phone",
        "client_comment",
        "recipient_name",
        "recipient_phone",
        "recipient_address",
        "need_to_call",
        "pay_type",
        "created_at",
        "updated_at",
        'delivery_type'
    ];


    public static function boot()
    {
        parent::boot();

        self::created(function (Order $model) {
            $model->updateFranchiseeBalance();
        });

        self::updated(function (Order $model) {
            $model->updateFranchiseeBalance();

            if ($model->isStatusPartnerAndDirty()) {
                $model->sendFranchiseeNotice();
            }
        });
    }


    public function promocode()
    {
        return $this->belongsTo(Promocode::class, 'promocode_id');
    }


    public function updateFranchiseeBalance()
    {
        if (!$this->isDirty('status')) return;

        if ($this->status != self::STATUS_PARTNER) return;

        $franchisee = $this->franchisee;

        if (!$franchisee) return;

        switch ($this->pay_type) {
            case 0:
                $sum = -intval($this->our_value);
                break;

            case 1:
                $sum = intval($this->partner_value);
                break;
        }

        $franchisee->balance = intval($franchisee->balance) + $sum;
        $franchisee->save();
    }


    public function usePromocode()
    {
        if (!$this->isDirty('status')) return;

        if ($this->status != self::STATUS_COMPLETED) return;

        $promocode = $this->promocode;

        if (!$promocode) return;

        $promocode->useNow();
    }


    public function getStatusText()
    {
        $key = $this->status;
        $items = $this->getStatuses();

        return isset($items[$key]) ? $items[$key] : null;
    }

    public static function getStatuses()
    {
        return array(
            0 => 'Новый',
            1 => 'Передан партнеру',
            2 => 'Отказ/Отмена',
            3 => 'Завершено'
        );
    }

    public static function getPayTypes()
    {
        return array(
            0 => 'Курьеру',
            1 => 'Оплата на сайте',
            2 => 'Оплата на карту'
        );
    }


    public function getPayTypeName()
    {
        $key = $this->pay_type;
        $options = $this->getPayTypes();

        return isset($options[$key]) ? $options[$key] : null;
    }


    public static function getCartPaymentTypes()
    {
        return [
            1 => 'cart.pay.online',
            0 => 'cart.pay.self'
        ];
    }

    public function getBouquetsListText()
    {
        $bouquets = $this->bouquets;
        $items = [];

        foreach ($bouquets as $bouquet) {
            $items[] = $bouquet->name_ru;
        }

        return implode(', ', $items);
    }

    public function getBouquetsListLinks()
    {
        $franchisee = $this->franchisee ?: Franchisee::getDefaultFranchisee();

        $bouquets = $this->bouquets;
        $items = [];

        foreach ($bouquets as $bouquet) {
            $items[] = '<a href="https://' . $franchisee->domain . '/catalog/product/' . $bouquet->id . '">' . $bouquet->name_ru . '</a>';
        }

        return implode(', ', $items);
    }

    public function franchisee()
    {
        return $this->belongsTo(Franchisee::class);
    }

    public function bouquets()
    {
        return $this->belongsToMany(Bouquet::class, 'order_bouquet')
            ->withPivot(['bouquet_size', 'count']);
    }

    public function additional_products()
    {
        return $this->belongsToMany(AdditionalProduct::class, 'order_additional_product')
            ->withPivot(['count']);
    }

    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }

    /**
     * Сумма заказа.
     *
     * @return float
     */
    public function getOrderSum()
    {
        return $this->total_value;
    }

    /**
     * Идентификатор плательщика в системе магазина.
     * В качестве идентификатора может использоваться номер договора плательщика, логин плательщика и т. п.
     *
     * Допустимы повторные оплаты по одному и тому же идентификатору плательщика.
     *
     * @return string
     */
    public function getCustomerNumber()
    {
        return $this->client_phone;
    }

    /**
     * Уникальный номер заказа в системе магазина.
     *
     * @return null|string
     */
    public function getOrderNumber()
    {
        return $this->id;
    }

    /**
     * Код способа оплаты.
     *
     * @link https://tech.yandex.ru/money/doc/payment-solution/reference/payment-type-codes-docpage/
     *
     * @return null|string
     */
    public function getPaymentType()
    {
        return null;
    }

    /**
     * Адрес электронной почты плательщика.
     *
     * @return null|string
     */
    public function getCustomerEmail()
    {
        return null;
    }

    /**
     * Номер мобильного телефона плательщика.
     *
     * @return null|string
     */
    public function getCustomerPhone()
    {
        return $this->client_phone;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function log()
    {
        return $this->hasOne(FranchiseeLog::class);
    }

    public function makeLog()
    {
        $log = new FranchiseeLog();
        $log->type = "in";
        $log->franchisee_id = $this->franchisee_id;
        $log->balance = $this->franchisee->balance;
        $log->order_id = $this->id;
        switch ($this->payment_type) {
            case self::PAY_CASH:
                $log->append = -$this->our_value;
                break;
            case self::PAY_ONLINE:
                $log->append = $this->partner_value;
                break;
        }
        $log->save();
        $this->log = $log;
        $this->franchisee->balance += $log->append;
        $this->franchisee->total_cash += $this->partner_value;
        $this->franchisee->save();
    }


    public function sendFranchiseeNotice()
    {
        $franchisee = $this->franchisee;
        $email = $franchisee ? $franchisee->email : null;

        if (!$email) return;

        $vars = [
            'order' => $this
        ];

        \Mail::send('site.mail.order-franchisee', $vars, function ($message) use ($email) {
            $message
                ->to($email)
                ->subject('Флористан. Новая заявка с сайта №' . $this->id);
        });
    }


    public function sendAdminNotice()
    {
        $email = (env('APP_MODE') == 'local') ? 'marat.icanall@ya.ru' : SettingService::shared()->get('email');

        if (!$email) return;

        $vars = [
            'order' => $this
        ];

        \Mail::send('site.mail.order-admin', $vars, function ($message) use ($email) {
            $message
                ->to($email)
                ->subject('Флористан. Новая заявка с сайта №' . $this->id);
        });
    }


    public function getOriginalTotal()
    {
        $total = 0;

        foreach ($this->bouquets as $bouquet) {
            $total += $bouquet->price($bouquet->pivot->bouquet_size, $this->franchisee_id, false);
        }

        return $total;
    }


    public function getSuccessUrl()
    {
        return url('checkout/success') . '?' . http_build_query(['order_id' => $this->id]);
    }


    public static function getPayedOptions()
    {
        return [
            0 => 'Не оплачен',
            1 => 'Оплачено'
        ];
    }


    public function getPayedName()
    {
        $key = (int)$this->payed;
        $items = $this->getPayedOptions();

        return isset($items[$key]) ? $items[$key] : null;
    }


    public function getDeliveryDateAndTimeWithFormat()
    {
        $date = trim("{$this->delivery_date} {$this->delivery_time}");

        if (!$date) return null;

        return Carbon::parse($date)->format('d.m.Y H:i');
    }


    public function isStatusPartnerAndDirty()
    {
        if ($this->status != self::STATUS_PARTNER) return false;

        if (!$this->isDirty('status')) return false;

        return true;
    }


    public function sendToAmoCrm()
    {
        $data = [];

        $data['type'] = 'zakaz';

        $data['title'] = 'Заявка с сайта';
        $data['tags'] = 'Заявка с сайта, floristan.ru';

        $data['name'] = $this->client_name;
        $data['phone'] = $this->client_phone;
        $data['email'] = null;

        $data['text_otkrytki'] = $this->postcard_text;

        $data['delivery_date'] = $this->delivery_date ? Carbon::parse($this->delivery_date)->format('d.m.Y') : null;

        $data['contact_phone'] = $this->recipient_name ?: $this->client_name; //если тот же получает, передаем данные первые
        $data['contact_name'] = $this->recipient_phone ?: $this->client_phone; //если тот же получает, передаем данные первые

        $data['delivery_address'] = $this->recipient_address ?: 'Уточнить по телефону'; // можно передать "Уточнить по телефону"
        $data['delivery_time'] = $this->delivery_time;

        switch ($this->pay_type) {
            case 1:
                $data['pay_type'] = '954677';
                break;

            default:
                $data['pay_type'] = '824331';
                break;
        }

        $data['zakaz'] = [];
        $franchisee = $this->franchisee ?: Franchisee::getDefaultFranchisee();
        $bouquets = $this->bouquets;

        foreach ($bouquets as $bouquet) {
            $data['zakaz'][] = [
                'name' => field($bouquet, 'name'),
                'razmer' => Bouquet::getSizeNameByCode(optional($bouquet->pivot)->bouquet_size),
                'link' => 'https://' . $franchisee->domain . '/catalog/product/' . $bouquet->id,
                'price' => $bouquet->price(optional($bouquet->pivot)->bouquet_size, $franchisee->id),
            ];
        }

        (new AmoCrm())->send_amo($data);

        return true;
    }


    public function getDeliveryTypeOptions()
    {
        return [
            'pickup' => 'Самовывоз',
            'delivery' => 'Доставка'
        ];
    }


    public function getDeliveryTypeName()
    {
        $value = $this->delivery_type;
        $options = $this->getDeliveryTypeOptions();

        return isset($options[$value]) ? $options[$value] : null;
    }

}
