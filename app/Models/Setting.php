<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 24.05.2018
 * Time: 12:28
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $primaryKey="key";
    public $keyType="string";
    public $fillable=[
        "key","value"
    ];
    public $timestamps = false;
}