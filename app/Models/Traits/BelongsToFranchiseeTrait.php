<?php

namespace App\Models\Traits;

use App\Models\Franchisee;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Trait BelongsToFranchiseeTrait
 * @package App\Models\Traits
 */
trait BelongsToFranchiseeTrait
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function franchisee()
    {
        return $this->belongsTo(Franchisee::class);
    }

    /**
     * @param null|string $modelClass
     * @param bool $needWithoutFilterItem
     * @return Collection
     */
    public static function franchisesItemsForSelect($modelClass = null, $needWithoutFilterItem = false)
    {
        $collection = new Collection();
        $withoutFranchisee = new Franchisee(['city_ru' => 'Без франчайзи']);
        $withoutFranchisee->setKeyType('string')->setAttribute('id', 'without');
        $collection->push($withoutFranchisee);

        if ($needWithoutFilterItem) {
            $withoutFilter = new Franchisee(['city_ru' => 'Без фильтра']);
            $collection->prepend($withoutFilter);
        }

        $query = Franchisee::query();

        if ($modelClass) {
            $ids = (new $modelClass)->query()->pluck('franchisee_id')->toArray();
            $query->whereIn('id', $ids);
        }

        return $collection->merge($query->get());
    }

    /**
     * @param string $modelClass
     * @return LengthAwarePaginator
     */
    public static function getModelsForFranchisee($modelClass)
    {
        $query = (new $modelClass)->query()->with('franchisee');

        if (\Request::has('franchisee_id')) {
            if (\Request::get('franchisee_id') == 'without') {
                $query->whereNull('franchisee_id');
            } elseif (\Request::get('franchisee_id')) {
                $query->where('franchisee_id', \Request::get('franchisee_id'));
            }
        }

        $collection = $query->paginate(30)->appends(\Request::only('franchisee_id'));

        return $collection;
    }
}
