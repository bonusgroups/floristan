<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{

    public $fillable = [
        'code',
        'discount',
        'percent',
        'count',
        'comment',
        'created_at'
    ];


    public function isAvailable()
    {
        return true;
    }


    public static function findByCode($code)
    {
        if (!$code) return null;

        return self::where('code', $code)->first();
    }


    public function useNow()
    {
        $this->count = intval($this->count) + 1;
        $this->save();
    }


    public function getDiscountPrice($total = 0)
    {
        if (!$total) return 0;

        $discount = intval($this->discount);
        $percent = intval($this->percent);
        $percentDiscount = $percent ? intval($total * ($percent / 100)) : 0;

        if ($discount and $percentDiscount) return min([$discount, $percentDiscount]);

        return $discount ?: $percentDiscount;
    }


    public function isFixed($total = 0)
    {
        $discount = intval($this->discount);
        $percent = intval($this->percent);
        $percentDiscount = $percent ? intval($total * ($percent / 100)) : 0;

        if ($discount and $percentDiscount) return ($discount < $percentDiscount);

        if ($discount) return true;

        if ($percentDiscount) return false;

        return false;
    }

}
