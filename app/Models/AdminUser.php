<?php

namespace App\Models;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App\Models
 */
class AdminUser extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * AdminUser constructor.
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

//        ResetPassword::toMailUsing(function (AdminUser $user, $token) {
//            return (new MailMessage)
//                ->greeting('Здравствуйте!')
//                ->line('Вы получили это письмо потому, что на сайте Floristan была запущена процедура сброса пароля.')
//                ->action(
//                    'Сбросить пароль',
//                    \URL::route('admin.password.reset', ['token' => $token, 'email' => $user->email])
//                )
//                ->line('Если не вы запустили данную процедуру, то игнорируйте это письмо');
//        });
    }
}
