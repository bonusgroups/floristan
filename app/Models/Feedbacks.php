<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 21.05.2018
 * Time: 18:25
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Feedbacks extends Model
{
    public $fillable = [
        "link_vk",
        "name",
        "description",
        "photo",
        "avatar",
        "active",
        'rating'
    ];

    public function franchisee()
    {
        return $this->belongsTo(Franchisee::class);
    }

    public function getPhotoAttribute()
    {
        return \Storage::disk('feedback_images')->url($this->attributes['photo']);
    }

    public function getAvatarAttribute()
    {
        return \Storage::disk('feedback_images')->url($this->attributes['avatar']);
    }

    public function hasPhoto()
    {
        return !empty($this->attributes['photo']);
    }

    public function hasAvatar()
    {
        return !empty($this->attributes['avatar']);
    }
}