<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BouquetVote extends Model
{

    public function bouquet()
    {
        return $this->belongsTo(Bouquet::class);
    }

}
