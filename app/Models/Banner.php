<?php namespace App\Models;

use App\BannerImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\UploadedFile;

class Banner extends Model
{

    public $fillable = [
        'type',
        'url'
    ];


    public function image()
    {
        return $this->hasOne(BannerImage::class);
    }


    public static function getTypeOptions()
    {
        return [
            'desktop' => 'Основная версия',
            'tablet' => 'Планшетная версия',
            'mobile' => 'Мобильная версия'
        ];
    }


    public function getTypeName()
    {
        $items = self::getTypeOptions();
        $key = $this->type;

        return isset($items[$key]) ? $items[$key] : null;
    }


    public function setImage(UploadedFile $file)
    {
        $storage = \Storage::disk('banner_images');

        if ($image = $this->image) {
            $path = $image->file_name;

            if ($storage->exists($path)) $storage->delete($path);

            $image->delete();
        }

        $fileName = bin2hex(openssl_random_pseudo_bytes(10)) . '.' . $file->clientExtension();

        $storage->put($fileName, file_get_contents($file->path()));

        $image = new BannerImage();
        $image->file_name = $fileName;
        $image->banner_id = $this->id;
        $image->save();

        return $image;
    }

}
