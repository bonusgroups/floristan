<?php

namespace App\Models;

use App\Models\Traits\BelongsToFranchiseeTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Delivery
 * @package App\Models
 */
class Delivery extends Model
{
    use BelongsToFranchiseeTrait;

    protected $fillable = [
        'name_ru',
        'name_en',
        'price',
        'franchisee_id',
    ];

    public $timestamps = false;
}
