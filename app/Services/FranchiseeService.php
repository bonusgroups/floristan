<?php

namespace App\Services;

use App\Exceptions\FranchiseeServiceException;
use App\Models\Franchisee;
use Route;

/**
 * Class FranchiseeService
 * @package App\Services
 */
class FranchiseeService
{
    /**
     * @var Franchisee
     */
    private $currentFranchisee;

    /**
     * FranchiseeService constructor.
     * @throws \Exception
     */
    public function __construct($franchisee = null)
    {
        if (request()->is('admin', 'admin/*')) return;

        $this->currentFranchisee = $franchisee ?: Franchisee::where('domain', request()->getHost())->first();
    }

    /**
     * @return Franchisee|\Illuminate\Database\Eloquent\Model|null|object|static
     * @throws FranchiseeServiceException
     */
    public function getCurrentFranchisee()
    {
        return $this->currentFranchisee;
    }

    /**
     * @return bool
     */
    public function isCurrentFranchiseeDetected()
    {
        return (bool)$this->currentFranchisee;
    }

    /**
     * @return Franchisee[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return Franchisee::select(['id', 'domain', 'city_ru', 'city_en'])
            ->groupBy(['city_ru', 'city_en'])
            ->get();
    }
}
