<?php namespace App\Services;

use App\Models\Order;
use YandexCheckout\Client;

class YaKassaService
{

    public $order = null;
    public $client = null;


    public static function instance()
    {
        return new self;
    }


    public function setOrder(Order $order = null)
    {
        $this->order = $order;
    }


    public function getOrder()
    {
        return $this->order;
    }


    public function getShopId()
    {
        return env('YA_KASSA_SHOP_ID', null);
    }


    public function getPassword()
    {
        return env('YA_KASSA_PASSWORD', null);
    }


    public function getOrderSum()
    {
        $order = $this->getOrder();

        return $order ? $order->getOrderSum() : null;
    }


    public function getOrderId()
    {
        $order = $this->getOrder();

        return $order ? $order->id : null;
    }


    public function getClient()
    {
        if ($this->client) return $this->client;

        $client = new Client();
        $client->setAuth($this->getShopId(), $this->getPassword());

        return $this->client = $client;
    }


    public function createPayment()
    {
        return $this->getClient()->createPayment(
            [
                'amount' => array(
                    'value' => $this->getOrderSum(),
                    'currency' => 'RUB',
                ),
                'confirmation' => array(
                    'type' => 'redirect',
                    'return_url' => $this->getReturnUrl(),
                ),
                'description' => 'Заказ №' . $this->getOrderId(),
                'metadata' => [
                    'order_id' => $this->getOrderId()
                ]
            ],
            uniqid('', true)
        );
    }


    public function getReturnUrl()
    {
        return url('/');
    }


    public function getUrl()
    {
        $payment = $this->createPayment();

        return optional($payment->confirmation)->confirmationUrl;
    }

}