<?php namespace App\Services;


use Curl\Curl;

class SmsService
{

    public $phone = null;
    public $text = null;


    public function getApiKey()
    {
        return SettingService::shared()->get('sms_api_key', null);
    }


    public function getPhone()
    {
        return $this->phone;
    }


    public function setPhone($phone)
    {
        $this->phone = $phone;
    }


    public function getText()
    {
        return $this->text;
    }


    public function setText($text)
    {
        $this->text = $text;
    }


    public function send()
    {
        if (!$this->getApiKey()) return true;

        $url = 'https://sms.ru/sms/send';

        $data = [
            'api_id' => $this->getApiKey(),
            'to' => $this->getPhone(),
            'msg' => $this->getText(),
            'json' => 1
        ];

        $data = (new Curl())->post($url, $data);
        $status = isset($data->status) ? $data->status : null;

        return (mb_strtolower($status) == 'ok');
    }


}