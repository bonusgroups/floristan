<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 18.05.2018
 * Time: 17:24
 */

namespace App\Services;


use App\Models\Bouquet;
use App\Models\BouquetCategory;
use App\Models\BouquetColor;
use App\Models\BouquetReason;
use App\Models\Flower;
use Illuminate\Support\Collection;

class CatalogService
{
    public function getCatalogsSliced()
    {
        $catalogs = $this->getItems();

        $iteration = 1;
        $catalogs = $catalogs->reduce(function (Collection $map, BouquetCategory $item) use (&$iteration) {
            if (!$map->count()) {
                $map->push($item);
                return $map;
            }

            if (!$map->has($iteration)) {
                $map->put($iteration, new Collection());
            }

            $inside = $map->get($iteration);
            $inside->push($item);
            if ($inside->count() == 2) {
                $iteration++;
            }

            return $map;
        }, new Collection());

        return $catalogs;
    }


    public function getItems()
    {
//        $slugs = [
////            '14-february',
//            '8-march',
//            'all',
//            'roses',
//            'compositions',
//            'sales',
//            'presents',
//            'insta-shop'
//        ];

        return BouquetCategory::query()
            ->published()
            ->onlyRoot()
            ->with('children')
//            ->whereIn('slug', $slugs)
            ->orderBy('sort_order', 'asc')
            ->orderBy('name_ru', 'asc')
            ->get();
    }


    public function getReasons()
    {
        return BouquetReason::whereHas('bouquets')->get();
    }


    public function getColors()
    {
        return BouquetColor::all();
    }


    public function getFlowers()
    {
        return Flower::query()
            ->isNotHideInFilters()
            ->whereHas('bouquets', function ($q) {
                $q->where('is_active', 1);
            }, '>', 0)
            ->get();
    }


    public function isActive($catId)
    {
        return request()->get("category", 0) == $catId;
    }
}