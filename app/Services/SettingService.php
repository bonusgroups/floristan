<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 24.05.2018
 * Time: 12:28
 */

namespace App\Services;


use App\Models\Setting;

class SettingService
{
    private static $self=null;
    private $settings = [];
    private function __construct()
    {
        self::$self = $this;
        $this->settings = $this->all();
    }

    public static function shared() {
        return self::$self?self::$self:new self();
    }

    public function all() {
        return Setting::all()->reduce(function($col, $item){
            $col[$item->key] = $item->value;
            return $col;
        },[]);
    }

    public function update($settings) {
        foreach($settings as $key=>$value) {
            $this->set($key,$value);
        }
    }

    public function get($key,$default=0) {
        return isset($this->settings[$key])?$this->settings[$key]:$default;
    }

    public function set($key,$value) {
        $setting = Setting::find($key);
        $this->settings[$key] = $value;
        if(is_null($setting)) {
            return Setting::create(["key"=>$key,"value"=>$value]);
        }
        $setting->update(["value"=>$value]);
        return $setting;
    }

    public function drop($key) {
        $setting = Setting::find($key);
        unset($this->settings[$key]);
        if($setting) {
            return $setting->remove();
        }

        return true;
    }
}