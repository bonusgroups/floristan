<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 21.05.2018
 * Time: 11:22
 */

namespace App\Services;

use App\Models\Bouquet;
use App\Models\BouquetCategory;
use App\Models\Franchisee;
use App\Models\FranchiseePrice;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class FilterService
{
    private static $priceMin;
    private static $priceMax;
    private static $priceMinCurrent;
    private static $priceMaxCurrent;
    private static $franchisee = null;
    private static $reasons = [];
    private static $colors = [];
    private static $flowers = [];
    private static $category = null;
    public $prices = null;

    public function __construct($franchisee = null)
    {
        if ($franchisee) {
            self::$franchisee = $franchisee;
        }

        if (is_null(self::$franchisee)) {
            self::$franchisee = app(FranchiseeService::class)->getCurrentFranchisee();

            $franchiseeId = self::$franchisee ? self::$franchisee->id : null;
            $category = optional(BouquetCategory::getFromUrl())->id;

            $min = null;
            $max = null;

            if ($franchiseeId) {
                $min = $this->getFiltersMinPrice($franchiseeId, $category);
                $max = $this->getFiltersMaxPrice($franchiseeId, $category);
            }

            self::$priceMin = $min;
            self::$priceMax = $max;
        }
    }

    public function updateFromRequest(Request $request)
    {
        self::$priceMinCurrent = floor((int)$request->get("f:min", self::$priceMin));
        self::$priceMaxCurrent = ceil((int)$request->get("f:max", self::$priceMax));
        self::$reasons = $request->get("f:reasons", []);
        self::$colors = $request->get("f:colors", []);
        self::$flowers = $request->get("f:flowers", []);
        self::$category = optional(BouquetCategory::getFromUrl())->id;
    }

    public function getMinPrice()
    {
        return self::$priceMin;
    }

    public function getMaxPrice()
    {
        return self::$priceMax;
    }

    public function getCategory()
    {
        return self::$category;
    }

    public function currentMinPrice()
    {
        return self::$priceMinCurrent ? self::$priceMinCurrent : self::$priceMin;
    }

    public function currentMaxPrice()
    {
        return self::$priceMaxCurrent ? self::$priceMaxCurrent : self::$priceMax;
    }

    public function hasPrice()
    {
        return self::$priceMin != self::$priceMinCurrent || self::$priceMax != self::$priceMaxCurrent;
    }

    public function hasReasons()
    {
        return sizeof(self::$reasons);
    }

    public function hasCategory()
    {
        return (bool)self::$category;
    }

    public function getReasons()
    {
        return self::$reasons;
    }

    public function hasReason($reason)
    {
        return in_array($reason->id, self::$reasons);
    }

    public function hasColors()
    {
        return sizeof(self::$colors);
    }

    public function getColors()
    {
        return self::$colors;
    }

    public function hasColor($color)
    {
        return in_array($color->id, self::$colors);
    }

    public function hasFlowers()
    {
        return sizeof(self::$flowers);
    }

    public function hasFlower($flower)
    {
        return in_array($flower->id, self::$flowers);
    }

    public function getFlowers()
    {
        return self::$flowers;
    }


    public static function getPriceQuery($franchiseeId)
    {
        return '
            SELECT
                sum(fp.price * bf.count * ((100 - bouquets.discount) / 100)) as price
            FROM bouquet_flower as bf
            JOIN flower_prices as fp ON fp.flower_id = bf.flower_id
            WHERE
                bf.bouquet_id = bouquets.id
                AND
                fp.franchisee_id = IF((
                    SELECT count(flower_prices2.id)
                    FROM flower_prices as flower_prices2
                    WHERE
                        flower_prices2.id = fp.id
                        AND
                        flower_prices2.franchisee_id = ' . $franchiseeId . '
                        AND
                        flower_prices2.price > 0
                ) > 0, ' . $franchiseeId . ', ' . Franchisee::getDefaultFranchiseeId() . ')
                AND
                bf.size = "' . Bouquet::SIZE_MIDDLE . '"
        ';
    }


    public function getAllPrices($franchiseeId, $categoryId = null)
    {
        if ($this->prices != null) return $this->prices;

        $priceQuery = self::getPriceQuery($franchiseeId);

        $query = Bouquet::query()->isActive()->selectRaw('(' . $priceQuery . ') as price');

        if ($categoryId) {
            switch ($categoryId) {
                case 1:
                    $query->where('discount', '>', 0);
                    break;

                case 6:
                    $query->isAdditional();
                    break;

                default:
                    $query->isBouquet();
                    $query->whereHas('categories', function (Builder $q) use ($categoryId) {
                        $q->where('bouquet_category_id', $categoryId);
                    });
                    break;
            }
        }

        return $query->get() ?: collect();
    }


    public function getFiltersMinPrice($franchiseeId, $categoryId = null)
    {
        $value = ceil($this->getAllPrices($franchiseeId, $categoryId)->min('price'));

        return floor(price_mul($value));
    }


    public function getFiltersMaxPrice($franchiseeId, $categoryId = null)
    {
        $value = ceil($this->getAllPrices($franchiseeId, $categoryId)->max('price'));

        return ceil(price_mul($value));
    }

}
