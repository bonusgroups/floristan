<?php namespace App\Services;


use App\Models\BouquetVote;

class BouquetVoteService
{

    public $bouquet;
    public $ip;


    public function getAverage()
    {
        $value = BouquetVote::query()
            ->where('bouquet_id', $this->getBouquet()->id)
            ->avg('value');

        return round((float)$value);
    }


    public function getValue()
    {
        $model = BouquetVote::query()
            ->where('bouquet_id', $this->getBouquet()->id)
            ->where('ip', $this->getIp())
            ->first();

        if (!$model) return 0;

        return (int)$model->value;
    }


    public function vote($value)
    {
        if ($this->exists()) {
            $this->delete();
        }

        $this->create($value);
    }


    public function create($value)
    {
        $model = new BouquetVote;
        $model->bouquet_id = $this->getBouquet()->id;
        $model->ip = $this->getIp();
        $model->value = $value;
        $model->save();
    }


    public function delete()
    {
        return BouquetVote::query()
            ->where('bouquet_id', $this->getBouquet()->id)
            ->where('ip', $this->getIp())
            ->delete();
    }


    public function exists()
    {
        return BouquetVote::query()
            ->where('bouquet_id', $this->getBouquet()->id)
            ->where('ip', $this->getIp())
            ->exists();
    }


    public function getBouquet()
    {
        return $this->bouquet;
    }


    public function setBouquet($bouquet)
    {
        $this->bouquet = $bouquet;
    }


    public function getIp()
    {
        return $this->ip;
    }


    public function setIp($ip)
    {
        $this->ip = $ip;
    }

}