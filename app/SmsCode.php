<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SmsCode extends Model
{

    public function isExpired()
    {
        return Carbon::now()->addMinutes(-5)->gt($this->created_at);
    }

}
