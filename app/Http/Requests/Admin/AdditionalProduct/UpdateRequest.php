<?php

namespace App\Http\Requests\Admin\AdditionalProduct;

use App\Http\Requests\Admin\BelongsToFranchiseeRequest;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Admin\AdditionalProduct
 */
class UpdateRequest extends BelongsToFranchiseeRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name_ru' => 'required',
            'name_en' => 'required'
        ]);
    }
}
