<?php

namespace App\Http\Requests\Admin\Bouquet;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Admin\Bouquet
 */
class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ru' => 'required',
            'name_en' => 'required',
            'is_active' => 'nullable',
            'is_new' => 'nullable',
            'hit' => 'nullable',
            'discount' => 'present|numeric|max:100'
        ];
    }

    public function validateResolved()
    {
        parent::validateResolved();

        if (!$this->request->has('is_active')) {
            $this->request->set('is_active', false);
        }

        if (!$this->request->has('is_new')) {
            $this->request->set('is_new', false);
        }

        if (!$this->request->has('hit')) {
            $this->request->set('hit', false);
        }
    }
}
