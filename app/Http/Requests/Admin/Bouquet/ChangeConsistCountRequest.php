<?php

namespace App\Http\Requests\Admin\Bouquet;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ChangeConsistCountRequest
 * @package App\Http\Requests\Admin\Bouquet
 */
class ChangeConsistCountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'flower_id' => 'required|exists:flowers,id',
            'count' => 'required|numeric',
            'bouquet_size' => 'required',
        ];
    }
}
