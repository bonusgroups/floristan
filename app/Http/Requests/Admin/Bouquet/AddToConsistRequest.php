<?php

namespace App\Http\Requests\Admin\Bouquet;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AddToConsistRequest
 * @package App\Http\Requests\Admin\Bouquet
 */
class AddToConsistRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'flower_id' => 'required|exists:flowers,id',
            'bouquet_size' => 'required',
            'count_for_adding' => 'required|numeric',
        ];
    }

}
