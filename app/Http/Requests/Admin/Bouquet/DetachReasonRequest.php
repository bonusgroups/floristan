<?php

namespace App\Http\Requests\Admin\Bouquet;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DetachReasonRequest
 * @package App\Http\Requests\Admin\Bouquet
 */
class DetachReasonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reason_id' => 'required|exists:bouquet_reasons,id'
        ];
    }
}
