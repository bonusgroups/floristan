<?php

namespace App\Http\Requests\Admin\Franchisee;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Admin\Franchisee
 */
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_ru' => 'required',
            'city_en' => 'required',
            'domain' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address_ru' => 'required',
            'address_en' => 'required',
            'opening_hours' => 'nullable|string',
            'commission_for_bouquets' => 'required|numeric',
            'requisites' => 'nullable|string',
            'password'=>'nullable|min:6',
            'password_confirm'=>'same:password'
        ];
    }
}
