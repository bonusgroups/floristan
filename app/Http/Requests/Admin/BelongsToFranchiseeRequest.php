<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BelongsToFranchiseeRequest
 * @package App\Http\Requests\Admin
 */
class BelongsToFranchiseeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'franchisee_id' => 'nullable',
        ];
    }

    public function validateResolved()
    {
        parent::validateResolved();

        if ($this->request->has('franchisee_id') and !is_numeric($this->request->get('franchisee_id'))) {
            $this->request->set('franchisee_id', null);
        }
    }
}
