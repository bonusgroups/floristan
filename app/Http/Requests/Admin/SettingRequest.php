<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 24.05.2018
 * Time: 12:40
 */

namespace App\Http\Requests\Admin;


use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mul_price' => 'required|integer|digits_between:0,1000',
            'mul_price_additional' => 'required|integer|digits_between:0,1000',
        ];
    }
}