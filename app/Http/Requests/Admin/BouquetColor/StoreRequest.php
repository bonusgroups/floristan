<?php

namespace App\Http\Requests\Admin\BouquetColor;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Admin\BouquetColor
 */
class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hex_value' => 'required',
            'bouquet_id' => 'nullable|exists:bouquets,id',
        ];
    }
}
