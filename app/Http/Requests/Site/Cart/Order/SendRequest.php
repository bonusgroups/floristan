<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 22.05.2018
 * Time: 21:56
 */

namespace App\Http\Requests\Site\Cart;


use App\Models\Promocode;
use Illuminate\Foundation\Http\FormRequest;

class SendRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [];

        if ($this->request->get('postcard')) {
            $rules['postcard_text'][] = 'required';
        }

        $rules['client_phone'][] = 'required';

        return $rules;
    }

}