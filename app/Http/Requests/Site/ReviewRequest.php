<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 31.05.2018
 * Time: 17:35
 */

namespace App\Http\Requests\Site;


use Illuminate\Foundation\Http\FormRequest;

class ReviewRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}