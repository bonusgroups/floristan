<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Franchisee\StoreRequest;
use App\Http\Requests\Admin\Franchisee\UpdateRequest;
use App\Models\Delivery;
use App\Models\Franchisee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class FranchiseeController
 * @package App\Http\Controllers\Admin
 */
class FranchiseeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $franchisees = Franchisee::paginate(30);

        return view('admin.franchisee.index')->with('franchisees', $franchisees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.franchisee.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        Franchisee::create($request->all());

        return redirect()->route('admin.franchisee.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Franchisee $franchisee
     * @return \Illuminate\Http\Response
     */
    public function show(Franchisee $franchisee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Franchisee $franchisee
     * @return \Illuminate\Http\Response
     */
    public function edit(Franchisee $franchisee)
    {
        return view('admin.franchisee.edit', ['franchisee' => $franchisee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  \App\Models\Franchisee $franchisee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Franchisee $franchisee)
    {
        $franchisee->update($request->all());

        return redirect()->route('admin.franchisee.edit', $franchisee);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Franchisee $franchisee
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Franchisee $franchisee)
    {
        $franchisee->delete();

        return redirect()->route('admin.franchisee.index');
    }
}
