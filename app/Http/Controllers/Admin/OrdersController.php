<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Site\Cart\SendRequest;
use App\Models\Bouquet;
use App\Models\Delivery;
use App\Models\Franchisee;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Traits\BelongsToFranchiseeTrait;

class OrdersController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::select();

        if ($request->input('franchisee_id')) {
            $orders->where('franchisee_id', $request->input('franchisee_id'));
        }

        if ($request->input('status') != '') {
            $orders->where('status', $request->input('status'));
        }

        if ($request->input('date_start') || $request->input('date_end')) {
            $date_start = $request->input('date_start') ? $request->input('date_start') : Carbon::now()->subDays(10);
            $date_end = $request->input('date_end') ? $request->input('date_end') : Carbon::now();
            $orders->whereBetween('delivery_date', array($date_start, $date_end));
        }

        $orders = $orders->orderBy('created_at', 'desc')->paginate(30);

        $franchisees = BelongsToFranchiseeTrait::franchisesItemsForSelect(Order::class, true);
        return view('admin.orders.index', compact('orders', 'franchisees'));
    }


    public function create()
    {
        $franchisees = Franchisee::all()->pluck('city_ru', 'id');
        $bouquets = Bouquet::all();
        $bouquets_size = Bouquet::getSizesArray();

        return view('admin.orders.add', compact('franchisees', 'bouquets', 'bouquets_size'));
    }


    public function edit(Request $request, Order $order)
    {
        $franchisees = Franchisee::all()->pluck('city_ru', 'id');
        $bouquets = Bouquet::all();
        $bouquets_size = Bouquet::getSizesArray();

        return view('admin.orders.edit', compact('franchisees', 'bouquets', 'bouquets_size', 'order'));
    }


    public function store(SendRequest $request)
    {
        $form = $request->all();

        $form['delivery_date'] = $request->get('delivery_date') ?: date("Y-m-d");
        $form['delivery_time'] = $request->get('delivery_time') ? ($request->get('delivery_time') . ' :00') : null;

        $order = Order::create($form);
        $bouquets = $request->get("bouquets", []);
        $sizes = $request->get("bouquets_size", []);
        foreach ($bouquets as $i => $bouquet) {
            $order->bouquets()->attach([$bouquet => [
                "count" => 1,
                "bouquet_size" => $sizes[$i]
            ]]);
        }

        if ($order->status == Order::STATUS_COMPLETED && !$order->log) {
            $order->makeLog();
        }

        return redirect()->route('admin.orders.index');
    }


    public function update(SendRequest $request, Order $order)
    {
        $form = $request->all();

        $form['delivery_date'] = $request->get('delivery_date') ?: date("Y-m-d");
        $form['delivery_time'] = $request->get('delivery_time') ? ($request->get('delivery_time') . ' :00') : null;

        $order->update($form);
        $bouquets = $request->get("bouquets", []);
        $sizes = $request->get("bouquets_size", []);
        $order->bouquets()->detach();

        foreach ($bouquets as $i => $bouquet) {
            $order->bouquets()->attach([$bouquet => [
                "count" => 1,
                "bouquet_size" => $sizes[$i]
            ]]);
        }

        if ($order->status == Order::STATUS_COMPLETED && !$order->log) {
            $order->makeLog();
        }

        return redirect()->route('admin.orders.index');
    }


    public function calculate(Request $request)
    {
        $franchisee = Franchisee::find($request->get("franchisee_id"));
        $items = $request->get("items", []);
        $ids = array_pluck($items, "id");
        $bouquets = Bouquet::whereIn("id", $ids)->get();
        $total_value = 0;
        $total_unmul = 0;

        foreach ($items as $item) {
            $bouquet = $bouquets->find($item['id']);
            $total_value += $bouquet->price($item['size'], $franchisee->id) * ((100 - $bouquet->discount) / 100);
            $total_unmul += $bouquet->price($item['size'], $franchisee->id, false);
        }

        $priceDifferent = $total_value - $total_unmul;

        $franchisee_commision = $franchisee->commission_for_bouquets;
        $our_value = $priceDifferent * ((100 - $franchisee_commision) / 100);
        $partner_value = $total_unmul + $priceDifferent * ($franchisee_commision / 100);

        $deliveries = Delivery::where("franchisee_id", $franchisee->id)->get();

        return [
            "total_value" => $total_value,
            "total_valueunmul" => $total_unmul,
            "partner_value" => $partner_value,
            "our_value" => $our_value,
            'deliveries' => $deliveries,
            "franchisee_commision" => $franchisee_commision
        ];
    }


    public function delete(Order $order)
    {
        $order->delete();

        return redirect()->back();
    }


}
