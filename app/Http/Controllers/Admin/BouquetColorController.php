<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BouquetColor\StoreRequest;
use App\Models\Bouquet;
use App\Models\BouquetColor;
use Illuminate\Http\Request;

/**
 * Class BouquetColorController
 * @package App\Http\Controllers\Admin
 */
class BouquetColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $color = $request->get('color_id') ? BouquetColor::find($request->get('color_id')) : null;
        $color = $color ?: BouquetColor::getByHex($request->get('hex_value')) ?: BouquetColor::create($request->all());

        if (\Request::has('bouquet_id')) {
            /** @var Bouquet $bouquet */
            $bouquet = Bouquet::find(\Request::get('bouquet_id'));

            if (!$bouquet->colors()->where('hex_value', $color->hex_value)->exists()) {
                $bouquet->colors()->attach($color);
            }

            return redirect()->route('admin.bouquet.edit', $bouquet)->with('active_tab', 'characteristics');
        }

        return redirect()->route('admin.bouquet_color.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BouquetColor  $bouquetColor
     * @return \Illuminate\Http\Response
     */
    public function show(BouquetColor $bouquetColor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BouquetColor  $bouquetColor
     * @return \Illuminate\Http\Response
     */
    public function edit(BouquetColor $bouquetColor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BouquetColor  $bouquetColor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BouquetColor $bouquetColor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BouquetColor  $bouquetColor
     * @return \Illuminate\Http\Response
     */
    public function destroy(BouquetColor $bouquetColor)
    {
        //
    }
}
