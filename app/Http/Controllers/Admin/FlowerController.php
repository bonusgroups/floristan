<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Flower\StoreRequest;
use App\Models\Flower;
use App\Models\Traits\BelongsToFranchiseeTrait;
use Illuminate\Http\Request;

/**
 * Class FlowerController
 * @package App\Http\Controllers
 */
class FlowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $flowers = Flower::paginate(30);

        return view('admin.flower.index')->with([
            'flowers' => $flowers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.flower.add');
    }


    public function store(StoreRequest $request)
    {
        $data = $request->only(['name_en', 'name_ru']);
        $data['is_additional'] = (int)$request->get('is_additional');
        $data['hide_in_filters'] = (int)$request->get('hide_in_filters');

        Flower::create($data);

        return redirect()->route('admin.flower.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Flower $flower
     * @return \Illuminate\Http\Response
     */
    public function show(Flower $flower)
    {
        //
    }



    public function edit(Flower $flower)
    {
        return view('admin.flower.edit')->with([
            'flower' => $flower,
        ]);
    }


    public function update(StoreRequest $request, Flower $flower)
    {
        $data = $request->only(['name_en', 'name_ru']);
        $data['is_additional'] = (int)$request->get('is_additional');
        $data['hide_in_filters'] = (int)$request->get('hide_in_filters');

        $flower->update($data);

        return redirect()->route('admin.flower.edit', $flower);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Flower $flower
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Flower $flower)
    {
        $flower->delete();

        return redirect()->route('admin.flower.index');
    }
}
