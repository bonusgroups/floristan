<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Banner\StoreRequest;
use App\Http\Requests\Admin\Banner\UpdateRequest;
use App\Models\Banner;


class BannerController extends Controller
{

    public function index()
    {
        $banners = Banner::query()->paginate(30);

        return view('admin.banner.index', compact('banners'));
    }


    public function create()
    {

    }


    public function store(StoreRequest $request)
    {

    }


    public function show(Banner $banner)
    {

    }


    public function edit(Banner $banner)
    {
        $image = $banner->image;

        return view('admin.banner.edit', compact('banner', 'image'));
    }


    public function update(Banner $banner)
    {
        $data = request()->all();

        $banner->update($data);

        if ($file = request()->file('image')) {
            $banner->setImage($file);
        }

        return redirect()->route('admin.banner.index', $banner);
    }


    public function destroy(Banner $banner)
    {
        $banner->delete();

        return redirect()->route('admin.banner.index');
    }

}
