<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Bouquet\AddToConsistRequest;
use App\Http\Requests\Admin\Bouquet\AttachReasonRequest;
use App\Http\Requests\Admin\Bouquet\ChangeConsistCountRequest;
use App\Http\Requests\Admin\Bouquet\DeleteFromConsistRequest;
use App\Http\Requests\Admin\Bouquet\DetachColorRequest;
use App\Http\Requests\Admin\Bouquet\DetachReasonRequest;
use App\Http\Requests\Admin\Bouquet\StoreRequest;
use App\Http\Requests\Admin\Bouquet\UpdateRequest;
use App\Models\Bouquet;
use App\Models\Traits\BelongsToFranchiseeTrait;
use Illuminate\Http\Request;

/**
 * Class AdditionalProductController
 * @package App\Http\Controllers\Admin
 */
class AdditionalProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bouquets = Bouquet::query()->isAdditional()->orderBy('name_ru', 'asc')->paginate(30);
        $franchisees = BelongsToFranchiseeTrait::franchisesItemsForSelect(Bouquet::class, true);

        return view('admin.additional_product.index')->with([
            'bouquets' => $bouquets,
            'franchisees' => $franchisees
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.additional_product.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data = $request->all();
        $data['is_active'] = isset($data['is_active']) ? $data['is_active'] : 0;
        $data['is_new'] = isset($data['is_new']) ? $data['is_new'] : 0;
        $data['hit'] = isset($data['hit']) ? $data['hit'] : 0;

        $bouquet = Bouquet::create($data);

        return redirect()->route('admin.additional_product.edit', $bouquet)->with('active_tab', 'characteristics');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bouquet $bouquet
     * @return \Illuminate\Http\Response
     */
    public function show(Bouquet $bouquet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bouquet $bouquet
     * @return \Illuminate\Http\Response
     */
    public function edit(Bouquet $bouquet)
    {
        $allColors = Bouquet::getAllColors();

        return view('admin.additional_product.edit', compact('allColors'))
            ->with('bouquet', $bouquet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  \App\Models\Bouquet $bouquet
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Bouquet $bouquet)
    {
        $data = $request->all();
        $data['is_active'] = isset($data['is_active']) ? $data['is_active'] : 0;
        $data['is_new'] = isset($data['is_new']) ? $data['is_new'] : 0;
        $data['hit'] = isset($data['hit']) ? $data['hit'] : 0;

        $bouquet->update($data);

        return redirect()->route('admin.additional_product.edit', $bouquet)->with('active_tab', 'characteristics');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bouquet $bouquet
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Bouquet $bouquet)
    {
        $bouquet->delete();

        return redirect()->route('admin.additional_product.index');
    }

    /**
     * @param ChangeConsistCountRequest $request
     * @param Bouquet $bouquet
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeConsistCount(ChangeConsistCountRequest $request, Bouquet $bouquet)
    {
        $bouquet->flowers()
            ->wherePivot('size', $request->get('bouquet_size'))
            ->updateExistingPivot($request->get('flower_id'), ['count' => $request->get('count')]);

        return redirect()->route('admin.additional_product.edit', $bouquet);
    }

    /**
     * @param DeleteFromConsistRequest $request
     * @param Bouquet $bouquet
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteFromConsist(DeleteFromConsistRequest $request, Bouquet $bouquet)
    {
        $bouquet->flowers()
            ->wherePivot('size', $request->get('bouquet_size'))
            ->wherePivot('flower_id', $request->get('flower_id'))
            ->detach();

        return redirect()->route('admin.additional_product.edit', $bouquet);
    }

    /**
     * @param AddToConsistRequest $request
     * @param Bouquet $bouquet
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addToConsist(AddToConsistRequest $request, Bouquet $bouquet)
    {
        $bouquet->flowers()->attach($request->get('flower_id'), [
            'size' => $request->get('bouquet_size'),
            'count' => $request->get('count_for_adding'),
        ]);

        return redirect()->route('admin.additional_product.edit', $bouquet);
    }

    /**
     * @param DetachReasonRequest $request
     * @param Bouquet $bouquet
     * @return \Illuminate\Http\RedirectResponse
     */
    public function detachReason(DetachReasonRequest $request, Bouquet $bouquet)
    {
        $bouquet->reasons()->detach($request->get('reason_id'));

        return redirect()->route('admin.additional_product.edit', $bouquet)->with('active_tab', 'characteristics');
    }

    /**
     * @param AttachReasonRequest $request
     * @param Bouquet $bouquet
     * @return \Illuminate\Http\RedirectResponse
     */
    public function attachReason(AttachReasonRequest $request, Bouquet $bouquet)
    {
        $bouquet->reasons()->attach($request->get('reason_id'));

        return redirect()->route('admin.additional_product.edit', $bouquet)->with('active_tab', 'characteristics');
    }

    /**
     * @param DetachColorRequest $request
     * @param Bouquet $bouquet
     * @return \Illuminate\Http\RedirectResponse
     */
    public function detachColor(DetachColorRequest $request, Bouquet $bouquet)
    {
        $bouquet->colors()->detach($request->get('color_id'));

        return redirect()->route('admin.additional_product.edit', $bouquet)->with('active_tab', 'characteristics');
    }

    /**
     * @param Request $request
     * @param Bouquet $bouquet
     * @return \Illuminate\Http\RedirectResponse
     */
    public function attachCategory(Request $request, Bouquet $bouquet)
    {
        $bouquet->categories()->sync(collect($request->get('category_id')));

        return redirect()->route('admin.additional_product.edit', $bouquet)->with('active_tab', 'characteristics');
    }
}
