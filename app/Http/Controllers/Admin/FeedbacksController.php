<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 21.05.2018
 * Time: 18:27
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Feedbacks\StoreRequest;
use App\Models\Feedbacks;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use League\Flysystem\Exception;

class FeedbacksController extends Controller
{
    public function index()
    {
        $feedbacks = Feedbacks::paginate(10);
        return view('admin.feedbacks.index', compact('feedbacks'));
    }

    public function show(Feedbacks $feedback)
    {
        return view('admin.feedbacks.show', compact('feedback'));
    }


    public function edit(Feedbacks $feedback)
    {
        return view('admin.feedbacks.edit', compact('feedback'));
    }


    public function update(StoreRequest $request, Feedbacks $feedback)
    {
        $feedback->name = $request->get('name');
        $feedback->vk_link = $request->get('vk_link');
        $feedback->description = $request->get('description');
        $feedback->active = (bool)$request->get('active');
        $feedback->rating = (int)$request->get('rating');
        $feedback->created_at = $this->carbon($request->get('created_at'));

        if ($image = $request->file('photo')) {
            $feedback->photo = \Storage::disk('feedback_images')->put(null, $image);
        }

        if ($image = $request->file('avatar')) {
            $feedback->avatar = \Storage::disk('feedback_images')->put(null, $image);
        }

        $feedback->save();

        return redirect()->route('admin.feedbacks.index');
    }


    public function carbon($value)
    {
        try {
            return Carbon::parse($value);
        } catch (Exception $exception) {
            return null;
        }
    }


    public function create()
    {
        return view('admin.feedbacks.add');
    }


    public function store(StoreRequest $request)
    {
        $feedback = new Feedbacks();
        $feedback->name = $request->get('name');
        $feedback->vk_link = $request->get('vk_link');
        $feedback->description = $request->get('description');
        $feedback->active = (bool)$request->get('active');
        $feedback->rating = (int)$request->get('rating');
        $feedback->created_at = $this->carbon($request->get('created_at'));

        if ($image = $request->file('photo')) {
            $feedback->photo = \Storage::disk('feedback_images')->put(null, $image);
        }

        if ($image = $request->file('avatar')) {
            $feedback->avatar = \Storage::disk('feedback_images')->put(null, $image);
        }

        $feedback->save();

        return redirect()->route('admin.feedbacks.index');
    }


    public function destroy(Feedbacks $feedback)
    {
        $feedback->delete();

        return redirect()->route('admin.feedbacks.index');
    }

}