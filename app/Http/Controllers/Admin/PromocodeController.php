<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Promocode\StoreRequest;
use App\Models\Promocode;
use Carbon\Carbon;

/**
 * Class PromocodesController
 * @package App\Http\Controllers
 */
class PromocodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promocodes = Promocode::paginate(30);

        return view('admin.promocode.index')->with([
            'promocodes' => $promocodes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promocode.add');
    }


    public function store(StoreRequest $request)
    {
        $data = $request->all();
        $createdAt = ($date = $request->get('created_at')) ? Carbon::parse($date) : Carbon::now();
        $data['created_at'] = $createdAt;

        $promocode = Promocode::create($data);

        return redirect()->route('admin.promocode.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Promocode $promocode
     * @return \Illuminate\Http\Response
     */
    public function show(Promocode $promocode)
    {
        //
    }


    public function edit(Promocode $promocode)
    {
        return view('admin.promocode.edit')->with([
            'promocode' => $promocode,
        ]);
    }


    public function update(StoreRequest $request, Promocode $promocode)
    {
        $data = $request->all();
        $createdAt = ($date = $request->get('created_at')) ? Carbon::parse($date) : null;

        if ($createdAt) {
            $data['created_at'] = $createdAt;
        }

        $promocode->update($data);

        return redirect()->route('admin.promocode.edit', $promocode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Promocode $promocode
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Promocode $promocode)
    {
        $promocode->delete();

        return redirect()->route('admin.promocode.index');
    }
}
