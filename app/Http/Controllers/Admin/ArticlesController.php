<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 29.05.2018
 * Time: 13:13
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Articles\StoreRequest;
use App\Models\Article;
use Request;

class ArticlesController extends Controller
{

    public function index()
    {
        $articles = Article::paginate(10);

        return view('admin.articles.index', compact('articles'));
    }


    public function edit(Article $article)
    {
        return view('admin.articles.edit', compact('article'));
    }


    public function update(StoreRequest $request, Article $article)
    {
        $article->update($request->all());

        return redirect()->route('admin.articles.index');
    }


    public function store(StoreRequest $request)
    {
        $article = Article::create($request->all());

        return redirect()->route('admin.articles.index');
    }


    public function destroy(Article $article)
    {
        try {
            $article->delete();
        } catch (\Exception $exception) {

        }

        return redirect()->route('admin.articles.index');
    }


    public function create()
    {
        return view('admin.articles.add');
    }


    public function show(Article $article)
    {
        //
    }


}