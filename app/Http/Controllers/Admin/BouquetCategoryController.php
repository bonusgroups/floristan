<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BouquetCategory;
use Illuminate\Http\Request;

/**
 * Class BouquetCategoryController
 * @package App\Http\Controllers
 */
class BouquetCategoryController extends Controller
{

    public function index()
    {
        $items = BouquetCategory::query()
            ->onlyRoot()
            ->with('children')
            ->orderBy('sort_order', 'asc')
            ->orderBy('name_ru', 'asc')
            ->paginate(30);

        return view('admin.bouquet_category.index')->with([
            'items' => $items
        ]);
    }


    public function create()
    {
        return view('admin.bouquet_category.add');
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $data['published'] = (int)$request->get('published');

        BouquetCategory::create($data);

        return redirect()->route('admin.bouquet_category.index');
    }


    public function show(BouquetCategory $bouquetCategory)
    {

    }


    public function edit(BouquetCategory $bouquetCategory)
    {
        return view('admin.bouquet_category.edit')->with([
            'model' => $bouquetCategory,
        ]);
    }


    public function update(BouquetCategory $bouquetCategory)
    {
        $data = request()->all();
        $data['published'] = (int)request('published');

        $bouquetCategory->update($data);

        return redirect()->route('admin.bouquet_category.edit', $bouquetCategory);
    }


    public function destroy(BouquetCategory $bouquetCategory)
    {
        $bouquetCategory->delete();

        return redirect()->route('admin.bouquet_category.index');
    }


    public function sort()
    {
        $ids = request('ids') ?: [];
        $items = $ids ? BouquetCategory::find($ids) : collect();

        foreach ($ids as $key => $id) {
            $model = $items->where('id', $id)->first();

            if (!$model) continue;

            $model->sort_order = $key;
            $model->save();
        }
    }

}
