<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 28.05.2018
 * Time: 11:56
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Franchisee;
use App\Models\FranchiseeLog;
use App\Models\Order;
use Illuminate\Http\Request;

class FinanceController extends Controller
{
    public function index()
    {
        $franchisees = Franchisee::orderBy("city_ru", 'ASC')->paginate(20);
        return view('admin.finance.list', compact('franchisees'));
    }

    public function show(Request $request, $id)
    {
        $franchisee = Franchisee::find($id);
        $today = date("Y-m-d");
        $logs = $franchisee->logs()->where("type", "out")->orderBy("id", "desc")->get();
        return view('admin.finance.show-ajax', compact('franchisee', 'today', 'logs'));
    }

    public function store(Request $request)
    {
        $date = $request->get("date");
        $sum = $request->get("sum");
        $franchise_id = $request->get("franchisee_id");
        $franchisee = Franchisee::find($franchise_id);
        $franchisee->makeLog($date, $sum);
        return ['sucess' => true];
    }

    public function destroy(Request $request, $id)
    {
        $log = FranchiseeLog::find($id);

        //reverse
        $franchisee = $log->franchisee;
        $franchisee->total_payment -= $log->append;
        $franchisee->balance += $log->append;
        $franchisee->save();

        return ['success' => $log->delete()];
    }
}