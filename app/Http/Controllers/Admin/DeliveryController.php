<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Delivery\StoreRequest;
use App\Models\Delivery;
use App\Models\Franchisee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        Delivery::create($request->all());

        if ($request->has('franchisee_id')) {

            return redirect()->route('admin.franchisee.edit', $request->get('franchisee_id'));
        }

        return redirect()->route('admin.delivery.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Delivery $delivery
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Delivery $delivery, Request $request)
    {
        $delivery->delete();

        if ($request->has('franchisee')) {
            return redirect()->route('admin.franchisee.edit', $request->get('franchisee'));
        }

        return redirect()->route('admin.delivery.index');
    }
}
