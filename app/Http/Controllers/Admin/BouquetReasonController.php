<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BouquetReason\StoreRequest;
use App\Models\Bouquet;
use App\Models\BouquetReason;
use Illuminate\Http\Request;

/**
 * Class BouquetReasonController
 * @package App\Http\Controllers\Admin
 */
class BouquetReasonController extends Controller
{

    public function index()
    {
        $items = BouquetReason::paginate(30);

        return view('admin.bouquet_reason.index')->with([
            'items' => $items
        ]);
    }


    public function create()
    {
        return view('admin.bouquet_reason.add');
    }


    public function store(StoreRequest $request)
    {
        $reason = BouquetReason::create($request->all());

        if (\Request::has('bouquet_id')) {
            /** @var Bouquet $bouquet */
            $bouquet = Bouquet::findOrFail(\Request::get('bouquet_id'));
            $bouquet->reasons()->attach($reason);

            return redirect()->route('admin.bouquet.edit', $bouquet)->with('active_tab', 'characteristics');
        }

        return redirect()->route('admin.bouquet_reason.index');
    }


    public function show(BouquetReason $bouquetReason)
    {
        //
    }


    public function edit(BouquetReason $bouquetReason)
    {
        return view('admin.bouquet_reason.edit')->with([
            'model' => $bouquetReason,
        ]);
    }


    public function update(StoreRequest $request, BouquetReason $bouquetReason)
    {
        $bouquetReason->is_additional = (int)$request->get('is_additional');
        $bouquetReason->update($request->only(['name_en', 'name_ru']));

        return redirect()->route('admin.bouquet_reason.edit', $bouquetReason);
    }


    public function destroy(BouquetReason $bouquetReason)
    {
        $bouquetReason->delete();

        return redirect()->route('admin.bouquet_reason.index');
    }

}
