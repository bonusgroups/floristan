<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Flower;
use Illuminate\Validation\ValidationException;


class FlowerForFiltersController extends Controller
{

    public function index()
    {
        $items = Flower::all();

        return view('admin.flower_for_filters.index', compact('items'));
    }


    public function change()
    {
        $id = request()->get('id');
        $flower = $id ? Flower::find($id) : null;

        if (!$flower) {
            throw new \Exception('Цветок не найден');
        }

        $flower->show_in_filters = (int)request()->get('value');
        $flower->save();
    }

}