<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Models\BouquetImage;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * Class BouquetImageController
 * @package App\Http\Controllers\Admin
 */
class BouquetImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bouquet = Bouquet::find($request->get('bouquet_id'));
        $uploadedFiles = $request->file('file');

        /** @var UploadedFile $uploadedFile */
        foreach ($uploadedFiles as $uploadedFile) {
            BouquetImage::store($bouquet, $uploadedFile);
        }

        return redirect()->route('admin.bouquet.edit', $bouquet)->with('active_tab', 'images');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BouquetImage  $bouquetImage
     * @return \Illuminate\Http\Response
     */
    public function show(BouquetImage $bouquetImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BouquetImage  $bouquetImage
     * @return \Illuminate\Http\Response
     */
    public function edit(BouquetImage $bouquetImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BouquetImage  $bouquetImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BouquetImage $bouquetImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BouquetImage $bouquetImage
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(BouquetImage $bouquetImage)
    {
        $bouquet = $bouquetImage->bouquet;

        $bouquetImage->delete();

        return redirect()->route('admin.bouquet.edit', $bouquet)->with('active_tab', 'images');
    }
}
