<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 24.05.2018
 * Time: 12:20
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SettingRequest;
use App\Services\SettingService;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        $settings = SettingService::shared()->all();

        return view('admin.settings.index', compact('settings'));
    }

    public function store(SettingRequest $request)
    {
        SettingService::shared()->update($request->only([
            'mul_price',
            'mul_price_additional',
            'sms_api_key',
            'email'
        ]));

        return redirect(route('admin.settings.index'));
    }
}