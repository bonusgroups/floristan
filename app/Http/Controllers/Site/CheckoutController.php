<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Models\Delivery;
use App\Models\Franchisee;
use App\Models\Order;
use App\Models\Promocode;
use App\Services\FranchiseeService;
use App\Services\YaKassaService;
use Carbon\Carbon;


class CheckoutController extends Controller
{

    public $order = null;
    public $originalPrice = null;
    public $cart = null;
    public $promocode;
    public $franchisee;


    public function setCart($cart)
    {
        $this->cart = $cart;
    }


    public function getOrder()
    {
        return $this->order ?: new Order();
    }


    public function setOrder($order)
    {
        $this->order = $order;
    }


    public function cartAction()
    {
        $controller = $this;

        $cart = $this->getCart();
        $products = $this->getCartProducts();
        $relatedProducts = $this->getRelatedProducts();

        return view('site.checkout.cart.index', compact(
            'controller',
            'cart',
            'products',
            'relatedProducts'
        ));
    }


    public function infoAction()
    {
        $controller = $this;

        return view('site.checkout.info.index', compact(
            'controller'
        ));
    }


    public function payAction()
    {
        $controller = $this;

        return view('site.checkout.pay.index', compact(
            'controller'
        ));
    }


    public function successAction()
    {
        $id = (int)request()->get('order_id');
        $order = $id ? Order::find($id) : null;

        return view('site.checkout.success', compact(
            'order'
        ));
    }


    public function submitAction()
    {
        $validator = $this->getSubmitValidator();

        if ($validator->errors()->count()) {
            return redirect($this->getCurrentStepUrl())
                ->withErrors($validator)
                ->withInput();
        }

        $request = request();

        switch ($this->getStep()) {
            case 'cart':
                $this->setDataValue('postcard', $request->get('postcard'));

                if ($request->get('postcard')) {
                    $this->setDataValue('postcard_text', $request->get('postcard_text'));
                }

                return redirect($this->getNextStepUrl());
                break;

            case 'info':
                $this->setDataValue('client_name', $request->get('client_name'));
                $this->setDataValue('client_phone', $request->get('client_phone'));
                $this->setDataValue('delivery_date', $request->get('delivery_date'));
                $this->setDataValue('client_email', $request->get('client_email'));

                $this->setDataValue('has_recipient', $request->get('has_recipient'));

                if ($request->get('has_recipient')) {
                    $this->setDataValue('recipient_name', $request->get('recipient_name'));
                    $this->setDataValue('recipient_phone', $request->get('recipient_phone'));
                }

                $this->setDataValue('has_recipient_address', $request->get('has_recipient_address'));

                if ($request->get('has_recipient_address')) {
                    $this->setDataValue('recipient_address', $request->get('recipient_address'));
                    $this->setDataValue('delivery_time', $request->get('delivery_time'));
                }

                $this->setDataValue('client_comment', $request->get('client_comment'));
                $this->setDataValue('delivery_type', $request->get('delivery_type'));

                return redirect($this->getNextStepUrl());
                break;

            case 'pay':
                $this->setDataValue('payment_method', $request->get('action'));

                $this->createOrder();
                $this->clearSession();

                $this->getOrder()->sendAdminNotice();
                $this->getOrder()->sendToAmoCrm();

                switch ($request->get('action')) {
                    case 'cash':
                        return redirect($this->getOrder()->getSuccessUrl());
                        break;

                    case 'online':
                        return redirect($this->getYaKassaUrl());
                        break;
                }
                break;
        }

        return null;
    }


    public function getSubmitValidator()
    {
        $request = request();
        $data = $request->all();

        $rules = [];
        $messages = [];
        $errors = [];

        switch ($this->getStep()) {
            case 'cart':
                break;

            case 'info':
                $rules['client_phone'] = 'required|max:255';
                $rules['client_email'] = 'nullable|email';
                $rules['delivery_date'] = 'required';

                $messages['client_email.email'] = 'E-mail имеет неверный формат';

                if ($request->get('has_recipient')) {
//                    $rules['recipient_phone'] = 'required|max:255';
                }

                if ($request->get('has_recipient_address')) {
//                    $rules['recipient_address'] = 'required|max:255';
//                    $rules['delivery_time'] = 'required';

                    if ($time = $request->get('delivery_time') and !$this->checkTime($time)) {
                        $errors['delivery_time'] = 'Неправильный формат';
                    }
                }

                if ($date = $request->get('delivery_date') and !$this->checkDate($date)) {
                    $errors['delivery_date'] = 'Неправильный формат';
                }

                if ($date = $request->get('delivery_date') and !$this->isDateInFuture($date)) {
                    $errors['delivery_date'] = 'Дата должна быть в будущем';
                }

                try {
                    $this->validateDeliveryTime();
                } catch (\Exception $exception) {
                    $errors['delivery_time'] = $exception->getMessage();
                }
                break;

            case 'pay':
                break;
        }

        $validator = \Validator::make($data, $rules, $messages);

        foreach ($errors as $key => $error) {
            $validator->errors()->add($key, $error);
        }

        if (!count($this->getCart())) {
            $validator->errors()->add('error', 'Ваша корзина пуста');
        }

        return $validator;
    }


    public function validateDeliveryTime()
    {
        $deliveryTime = request('delivery_time');

        if (!$deliveryTime) return;

        $deliveryDate = request('delivery_date') ?: date('d.m.Y');

        $now = Carbon::now();
        $date = Carbon::parse("{$deliveryDate} {$deliveryTime}");

        if ($date->lt($now)) {
            throw new \Exception('Время доставки должно быть в будущем');
        }

        if ($date->diffInMinutes($now) < 120) {
            throw new \Exception('Необходимо не менее 2 часов на доставку');
        }

//        $time = Carbon::parse(date('d.m.Y') . ' ' . $deliveryTime);
//
//        $timeMin = Carbon::parse(date('d.m.Y') . ' 07:00');
//        $timeMax = Carbon::parse(date('d.m.Y') . ' 23:59');
//
//        if (!$time->between($timeMin, $timeMax)) {
//            throw new \Exception('Доставка осуществляется с 07:00 до 23:59');
//        }
    }


    public function isDateInFuture($date)
    {
        $time = strtotime($date . ' ' . date('H:i:s'));

        return ($time >= time());
    }


    public function createOrder()
    {
        $promocode = $this->getPromocode();
        $franchisee = $this->getCurrentFranchisee();
        $cart = $this->getCart();
        $orderTotal = $this->getOrderTotal();

        \DB::beginTransaction();

        $order = $this->getOrder();
        $order->franchisee_id = $franchisee->id;
        $order->postcard_text = $this->getDataValue('postcard') ? $this->getDataValue('postcard_text') : null;
        $order->delivery_id = null;
        $order->pay_type = $this->getPaymentMethodKey();
        $order->status = 0;
        $order->points_value = 0;
        $order->client_comment = null;
        $order->client_name = $this->getDataValue('client_name');
        $order->client_phone = $this->getDataValue('client_phone');
        $order->client_email = $this->getDataValue('client_email');
        $order->recipient_name = $this->getDataValue('recipient_name');
        $order->recipient_phone = $this->getDataValue('recipient_phone');
        $order->recipient_address = $this->getDataValue('has_recipient_address') ? $this->getDataValue('recipient_address') : null;
        $order->need_to_call = (int)!$this->getDataValue('has_recipient_address');
        $order->delivery_date = $this->getDataValue('delivery_date') ? Carbon::parse($this->getDataValue('delivery_date')) : null;
        $order->delivery_time = $this->getDataValue('delivery_time') ? ($this->getDataValue('delivery_time') . ':00') : null;
        $order->delivery_type = $this->getDataValue('delivery_type');
        $order->client_comment = $this->getDataValue('client_comment');
        $order->franchisee_id = $franchisee->id;
        $order->promocode_id = optional($promocode)->id;

        $order->base_total_value = $this->getBaseTotal();
        $order->base_total_value_without_discount_bouquets = $this->getBaseTotalOnlyWithoutDiscountBouquets();
        $order->total_value = $orderTotal;
        $order->partner_value = $this->getPartnerValue();
        $order->our_value = $orderTotal - $order->partner_value;

        $order->save();

        if ($promocode) $promocode->useNow();

        $order->bouquets()->sync([]);

        foreach ($cart as $item) {
            $order->bouquets()->attach($item['id'], [
                'count' => 1,
                'bouquet_size' => $item['bouquet_size']
            ]);
        }

        $this->setOrder($order);

        \DB::commit();

        return $order;
    }


    public function checkTime($string)
    {
        $parts = explode(':', trim($string));

        if (count($parts) != 2) return false;

        foreach ($parts as $part) {
            if (mb_strlen($part) != 2) return false;
        }

        return true;
    }


    public function checkDate($date)
    {
        try {
            Carbon::parse($date);
        } catch (\Exception $exception) {
            return false;
        }

        return true;
    }


    public function clearSession()
    {
        session()->put('cart', []);
        session()->put('cart_size', 0);
        session()->put('ordernumber', $this->getOrder()->id);

        $this->setData([]);
    }


    public function getYaKassaUrl()
    {
        $order = $this->getOrder();

        $yaKassa = new YaKassaService();
        $yaKassa->setOrder($order);

        return $yaKassa->getUrl();
    }


    public function getPaymentMethodKey()
    {
        $code = $this->getDataValue('payment_method');

        switch ($code) {
            case 'cash':
                return 0;
                break;

            case 'online':
                return 1;
                break;
        }

        return null;
    }


    public function getCurrentStepUrl()
    {
        $step = $this->getStep();

        return $this->getStepUrl($step);
    }


    public function getSteps()
    {
        return [
            'cart' => 'Корзина',
            'info' => 'Контакты и доставка',
            'pay' => 'Оплата'
        ];
    }


    public function getStep()
    {
        if ($step = request()->get('step')) return $step;

        return request()->segment(2) ?: $this->getDefaultStep();
    }


    public function getDefaultStep()
    {
        return 'cart';
    }


    public function isCurrentStep($step)
    {
        return ($step == $this->getStep());
    }


    public function isDefaultStep($step)
    {
        return ($step = $this->getDefaultStep());
    }


    public function isCompletedStep($step)
    {
        $currentStep = $this->getStep();

        foreach ($this->getSteps() as $key => $name) {
            if ($currentStep == $key) break;

            if ($step == $key) return true;
        }

        return false;
    }


    public function getNextStep()
    {
        $step = $this->getStep();
        $next = false;

        foreach ($this->getSteps() as $key => $name) {
            if ($next) return $key;

            if ($key == $step) $next = true;
        }

        return null;
    }


    public function getNextStepUrl()
    {
        $step = $this->getNextStep();

        if (!$step) return null;

        return $this->getStepUrl($step);
    }


    public function getStepUrl($key)
    {
        return url($this->getStepPath($key));
    }


    public function getStepPath($key)
    {
        if ($this->getDefaultStep() == $key) return 'checkout';

        return 'checkout/' . $key;
    }


    public function getCart()
    {
        if ($this->cart !== null) return $this->cart;

        return $this->cart = session()->get('cart') ?: [];
    }


    public function getCartProducts()
    {
        $cart = $this->getCart();
        $ids = array_column($cart, 'id');

        if (!$ids) return collect();

        return Bouquet::whereIn('id', $ids)->with('images')->get();
    }


    public function getRelatedProducts()
    {
        return Bouquet::inRandomOrder()
            ->isAdditional()
            ->isActive()
            ->with('images')
            ->limit(12)
            ->get();
    }


    public function getFormId()
    {
        return 'checkout-step-form';
    }


    public function getFormElementId($key)
    {
        return $this->getFormId() . '-' . $this->getStep() . '-' . $key;
    }


    public function getData()
    {
        return session()->get('checkout_data') ?: [];
    }


    public function setData($data)
    {
        session()->put('checkout_data', $data);
    }


    public function getDataValue($key, $default = null)
    {
        $data = $this->getData();

        return data_get($data, $key) ?: $default;
    }


    public function setDataValue($key, $value)
    {
        $data = $this->getData();
        $data[$key] = $value;

        $this->setData($data);
    }


    public function setCurrentFranchise($franchisee)
    {
        $this->franchisee = $franchisee;
    }


    public function getCurrentFranchisee()
    {
        if ($this->franchisee !== null) return $this->franchisee;

        return $this->franchisee = Franchisee::getCurrent();
    }


    public function getBaseTotal($options = [])
    {
        $options += [
            'onlyWithoutDiscountBouquets' => false
        ];

        $cart = collect($this->getCart());
        $ids = $cart->pluck('id');
        $bouquets = $ids ? Bouquet::find($ids) : collect();

        if ($options['onlyWithoutDiscountBouquets']) {
            $bouquets = $bouquets->filter(function (Bouquet $bouquet) {
                if ($bouquet->isAdditional()) return false;
                if ($bouquet->discount) return false;

                return true;
            });
        }

        $total = 0;

        foreach ($cart as $item) {
            $bouquet = $bouquets->where('id', $item['id'])->first();

            if (!$bouquet) continue;

            $total += $bouquet->priceWithDiscount($item['bouquet_size']);
        }

        return $total;
    }


    public function getBaseTotalOnlyWithoutDiscountBouquets()
    {
        return $this->getBaseTotal(['onlyWithoutDiscountBouquets' => true]);
    }


    public function getOrderTotal()
    {
        $total = $this->getBaseTotal();

        $total += $this->getDeliveryPrice();
        $total -= $this->getPromocodeTotal();

        if ($total < 0) return 0;

        return $total;
    }


    public function getOrderTotalWithFormat()
    {
        return number_format($this->getOrderTotal(), 0, ', ', ' ');
    }


    public function getDeliveryPrice()
    {
        if ($this->getDataValue('delivery_type') == 'delivery') {
            if ($this->isTimeBetween($this->getDataValue('delivery_time'), '8:00', '21:00')) {
                return 0;
            }

            return 200;
        }

        return 0;
    }


    public function isTimeBetween($time, $from, $to)
    {
        if (!$time) return false;

        $date = strtotime(date('d.m.Y') . ' ' . $time);

        $fromDate = strtotime(date('d.m.Y') . ' ' . $from);
        $toDate = strtotime(date('d.m.Y') . ' ' . $to);

        return ($date >= $fromDate and $date <= $toDate);
    }


    public function getOriginalPrice()
    {
        if ($this->originalPrice !== null) return $this->originalPrice;

        $bouquets = $this->getCartProducts();
        $price = 0;

        foreach ($this->getCart() as $item) {
            $bouquet = $bouquets->where('id', $item['id'])->first();
            $price += $bouquet->price($item['bouquet_size'], null, false);
        }

        return $this->originalPrice = $price;
    }


    public function getPartnerValue()
    {
        $franchiseeCommission = $this->getFranchiseeCommission();
        $profit = $this->getOrderTotal() - $this->getOriginalPrice();

        if ($profit > 0 == false) return 0;

        return $profit * ($franchiseeCommission / 100);
    }


    public function getFranchiseeCommission()
    {
        $franchisee = $this->getCurrentFranchisee();

        return intval($franchisee->commission_for_bouquets);
    }


    public function getPriceMul(Bouquet $bouquet = null)
    {
        if ($bouquet and $bouquet->isAdditional()) {
            return \App\Services\SettingService::shared()->get("mul_price_additional", 250) / 100;
        }

        return \App\Services\SettingService::shared()->get("mul_price", 250) / 100;
    }


    public function getPromocodeText(Promocode $promocode = null)
    {
        if (!$promocode) return null;

        return 'Сумма скидки по купону: ' . $promocode->getDiscountPrice($this->getBaseTotalOnlyWithoutDiscountBouquets()) . ' Р';
    }


    public function getPromocodeFromRequest()
    {
        $request = request();

        $code = trim($request->get('promocode'));

        if (!$code) return null;

        $promocode = Promocode::findByCode($code);

        if (!$promocode) return null;

        if (!$promocode->isAvailable()) return null;

        return $promocode;
    }


    public function setPromocode($promocode)
    {
        $this->promocode = $promocode;
    }


    public function getPromocode()
    {
        if ($this->promocode !== null) return $this->promocode;

        $promocodeId = $this->getDataValue('promocode_id');

        return $this->promocode = ($promocodeId ? Promocode::find($promocodeId) : null);
    }


    public function getPromocodeTotal()
    {
        $promocode = $this->getPromocode();

        if (!$promocode) return 0;

        return $promocode->getDiscountPrice($this->getBaseTotalOnlyWithoutDiscountBouquets());
    }


    public function promocodeAction()
    {
        try {
            $this->promocodeValidate();
        } catch (\Exception $exception) {
            $error = $exception->getMessage();

            return [
                'update' => [
                    '[data-promocode-form-messages]' => view('site.checkout.cart.promocode-error', compact('error'))->render(),
                ],
                'hasError' => true
            ];
        }

        $this->setDataValue('promocode_id', $this->getPromocodeFromRequest()->id);

        $controller = $this;

        return [
            'update' => [
                '[data-promocode-form-messages]' => null,
                '[data-checkout-step-form-order-total]' => $this->getOrderTotalWithFormat(),
                '[data-checkout-step-form-promocode-info]' => view('site.checkout.cart.promocode-info', compact('controller'))->render()
            ]
        ];
    }


    public function promocodeValidate()
    {
        $code = trim(request()->get('promocode'));

        if (!$code) {
            throw new \Exception('Введите промокод');
        }

        $promocode = $this->getPromocodeFromRequest();

        if (!$promocode) {
            throw new \Exception('Промокод неверный');
        }
    }


    public function promocodeDeleteAction()
    {
        $data = $this->getData();

        unset($data['promocode_id']);

        $this->setData($data);

        $controller = $this;

        return [
            'update' => [
                '[data-checkout-step-form-order-total]' => $this->getOrderTotalWithFormat(),
                '[data-checkout-step-form-promocode-info]' => view('site.checkout.cart.promocode-info', compact('controller'))->render()
            ]
        ];
    }


    public function getDeliveryTypeOptions()
    {
        return (new Order())->getDeliveryTypeOptions();
    }

}
