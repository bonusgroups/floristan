<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 18.05.2018
 * Time: 12:37
 */

namespace App\Http\Controllers\Site;


use App\Http\Controllers\Controller;
use App\Models\Franchisee;
use App\Models\Order;
use App\Notifications\CallbackNotification;
use App\Services\FranchiseeService;
use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;

class CallbackResponseController extends Controller
{
    public function sendCallback($city, Request $request) {
        $franchisee = (new FranchiseeService())->getCurrentFranchisee();

        $order = new Order();
        $order->client_phone = $request->get('phone');
        $order->franchisee_id = $franchisee->id;
        $order->save();

        $order->sendAdminNotice();

        return [
            'error' => null
        ];
    }
}