<?php

namespace App\Http\Controllers\Site;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ArticlesController
 * @package App\Http\Controllers\Site
 */
class ArticlesController extends Controller
{
    public function index()
    {
        $articles = Article::orderBy("id","desc")->get();
        return view('site.articles.index',compact('articles'));
    }

    public function show($city, $slug)
    {
        $article = Article::where("slug",$slug)->first();
        if(!$article) {
            abort(404);
        }
        $articles = Article::where("id","!=",$article->id)->inRandomOrder()->limit(3)->get();
        return view('site.articles.show',compact('article','articles'));
    }
}
