<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\PageInfo;

class PageInfoController extends Controller
{

    public function popup()
    {
        if (!$this->hasAccess()) return null;

        $path = request('path');
        $model = PageInfo::getByPath($path) ?: new PageInfo();

        return [
            'content' => view('site.page-info.popup', compact('path', 'model'))->render()
        ];
    }


    public function submit()
    {
        if (!$this->hasAccess()) return null;

        $id = request('id');
        $path = request('path');

        $model = $id ? PageInfo::find($id) : null;
        $model = $model ?: PageInfo::getByPath($path) ?: new PageInfo();

        $model->path = $path;
        $model->title = request('title');
        $model->meta_keywords = request('meta_keywords');
        $model->meta_description = request('meta_description');
        $model->save();

        $messages = [
            'success' => [
                'Данные сохранены'
            ]
        ];

        return [
            'messages' => view('site.page-info.messages', compact('messages'))->render()
        ];
    }


    public function hasAccess()
    {
        return \Auth::guard('admin')->check();
    }

}
