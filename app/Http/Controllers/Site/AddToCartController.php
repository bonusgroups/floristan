<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Models\Franchisee;
use App\Models\Order;
use Carbon\Carbon;

class AddToCartController extends Controller
{

    public function index()
    {
        $id = request('id');
        $bouquet = $id ? Bouquet::find($id) : null;

        if (!$bouquet) {
            throw new \Exception('Букет не найден');
        }

        $size = request('size');

        $total = $this->addToCart($bouquet, $size);

        return [
            'popup' => view('site.add_to_cart.popup', compact('bouquet'))->render(),
            'total' => number_format($total, 0, ', ', ' ')
        ];
    }


    public function addToCart($bouquet, $size)
    {
        $cart = $this->getCart();

        $cart[] = [
            'id' => $bouquet->id,
            'bouquet_size' => $size
        ];

        session()->put('cart', $cart);

        $total = (new CartController())->get_cart_size($cart);

        session()->put('cart_size', $total);

        return $total;
    }


    public function getCart()
    {
        return session()->get('cart') ?: [];
    }

}
