<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Services\BouquetVoteService;


class BouquetVoteController extends Controller
{

    public function vote()
    {
        $bouquetId = request('bouquet_id');
        $value = request('value');

        $bouquet = Bouquet::find($bouquetId);

        $service = new BouquetVoteService();
        $service->setBouquet($bouquet);
        $service->setIp(request()->ip());
        $service->vote($value);

        return [
            'bouquet_id' => $bouquetId,
            'stars' => view('site.catalog.rating', compact('bouquet'))->render()
        ];
    }

}
