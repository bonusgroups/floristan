<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Franchisee;

/**
 * Class SelectCityController
 * @package App\Http\Controllers\Site
 */
class SelectCityController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $franchisees = Franchisee::select(['id', 'domain', 'city_ru', 'city_en'])
            ->groupBy(['city_ru', 'city_en'])
            ->get();

        return view('site.index.cities_list')->with('franchisees', $franchisees);
    }
}
