<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Bouquet;
use App\Models\Feedbacks;
use App\Services\FranchiseeService;
use Session;

/**
 * Class HomePageController
 * @package App\Http\Controllers\Site
 */
class HomePageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        $franchisee = app(FranchiseeService::class)->getCurrentFranchisee();
        $feedbacks = Feedbacks::where("active", true)->orderByRaw("rand()")->where('active', true)->limit(5)->get();
        $bouquets = Bouquet::isActive()->isBouquet()->where('hit', true)->with('images')->orderByRaw("rand()")->limit(6)->get();
        $bouquetsNew = Bouquet::isActive()->isBouquet()->where('is_new', true)->with('images')->orderByRaw("rand()")->limit(6)->get();
        $banners = $this->getBanners();

        $this->updateCartInfo();

        return view('site.home_page.index', compact('bouquets', 'feedbacks', 'bouquetsNew', 'franchisee', 'banners'));
    }


    public function updateCartInfo()
    {
        $cart = Session::get('cart', []);

        Session::put('cart_size', (new \App\Http\Controllers\Site\CartController())->get_cart_size($cart));
    }


    public function getBanners()
    {
        return Banner::with('image')->get();
    }

}