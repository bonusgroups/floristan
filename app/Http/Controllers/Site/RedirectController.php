<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RedirectController extends Controller
{

    public function index()
    {
        $url = request()->get('url');

        if ($url) return redirect($url);

        abort(404);
    }

}
