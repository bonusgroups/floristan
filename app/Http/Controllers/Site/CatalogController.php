<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Models\BouquetCategory;
use App\Models\BouquetsFranchiseePrice;
use App\Models\Feedbacks;
use App\Models\Flower;
use App\Models\Franchisee;
use App\PageInfo;
use App\Services\FilterService;
use App\Services\FranchiseeService;
use App\Services\SettingService;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CatalogController
 * @package App\Http\Controllers\Site
 */
class CatalogController extends Controller
{

    public $tokens = null;


    public function __invoke()
    {
        if ($this->isCatalogPage() and $this->isNumericSlug()) {
            return redirect($this->getCatalogDefaultUrl(), 301);
        }

        $category = BouquetCategory::getFromUrl();

        if ($this->isCatalogPage() and !$category) {
            abort(404);
            return null;
        }

        $bouquets = $this->getBouquets($category);

        $franchisee = app(FranchiseeService::class)->getCurrentFranchisee();

        $title = $this->replaceTokens($category->title);
        $metaDescription = $this->replaceTokens($category->description);
        $h1 = $this->replaceTokens($category->h1);
        $text = $this->replaceTokens($category->text);
        $seoTitle = $this->replaceTokens($category->seo_title);

        $emptyPage = (!$bouquets->count() and (int)request()->get('page') > 1);

        $view = view('site.catalog.index', compact(
            'franchisee',
            'category',
            'title',
            'metaDescription',
            'h1',
            'text',
            'seoTitle'
        ))->with('bouquets', $bouquets);

        return response($view, $emptyPage ? 404 : 200);
    }


    public function getBouquets($category)
    {
        $categoryId = $category ? $category->id : null;

        $query = $this->getQuery()
            ->with('images')
            ->groupBy('bouquets.id');

        if ($categoryId) {
            $query->whereHas('categories', function (Builder $belongsToMany) use ($categoryId) {
                $belongsToMany->where('bouquet_categories.id', $categoryId);
            });
        }

        $filter = app(FilterService::class);

        if ($filter->hasReasons()) {
            $query->whereHas('reasons', function ($q) use ($filter) {
                $q->whereIn('bouquet_reason_id', $filter->getReasons());
            });
        }

        if ($filter->hasColors()) {
            $query->whereHas('colors', function ($q) use ($filter) {
                $q->whereIn('bouquet_color_id', $filter->getColors());
            });
        }

        if ($filter->hasFlowers()) {
            $query->whereHas('flowers', function ($q) use ($filter) {
                $q->whereIn('flower_id', $filter->getFlowers());
            });
        }

        $franchisee = app(FranchiseeService::class)->getCurrentFranchisee();

        if ($filter->hasPrice()) {
            $price_mul = SettingService::shared()->get("mul_price", 250);

            $priceQuery = FilterService::getPriceQuery($franchisee->id);

            $query->whereRaw('(' . $priceQuery . ') >= ?', [$filter->currentMinPrice() / ($price_mul / 100)]);
            $query->whereRaw('(' . $priceQuery . ') <= ?', [$filter->currentMaxPrice() / ($price_mul / 100)]);
        }

        if ($category and $category->slug == 'all') {
            $excludeIds = Bouquet::query()
                ->whereHas('categories', function ($q) {
                    $q->where('bouquet_categories.slug', 'insta-shop');
                })
                ->isActive()
                ->pluck('id')
                ->toArray();

            if ($excludeIds) {
                $query->whereNotIn('bouquets.id', $excludeIds);
            }
        }

        $data = request()->only([
            'f:min',
            'f:max',
            'f:reasons',
            'f:flowers',
            'q'
        ]);

        return $query->paginate(60)
            ->setPath($category ? $category->getPageUrl() : 'search')
            ->appends($data);
    }


    public function replaceTokens($text)
    {
        return (new PageInfo())->replaceTokens($text);
    }


    public function isCatalogPage()
    {
        return request()->is('catalog', 'catalog/*');
    }


    public function isNumericSlug()
    {
        return is_numeric(request()->route()->parameter('slug'));
    }


    public function getCatalogDefaultUrl()
    {
        return url('catalog/all');
    }


    public function show(Request $request, Bouquet $bouquet)
    {
        $contains = $bouquet->flowers->reduce(function ($sizes, Flower $item) {
            $sizes[$item->pivot->size][] = $item;
            return $sizes;
        }, []);

        $similarBouquets = $this->getSimilarBouquets($bouquet);
        $feedbacks = $this->getReviews();
        $franchisee = Franchisee::getCurrent();
        $franchiseeName = $franchisee->prepositional_name ?: $franchisee->city_ru;

        return view('site.catalog.show', compact('bouquet', 'contains', 'similarBouquets', 'feedbacks', 'franchisee', 'franchiseeName'));
    }


    public function getQuery()
    {
        $query = Bouquet::query()->isActive();

        $category = (int)optional(BouquetCategory::getFromUrl())->id;

        if ($category) {
            switch ($category) {
                case 1:
                    $query->where('discount', '>', 0);
                    break;

                case 6:
                    $query->isAdditional();
                    break;

                default:
                    $query->isBouquet();
                    break;
            }
        }

        return $query;
    }


    public function getSimilarBouquets(Bouquet $bouquet)
    {
        $categoryIds = $bouquet->categories()->pluck('bouquet_category_id')->toArray();

        if (!$categoryIds) return collect();

        return Bouquet::inRandomOrder()
            ->isActive()
            ->whereHas('categories', function (Builder $q) use ($categoryIds) {
                $q->whereIn('bouquet_category_id', $categoryIds);
            })
            ->limit(4)
            ->get();
    }


    public function getReviews()
    {
        return Feedbacks::query()
            ->where("active", true)
            ->orderByRaw("rand()")
            ->where('active', true)
            ->limit(9)
            ->get();
    }


    public function submitFilters()
    {
        $category = BouquetCategory::find(request('category'));
        $bouquets = $this->getBouquets($category);
        $result = view('site.catalog.products', compact('bouquets'))->render();

        $data = request()->only([
            'f:min',
            'f:max',
            'f:reasons',
            'f:flowers',
            'q'
        ]);

        $url = $category->getPageUrl() . '?' . http_build_query($data);

        return [
            'result' => $result,
            'url' => $url
        ];
    }

}
