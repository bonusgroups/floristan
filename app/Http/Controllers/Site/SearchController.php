<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 18.05.2018
 * Time: 14:44
 */

namespace App\Http\Controllers\Site;


use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Swis\LaravelFulltext\Search;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $query = htmlspecialchars(strip_tags($request->get("q")));

        $search = new Search();
        $query = $query ? ('*' . trim($query, '+') . '*') : null;

        $bouquets = $search->run($query, Bouquet::class)->map(function ($item) {
            return $item->indexable;
        })->filter(function ($item) {
            return $item;
        });

        return view('site.search.results', compact('bouquets', 'query'));
    }
}