<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Models\Franchisee;
use App\Models\Order;
use Carbon\Carbon;

class BuyOneClickController extends Controller
{

    public function popup()
    {
        $id = request('id');
        $bouquet = $id ? Bouquet::find($id) : null;
        $size = request('size') ?: Bouquet::SIZE_MIDDLE;

        $total = $this->addToCart($bouquet, $size);

        return [
            'popup' => view('site.buy_one_click.popup', compact('bouquet', 'size'))->render(),
            'total' => number_format($total, 0, ', ', ' ')
        ];
    }


    public function addToCart($bouquet, $size)
    {
        $cart = $this->getCart();

        $cart[] = [
            'id' => $bouquet->id,
            'bouquet_size' => $size
        ];

        session()->put('cart', $cart);

        $total = (new CartController())->get_cart_size($cart);

        session()->put('cart_size', $total);

        return $total;
    }


    public function submit()
    {
        $phone = trim(request()->get('phone'));

        if (!$phone) {
            throw new \Exception('Введите, пожалуйста, телефон');
        }

        $id = request()->get('id');
        $bouquet = $id ? Bouquet::find($id) : null;

        if (!$bouquet) {
            throw new \Exception('Букет не найден');
        }

        $order = $this->createOrder($bouquet);

        $order->sendAdminNotice();
        $order->sendToAmoCrm();

        return [
            'url' => $order->getSuccessUrl()
        ];
    }


    public function createOrder(Bouquet $bouquet = null)
    {
        $cart = $this->getCart();

        $franchise = Franchisee::getCurrent();

        $checkoutController = new CheckoutController();
        $checkoutController->setCart($cart);
        $checkoutController->setPromocode(null);
        $checkoutController->setCurrentFranchise($franchise);

        $orderTotal = $checkoutController->getOrderTotal();

        $order = new Order();
        $order->franchisee_id = optional($franchise)->id;
        $order->client_phone = request('phone');
        $order->pay_type = Order::PAY_CARD;
        $order->status = 0;

        $order->base_total_value = $checkoutController->getBaseTotal();
        $order->base_total_value_without_discount_bouquets = $checkoutController->getBaseTotalOnlyWithoutDiscountBouquets();
        $order->total_value = $orderTotal;
        $order->partner_value = $checkoutController->getPartnerValue();
        $order->our_value = $orderTotal - $order->partner_value;

        $order->save();

        $order->bouquets()->sync([]);

        foreach ($cart as $item) {
            $count = isset($item['count']) ? (int)$item['count'] : 1;

            for ($i = 1; $i <= $count; $i++) {
                $order->bouquets()->attach($item['id'], [
                    'count' => 1,
                    'bouquet_size' => $item['bouquet_size']
                ]);
            }
        }

        $checkoutController->clearSession();

        return $order;
    }


    public function getCart()
    {
        return session()->get('cart') ?: [];
    }

}
