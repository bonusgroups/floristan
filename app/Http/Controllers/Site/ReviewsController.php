<?php

namespace App\Http\Controllers\Site;

use App\Http\Middleware\CityDetectedMiddleware;
use App\Http\Requests\Site\ReviewRequest;
use App\Models\Feedbacks;
use App\Models\Franchisee;
use App\Services\FranchiseeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ReviewsController
 * @package App\Http\Controllers\Site
 */
class ReviewsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $franchisee = app(FranchiseeService::class)->getCurrentFranchisee();

        $feedbacks = Feedbacks::where("active", true)
            ->orderBy("updated_at", "DESC")->limit(15)
            ->where('active', true)
            ->paginate(5);

        return view('site.reviews.index', compact('feedbacks', 'franchisee'));
    }

    public function send(ReviewRequest $request)
    {
        $name = $request->get("name") ?: '';
        $social = $request->get("social_link") ?: '';
        $review = $request->get("review") ?: '';
        $photo = $request->get("photo") ?: '';
        $avatar = $request->get("avatar") ?: '';

        $feedback = new Feedbacks();
        $feedback->name = $name;
        $feedback->vk_link = $social;
        $feedback->description = $review;
        $feedback->active = false;
        $feedback->city = request('city') ?: '';
        $feedback->phone = request('phone') ?: '';

        if (!empty($photo)) {
            $photo = json_decode($photo);
            if ($photo && isset($photo->output)) {
                $image = $photo->output->image;
                $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
                $photoName = uniqid("photo") . ".jpg";
                \Storage::disk('feedback_images')->put($photoName, $image);
                $feedback->photo = $photoName;
            }
        }

        if (!empty($avatar)) {
            $avatar = json_decode($avatar);

            if ($avatar && isset($avatar->output)) {
                $image = $avatar->output->image;
                $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
                $photoName = uniqid("avatar") . ".jpg";

                \Storage::disk('feedback_images')->put($photoName, $image);

                $feedback->avatar = $photoName;
            }
        }

        $feedback->save();

        return ['success' => true];
    }
}
