<?php

namespace App\Http\Controllers\Site;

use App\Http\Requests\Site\Cart\SendRequest;
use App\Models\Bouquet;
use App\Models\Delivery;
use App\Models\Order;
use App\Models\Promocode;
use App\Services\FranchiseeService;
use App\Services\YaKassaService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

/**
 * Class CartController
 * @package App\Http\Controllers\Site
 */
class CartController extends Controller
{

    public $balls = 0;
    public $cartBouquets = null;
    public $originalPrice = 0;


    public function __invoke()
    {
        $cart = Session::get('cart', []);

        $products = collect();
        $ids = [];

        if (count($cart)) {
            foreach ($cart as $cart_item) {
                $ids[] = $cart_item['id'];
            }

            $products = Bouquet::whereIn('id', $ids)->with('images')->get();
        }

        $related_products = Bouquet::inRandomOrder()->isAdditional()->with('images')->limit(3)->get();

        $payUrl = request()->get('pay_url');

        return view('site.cart.index', compact(
            'products',
            'cart',
            'related_products',
            'payUrl'
        ));
    }

    public function order(Request $request)
    {
        $cart = Session::get('cart', []);

        if (!sizeof($cart)) {
            return redirect(routeWithCity('site.cart'));
        }

        $products = collect();
        $ids = [];

        if (count($cart)) {
            foreach ($cart as $cart_item) {
                $ids[] = $cart_item['id'];
            }

            $products = Bouquet::whereIn('id', $ids)->with('images')->get();
        }

        $related_products = Bouquet::inRandomOrder()->isAdditional()->with('images')->limit(3)->get();
        $franchisee = app(FranchiseeService::class)->getCurrentFranchisee();
        $deliveries = Delivery::where("franchisee_id", $franchisee->id)->get();
        $payments = Order::getCartPaymentTypes();

        $balls = collect($cart)->reduce(function ($balls, $cart_item) use ($products) {
            $bouquet = $products->where('id', $cart_item['id'])->first();
            $balls += $bouquet->ballsFromPrice($cart_item['bouquet_size']);

            return $balls;
        }, 0);

        $timezone = $request->cookie("tzo", 180) / 60 * 100;
        //calculation time and day to franchisee
        $hours = explode("-", $franchisee->opening_hours);
        $fBegin = isset($hours[0]) ? $hours[0] : 0;
        $ofBegin = $fBegin;
        $fEnd = isset($hours[1]) ? $hours[1] : 0;
        $iBegin = (float)preg_replace("/[^0-9]+/mui", "", $fBegin);
        $iEnd = preg_replace("/[^0-9]+/mui", "", $fEnd);
        $iCurrent = date("Hi") + 100 + $timezone;
        $iCurrent /= 100;
        $iH = floor($iCurrent) * 100;
        $iM = $iCurrent - floor($iCurrent);
        $iM = ceil($iM * 100 / 15) * 15;
        if ($iM == 60) {
            $iM = 0;
            $iH += 100;
        }
        $iCurrent = $iH + $iM;

        $startDay = 0;
        $dDay = Carbon::now();
        if ($iCurrent >= $iEnd) {
            $startDay += 1;
            $dDay->add(\DateInterval::createFromDateString("1 day"));

            $iCurrent = $iBegin;
        } else {
            if ($iCurrent > $iBegin) {
                $iBegin = $iCurrent;
            }
        }

        $fBegin = floor($iBegin / 100) . ":" . mb_substr($iBegin, -2, 2);
        $fEnd = floor($iEnd / 100) . ":" . mb_substr($iEnd, -2, 2);
        $dDay = $dDay->format("d.m.Y");

        $promocodeText = $this->getPromocodeText($this->getPromocodeFromRequest($request));

        return view('site.cart.order', compact(
            'products',
            'cart',
            'related_products',
            'deliveries',
            'payments',
            'balls',
            'fBegin',
            'fEnd',
            'startDay',
            'dDay',
            'ofBegin',
            'promocodeText'
        ));
    }

    public function orderSend(SendRequest $request)
    {
        $promocode = $this->getPromocodeFromRequest($request);

        if ($request->get('promocode') and !$promocode) {
            return $this->order($request)->withErrors([
                'promocode' => 'Код купона неверный'
            ]);
        }

        $franchisee = app(FranchiseeService::class)->getCurrentFranchisee();
        $cart = session('cart') ?: [];
        $orderTotal = $this->getOrderTotal();

        \DB::beginTransaction();

        $order = new Order();
        $order->franchisee_id = $franchisee->id;
        $order->postcard_text = $request->get("postcard_text");
        $order->delivery_id = $request->get("delivery");
        $order->pay_type = $request->get("pay_type");
        $order->status = 0;
        $order->points_value = $request->get('use_balls', 0);
        $order->client_comment = $request->get("client_comment", '');
        $order->client_name = $request->get("client_name", '');
        $order->client_phone = $request->get("client_phone", '');
        $order->recipient_name = $request->get("recipient_name", '');
        $order->recipient_phone = $request->get("recipient_phone", '');
        $order->recipient_address = $request->get("recipient_address", '');
        $order->need_to_call = $request->get("need_to_call", 0);
        $order->delivery_date = date("Y-m-d", strtotime($request->get("delivery_date", date("d.m.Y"))));
        $order->delivery_time = $request->get("delivery_time", "10:00");
        $order->promocode_id = optional($promocode)->id;

        $order->base_total_value = $this->getBaseTotal();
        $order->base_total_value_without_discount_bouquets = $this->getBaseTotalOnlyWithoutDiscountBouquets();
        $order->total_value = $orderTotal;
        $order->partner_value = $this->getPartnerValue();
        $order->our_value = $orderTotal - $order->partner_value;

        $order->save();

        if ($promocode) $promocode->useNow();

        $order->bouquets()->sync([]);

        foreach ($cart as $item) {
            $order->bouquets()->attach($item['id'], [
                'count' => 1,
                'bouquet_size' => $item['bouquet_size']
            ]);
        }

        \DB::commit();

        Session::put('cart', []);
        Session::put('cart_size', 0);
        Session::put('ordernumber', $order->id);

        if (intval($order->pay_type) == Order::PAY_ONLINE) {
            return redirect('cart?pay_url=' . $this->getYaKassaUrl($order));
        }

        $order->sendFranchiseeNotice();

        return redirect(routeWithCity('site.cart.success'));
    }


    public function getCartBouquets()
    {
        if ($this->cartBouquets) return $this->cartBouquets;

        $cart = session('cart') ?: [];
        $ids = [];

        if (count($cart)) {
            foreach ($cart as $cart_item) {
                $ids[] = $cart_item['id'];
            }
        }

        $bouquets = $ids ? Bouquet::whereIn('id', $ids)->with('images')->get() : collect();

        return $this->cartBouquets = $bouquets;
    }


    public function getOriginalPrice()
    {
        if ($this->originalPrice) return $this->originalPrice;

        $cart = session('cart') ?: [];
        $bouquets = $this->getCartBouquets();

        $price = collect($cart)->reduce(function ($price, $cart_item) use ($bouquets) {
            $bouquet = $bouquets->where('id', $cart_item['id'])->first();
            $price = intval($price) + $bouquet->price($cart_item['bouquet_size'], null, false);

            return $price;
        }, 0);

        return $this->originalPrice = $price;
    }


    public function getPartnerValue()
    {
        $total = 0;
        $originalPrice = $this->getOriginalPrice();
        $franchiseeCommission = $this->getFranchiseeCommission();

        foreach ($this->getCartBouquets() as $bouquet) {
            $mul = $this->getPriceMul($bouquet);

            $total += ($originalPrice + ($originalPrice * $mul)) * ($franchiseeCommission / 100);
        }

        return $total;
    }


    public function getFranchiseeCommission()
    {
        $franchisee = app(FranchiseeService::class)->getCurrentFranchisee();

        return intval($franchisee->commission_for_bouquets);
    }


    public function getPriceMul(Bouquet $bouquet = null)
    {
        if ($bouquet and $bouquet->isAdditional()) {
            return \App\Services\SettingService::shared()->get("mul_price_additional", 250) / 100;
        }

        return \App\Services\SettingService::shared()->get("mul_price", 250) / 100;
    }


    public function getYaKassaUrl(Order $order)
    {
        $yaKassa = new YaKassaService();
        $yaKassa->setOrder($order);

        return $yaKassa->getUrl();
    }


    public function success()
    {
        $orderNumber = session('ordernumber') ?: request()->get('order_id');
        $order = Order::find($orderNumber);

        if (is_null($order)) {
            return redirect(routeWithCity('site.cart'));
        }

        $paymentForm = null;

        return view('site.cart.success', compact('orderNumber', 'order', 'paymentForm'));
    }

    public function cart_add(Request $request)
    {
        $result = array();
        $id = $request->input('id');
        $bouquet = Bouquet::find($id);

        if (is_null($bouquet)) {
            return null;
        }

        $bouquet_size = $request->has('bouquet_size') ? $request->input('bouquet_size') : $bouquet->getFirstAvailbleSize();
        $cart = Session::get('cart', []);

        $cart_count = str_random(4);

        Session::put('cart.' . $cart_count . '.id', $id);
        Session::put('cart.' . $cart_count . '.bouquet_size', $bouquet_size);

        $cart = Session::get('cart', []);

        Session::put('cart_size', $this->get_cart_size($cart));

        $result['id'] = $id;
        $result['cart_size'] = $this->getOrderTotalWithFormat();
        $result['balls_count'] = $this->balls;
        $result['popup'] = view('site.partials.product-added-to-cart', compact('bouquet'))->render();

        return $result;
    }


    public function cart_del(Request $request)
    {
        $result = array();
        $id = $request->input('id');

        Session::pull('cart.' . $id);

        $cart = Session::get('cart', []);

        Session::put('cart_size', $this->get_cart_size($cart));

        $result['id'] = $id;
        $result['cart_size'] = $this->getOrderTotalWithFormat();
        $result['balls_count'] = $this->balls;

        return $result;
    }

    public function get_cart_size($cart)
    {
        $cart_size = 0;
        $balls = 0;

        if (count($cart)) {
            foreach ($cart as $cart_item) {
                $id = (int)$cart_item['id'];
                $bouquet_size = $cart_item['bouquet_size'];

                $bouquet = Bouquet::find($id);

                if ($bouquet) {
                    $cart_size += $bouquet->priceWithDiscount($bouquet_size);
                    $balls += $bouquet->ballsFromPrice($cart_item['bouquet_size']);
                }
            }
        }

        $this->balls = $balls;

        return $cart_size;
    }


    public function deliveryChange()
    {
        return [
            'orderTotal' => $this->getOrderTotalWithFormat()
        ];
    }


    public function getBaseTotal($options = [])
    {
        $options += [
            'onlyWithoutDiscountBouquets' => false
        ];

        $cart = collect(session('cart') ?: []);
        $ids = $cart->pluck('id');
        $bouquets = $ids ? Bouquet::find($ids) : collect();

        if ($options['onlyWithoutDiscountBouquets']) {
            $bouquets = $bouquets->filter(function (Bouquet $bouquet) {
                if ($bouquet->isAdditional()) return false;
                if ($bouquet->discount) return false;

                return true;
            });
        }

        $total = 0;

        foreach ($cart as $item) {
            $bouquet = $bouquets->where('id', $item['id'])->first();

            if (!$bouquet) continue;

            $total += $bouquet->priceWithDiscount($item['bouquet_size']);
        }

        return $total;
    }


    public function getBaseTotalOnlyWithoutDiscountBouquets()
    {
        return $this->getBaseTotal(['onlyWithoutDiscountBouquets' => true]);
    }


    public function getOrderTotal()
    {
        $total = $this->getBaseTotal();

        $total += $this->getDeliveryPrice(request()->get('delivery_id'));
        $total -= $this->getPromocodePriceFromRequest(request());

        if ($total < 0) return 0;

        return $total;
    }


    public function getOrderTotalWithFormat()
    {
        return number_format($this->getOrderTotal(), 0, ', ', ' ');
    }


    public function getDeliveryPrice($deliveryId)
    {
        $delivery = Delivery::find($deliveryId);

        return $delivery ? intval($delivery->price) : 0;
    }


    public function promocodeChange()
    {
        $code = trim(request()->get('promocode'));
        $promocode = $this->getPromocodeFromRequest(request());
        $error = null;

        if ($code and !$promocode) {
            $error = 'Код купона неверный';
        }

        return [
            'error' => $error,
            'promocodeText' => $this->getPromocodeText($promocode),
            'orderTotal' => $this->getOrderTotalWithFormat()
        ];
    }


    public function getPromocodeText(Promocode $promocode = null)
    {
        if (!$promocode) return null;

        return 'Сумма скидки по купону: ' . $promocode->getDiscountPrice($this->getBaseTotalOnlyWithoutDiscountBouquets()) . ' Р';
    }


    public function getPromocodeFromRequest($request)
    {
        $code = trim($request->get('promocode'));

        if (!$code) return null;

        $promocode = Promocode::findByCode($code);

        if (!$promocode) return null;

        if (!$promocode->isAvailable()) return null;

        return $promocode;
    }


    public function getPromocodePriceFromRequest($request)
    {
        $promocode = $this->getPromocodeFromRequest($request);

        return $promocode ? $promocode->getDiscountPrice($this->getBaseTotalOnlyWithoutDiscountBouquets()) : 0;
    }

}
