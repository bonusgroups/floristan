<?php

namespace App\Http\Controllers\Site;

use App\Models\Order;
use App\Services\YaKassaService;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderStatusController extends Controller
{

    public function index()
    {
        return view('site.order-status.index');
    }


    public function submit()
    {
        try {
            $this->validateForm();
        } catch (ValidationException $exception) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors([
                    'number' => $exception->getMessage()
                ]);
        }

        $order = Order::find((int)request('number'));

        switch (request('action')) {
            case 'status':
                $status = $order->getStatusText();

                session()->flash('message', "Статус заказа: {$status}");

                return redirect()->back()->withInput();
                break;

            case 'pay':
                $yaKassa = new YaKassaService();
                $yaKassa->setOrder($order);

                return redirect($yaKassa->getUrl());
                break;
        }
    }


    public function validateForm()
    {
        $id = (int)request('number');

        if (!$id) {
            throw new ValidationException('Введите номер заказа');
        }

        $order = Order::find($id);

        if (!$order) {
            throw new ValidationException('Заказ с данным номером не найден');
        }

        switch (request('action')) {
            case 'pay':
                if (in_array($order->status, [
                    Order::STATUS_CANCEL,
                    Order::STATUS_COMPLETED
                ])) {
                    throw new ValidationException('Заказ со статусом "' . $order->getStatusText() . '" нельзя оплатить');
                }
                break;
        }
    }


}
