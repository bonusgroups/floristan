<?php

namespace App\Http\Controllers\Site;

use App\Services\FranchiseeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class DeliveryController
 * @package App\Http\Controllers\Site
 */
class DeliveryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        $franchisee = app(FranchiseeService::class)->getCurrentFranchisee();

        return view('site.delivery.index', compact('franchisee'));
    }
}
