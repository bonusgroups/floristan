<?php

namespace App\Http\Controllers\Site;

use App\Services\FranchiseeService;
use App\Services\SettingService;
use Exception;
use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use Validator;

/**
 * Class AboutController
 * @package App\Http\Controllers\Site
 */
class CompetitionController extends Controller
{

    public function index()
    {
        return view('site.competition.index');
    }


    public function submit()
    {
        $rules = [
            'phone' => ['required']
        ];

        $messages = [
            'phone.required' => 'Введите телефон, пожалуйста'
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);

        if ($validator->fails()) return redirect()->back()->withErrors($validator);

//        $vars = request()->all();
//
//        $email = (env('APP_MODE') == 'local') ? 'marat.icanall@ya.ru' : SettingService::shared()->get('email');
//
//        \Mail::send('site.mail.competition', $vars, function ($message) use ($email) {
//            $message
//                ->to($email)
//                ->subject('Флористан. Заявка из формы на конкурс');
//        });

        $this->sendToGoogleSheet();

        return redirect(url('competition/completed'));
    }


    public function rules()
    {
        return view('site.competition.rules')->render();
    }


    public function completed()
    {
        return view('site.competition.index', [
            'completed' => true
        ]);
    }


    public function sendToGoogleSheet()
    {
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);

        $spreadsheetId = '1vwz_iKqI3ffMH4rwjBgCzrhxZEf5D4DEgILomN4-FJ8';

        $count = count($service->spreadsheets_values->get($spreadsheetId, 'A:A')->getValues());
        $rowNumber = $count + 1;

        $values = [
            [
                date('d.m.Y'),
                request('name') ?: '',
                preg_replace("/[^0-9]/", '', request('phone'))  ?: '',
                request('holiday_1_name') ?: '',
                request('holiday_1_date') ?: '',
                request('holiday_2_name') ?: '',
                request('holiday_2_date') ?: '',
                request('holiday_3_name') ?: '',
                request('holiday_3_date') ?: ''
            ]
        ];

        $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

        $service->spreadsheets_values->update($spreadsheetId, 'A' . $rowNumber, $body, ['valueInputOption' => 'USER_ENTERED']);
    }


    public function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Google Sheets API PHP Quickstart');
        $client->setScopes(Google_Service_Sheets::SPREADSHEETS);

        $client->setAuthConfig(Storage::path('credentials.json'));
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        $tokenPath = Storage::path('token.json');

        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

}
