<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class RobotsController extends Controller
{

    public function index()
    {
        return response()
            ->view('site.robots.index')
            ->header('Content-Type', 'text/plain');
    }

}
