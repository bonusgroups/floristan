<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\BouquetCategory;
use App\Services\FranchiseeService;

class SiteMapController extends Controller
{
    public function index()
    {
        $franchisee = (new FranchiseeService())->getCurrentFranchisee();
        $content = \File::get('storage/sitemap/' . $franchisee->domain . '.xml');

        return response()->make($content)->header('Content-Type', 'text/xml;charset=utf-8');
    }


    public function page()
    {
        $categories = BouquetCategory::query()->published()->get();

        return view('site.sitemap.page', compact(
            'categories'
        ));
    }

}