<?php

namespace App\Http\Controllers\Site;

use App\Services\FranchiseeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ContactsController
 * @package App\Http\Controllers\Site
 */
class ContactsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        $franchisee = app(FranchiseeService::class)->getCurrentfranchisee();

        return view('site.contacts.index',compact('franchisee'));
    }
}
