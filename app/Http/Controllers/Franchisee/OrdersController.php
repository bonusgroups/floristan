<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 17.05.2018
 * Time: 12:17
 */

namespace App\Http\Controllers\Franchisee;


use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Models\Delivery;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class OrdersController extends Controller
{
    public function index(Request $request)
    {
        $franchisee = auth('franchisee')->user();

        $q = Order::where("franchisee_id", $franchisee->id);
        $q->where("status", $request->get('status', Order::STATUS_PARTNER));

        if ($request->get('date_start', null) || $request->get('date_end', null)) {
            $date_start = $request->get('date_start') ? $request->get('date_start') : Carbon::now()->subDays(10);
            $date_end = $request->get('date_end') ? $request->get('date_end') : Carbon::now();
            $q->whereBetween('delivery_date', array($date_start, $date_end));
        }

        $orders = $q->paginate(5);

        $filter = (object)[
            "status" => $request->get("status"),
            "date_start" => $request->get("date_start"),
            "date_end" => $request->get("date_end")
        ];

        $statuses = Order::getStatuses();

        unset($statuses[Order::STATUS_NEW]);

        return view('franchisee.orders.index', compact('orders', 'filter', 'statuses'));
    }


    public function getAllowedStatuses(Order $order)
    {
        return array_unique([Order::STATUS_COMPLETED, $order->status]);
    }


    public function getAllowedStatusNames(Order $order)
    {
        $statusOptions = Order::getStatuses();
        $items = [];

        foreach ($this->getAllowedStatuses($order) as $id) {
            $name = isset($statusOptions[$id]) ? $statusOptions[$id] : null;

            if (!$name) continue;

            $items[] = $name;
        }

        return $items;
    }


    public function update(Request $request, Order $order)
    {
        $franchisee = auth('franchisee')->user();

        if (is_null($order)) {
            return ['error' => '404'];
        }

        if ($franchisee->id !== $order->franchisee_id) {
            return ['error' => '403'];
        }

        if (!in_array($request->get('status'), $this->getAllowedStatuses($order))) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors([
                    'status' => 'Можно выбрать только статусы: ' . implode(', ', $this->getAllowedStatusNames($order))
                ]);
        }

        \DB::beginTransaction();

        $order->status = $request->get("status");
        $order->save();

        //check status is payment & check if log presented
        if ($order->status == Order::STATUS_COMPLETED && !$order->log) {
            //add log
            $order->makeLog();
        }

        \DB::commit();

        if ($request->ajax()) {
            return ['error' => null];
        } else {
            return redirect(route('franchisee.orders.index'));
        }
    }

    public function show(Request $request, Order $order)
    {
        $bouquets = Bouquet::all();
        $bouquets_size = Bouquet::getSizesArray();
        $deliveries = array_merge(["" => "Самовывоз"], Delivery::where("franchisee_id", auth('franchisee')->user()->id)->get()->pluck("name_ru", "id")->toArray());
        return view('franchisee.orders.show', compact('bouquets', 'bouquets_size', 'order', 'deliveries'));
    }
}