<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 28.05.2018
 * Time: 11:56
 */

namespace App\Http\Controllers\Franchisee;


use App\Http\Controllers\Controller;

class FinanceController extends Controller
{
    public function index() {
        $franchisee = auth('franchisee')->user();
        $logs = $franchisee->logs()->where("type","out")->orderBy("id","desc")->paginate(10);
        return view('franchisee.finance.list',compact('franchisee','logs'));
    }
}