<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 17.05.2018
 * Time: 13:46
 */

namespace App\Http\Controllers\Franchisee;


use App\Http\Controllers\Controller;
use App\Models\AdditionalProduct;
use App\Models\Flower;
use Illuminate\Http\Request;

class AdditionalProductsController extends Controller
{
    public function index() {
        $flowers = Flower::select(['id','name_ru'])
            ->with(['prices'=>function($q) {
                $q->where("franchisee_id",auth('franchisee')->user()->id);
            }])
            ->where('is_additional', 1)
            ->get()
            ->map(function(Flower $item) {
                $item->franchise_price = $item->prices->count()?$item->prices->first()->price:'';
                return $item;
            });

        return view('franchisee.products.index',compact('flowers'));
    }

    public function store(Request $request) {
        $prices = $request->get("prices");

        foreach($prices as $flower_id=>$price) {
            $flower = Flower::find($flower_id);
            $mPrice = $flower->prices()->where("franchisee_id",auth('franchisee')->user()->id)->first();

            if(empty($price) && !is_null($mPrice)) {
                $mPrice->delete();
            } elseif(!empty($price)) {
                //update price if it not equals
                if(is_null($mPrice)) {
                    $flower->prices()->create([
                        'franchisee_id'=>auth('franchisee')->user()->id,
                        'price' => $price
                    ]);
                }else{
                    $mPrice->update(['price'=>$price]);
                }
            }
        }

        return redirect(route('franchisee.products.index'));
    }
}