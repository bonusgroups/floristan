<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Models\Promocode;

class ApiCheckingPromoController extends Controller
{

    public $data = null;
    public $promocode = null;
    public $total = null;


    public function index()
    {
        $data = $this->getResult();

        return response()->json($data, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }


    public function getResult()
    {
        try {
            $this->customValidate();
        } catch (\Exception $exception) {
            return [
                'success' => false,
                'error' => $exception->getMessage()
            ];
        }

        return [
            'success' => true,
            'price' => $this->getPromocode()->getDiscountPrice($this->getTotal())
        ];
    }


    public function customValidate()
    {
        $code = $this->getCode();

        if (!$code) {
            throw new \Exception('Код купона пустой');
        }

        $promocode = $this->getPromocode();

        if (!$promocode or !$promocode->isAvailable()) {
            throw new \Exception('Код купона неверный');
        }
    }


    public function getTotal()
    {
        if ($this->total !== null) return $this->total;

        $total = 0;
        $hasDiscountIds = $this->getHasDiscountBouquetIds();
        $additionalIds = $this->getAdditionalBouquetIds();
        $items = $this->getDataValue('OrderList') ?: [];

        foreach ($items as $item) {
            if (in_array($item['id'], $hasDiscountIds)) continue;
            if (in_array($item['id'], $additionalIds)) continue;

            $total += (float)$item['fullprice'];
        }

        $total = (int)$total;

        $this->setTotal($total);

        return $total;
    }


    public function getBouquetIds()
    {
        $ids = [];
        $items = $this->getDataValue('OrderList') ?: [];

        foreach ($items as $item) {
            $ids[] = $item['id'];
        }

        return $ids;
    }


    public function getHasDiscountBouquetIds()
    {
        return Bouquet::query()
            ->whereIn('id', $this->getBouquetIds())
            ->where('discount', '>', 0)
            ->pluck('id')
            ->toArray();
    }


    public function getAdditionalBouquetIds()
    {
        return Bouquet::query()
            ->whereIn('id', $this->getBouquetIds())
            ->isAdditional()
            ->pluck('id')
            ->toArray();
    }


    public function setTotal($total)
    {
        return $this->total = $total;
    }


    public function getCode()
    {
        return $this->getDataValue('PromoCode');
    }


    public function getPromocode()
    {
        if ($this->promocode !== null) return $this->promocode;

        $promocode = Promocode::findByCode($this->getCode()) ?: false;

        $this->setPromocode($promocode);

        return $promocode;
    }


    public function setPromocode($promocode)
    {
        $this->promocode = $promocode;
    }


    public function getDataValue($key, $default = null)
    {
        $data = $this->getData();

        return data_get($data, $key) ?: $default;
    }


    public function getData()
    {
        if ($this->data !== null) return $this->data;

        $content = request()->getContent();

        $data = json_decode($content, true);

        $this->setData($data);

        return $data;
    }


    public function setData($data)
    {
        $this->data = $data;
    }

}
