<?php namespace App\Http\Controllers\Api;

use App\Models\Bouquet;
use App\Http\Controllers\Controller;
use App\Models\BouquetCategory;
use App\Models\BouquetColor;
use App\Models\BouquetReason;
use App\Models\Flower;
use App\Models\Franchisee;
use App\Services\FilterService;
use App\Services\FranchiseeService;

class ApiGetFiltersController extends Controller
{

    public $franchiseId = null;
    public $franchise = null;
    public $category = null;


    public function getFranchisee()
    {
        if ($this->franchise) return $this->franchise;

        $id = $this->getFranchiseId();
        $franchisee = $id ? Franchisee::find($id) : null;

        return $this->franchise = $franchisee;
    }


    public function getFranchiseId()
    {
        if ($this->franchiseId) return $this->franchiseId;

        $id = request()->input('franchise_id') ?: optional((new FranchiseeService)->getCurrentFranchisee())->id ?: Franchisee::getDefaultFranchiseeId();

        return $this->franchiseId = $id;
    }


    public function getCategoryId()
    {
        if ($this->category !== null) return $this->category;

        $id = (int)request()->get('category') ?: optional(BouquetCategory::getFromUrl())->id ?: false;

        return $this->category = $id;
    }


    public function index()
    {
        $data = $this->getData();

        return response()->json($data, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }


    public function getData()
    {
        $filterService = new FilterService($this->getFranchisee());

        return [
            'success' => true,
            'franchise_id' => $this->getFranchiseId(),
            'category' => $this->getCategoryId(),
            'prices' => [
                'min' => $filterService->getFiltersMinPrice($this->getFranchiseId(), $this->getCategoryId()),
                'max' => $filterService->getFiltersMaxPrice($this->getFranchiseId(), $this->getCategoryId())
            ],
            'categories' => BouquetCategory::all()->map(function (BouquetCategory $category) {
                return [
                    'id' => $category->id,
                    'name' => field($category, 'name')
                ];
            }),
            'reasons' => BouquetReason::all()->map(function (BouquetReason $reason) {
                return [
                    'id' => $reason->id,
                    'name' => field($reason, 'name')
                ];
            }),
            'colors' => BouquetColor::query()
                ->whereHas('bouquets', function ($q) {
                })
                ->get()
                ->map(function (BouquetColor $color) {
                    return [
                        'id' => $color->id,
                        'hex' => $color->hex_value
                    ];
                }),
            'flowers' => Flower::query()
                ->where('show_in_filters', 1)
                ->get()
                ->map(function (Flower $flower) {
                    return [
                        'id' => $flower->id,
                        'name' => field($flower, 'name')
                    ];
                })
        ];
    }

}
