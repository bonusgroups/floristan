<?php namespace App\Http\Controllers\Api;

use App\Models\Bouquet;
use App\Http\Controllers\Controller;
use App\Models\Franchisee;
use App\Services\FilterService;
use App\Services\FranchiseeService;
use App\Services\SettingService;
use Illuminate\Database\Eloquent\Builder;

class ApiBouquetsController extends Controller
{

    public $franchisee = null;


    public function getFranchisee()
    {
        if ($this->franchisee) return $this->franchisee;

        $id = request()->input('franchise_id');
        $franchisee = $id ? Franchisee::find($id) : null;
        $franchisee = $franchisee ?: (new FranchiseeService)->getCurrentFranchisee() ?: Franchisee::getDefaultFranchisee();

        return $this->franchisee = $franchisee;
    }


    public function getFranchiseeId()
    {
        return $this->getFranchisee()->id;
    }


    public function index($page = 1)
    {
        $data = $this->getData($page);

        return response()->json($data, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }


    public function getData($page)
    {
        $page = intval($page);
        $totalCount = $this->getQuery()->count();
        $perPage = $this->getPerPage();

        return [
            'success' => true,
            'pagination' => [
                'page' => $page,
                'per_page' => $perPage,
                'total_count' => $totalCount,
                'total_pages_count' => ceil($totalCount / $perPage)
            ],
            'bouquets' => $this->getBouquets($page),
            'error' => null
        ];
    }


    public function getPerPage()
    {
        return 10;
    }


    public function getQuery()
    {
        $query = Bouquet::query()->where('is_active', true);

        if ($category = request()->get('category')) {
            switch ($category) {
                case 1:
                    $query->isBouquet();
                    $query->where('discount', '>', 0);
                    break;

                case 6:
                    $query->isAdditional();
                    break;

                default:
                    $query->isBouquet();
                    $query->whereHas('categories', function (Builder $q) use ($category) {
                        $q->where('bouquet_categories.id', $category);
                    });
                    break;
            }
        }

        if ($value = request()->get('reasons') ?: []) {
            $query->whereHas('reasons', function (Builder $q) use ($value) {
                $q->whereIn('bouquet_reason_id', $value);
            });
        }

        if ($value = request()->get('colors') ?: []) {
            $query->whereHas('colors', function (Builder $q) use ($value) {
                $q->whereIn('bouquet_color_id', $value);
            });
        }

        if ($value = request()->get('flowers') ?: []) {
            $query->whereHas('flowers', function (Builder $q) use ($value) {
                $q->whereIn('flower_id', $value);
            });
        }

        $priceMin = (int)request()->get('min');
        $priceMax = (int)request()->get('max');

        if ($priceMin or $priceMax) {
            $franchisee = $this->getFranchisee();
            $price_mul = SettingService::shared()->get("mul_price", 250);
            $priceQuery = FilterService::getPriceQuery($franchisee->id);

            if ($priceMin) {
                $query->whereRaw('(' . $priceQuery . ') >= ?', [$priceMin / ($price_mul / 100)]);
            }

            if ($priceMax) {
                $query->whereRaw('(' . $priceQuery . ') <= ?', [$priceMax / ($price_mul / 100)]);
            }
        }

        return $query;
    }


    public function getBouquets($page)
    {
        $perPage = $this->getPerPage();
        $skip = $page * $perPage - $perPage;
        $size = Bouquet::SIZE_MIDDLE;

        return $this->getQuery()
            ->with('images')
            ->skip($skip)
            ->limit($perPage)
            ->get()
            ->map(function (Bouquet $item) use ($size) {
                $image = $item->images->first();
                $fileName = $image ? url('storage/bouquets_images', $image->file_name) : null;
                $oldPrice = $size ? $item->price($size, $this->getFranchiseeId()) : null;
                $price = $size ? $item->priceWithDiscount($size, $this->getFranchiseeId()) : null;

                return (object)[
                    'id' => $item->id,
                    'name' => $item->name_ru,
                    'price' => $price,
                    'old_price' => $oldPrice != $price ? $oldPrice : null,
                    'tag_discount' => (bool)$item->discount,
                    'tag_new' => (bool)$item->is_new,
                    'tag_hit' => (bool)$item->hitIndex,
                    'image' => $fileName
                ];
            });
    }

}
