<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Franchisee;

class ApiFranchisesController extends Controller
{

    public function index()
    {
        $data = $this->getData();

        return response()->json($data, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }


    public function getData()
    {
        return [
            'cities' => Franchisee::all()->map(function (Franchisee $franchisee) {
                return [
                    'id' => $franchisee->id,
                    'name' => field($franchisee, 'city')
                ];
            })
        ];
    }

}
