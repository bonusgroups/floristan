<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Site\CheckoutController;
use App\Models\Bouquet;
use App\Http\Controllers\Controller;
use App\Models\Franchisee;
use App\Models\Order;
use App\Models\Promocode;
use App\Services\FranchiseeService;
use App\Services\YaKassaService;
use Carbon\Carbon;

class ApiOrderController extends Controller
{

    public $data = null;
    public $promocode = null;
    public $total = null;


    public function payUrl($id)
    {
        $order = Order::find($id);

        $yaKassa = new YaKassaService();
        $yaKassa->setOrder($order);

        $data = [
            'url' => $yaKassa->getUrl()
        ];

        return response()->json($data, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }


    public function create()
    {
        $data = $this->getCreateResult();

        return response()->json($data, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }


    public function getCreateResult()
    {
        try {
            $this->createValidate();
        } catch (\Exception $exception) {
            return [
                'success' => false,
                'error' => $exception->getMessage()
            ];
        }

        $order = $this->createOrder();

        return [
            'success' => true,
            'order_id' => $order->id
        ];
    }


    public function createValidate()
    {
        $code = $this->getCode();
        $promocode = $this->getPromocode();

        if ($code) {
            if (!$promocode or !$promocode->isAvailable()) {
                throw new \Exception('Код купона неверный');
            }
        }

        if (!$this->getDataValue('City_id')) {
            throw new \Exception('City_id пуст');
        }
    }


    public function createOrder()
    {
        $cart = $this->getCart();
        $promocode = $this->getPromocode();
        $franchise = $this->getFranchisee();

        $checkoutController = new CheckoutController();
        $checkoutController->setCart($cart);
        $checkoutController->setPromocode($promocode);
        $checkoutController->setCurrentFranchise($franchise);

        $orderTotal = $checkoutController->getOrderTotal();

        $order = new Order();
        $order->franchisee_id = optional($franchise)->id;
        $order->postcard_text = $this->getDataValue('PostcardState') ? $this->getDataValue('PostcardText') : null;
        $order->delivery_id = null;
        $order->pay_type = Order::PAY_CARD;
        $order->status = 0;
        $order->points_value = 0;
        $order->client_comment = null;
        $order->client_name = $this->getDataValue('OwnerName');
        $order->client_phone = $this->getDataValue('OwnerPhone');
        $order->recipient_name = $this->getDataValue('DeliveryName');
        $order->recipient_phone = $this->getDataValue('DeliveryPhone');
        $order->recipient_address = $this->getDataValue('AdressState') ? $this->getDataValue('AdressText') : null;
        $order->need_to_call = (int)!$this->getDataValue('AdressState');
        $order->delivery_date = $this->getDataValue('Date') ? Carbon::parse($this->getDataValue('Date')) : null;
        $order->delivery_time = $this->getDataValue('Time') ? $this->getDataValue('Time') : null;
        $order->promocode_id = optional($promocode)->id;

        $order->base_total_value = $checkoutController->getBaseTotal();
        $order->base_total_value_without_discount_bouquets = $checkoutController->getBaseTotalOnlyWithoutDiscountBouquets();
        $order->total_value = $orderTotal;
        $order->partner_value = $checkoutController->getPartnerValue();
        $order->our_value = $orderTotal - $order->partner_value;

        $order->save();

        if ($promocode) $promocode->useNow();

        $order->bouquets()->sync([]);

        foreach ($cart as $item) {
            $count = (int)$item['count'] ?: 1;

            for ($i = 1; $i <= $count; $i++) {
                $order->bouquets()->attach($item['id'], [
                    'count' => 1,
                    'bouquet_size' => $item['bouquet_size']
                ]);
            }
        }

        return $order;
    }


    public function getCart()
    {
        return collect($this->getDataValue('OrderList') ?: [])
            ->map(function ($value) {
                switch ($value['size']) {
                    case 'маленький':
                        $value['bouquet_size'] = Bouquet::SIZE_SMALL;
                        break;

                    case 'средний':
                        $value['bouquet_size'] = Bouquet::SIZE_MIDDLE;
                        break;

                    case 'большой':
                        $value['bouquet_size'] = Bouquet::SIZE_BIG;
                        break;
                }

                return $value;
            })
            ->toArray();
    }


    public function getFranchisee()
    {
        return Franchisee::find($this->getDataValue('City_id')) ?: Franchisee::getDefaultFranchisee();
    }


    public function getBaseTotalForPromocode()
    {
        if ($this->total !== null) return $this->total;

        $total = 0;
        $hasDiscountIds = $this->getHasDiscountBouquetIds();
        $items = $this->getDataValue('OrderList') ?: [];

        foreach ($items as $item) {
            if (in_array($item['id'], $hasDiscountIds)) continue;

            $total += (float)$item['fullprice'];
        }

        $total = (int)$total;

        $this->setTotal($total);

        return $total;
    }


    public function getBouquetIds()
    {
        $ids = [];
        $items = $this->getDataValue('OrderList') ?: [];

        foreach ($items as $item) {
            $ids[] = $item['id'];
        }

        return $ids;
    }


    public function getHasDiscountBouquetIds()
    {
        return Bouquet::query()
            ->whereIn('id', $this->getBouquetIds())
            ->where('discount', '>', 0)
            ->pluck('id')
            ->toArray();
    }


    public function getAdditionalBouquetIds()
    {
        return Bouquet::query()
            ->whereIn('id', $this->getBouquetIds())
            ->isAdditional()
            ->pluck('id')
            ->toArray();
    }


    public function setTotal($total)
    {
        return $this->total = $total;
    }


    public function getCode()
    {
        return $this->getDataValue('PromoCode');
    }


    public function getPromocode()
    {
        if ($this->promocode !== null) return $this->promocode;

        $code = $this->getCode();

        $promocode = ($code ? Promocode::findByCode($code) : null) ?: false;

        $this->setPromocode($promocode);

        return $promocode;
    }


    public function setPromocode($promocode)
    {
        $this->promocode = $promocode;
    }


    public function getDataValue($key, $default = null)
    {
        $data = $this->getData();

        return data_get($data, $key) ?: $default;
    }


    public function getData()
    {
        if ($this->data !== null) return $this->data;

        $content = request()->getContent();

        $data = json_decode($content, true);

        $this->setData($data);

        return $data;
    }


    public function setData($data)
    {
        $this->data = $data;
    }

}
