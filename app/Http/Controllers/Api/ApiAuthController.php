<?php namespace App\Http\Controllers\Api;

use App\ApiToken;
use App\Http\Controllers\Controller;
use App\Services\SmsService;
use App\SmsCode;

class ApiAuthController extends Controller
{

    public function getPhone()
    {
        return request()->get('phone');
    }


    public function getSms()
    {
        return request()->get('code');
    }


    public function getToken()
    {
        return request()->get('token');
    }


    public function index()
    {
        $data = $this->getData();

        return response()->json($data, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }


    public function getData()
    {
        switch ($this->getRequestType()) {
            case 'send-sms':
                return [
                    'success' => $this->sendCode()
                ];
                break;

            case 'check-sms':
                $smsCode = $this->findSmsCode();

                if (!$smsCode) {
                    return [
                        'success' => false,
                        'error' => 'Неверный код'
                    ];
                }

                if ($smsCode->code != $this->getSms()) {
                    return [
                        'success' => false,
                        'error' => 'Неверный код'
                    ];
                }

                if ($smsCode->isExpired()) {
                    return [
                        'success' => false,
                        'error' => 'Время действия кода истекло'
                    ];
                }

                return [
                    'success' => true,
                    'token' => $this->createApiToken()->token
                ];
                break;

            case 'check-token':
                $token = $this->findToken();

                if (!$token) {
                    return [
                        'success' => false,
                        'error' => 'Неверный токен'
                    ];
                }

                if ($token->isExpired()) {
                    return [
                        'success' => false,
                        'error' => 'Время действия токена истекло'
                    ];
                }

                return [
                    'success' => true
                ];
                break;
        }

        return [];
    }


    public function getRequestType()
    {
        $phone = $this->getPhone();
        $sms = $this->getSms();
        $token = $this->getToken();

        if ($phone and !$sms and !$token) {
            return 'send-sms';
        }

        if ($phone and $sms and !$token) {
            return 'check-sms';
        }

        if ($phone and $token) {
            return 'check-token';
        }

        return null;
    }


    public function createSmsCode()
    {
        $model = new SmsCode();
        $model->phone = $this->getPhone();
        $model->code = rand(1000, 9999);
        $model->save();

        return $model;
    }


    public function findSmsCode()
    {
        return SmsCode::query()
            ->where('phone', $this->getPhone())
            ->orderBy('created_at', 'desc')
            ->first();
    }


    public function createApiToken()
    {
        $model = new ApiToken();
        $model->phone = $this->getPhone();
        $model->token = $this->createTokenString();
        $model->save();

        return $model;
    }


    public function createTokenString($length = 15)
    {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }


    public function findToken()
    {
        return ApiToken::query()
            ->where('phone', $this->getPhone())
            ->where('token', $this->getToken())
            ->orderBy('created_at', 'desc')
            ->first();
    }


    public function sendCode()
    {
        $code = $this->createSmsCode()->code;

        $smsService = new SmsService();
        $smsService->setPhone($this->getPhone());
        $smsService->setText($code);

        return $smsService->send();
    }

}
