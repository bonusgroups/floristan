<?php namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Services\YaKassaService;
use App\Http\Controllers\Controller;

class YaKassaController extends Controller
{

    public function index()
    {
        $request = request();
        $status = $request->input('object.status');

        switch ($status) {
            case 'waiting_for_capture':
                $amount = $request->input('object.amount.value');
                $paymentId = $request->input('object.id');

                $this->getClient()->capturePayment(
                    [
                        'amount' => [
                            'value' => $amount,
                            'currency' => 'RUB',
                        ]
                    ],
                    $paymentId,
                    uniqid('', true)
                );

                $this->updateOrder();

                return 'ok';
                break;
        }

        return abort(403);
    }


    public function updateOrder()
    {
        $id = request()->input('object.metadata.order_id');

        $order = Order::find($id);
        $order->payed = 1;
        $order->save();

        $order->sendFranchiseeNotice();
    }


    public function getYaKassaService()
    {
        return YaKassaService::instance();
    }


    public function getClient()
    {
        return $this->getYaKassaService()->getClient();
    }

}

