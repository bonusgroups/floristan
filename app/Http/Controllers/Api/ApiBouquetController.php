<?php namespace App\Http\Controllers\Api;

use App\Models\Bouquet;
use App\Http\Controllers\Controller;
use App\Models\Flower;
use App\Services\FranchiseeService;

class ApiBouquetController extends Controller
{

    public $franchiseId = null;


    public function getFranchiseId()
    {
        if ($this->franchiseId) return $this->franchiseId;

        $id = request()->input('franchise_id') ?: optional((new FranchiseeService)->getCurrentFranchisee())->id;

        return $this->franchiseId = $id;
    }


    public function index($page = 1)
    {
        $data = $this->getData($page);

        return response()->json($data, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }


    public function getData($id)
    {
        $item = $this->getBouquet($id);

        return ['success' => (bool)$item] + $item;
    }


    public function getPerPage()
    {
        return 10;
    }


    public function getBouquet($id)
    {
        $bouquet = Bouquet::query()
            ->with(
                'flowers',
                'flowers.prices'
            )
            ->find($id);

        if (!$bouquet) return [];

        $size = Bouquet::SIZE_MIDDLE;
        $oldPrice = $size ? $bouquet->price($size, $this->getFranchiseId()) : null;
        $price = $size ? $bouquet->priceWithDiscount($size, $this->getFranchiseId()) : null;

        return [
            'id' => $bouquet->id,
            'name' => $bouquet->name_ru,
            'description' => strip_tags($bouquet->description_ru),
            'price' => $price,
            'old_price' => ($oldPrice != $price) ? $oldPrice : null,
            'tag_discount' => (bool)$bouquet->discount,
            'tag_new' => (bool)$bouquet->is_new,
            'tag_hit' => (bool)$bouquet->hitIndex,
            'sizes' => collect(Bouquet::getSizesArray())->values()->map(function ($size) use ($bouquet) {
                $oldPrice = $bouquet->price($size, $this->getFranchiseId());
                $price = $bouquet->priceWithDiscount($size, $this->getFranchiseId());

                if (!$price) {
                    return [
                        'name' => $size,
                        'status' => false,
                    ];
                }

                $flowers = $bouquet->flowers()->where('size', $size)->get();

                return [
                    'name' => $size,
                    'status' => true,
                    'price' => $price,
                    'old_price' => $oldPrice != $price ? $oldPrice : null,
                    'composition' => $flowers->map(function (Flower $flower) {
                        return [
                            'name' => $flower->name_ru,
                            'count' => optional($flower->pivot)->count
                        ];
                    })
                ];
            }),
            'files' => $bouquet->images->map(function ($image) {
                return url('storage/bouquets_images', $image->file_name);
            }),
            'similar' => $this->getSimilar($bouquet)
        ];
    }


    public function getSimilar(Bouquet $bouquet)
    {
        $category = $bouquet->categories()->orderBy('bouquet_bouquet_category.id', 'asc')->first();
        $categoryId = $category ? $category->id : null;

        if (!$categoryId) return [];

        $items = Bouquet::inRandomOrder()
            ->whereHas('categories', function ($q) use ($categoryId) {
                $q->where('bouquet_category_id', $categoryId);
            })
            ->limit(5)
            ->get();

        return $items->map(function (Bouquet $item) {
            $image = $item->images->first();
            $fileName = $image ? url('storage/bouquets_images', $image->file_name) : null;
            $size = $item->getMinimalPriceSize();
            $oldPrice = $size ? $item->price($size, $this->getFranchiseId()) : null;
            $price = $size ? $item->priceWithDiscount($size, $this->getFranchiseId()) : null;

            return (object)[
                'id' => $item->id,
                'name' => $item->name_ru,
                'price' => $price,
                'old_price' => $oldPrice != $price ? $oldPrice : null,
                'tag_discount' => (bool)$item->discount,
                'tag_new' => (bool)$item->is_new,
                'tag_hit' => (bool)$item->hitIndex,
                'image' => $fileName
            ];
        })->toArray();
    }


    public function files($id)
    {
        $bouquet = Bouquet::find($id);

        if (!$bouquet) {
            return [];
        }

        $data = $bouquet->images->map(function ($image) {
            return url('storage/bouquets_images', $image->file_name);
        });

        return response()->json($data, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }


}
