<?php

namespace App\Http\Middleware;

use App\Services\FranchiseeService;
use Closure;

/**
 * Class CityDetectedMiddleware
 * @package App\Http\Middleware
 */
class CityDetectedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $franchiseeService = app(FranchiseeService::class);

        if ($franchiseeService->isCurrentFranchiseeDetected()) {
            return $next($request);
        }

        abort(404);

//        return redirect()->route('site.city.display_list');
    }
}
