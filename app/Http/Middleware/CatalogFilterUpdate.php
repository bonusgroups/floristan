<?php

namespace App\Http\Middleware;

use App\Services\FilterService;
use Closure;

class CatalogFilterUpdate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $filter = app(FilterService::class);

        $filter->updateFromRequest($request);

        return $next($request);
    }
}
