<?php

namespace App\Http\Middleware;

use Closure;

class LanguageDetection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentLang = $request->cookie('language', null);

        if (is_null($currentLang)) {
            $currentLang = function_exists("locale_accept_from_http") ? explode("_", locale_accept_from_http(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : "")) : "";
            if (isset($currentLang[0]) && in_array($currentLang[0], ['en', 'ru']))
                $currentLang = mb_strtolower($currentLang[0]);
            else
                $currentLang = null;
        }

        if (is_null($currentLang)) {
            $currentLang = 'ru';
        }

        \App::setLocale($currentLang);

        switch ($currentLang) {
            case 'ru':
                setlocale(LC_ALL, 'ru_RU.UTF-8');
                break;
            default:
                setlocale(LC_ALL, 'en_US.UTF-8');
                break;
        }

        session()->put('language', $currentLang);

        return $next($request);
    }
}
