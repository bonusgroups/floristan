<?php

namespace App\Http\Middleware;

use App\Models\Franchisee;
use Closure;

/**
 * Class CityDetectedMiddleware
 * @package App\Http\Middleware
 */
class RedirectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->get('page') == 1) {
            $url = url($request->path());

            if ($query = $request->except(['page'])) {
                $url .= '?' . http_build_query($query);
            }

            return redirect($url, 301);
        }

        if ($request->is('catalog', 'catalog/floristan')) {
            return redirect('catalog/all', 301);
        }

        if ($request->is('catalog/*')) {
            $franchisee = Franchisee::findByDomain($request->getHost());

            if ($franchisee and $request->is('catalog/' . $franchisee->getCode())) {
                return redirect('catalog/all', 301);
            }
        }

        return $next($request);
    }
}
