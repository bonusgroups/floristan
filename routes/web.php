<?php


//Route::group([
//    'domain' => env('APP_BASE_DOMAIN')
//], function () {
//    Route::name('site.')->group(function () {
//        Route::get('/', 'Site\SelectCityController@index')->name('site.city.display_list');
//    });
//});


//    '{city}.' . env('APP_BASE_MAIN_DOMAIN')

Route::group([
    'middleware' => ['redirect', 'city_of_franchisee'],
], function () {
    Route::name('site.')->group(function () {
        Route::get('/', 'Site\HomePageController')->name('home_page');
        Route::get('/catalog/{slug?}', 'Site\CatalogController')->name('catalog');
        Route::get('/catalog/product/{bouquet}', 'Site\CatalogController@show')->name('catalog.show');
        Route::get('/delivery', 'Site\DeliveryController')->name('delivery');
        Route::get('/reviews/', 'Site\ReviewsController@index')->name('reviews');
        Route::post('/reviews/send', 'Site\ReviewsController@send')->name('reviews.send');
        Route::get('/about', 'Site\AboutController')->name('about');
        Route::get('/contacts', 'Site\ContactsController')->name('contacts');
        Route::get('/articles', 'Site\ArticlesController@index')->name('articles');
        Route::get('/articles/{slug}', 'Site\ArticlesController@show')->name('articles.show');

        Route::get('/cart/add', 'Site\CartController@cart_add')->name('products-cart-add');
        Route::get('/cart/del', 'Site\CartController@cart_del')->name('products-cart-del');

        Route::post('/callback', 'Site\CallbackResponseController@sendCallback')->name('callback');
        Route::get('/search', 'Site\SearchController@search')->name('search');
        Route::get('/sitemap.xml', 'Site\SiteMapController@index')->name('sitemap');
        Route::get('/sitemap', 'Site\SiteMapController@page')->name('sitemap.page');

        Route::get('/checkout', 'Site\CheckoutController@cartAction')->name('checkout.cart');
        Route::get('/checkout/info', 'Site\CheckoutController@infoAction')->name('checkout.info');
        Route::get('/checkout/pay', 'Site\CheckoutController@payAction')->name('checkout.pay');
        Route::get('/checkout/success', 'Site\CheckoutController@successAction')->name('checkout.success');

        Route::post('/checkout/submit', 'Site\CheckoutController@submitAction')->name('checkout.submit');
        Route::post('/checkout/promocode', 'Site\CheckoutController@promocodeAction')->name('checkout.promocode');
        Route::post('/checkout/promocode-delete', 'Site\CheckoutController@promocodeDeleteAction')->name('checkout.promocode-delete');

        Route::post('/ajax/buy-one-click', 'Site\BuyOneClickController@popup')->name('ajax.buy-one-click');
        Route::post('/ajax/buy-one-click/submit', 'Site\BuyOneClickController@submit')->name('ajax.buy-one-click.submit');
        Route::post('/ajax/add-to-cart', 'Site\AddToCartController@index')->name('ajax.add-to-cart');

        Route::get('/competition', 'Site\CompetitionController@index')->name('competition');
        Route::post('/competition/submit', 'Site\CompetitionController@submit')->name('competition.submit');
        Route::get('/competition/completed', 'Site\CompetitionController@completed')->name('competition.completed');

        Route::get('/order-status', 'Site\OrderStatusController@index');
        Route::post('/order-status/submit', 'Site\OrderStatusController@submit');

        Route::post('/ajax/competition-rules', 'Site\CompetitionController@rules');
        Route::post('/ajax/bouquet-vote', 'Site\BouquetVoteController@vote');

        Route::post('/ajax/page-info', 'Site\PageInfoController@popup');
        Route::post('/ajax/page-info/submit', 'Site\PageInfoController@submit');

        Route::post('/ajax/filters', 'Site\CatalogController@submitFilters');

        Route::get('/redirect', 'Site\RedirectController@index');
        Route::get('/robots.txt', 'Site\RobotsController@index');

        Route::get('/test', '\App\Console\Commands\TestCommand@test');
    });
});


Route::group(['prefix' => 'admin', 'middleware' => 'auth:admin'], function () {
    Route::name('admin.')->group(function () {
        Route::get('/', 'Admin\MainPageController')->name('main_page');

        Route::resource('franchisee', 'Admin\FranchiseeController');
        Route::resource('delivery', 'Admin\DeliveryController');
        Route::resource('flower', 'Admin\FlowerController');
        Route::resource('promocode', 'Admin\PromocodeController');
        Route::resource('bouquet', 'Admin\BouquetController');
        Route::resource('additional_product', 'Admin\AdditionalProductController', [
            'parameters' => [
                'additional_product' => 'bouquet'
            ]
        ]);
        Route::resource('bouquet_image', 'Admin\BouquetImageController');
        Route::resource('settings/bouquet_reason', 'Admin\BouquetReasonController');
        Route::resource('bouquet_color', 'Admin\BouquetColorController');

        Route::resource('bouquet_category', 'Admin\BouquetCategoryController');
        Route::post('/ajax/bouquet-category-sort', 'Admin\BouquetCategoryController@sort')->name('ajax.bouquet-category-sort');


        Route::get('settings/flower_for_filters', 'Admin\FlowerForFiltersController@index')
            ->name('settings.flower_for_filters.index');
        Route::post('settings/flower_for_filters/change', 'Admin\FlowerForFiltersController@change')
            ->name('settings.flower_for_filters.change');

        Route::resource('settings/banner', 'Admin\BannerController');

        Route::post('bouquet/{bouquet}/update_consist_count', 'Admin\BouquetController@changeConsistCount')
            ->name('bouquet.update_consist_count');
        Route::delete('bouquet/{bouquet}/delete_from_consist', 'Admin\BouquetController@deleteFromConsist')
            ->name('bouquet.delete_flower_from_consist');
        Route::post('bouquet/{bouquet}/add_to_consist', 'Admin\BouquetController@addToConsist')
            ->name('bouquet.add_to_consist');
        Route::post('bouquet/{bouquet}/detach_reason', 'Admin\BouquetController@detachReason')
            ->name('bouquet.detach_reason');
        Route::post('bouquet/{bouquet}/attach_reason', 'Admin\BouquetController@attachReason')
            ->name('bouquet.attach_reason');
        Route::post('bouquet/{bouquet}/detach_color', 'Admin\BouquetController@detachColor')
            ->name('bouquet.detach_color');
        Route::post('bouquet/{bouquet}/attach_category', 'Admin\BouquetController@attachCategory')
            ->name('bouquet.attach_category');

        Route::post('additional_product/{bouquet}/update_consist_count', 'Admin\AdditionalProductController@changeConsistCount')
            ->name('additional_product.update_consist_count');
        Route::delete('additional_product/{bouquet}/delete_from_consist', 'Admin\AdditionalProductController@deleteFromConsist')
            ->name('additional_product.delete_flower_from_consist');
        Route::post('additional_product/{bouquet}/add_to_consist', 'Admin\AdditionalProductController@addToConsist')
            ->name('additional_product.add_to_consist');
        Route::post('additional_product/{bouquet}/detach_reason', 'Admin\AdditionalProductController@detachReason')
            ->name('additional_product.detach_reason');
        Route::post('additional_product/{bouquet}/attach_reason', 'Admin\AdditionalProductController@attachReason')
            ->name('additional_product.attach_reason');
        Route::post('additional_product/{bouquet}/detach_color', 'Admin\AdditionalProductController@detachColor')
            ->name('additional_product.detach_color');
        Route::post('additional_product/{bouquet}/attach_category', 'Admin\AdditionalProductController@attachCategory')
            ->name('additional_product.attach_category');

        Route::get('orders', 'Admin\OrdersController@index')->name('orders.index');
        Route::get('orders/create', 'Admin\OrdersController@create')->name('orders.create');
        Route::post('orders/create', 'Admin\OrdersController@store')->name('orders.store');
        Route::get('orders/edit/{order}', 'Admin\OrdersController@edit')->name('orders.edit');
        Route::put('orders/update/{order}', 'Admin\OrdersController@update')->name('orders.update');
        Route::post('orders/delete/{order}', 'Admin\OrdersController@delete')->name('orders.delete');
        Route::post('orders/calculate', 'Admin\OrdersController@calculate')->name('orders.calculate');

        Route::resource('feedbacks', 'Admin\FeedbacksController');
        Route::resource('articles', 'Admin\ArticlesController');
        Route::resource('settings', 'Admin\SettingsController');
        Route::resource('finance', 'Admin\FinanceController');
    });
});

Route::group(['prefix' => 'admin'], function () {
    Route::name('admin.')->group(function () {
        // Authentication Routes...
        $this->get('login', 'Admin\Auth\LoginController@showLoginForm')->name('login.form');
        $this->post('login', 'Admin\Auth\LoginController@login')->name('login.action');
        $this->post('logout', 'Admin\Auth\LoginController@logout')->name('logout');

        // Password Reset Routes...
        $this->post('password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        $this->get('password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')
            ->name('password.reset');
        $this->post('password/reset', 'Admin\Auth\ResetPasswordController@reset')->name('password.reset.action');
    });
});

Route::group(['prefix' => 'franchisee', 'middleware' => 'auth:franchisee'], function () {
    Route::name('franchisee.')->group(function () {
        Route::get('/', 'Franchisee\MainPageController')->name('main_page');
        Route::resource('orders', 'Franchisee\OrdersController');
        Route::resource('flowers', 'Franchisee\FlowersController');
        Route::resource('products', 'Franchisee\AdditionalProductsController');
        Route::resource('finance', 'Franchisee\FinanceController');
    });
});

Route::group(['prefix' => 'franchisee'], function () {
    Route::name('franchisee.')->group(function () {
        //Auth
        $this->get('login', 'Franchisee\Auth\LoginController@showLoginForm')->name('login.form');
        $this->post('login', 'Franchisee\Auth\LoginController@login')->name('login.action');
        $this->post('logout', 'Franchisee\Auth\LoginController@logout')->name('logout');

        // Password Reset Routes...
        $this->post('password/email', 'Franchisee\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        $this->get('password/reset/{token}', 'Franchisee\Auth\ResetPasswordController@showResetForm')
            ->name('password.reset');
        $this->post('password/reset', 'Franchisee\Auth\ResetPasswordController@reset')->name('password.reset.action');
    });
});