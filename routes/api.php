<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('/auth', '\App\Http\Controllers\Api\ApiAuthController@index');

Route::any('/getbouquets/{page?}', '\App\Http\Controllers\Api\ApiBouquetsController@index');
Route::any('/getbouquet/{id}', '\App\Http\Controllers\Api\ApiBouquetController@index');
Route::any('/getbouquet/{id}/getfiles', '\App\Http\Controllers\Api\ApiBouquetController@files');
Route::any('/getfilters', '\App\Http\Controllers\Api\ApiGetFiltersController@index');
Route::any('/franchises', '\App\Http\Controllers\Api\ApiFranchisesController@index');
Route::any('/order', '\App\Http\Controllers\Api\ApiOrderController@create');
Route::any('/order/{id}/payurl', '\App\Http\Controllers\Api\ApiOrderController@payUrl');
Route::any('/checkingpromo', '\App\Http\Controllers\Api\ApiCheckingPromoController@index');

Route::any('/webhooks', '\App\Http\Controllers\Api\YaKassaController@index');

