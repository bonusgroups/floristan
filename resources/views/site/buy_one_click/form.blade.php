{{ Form::open([
    'route' => 'site.ajax.buy-one-click.submit',
    'method' => 'post',
    'class' => 'buy-one-click-form'
]) }}

{{ Form::hidden('id', $bouquet->id) }}
{{ Form::hidden('size', $size) }}

<div data-buy-one-click-form-errors class="errors"></div>

<div class="form-group">
    <div class="text">Оставьте Ваш номер телефона</div>

    {{ Form::text('phone', null, [
        'placeholder' => 'Телефон'
    ]) }}

    <div class="text">и наш оператор примет заказ в течении 3 минут</div>
</div>

<div class="form-group form-actions">
    {{ Form::button('Оформить заказ', [
        'type' => 'submit'
    ]) }}
</div>

{{ Form::close() }}