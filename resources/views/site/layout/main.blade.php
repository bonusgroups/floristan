<!DOCTYPE html>

@php($v = '1.63')
@php($pageInfo = \App\PageInfo::getByPath(request()->path()) ?: new \App\PageInfo)
@php($canonical = \App\PageInfo::getCanonical())


<html class="@yield('htmlClass')">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.65, maximum-scale=0.65, user-scalable=no">

    @if (!empty($pageInfo->getTitle()))
        <title>{{ $pageInfo->getTitle() }}</title>
    @else
        <title>@yield('title')</title>
    @endif

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="yandex-verification" content="4ea103ae89ae5bf7"/>
    <link rel="icon" type="image/png" href="{{ url('img/site/favicon.png') }}"/>

    <meta name="google-site-verification" content="vkbx0tALczvF0Hr4OagFxHo6Q6JNiDl6HoDG52uXB7Q"/>

    @if (!empty($pageInfo->getMetaKeywords()))
        <meta name="keywords" content="{{ $pageInfo->getMetaKeywords() }}">
    @else
        <meta name="keywords" content="@yield('keywords')">
    @endif

    @if (!empty($pageInfo->getMetaDescription()))
        <meta name="description" content="{{ $pageInfo->getMetaDescription() }}">
    @else
        <meta name="description" content="@yield('description')">
    @endif

    @if ($canonical)
        <link rel="canonical" href="{{ $canonical }}"/>
    @endif

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,700,700i,900,900i&amp;subset=cyrillic"
          rel="stylesheet">

    {{ Html::style('/css/site/reset.css') }}
    {{ Html::style('/css/site/flexslider.css') }}
    {{ Html::style('/css/site/bootstrap-datepicker3.standalone.min.css') }}
    {{ Html::style('/css/site/jquery.timepicker.min.css') }}
    {{ Html::style('/css/site/slim.min.css') }}
    {{ Html::style('/css/site/owl.carousel.min.css') }}
    {{ Html::style('/js/vendor/colorbox/example2/colorbox.css') }}
    {{ Html::style('/js/vendor/flatpickr/flatpickr.min.css') }}
    {{ Html::style('/css/site/fonts.css?v=' . $v) }}
    {{ Html::style('/css/site/style.css?v=' . $v) }}
    {{ Html::style('/css/site/tablet.css?v=' . $v) }}
    {{ Html::style('/css/site/mobile.css?v=' . $v) }}

    <script type="text/javascript" src="{{ URL::asset('js/site/jquery-3.3.1.min.js') }}"></script>

    @if ($currentFranchisee->meta_yandex_verification)
        <meta name="yandex-verification" content="{{ $currentFranchisee->meta_yandex_verification }}"/>
    @endif

    @if ($currentFranchisee->meta_google_verification)
        <meta name="google-site-verification" content="{{ $currentFranchisee->meta_google_verification }}"/>
    @endif

    @include('site.layout.inc.counters')

</head>
<body>

@include('site.layout.header-new')
@yield('content')
@include('site.layout.footer')

@include('site.layout.inc.citymodal')
@include('site.layout.inc.callback')

<script type="text/javascript" src="{{ URL::asset('js/site/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/jquery-scrolltofixed-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/slim.kickstart.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/jquery.flexslider-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/helpers.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/jquery.maskedinput.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/bootstrap-datepicker.ru.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/jquery.timepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/jquery.zoom.min.js?v=' . $v) }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/vendor/flatpickr/flatpickr.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/vendor/flatpickr/ru.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/vendor/colorbox/jquery.colorbox-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/site/js.js?v=' . $v) }}"></script>

@yield('js')

</body>
</html>