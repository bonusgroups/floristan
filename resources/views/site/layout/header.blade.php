<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=05db0bf59970dca55e825f2bd279dd3b"
        charset="UTF-8" async></script>


<div id="header">
    <div class="only-desktop">
        <div class="box-2">
            <div class="container clearfix">
                <a href="{{ url('/') }}" class="logo">
                    <img src="/img/site/logo.png" alt="Floristan">
                </a>
                <div class="location">
                    <div class="city">{{lang('header.middle.city')}}
                        <a href="#" data-open-cities rel="nofollow">{{ field($currentFranchisee,'city') }}</a>
                    </div>
                    <div class="address">{{ field($currentFranchisee,'address') }}</div>
                </div>
                <div class="search">
                    <form action="{{routeWithCity('site.search')}}">
                        <div class="form-group">
                            <input type="text" value="{{ request()->get('q') }}"
                                   placeholder="{{lang('header.middle.search')}}" name="q">
                        </div>
                        <div class="form-group">
                            <button>Найти</button>
                        </div>
                    </form>
                </div>
                <div class="social">
                    @php
                        $franchisee = app(\App\Services\FranchiseeService::class)->getCurrentFranchisee();
                    @endphp
                    @if(!empty($franchisee->social_vk))
                        <a href="{{$franchisee->social_vk}}" class="vk" target="_blank"></a>
                    @endif
                    @if(!empty($franchisee->social_ig))
                        <a href="{{$franchisee->social_ig}}" class="ig" target="_blank"></a>
                    @endif
                    @if(!empty($franchisee->social_wa))
                        <a href="{{$franchisee->social_wa}}" class="wa" target="_blank"></a>
                    @endif
                    @if(!empty($franchisee->social_tg))
                        <a href="{{$franchisee->social_tg}}" class="tg" target="_blank"></a>
                    @endif
                </div>
                <div class="contacts">
                    <div class="phones">
                        {{--<div class="phone">8 (843) 245-14-65</div>--}}
                        <div class="phone">{{ $currentFranchisee->getPhoneWithFormat() }}</div>
                        <div class="phone">8 (800) 200-23-37</div>
                        <a href="#" class="call-me" rel="nofollow">{{lang('header.middle.callback')}}</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-1">
            <div class="container clearfix">
                <div class="left">
                    <ul class="menu">
                        <li>
                            <a href="{{ url('/') }}"
                               class="@include('site.layout.inc.active_class', ['routeName' => 'site.home_page'])">
                                {{lang('header.menu.index')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('catalog/all') }}"
                               class="@include('site.layout.inc.active_class', ['routeName' => 'site.catalog'])">
                                {{lang('header.menu.catalog')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('competition') }}"
                               class="@include('site.layout.inc.active_class', ['routeName' => 'site.competition'])">
                                Конкурс
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('delivery') }}"
                               class="@include('site.layout.inc.active_class', ['routeName' => 'site.delivery'])">
                                {{lang('header.menu.delivery')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('reviews') }}"
                               class="@include('site.layout.inc.active_class', ['routeName' => 'site.reviews'])">
                                {{lang('header.menu.feedbacks')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('about') }}"
                               class="@include('site.layout.inc.active_class', ['routeName' => 'site.about'])">
                                {{lang('header.menu.aboutus')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('contacts') }}"
                               class="@include('site.layout.inc.active_class', ['routeName' => 'site.contacts'])">
                                {{lang('header.menu.contacts')}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="right">
                    {{--<ul class="user-links">--}}
                    {{--<li><a href="#">{{lang('header.login')}}</a></li>--}}
                    {{--<li><a href="#">{{lang('header.register')}}</a></li>--}}
                    {{--</ul>--}}
                    <a class="cart" href="{{ url('checkout') }}">
                        <span class="icon"></span>
                        <span class="value cart_value">{{get_cart_size()}} ₽</span>
                    </a>
                    <div class="lang">
                        <a href="#" @if(session('language','ru')==='ru')class="active" @endif data-set-lang="ru" rel="nofollow">RU</a>
                        <a href="#" @if(session('language','ru')==='en')class="active" @endif data-set-lang="en" rel="nofollow">EN</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="only-tablet-or-mobile">
        <div class="mobile">
            <div class="container clearfix">
                <a href="#" class="menu-link">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <div class="location">
                    <div class="city">{{lang('header.middle.city')}}
                        <a href="#" data-open-cities rel="nofollow">{{ field($currentFranchisee,'city') }}</a>
                    </div>
                    <div class="address">{{ field($currentFranchisee,'address') }}</div>
                </div>
                <div class="right">
                    <a class="cart" href="{{ url('checkout') }}" rel="nofollow">
                        <span class="icon"></span>
                        <span class="value cart_value">{{get_cart_size()}} ₽</span>
                    </a>
                    <div class="contacts">
                        <div class="phones">
                            {{--<div class="phone">8 (843) 245-14-65</div>--}}
                            <div class="phone only-tablet">{{ $currentFranchisee->getPhoneWithFormat() }}</div>
                            <a href="#" class="call-me" rel="nofollow">{{lang('header.middle.callback')}}</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="popup-overlay"></div>
            <div class="popup">
                <a href="#" class="close"></a>
                <div class="only-mobile">
                    <div class="search">
                        <form action="{{routeWithCity('site.search')}}">
                            <div class="form-group">
                                <input type="text" value="" name="q" placeholder="{{lang('header.middle.search')}}">
                            </div>
                            <div class="form-group">
                                <button>Найти</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="only-tablet">
                    <a href="{{ URL::route('site.home_page') }}" class="logo">
                        <img src="/img/site/logo.png" alt="Floristan">
                    </a>
                </div>
                <ul class="menu">
                    <li><a href="{{ URL::route('site.home_page') }}"
                           class="@include('site.layout.inc.active_class', ['routeName' => 'site.home_page'])">{{lang('header.menu.index')}}</a>
                    </li>
                    <li><a href="{{ URL::route('site.catalog') }}"
                           class="@include('site.layout.inc.active_class', ['routeName' => 'site.catalog'])">{{lang('header.menu.catalog')}}</a>
                    </li>
                    <li>
                        <a href="{{ url('competition') }}"
                           class="@include('site.layout.inc.active_class', ['routeName' => 'site.competition'])">
                            Конкурс
                        </a>
                    </li>
                    <li><a href="{{ URL::route('site.delivery') }}"
                           class="@include('site.layout.inc.active_class', ['routeName' => 'site.delivery'])">{{lang('header.menu.delivery')}}</a>
                    </li>
                    <li><a href="{{ URL::route('site.reviews') }}"
                           class="@include('site.layout.inc.active_class', ['routeName' => 'site.reviews'])">{{lang('header.menu.reviews')}}</a>
                    </li>
                    <li><a href="{{ URL::route('site.about') }}"
                           class="@include('site.layout.inc.active_class', ['routeName' => 'site.about'])">{{lang('header.menu.about')}}</a>
                    </li>
                    <li><a href="{{ URL::route('site.contacts') }}"
                           class="@include('site.layout.inc.active_class', ['routeName' => 'site.contacts'])">{{lang('header.menu.contacts')}}</a>
                    </li>
                </ul>
                {{--<ul class="user-links">--}}
                {{--<li><a href="#">{{lang('header.login')}}</a></li>--}}
                {{--<li><a href="#">{{lang('header.register')}}</a></li>--}}
                {{--</ul>--}}
                {{--<a href="{{ url('cart') }}" class="cart">--}}
                {{--<span class="icon"></span>--}}
                {{--<span class="value cart_value">{{get_cart_size()}} ₽</span>--}}
                {{--</a>--}}
                <div class="social">
                    @php
                        $franchisee = app(\App\Services\FranchiseeService::class)->getCurrentFranchisee();
                    @endphp
                    @if(!empty($franchisee->social_vk))
                        <a href="{{$franchisee->social_vk}}" class="vk" target="_blank"></a>
                    @endif
                    @if(!empty($franchisee->social_ig))
                        <a href="{{$franchisee->social_ig}}" class="ig" target="_blank"></a>
                    @endif
                    @if(!empty($franchisee->social_wa))
                        <a href="{{$franchisee->social_wa}}" class="wa" target="_blank"></a>
                    @endif
                    @if(!empty($franchisee->social_tg))
                        <a href="{{$franchisee->social_tg}}" class="tg" target="_blank"></a>
                    @endif
                </div>
                {{--<div class="contacts">--}}
                {{--<div class="phones">--}}
                {{--<div class="phone">8 (843) 245-14-65</div>--}}
                {{--<div class="phone">8 (800) 200-23-37</div>--}}
                {{--<a href="#" class="call-me">{{lang('header.middle.callback')}}</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="lang">
                    <a href="#" @if(session('language','ru')==='ru')class="active" @endif data-set-lang="ru">RU</a>
                    <a href="#" @if(session('language','ru')==='en')class="active" @endif data-set-lang="en">EN</a>
                </div>
            </div>
        </div>
    </div>
</div>
