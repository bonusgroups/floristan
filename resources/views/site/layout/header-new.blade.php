<div class="header-block">

    <div class="header">
        <div class="inner container flex flex-nowrap">
            <div class="header-left flex flex-middle">
                <a href="{{ url('/') }}" class="logo">
                    <img src="{{ url('img/site/logo.png') }}" alt="">
                </a>
            </div>
            <div class="header-right flex flex-column">
                <div class="menu-and-lang flex flex-middle">
                    @include('site.layout.inc.menu')
                    @include('site.layout.inc.lang')

                    <div class="only-mobile">
                        @include('site.layout.inc.cart')
                    </div>
                </div>
                <div class="city-contacts-and-cart flex">
                    @include('site.layout.inc.contacts')
                    @include('site.layout.inc.city')
                    @include('site.layout.inc.cart')
                </div>
            </div>
        </div>
    </div>

    @include('site.layout.inc.categories')

    <div class="only-mobile">
        @include('.site.layout.inc.header-block-popup')
    </div>

    @if(Auth::guard('admin')->check())
        <div class="container">
            <div class="admin-links">
                <a href="{{ url('ajax/page-info') }}?path={{ request()->path() }}" class="admin-link" data-ajax>Редактировать метатеги</a>
            </div>
        </div>
    @endif

    <div class="only-mobile">
        <div class="contacts-and-search-and-menu flex flex-nowrap">
            <div class="contacts-and-search flex-nowrap">
                @include('site.layout.inc.contacts-mobile')
                @include('site.layout.inc.search')
            </div>

            <div class="search-toggle-and-menu flex flex-middle flex-nowrap">
                <a href="#" class="search-toggle"></a>
                @include('site.layout.inc.menu-link')
            </div>
        </div>
        <div class="menu-dropdown">
            @include('site.layout.inc.categories')
        </div>
        @include('site.layout.inc.city-mobile')
    </div>

</div>