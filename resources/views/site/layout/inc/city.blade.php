<div class="city">
    <div class="label">Город доставки</div>
    <div class="value">
        <a href="#" data-open-cities rel="nofollow">{{ field($currentFranchisee, 'city') }}</a>
    </div>
</div>
