<div class="lang">
    <div class="lang-inner">
        @if(session('language','ru') == 'ru')
            <a href="#" data-set-lang="ru" rel="nofollow">RU</a>
            <a href="#" data-set-lang="en" rel="nofollow">EN</a>
        @else
            <a href="#" data-set-lang="en" rel="nofollow">EN</a>
            <a href="#" data-set-lang="ru" rel="nofollow">RU</a>
        @endif
    </div>
</div>