<div class="contacts flex flex-middle">
    <div class="item has-dropdown">
        <div class="phone icon">{{ $currentFranchisee->getPhoneWithFormat() }}</div>
        <div class="dropdown">
            <div class="dropdown-inner">
                <div class="phone icon icon-telegram">{{ $currentFranchisee->getPhoneWithFormat() }}</div>
                <div class="separate"></div>
                <div class="label">Главный офис</div>
                <div class="phone">8 843 245-14-65</div>
            </div>
            <a href="#" class="call-me" rel="nofollow">Заказать обратный звонок</a>
        </div>
    </div>
</div>