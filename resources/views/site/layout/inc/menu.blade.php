<div class="menu">
    <ul class="flex">
        <li><a href="{{ url('about') }}"
               class="@include('site.layout.inc.active_class', ['routeName' => 'site.about'])">{{lang('header.menu.about')}}</a>
        </li>
        <li><a href="{{ url('delivery') }}"
               class="@include('site.layout.inc.active_class', ['routeName' => 'site.delivery'])">{{lang('header.menu.delivery')}}</a>
        </li>
        <li><a href="{{ url('reviews') }}"
               class="@include('site.layout.inc.active_class', ['routeName' => 'site.reviews'])">{{lang('header.menu.reviews')}}</a>
        </li>
        <li><a href="{{ url('contacts') }}"
               class="@include('site.layout.inc.active_class', ['routeName' => 'site.contacts'])">{{lang('header.menu.contacts')}}</a>
        </li>
        {{--<li>--}}
            {{--<a href="{{ url('competition') }}"--}}
               {{--class="@include('site.layout.inc.active_class', ['routeName' => 'site.competition'])">--}}
                {{--Конкурс--}}
            {{--</a>--}}
        {{--</li>--}}
        <li>
            <a href="{{ url('order-status') }}"
               rel="nofollow"
               class="{{ request()->is('order-status') }}"
            >Статус и оплата заказа</a>
        </li>
    </ul>
</div>