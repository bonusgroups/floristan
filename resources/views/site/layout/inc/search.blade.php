<form action="{{ routeWithCity('site.search') }}" class="search-form flex flex-nowrap">
    <input type="text" value="{{ request('q') }}" name="q" placeholder="Поиск букетов и подарков">
    <button>Найти</button>
</form>