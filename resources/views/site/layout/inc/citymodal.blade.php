@inject('franchisees','App\Services\FranchiseeService')
<div data-ui-dialog-ex="cities-form"
     data-ui-dialog-width="600"
     data-ui-dialog-class="without-title"
     style="display:none;"
>

    <!--noindex-->

    <form action="#" class="cities-form">
        <div class="form-group item-city">
            <input type="text" placeholder="Введите город" data-cities-filter-city>
        </div>
        <div class="form-group item-cities">
            <ul>
                @foreach($franchisees->all() as $franchisee)
                    <li>
                        <a href="{{ URL::formatScheme(true) }}{{ URL::format($franchisee->domain, '/') }}"
                           data-filter-city="{{ $franchisee->city_ru }} {{ $franchisee->city_en }}"
                           rel="nofollow"
                        >{{ $franchisee->city_ru }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </form>

    <!--/noindex-->

</div>
