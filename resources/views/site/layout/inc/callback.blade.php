<div data-ui-dialog-ex="call-me-form"
     data-ui-dialog-title="{{lang('header.middle.callback.message')}}"
     data-ui-dialog-width="430" style="display: none">

    {{Form::open([
        'url' => routeWithCity('site.callback'),
        'class' => 'call-me-form'
    ]) }}
    <div class="form-group">
        <input type="text" placeholder="Ваш номер телефона" name="phone">
    </div>
    <div class="form-group form-actions">
        <button type="submit" class="medium">{{lang('header.middle.callback')}}</button>
    </div>

    {{Form::close()}}
</div>
