<div class="header-block-popup">
    <a href="#" class="close"></a>
    
    <div class="header-block-popup-inner">

        @include('site.layout.inc.menu')
        @include('site.layout.inc.categories')
        @include('site.layout.inc.city')
        @include('site.layout.inc.contacts')
        @include('site.layout.inc.cart')

    </div>
</div>

<div class="header-block-popup-overlay"></div>