@inject('cats',\App\Services\CatalogService)

<div class="categories">
    <div class="container">
        <ul>
            @foreach ($cats->getItems() as $item)
                <li class="item-{{ $item->slug }}">
                    <a href="{{ $item->getPageUrl() }}" class="@if ($cats->isActive($item->id)) active @endif">
                        {{ field($item, 'name') }}
                    </a>

                    @if ($item->children->count())
                        <ul>
                            @foreach($item->children as $item)
                                <li class="item-{{ $item->slug }}">
                                    <a href="{{ $item->getPageUrl() }}" class="@if ($cats->isActive($item->id)) active @endif">
                                        {{ field($item, 'name') }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</div>