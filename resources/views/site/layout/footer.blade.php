<div id="footer">
    <div class="container clearfix">
        <div class="only-desktop-or-tablet">
            <a href="#" class="logo">
                <img src="/img/site/logo.png" alt="">
            </a>
        </div>

        <div class="only-mobile">
            <div class="flex">
                <a href="#" class="logo">
                    <img src="/img/site/logo.png" alt="">
                </a>
                <div>
                    <div class="social">
                        @php
                            $franchisee = app(\App\Services\FranchiseeService::class)->getCurrentFranchisee();
                        @endphp
                        @if(!empty($franchisee->social_vk))
                            <a href="{{$franchisee->social_vk}}" class="vk"></a>
                        @endif
                        @if(!empty($franchisee->social_fb))
                            <a href="{{$franchisee->social_fb}}" class="fb"></a>
                        @endif
                        @if(!empty($franchisee->social_ig))
                            <a href="{{$franchisee->social_ig}}" class="ig"></a>
                        @endif
                        @if(!empty($franchisee->social_wa))
                            <a href="{{$franchisee->social_wa}}" class="wa"></a>
                        @endif
                        @if(!empty($franchisee->social_tg))
                            <a href="{{$franchisee->social_tg}}" class="tg"></a>
                        @endif
                        @if(!empty($franchisee->social_vb))
                            <a href="{{$franchisee->social_vb}}" class="vb"></a>
                        @endif
                    </div>
                    <div class="contacts">
                        <div class="phones">
                            <div class="phone">{{ $currentFranchisee->getPhoneWithFormat() }}</div>
                            <div class="phone">8 (800) 200-23-37</div>
                            <a href="#" class="call-me" rel="nofollow">{{lang('header.middle.callback')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="menu-and-requisites">
            <ul class="menu">
                <li><a href="{{ url('/')}}">{{lang('header.menu.index')}}</a></li>
                <li><a href="{{ url('catalog/all') }}">{{lang('header.menu.catalog')}}</a></li>
                <li><a href="{{ url('delivery') }}">{{lang('header.menu.delivery')}}</a></li>
                <li><a href="{{ url('reviews') }}">{{lang('header.menu.feedbacks')}}</a></li>
                <li><a href="{{ url('about') }}">{{lang('header.menu.aboutus')}}</a></li>
                <li><a href="{{ url('contacts') }}">{{lang('header.menu.contacts')}}</a></li>
                {{--<li><a href="{{ URL::route('site.articles') }}">{{lang('header.menu.articles')}}</a></li>--}}
            </ul>
            <div class="requisites">ИП Каримов Р.М. | ОГРН: 313167517700020 | ИНН: 162612049374</div>
        </div>
        <div class="only-desktop-or-tablet">
            <div class="social">
                @php
                    $franchisee = app(\App\Services\FranchiseeService::class)->getCurrentFranchisee();
                @endphp
                @if(!empty($franchisee->social_vk))
                    <a href="{{$franchisee->social_vk}}" class="vk"></a>
                @endif
                @if(!empty($franchisee->social_fb))
                    <a href="{{$franchisee->social_fb}}" class="fb"></a>
                @endif
                @if(!empty($franchisee->social_ig))
                    <a href="{{$franchisee->social_ig}}" class="ig"></a>
                @endif
                @if(!empty($franchisee->social_wa))
                    <a href="{{$franchisee->social_wa}}" class="wa"></a>
                @endif
                @if(!empty($franchisee->social_tg))
                    <a href="{{$franchisee->social_tg}}" class="tg"></a>
                @endif
                @if(!empty($franchisee->social_vb))
                    <a href="{{$franchisee->social_vb}}" class="vb"></a>
                @endif
            </div>
            <div class="contacts">
                <div class="phones">
                    <div class="phone">
                        <div class="phone">{{ $currentFranchisee->getPhoneWithFormat() }}</div>
                        <div class="phone">8 (800) 200-23-37</div>
                    </div>
                    <a href="#" class="call-me" rel="nofollow">{{lang('header.middle.callback')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>