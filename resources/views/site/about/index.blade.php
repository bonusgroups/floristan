@extends('site.layout.main')

@section('title', "Флористан - О службе доставки цветов \"Флористан\"")
@section('description', "Международная служба доставки цветов Флористан. Доставляем эмоции уже с 2011 года. Вежливые курьеры, свежие цветы, быстрая доставка по всей России.")
@section('keywords', "Флористан, служба доставки, цветы, букеты, доставка")

@section('content')
<!--    @include('site.partials.categories', ['small' => true])
-->

    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="#">{{lang('bread.index')}}</a></li>
                <li>{{lang('bread.aboutus')}}</li>
            </ul>
        </div>
    </div>


    <h1>{{lang('aboutus.title')}}</h1>


    <div id="about-page">
        <div class="container">
            <div class="items">
                <div class="item clearfix">
                    <div class="left">
                        {!! lang('aboutus.line1') !!}
                    </div>
                    <div class="right">
                        <img src="/img/site/about-1.jpg" alt="">
                    </div>
                </div>
                <div class="item clearfix">
                    <div class="left">
                        <img src="/img/site/about-2.jpg" alt="">
                    </div>
                    <div class="right">
			<H2>{!! lang('aboutus.line2.title') !!}</H2>
                        {!! lang('aboutus.line2.content') !!}
                    </div>
                </div>
                <div class="item clearfix">
                    <div class="left">
                        {!! lang('aboutus.line3') !!}
                    </div>
                    <div class="right">
                        <img src="/img/site/about3.jpg" alt="">
                    </div>
                </div>
                <div class="item clearfix">
                    <div class="left">
                        <img src="/img/site/about4.jpg" alt="">
                    </div>
                    <div class="right">
			<H2>{!! lang('aboutus.line4.title') !!}</H2>
                        {!! lang('aboutus.line4.content') !!}
                    </div>
                </div>
            </div>

            <div class="footer-text">
                <h2>{{lang('aboutus.line5.title')}}</h2>
		{!! lang('aboutus.line5.content') !!}
            </div>
        </div>
    </div>
    <div id="certificates">
        <div class="container clearfix">
            <div class="only-desktop-or-tablet">
                <div class="items">
                    <div class="item">
                        <img src="/img/site/cert-1.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="/img/site/cert-2.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="/img/site/cert-3.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="only-mobile">
                <div class="owl-carousel">
                    <div>
                        <img src="/img/site/cert-1.jpg" alt="">
                    </div>
                    <div>
                        <img src="/img/site/cert-2.jpg" alt="">
                    </div>
                    <div>
                        <img src="/img/site/cert-3.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
