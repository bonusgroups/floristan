<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta name="yandex-verification" content="4ea103ae89ae5bf7" />
    <link rel="icon" type="image/png" href="{{ url('img/site/favicon.png') }}" />
    <title>Home</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i&amp;subset=cyrillic"
          rel="stylesheet">

    {{ Html::style(mix('/css/site/styles.css')) }}

    @include('site.layout.inc.counters')
</head>
<body>

<div data-ui-dialog="cities-form"
     data-ui-dialog-width="600"
     data-ui-dialog-class="without-title without-close"
>
    <form action="#" class="cities-form">
        <div class="form-group item-city">
            <input type="text" placeholder="Введите город" data-cities-filter-city>
        </div>
        <div class="form-group item-cities">
            <ul>
                @foreach($franchisees as $franchisee)
                    <li>
                        <a href="{{ URL::formatScheme(true) }}{{ URL::format($franchisee->domain, '/') }}"
                           data-filter-city="{{ $franchisee->city_ru }} {{ $franchisee->city_en }}"
                           rel="nofollow"
                        >{{ $franchisee->city_ru }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </form>
</div>

{{--{{ Html::script(mix('/js/site/jquery.ui.touch-punch.min.js')) }}--}}
{{ Html::script(mix('/js/site/scripts.js')) }}

</body>
</html>