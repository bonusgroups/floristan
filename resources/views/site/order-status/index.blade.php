@extends('site.layout.main')

@section('title', '')
@section('description', '')
@section('keywords', '')

@section('content')
    <h1>Статус и оплата заказа</h1>

    <div class="order-status-page">
        <div class="container">

            <div class="header-text">
                Чтобы узнать статус заказа и оплатить, введите его номер и нажмите <strong>"Проверить"</strong> или <strong>"Оплатить"</strong>
            </div>

            @include('site.order-status.form')

        </div>
    </div>
@stop
