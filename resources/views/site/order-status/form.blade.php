<form action="{{ url('order-status/submit') }}" method="post" class="order-status-form">
    {{ csrf_field() }}

    @if($message = session()->get('message'))
        <div class="message">
            <span>{{ $message }}</span>
        </div>
    @endif

    <div class="form-group">
        {{ Form::text('number', null, [
            'placeholder' => 'Номер заказа'
        ]) }}

        @if($errors->has('number'))
            <div class="error">{{ $errors->first('number') }}</div>
        @endif
    </div>
    <div class="form-group item-actions">
        <button type="submit" name="action" value="status">
            <span>Проверить</span>
        </button>
        <button type="submit" name="action" value="pay">
            <span>Оплатить</span>
        </button>
    </div>
</form>