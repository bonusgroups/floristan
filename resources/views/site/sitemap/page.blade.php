@extends('site.layout.main')

@section('title', "Карта сайта")

@section('content')
    <div class="sitemap-page">
        <div class="container">
            <h1>Карта сайта</h1>

            <div class="items">
                <ul>
                    <li><a href="{{ url('about') }}">О нас</a></li>
                    <li><a href="{{ url('delivery') }}">Доставка</a></li>
                    <li><a href="{{ url('reviews') }}">Услуги</a></li>
                    <li><a href="{{ url('contacts') }}">Контакты</a></li>
                    <li><a href="{{ url('competition') }}">Конкурс</a></li>
                    <li><a href="{{ url('order-status') }}" rel="nofollow">Статус и оплата заказа</a></li>

                    <li>
                        <a href="{{ url('catalog/all') }}">Каталог</a>

                        <ul>
                            @foreach($categories as $category)
                                <li><a href="{{ $category->getPageUrl() }}">{{ $category->name_ru }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop