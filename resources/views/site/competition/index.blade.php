@extends('site.layout.main')

@section('htmlClass', 'is-competition-page')

@section('content')

    <div class="competition-page">

        <div class="header">
            <div class="inner">
                <div class="title">Бесплатные букеты<br>круглый год</div>
                <div class="text">
                    <p>
                        Специально для наших любимых клиентов<br>
                        с <span class="underline">29.03.2019 по 01.05.2019</span><br>
                        мы проводим <strong>МЕГА–РОЗЫГРЫШ</strong>
                    </p>

                    <p style="font-size: 18px; font-weight: normal;">В розыгрыше не участвуют следующие даты: <br>11.02 - 17.02 и 05.03 - 11.03</p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="form">
                <div class="inner">
                    @if(empty($completed))
                        @include('site.competition.form')
                    @else
                        <div class="completed">
                            <p>Спасибо за участие в розыгрыше!</p>
                        </div>
                    @endif
                </div>

                <img src="{{ url('img/site/competition1.png') }}" alt="" class="flower flower-1">
                <img src="{{ url('img/site/competition2.png') }}" alt="" class="flower flower-2">
                <img src="{{ url('img/site/competition3.png') }}" alt="" class="flower flower-3">
                <img src="{{ url('img/site/competition4.png') }}" alt="" class="flower flower-4">
                <img src="{{ url('img/site/competition5.png') }}" alt="" class="flower flower-5">
            </div>

            <div class="winners">
                <div class="title">Победители предыдущих розыгрышей</div>
                <div class="items">
                    <div class="item">
                        <div class="name">Тимур</div>
                        <div class="phone">+7 (9..) ... 96-55</div>
                        <div class="date">Розыгрыш от 14.09.2018</div>
                        <div class="text">
                            Я очень рад, что выиграл. Вы, ребята, молодцы. Буду теперь чаще радовать жену и сестру!
                            Спасибо!
                        </div>
                    </div>
                    <div class="item">
                        <div class="name">Альфия</div>
                        <div class="phone">+7 (9..) ... 14-66</div>
                        <div class="date">Розыгрыш от 03.12.2018</div>
                        <div class="text">
                            Спасибо огромное! Очень приятно выиграть в таком необычном конкурсе)) Теперь не нужно думать
                            о цветах перед праздниками! Вы самые лучше!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
@stop
