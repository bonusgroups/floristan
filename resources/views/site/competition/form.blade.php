{{ Form::open([
    'route' => 'site.competition.submit',
    'method' => 'post',
    'class' => 'competition-form'
]) }}

<div class="title">
    Укажите 3 праздника, на которые вы<br>
    хотели бы получить бесплатные<br>
    букеты в течение года
</div>

<div class="form-items">
    <div class="form-group">
        {{ Form::label('competition-form-name', 'Как к вам обращаться?') }}
        {{ Form::text('name', null, [
            'id' => 'competition-form-name',
            'placeholder' => 'Имя'
        ]) }}
    </div>
    <div class="form-group">
        {{ Form::label('competition-form-phone', 'Ваш номер телефона') }}
        {{ Form::text('phone', null, [
            'id' => 'competition-form-phone',
            'placeholder' => 'Телефон'
        ]) }}

        @if ($errors->has('phone'))
            <div class="error">{{ $errors->first('phone') }}</div>
        @endif
    </div>

    <div class="form-group">
        {{ Form::label('competition-form-holiday-1-name', 'Наименование 1-го праздника') }}
        {{ Form::text('holiday_1_name', null, [
            'id' => 'competition-form-holiday-1-name',
            'placeholder' => 'Какой у Вас праздник?'
        ]) }}
    </div>
    <div class="form-group">
        {{ Form::label('competition-form-holiday-1-date', 'Дата 1-го праздника') }}
        {{ Form::text('holiday_1_date', null, [
            'id' => 'competition-form-holiday-1-date',
            'placeholder' => 'Выбрать дату'
        ]) }}
    </div>

    <div class="form-group">
        {{ Form::label('competition-form-holiday-2-name', 'Наименование 2-го праздника') }}
        {{ Form::text('holiday_2_name', null, [
            'id' => 'competition-form-holiday-2-name',
            'placeholder' => 'Какой у Вас праздник?'
        ]) }}
    </div>
    <div class="form-group">
        {{ Form::label('competition-form-holiday-2-date', 'Дата 2-го праздника') }}
        {{ Form::text('holiday_2_date', null, [
            'id' => 'competition-form-holiday-2-date',
            'placeholder' => 'Выбрать дату'
        ]) }}
    </div>

    <div class="form-group">
        {{ Form::label('competition-form-holiday-3-name', 'Наименование 3-го праздника') }}
        {{ Form::text('holiday_3_name', null, [
            'id' => 'competition-form-holiday-3-name',
            'placeholder' => 'Какой у Вас праздник?'
        ]) }}
    </div>
    <div class="form-group">
        {{ Form::label('competition-form-holiday-3-date', 'Дата 3-го праздника') }}
        {{ Form::text('holiday_3_date', null, [
            'id' => 'competition-form-holiday-3-date',
            'placeholder' => 'Выбрать дату'
        ]) }}
    </div>
</div>

<div class="form-group item-actions">
    {{ Form::button('Участвовать в розыгрыше', [
        'type' => 'submit'
    ]) }}

    <div class="rules">
        <a href="#">Условия розыгрыша</a>
    </div>
</div>

{{ Form::close() }}