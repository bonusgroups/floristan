@if ($errors->has('error'))
    <div class="container">
        <div class="alert alert-error">
            {{ $errors->first('error') }}
        </div>
    </div>
@endif