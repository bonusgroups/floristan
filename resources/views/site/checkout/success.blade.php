@extends('site.layout.main')


@section('content')
    <div id="checkout-page">
        <div class="container">
            <div id="breadcrumb">
                <div class="container">
                    <ul>
                        <li><a href="{{ url('/') }}">{{ lang('bread.index') }}</a></li>
                        <li><a href="{{ url('checkout') }}">Оформление заказа</a></li>
                        <li>Заказ оформлен</li>
                    </ul>
                </div>
            </div>

            <h1>Заказ оформлен</h1>

            <div class="order-success">
                <p>Спасибо, Ваш Заказ №{{ $order->id }} успешно оформлен. <br> В ближайшее время с Вами свяжется наш менеджер и уточнит детали заказа. </p>
            </div>
        </div>
    </div>
@stop
