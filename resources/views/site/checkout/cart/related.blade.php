<div class="checkout-related-products">
    <div class="container">

        <div class="block-title">Добавьте к букету</div>

        <div class="items">
            @foreach($relatedProducts as $bouquet)
                <div class="item">
                    <div class="img">
                        <a href="{{ url('catalog/product', $bouquet->id) }}">
                            <img src="{{ $bouquet->images->isNotEmpty() ? $bouquet->images->first()->url : \Faker\Factory::create()->imageUrl(210, 210) }}"
                                 alt="{{ field($bouquet,'name') }}">
                        </a>
                    </div>
                    <div class="info-and-buy">
                        <div class="info">
                            <div class="name">
                                <a href="{{ url('catalog/product', $bouquet->id) }}">{{ field($bouquet,'name')  }}</a>
                            </div>
                            <div class="price">
                                {{ $bouquet->getMinimalPriceWithDiscountWithFormat() }}₽
                            </div>

                            @if($bouquet->discount)
                                <div class="price">
                                    <span style="text-decoration: line-through;">
                                        {{ $bouquet->getMinimalPriceWithFormat() }} ₽
                                    </span>
                                </div>
                            @endif

                            {{--<br/>--}}
                            {{--<div class="balls">+{{$bouquet->ballsFromPrice(\App\Models\Bouquet::SIZE_SMALL)}} {{lang('catalog.show.bales')}}</div>--}}
                        </div>
                        <div class="buy">
                            <a href="javascript:cart_add_in_cart({{ $bouquet->id }}, '{{ $bouquet->getMinimalPriceSize() }}')">Добавить</a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>

    </div>
</div>