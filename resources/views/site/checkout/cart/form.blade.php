<div class="container">
    {{ Form::open([
        'route' => ['site.checkout.submit', $controller->getCurrentFranchisee()->cityDomain],
        'method' => 'post',
        'class' => $controller->getFormId()
    ]) }}

    {{ Form::hidden('step', $controller->getStep()) }}

    <div class="form-group item-box">
        <div class="title">Открытка:</div>
        <div class="form-group type-radios item-postcards">
            <div class="form-group item-postcard">
                {{ Form::radio('postcard', 0, $controller->getDataValue('postcard') == 0, [
                    'id' => $controller->getFormElementId('postcard-0')
                ]) }}
                {{ Form::label($controller->getFormElementId('postcard-0'), 'Без открытки') }}
            </div>
            <div class="form-group item-postcard">
                {{ Form::radio('postcard', 1, $controller->getDataValue('postcard') == 1, [
                    'id' => $controller->getFormElementId('postcard-1')
                ]) }}
                {{ Form::label($controller->getFormElementId('postcard-1'), 'Приложить') }}
            </div>
        </div>
        <div class="form-group item-postcard-text">
            {{ Form::textarea('postcard_text', $controller->getDataValue('postcard_text'), [
                'placeholder' => 'Текст открытки',
                'rows' => 7
            ]) }}
        </div>
    </div>

    <div class="form-group item-total">
        <div class="title">Сумма заказа:</div>
        <div class="value">
            <span data-checkout-step-form-order-total>{{ number_format($controller->getOrderTotal(), 0, ', ', ' ') }}</span> ₽
        </div>
    </div>

    <div class="form-group item-promocode">
        <div data-checkout-step-form-promocode-info>
            @include('site.checkout.cart.promocode-info')
        </div>
    </div>

    <div class="form-group form-actions">
        {{ Form::button('Далее', [
            'type' => 'submit'
        ]) }}
    </div>

    {{ Form::close() }}
</div>


@include('site.checkout.cart.promocode-form')
