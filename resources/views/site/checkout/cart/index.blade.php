@extends('site.layout.main')

@section('htmlClass', 'is-checkout-page')

@section('content')
    <div id="checkout-page">
        @include('site.checkout.cart.breadcrumb-and-h1')
        @include('site.checkout.steps')
        @include('site.checkout.cart.cart-box')
        @include('site.checkout.alert')

        @if ($controller->getCart())
            @include('site.checkout.cart.form')
            @include('site.checkout.cart.related')
        @endif
    </div>
@stop

@section('js')
    <script src="{{ url('js/site/cart.js') }}"></script>
@stop

