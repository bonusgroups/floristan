@php
    $promocode = $controller->getPromocode()
@endphp


@if ($promocode)
    <div class="promocode-info">
        Использован промокод
        <b>{{ $promocode->code }}</b>,
        скидка
        <b>{{ number_format($controller->getPromocodeTotal(), 0, ', ', ' ') }} ₽</b>
        <a href="{{ url('checkout/promocode-delete') }}" class="button small" data-delete-promocode-link>удалить</a>
    </div>
@else
    <div class="promocode-link">
        <a href="#">У меня есть промокод</a>
    </div>
@endif
