@if ($controller->getCart())
    <div id="cart-page">
        <div class="container clearfix">
            <div class="box">
                <div class="title">{{lang('cart.inside')}}</div>
                <div class="list">
                    @include('site.checkout.cart.cart')
                </div>
            </div>

            {{--<div class="box right">--}}
                {{--<div class="title">{{lang('cart.other-add')}}</div>--}}
                {{--<div class="list">--}}
                    {{--@include('site.checkout.cart.related')--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
    </div>
@else
    <div class="empty">
        <div class="container">
            Корзина пуста
        </div>
    </div>
@endif