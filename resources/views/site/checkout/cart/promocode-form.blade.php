<div class="hide">
    <div class="promocode-popup">
        {{ Form::open([
            'route' => ['site.checkout.promocode', $controller->getCurrentFranchisee()->cityDomain],
            'method' => 'post',
            'class' => 'promocode-form'
        ]) }}

        <div class="form-group">
            {{ Form::text('promocode', $controller->getDataValue('promocode'), [
                'placeholder' => 'Промо-код'
            ]) }}
        </div>

        <div class="form-group">
            {{ Form::button('Активировать') }}
        </div>

        <div data-promocode-form-messages></div>

        {{ Form::close() }}
    </div>
</div>
