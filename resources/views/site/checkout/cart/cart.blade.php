@php
    $balls = 0;
@endphp
@forelse($cart as $key => $item)
    @php
        $bouquet = $products->where('id', $item['id'])->first();
        $balls += $bouquet->ballsFromPrice($item['bouquet_size']);
    @endphp

    <div id="item-{{$key}}" class="item">
        <div class="img">
            <a href="{{ url('catalog/product', $bouquet->id) }}">
                <img src="{{ $bouquet->images->isNotEmpty() ? $bouquet->images->first()->url : \Faker\Factory::create()->imageUrl(210, 210) }}"
                     alt="{{ field($bouquet,'name') }}">
            </a>
        </div>
        <div class="info">
            <div class="name">
                <a href="{{ url('catalog/product', $bouquet->id) }}">
                    @if ($bouquet->isBouquet())
                        {{lang('catalog.items.title', ['name'=>field($bouquet,'name')]) }}
                    @else
                        {{ field($bouquet, 'name') }}
                    @endif
                </a>
                @if($item['bouquet_size'] == \App\Models\Bouquet::SIZE_SMALL){{lang("catalog.show.size.small")}}@endif
                @if($item['bouquet_size'] == \App\Models\Bouquet::SIZE_MIDDLE){{lang("catalog.show.size.medium")}}@endif
                @if($item['bouquet_size'] == \App\Models\Bouquet::SIZE_BIG){{lang("catalog.show.size.big")}}@endif
            </div>
            <div class="price">{{ $bouquet->priceWithDiscountWithFormat($item['bouquet_size']) }}
                ₽
            </div>

            @if ($bouquet->discount)
                <div class="price">
                    <small>
                        <span style="text-decoration: line-through;">
                            {{ $bouquet->priceWithFormat($item['bouquet_size']) }} ₽
                        </span>
                    </small>
                </div>
            @endif

            <br/>
            {{--<div class="balls">+{{$bouquet->ballsFromPrice($item['bouquet_size'])}} {{lang('catalog.show.bales')}}</div>--}}
        </div>
        <div class="delete">
            <a href="javascript:cart_del('{{$key}}')">{{lang('cart.items.delete')}}</a>
        </div>
    </div>
@empty

@endforelse