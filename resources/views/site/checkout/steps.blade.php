<div class="container">
    <div class="steps">
        <div class="items">
            @foreach ($controller->getSteps() as $step => $name)
                @if ($controller->isCompletedStep($step))
                    <a href="{{ $controller->getStepUrl($step) }}" class="step {{ $controller->isCurrentStep($step) ? 'active' : '' }} {{ $controller->isCompletedStep($step) ? 'completed' : '' }}">
                        <div class="icon">
                            <span></span>
                        </div>
                        <div class="name">{{ $name }}</div>
                    </a>
                @else
                    <div class="step {{ $controller->isCurrentStep($step) ? 'active' : '' }} {{ $controller->isCompletedStep($step) ? 'completed' : '' }}">
                        <div class="icon">
                            <span></span>
                        </div>
                        <div class="name">{{ $name }}</div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>