<div class="container">
    {{ Form::open([
        'route' => ['site.checkout.submit', $controller->getCurrentFranchisee()->cityDomain],
        'method' => 'post',
        'class' => $controller->getFormId() . ' step-info'
    ]) }}

    {{ Form::hidden('step', $controller->getStep()) }}

    <div class="form-group item-box">
        <div class="title">Ваши контакты:</div>
        <div class="form-group form-inline flex-left">
            <div class="form-group item-client-name">
                {{ Form::text('client_name', $controller->getDataValue('client_name'), [
                    'placeholder' => 'ФИО'
                ]) }}

                @if ($errors->has('client_name'))
                    <span class="help-block">{{ $errors->first('client_name') }}</span>
                @endif
            </div>
            <div class="form-group item-client-name">
                {{ Form::text('client_email', $controller->getDataValue('client_name'), [
                    'placeholder' => 'E-mail'
                ]) }}

                @if ($errors->has('client_email'))
                    <span class="help-block">{{ $errors->first('client_email') }}</span>
                @endif
            </div>
            <div class="form-group item-client-phone">
                {{ Form::text('client_phone', $controller->getDataValue('client_phone'), [
                    'placeholder' => 'Телефон'
                ]) }}

                @if ($errors->has('client_phone'))
                    <span class="help-block">{{ $errors->first('client_phone') }}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group item-box">
        <div class="title">Способ доставки:</div>
        <div class="form-group item-delivery-type">
            @foreach($controller->getDeliveryTypeOptions() as $key => $name)
                @php($value = $controller->getDataValue('delivery_type') ?: 'pickup')

                <div class="form-group item-has-recipient-address">
                    {{ Form::radio('delivery_type', $key, $value == $key, [
                        'id' => $controller->getFormElementId('delivery-type-' . $key)
                    ]) }}
                    {{ Form::label($controller->getFormElementId('delivery-type-' . $key), $name) }}
                </div>
            @endforeach

            @if ($errors->has('delivery_type'))
                <span class="help-block">{{ $errors->first('delivery_type') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group item-box">
        <div class="title">Дата доставки:</div>
        <div class="form-group item-delivery-date">
            {{ Form::text('delivery_date', $controller->getDataValue('delivery_date'), [
                'placeholder' => 'Дата доставки',
                'autocomplete' => 'off'
            ]) }}

            @if ($errors->has('delivery_date'))
                <span class="help-block">{{ $errors->first('delivery_date') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group item-box">
        <div class="box-header flex flex-left">
            <div class="title">Контакты получателя:</div>
            <div class="form-group type-radios item-has-recipient-wrapper">
                <div class="form-group item-recipient">
                    {{ Form::radio('has_recipient', 1, $controller->getDataValue('has_recipient') == 1, [
                        'id' => $controller->getFormElementId('has-recipient-1')
                    ]) }}
                    {{ Form::label($controller->getFormElementId('has-recipient-1'), 'Указать получателя') }}
                </div>
                <div class="form-group item-recipient">
                    {{ Form::radio('has_recipient', 0, $controller->getDataValue('has_recipient') == 0, [
                        'id' => $controller->getFormElementId('has-recipient-0')
                    ]) }}
                    {{ Form::label($controller->getFormElementId('has-recipient-0'), 'Получаю я сам(-а)') }}
                </div>
            </div>
        </div>
        <div class="form-group form-inline item-recipient-contacts">
            <div class="form-group item-name">
                {{ Form::text('recipient_name', $controller->getDataValue('recipient_name'), [
                    'placeholder' => 'Имя'
                ]) }}

                @if ($errors->has('recipient_name'))
                    <span class="help-block">{{ $errors->first('recipient_name') }}</span>
                @endif
            </div>
            <div class="form-group item-phone">
                {{ Form::text('recipient_phone', $controller->getDataValue('recipient_phone'), [
                    'placeholder' => 'Телефон'
                ]) }}

                @if ($errors->has('recipient_phone'))
                    <span class="help-block">{{ $errors->first('recipient_phone') }}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group item-box" data-checkout-step-form-box-recipient-address>
        <div class="box-header flex flex-left">
            <div class="title">Адрес получателя:</div>
            <div class="form-group type-radios item-has-recipient-address-wrapper">
                <div class="form-group item-has-recipient-address">
                    {{ Form::radio('has_recipient_address', 1, $controller->getDataValue('has_recipient_address') == 1, [
                        'id' => $controller->getFormElementId('has-recipient-address-1')
                    ]) }}
                    {{ Form::label($controller->getFormElementId('has-recipient-address-1'), 'Везти без звонка в указанный промежуток времени') }}
                </div>
                <div class="form-group item-has-recipient-address">
                    {{ Form::radio('has_recipient_address', 0, $controller->getDataValue('has_recipient_address') == 0, [
                        'id' => $controller->getFormElementId('has-recipient-address-0')
                    ]) }}
                    {{ Form::label($controller->getFormElementId('has-recipient-address-0'), 'Позвонить получателю для уточнения адреса') }}
                </div>
            </div>
        </div>
        <div class="form-group item-recipient-address-and-delivery-time form-inline flex-left">
            <div class="form-group item-recipient-address">
                {{ Form::text('recipient_address', $controller->getDataValue('recipient_address'), [
                    'placeholder' => 'Адрес'
                ]) }}

                @if ($errors->has('recipient_address'))
                    <span class="help-block">{{ $errors->first('recipient_address') }}</span>
                @endif
            </div>
            <div class="form-group item-delivery-time">
                {{ Form::text('delivery_time', $controller->getDataValue('delivery_time'), [
                    'placeholder' => '00:00'
                ]) }}

                @if ($errors->has('delivery_time'))
                    <span class="help-block">{{ $errors->first('delivery_time') }}</span>
                @endif
            </div>
        </div>
    </div>


    <div class="form-group item-box">
        <div class="title">Комментарий:</div>
        <div class="form-group item-delivery-time">
            {{ Form::textarea('client_comment', $controller->getDataValue('client_comment'), [
                'placeholder' => 'Комментарий',
                'rows' => 4
            ]) }}

            @if ($errors->has('client_comment'))
                <span class="help-block">{{ $errors->first('client_comment') }}</span>
            @endif
        </div>
    </div>


    <div class="form-group form-actions">
        {{ Form::button('Далее', [
            'type' => 'submit'
        ]) }}
    </div>

    {{ Form::close() }}
</div>
