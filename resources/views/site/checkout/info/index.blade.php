@extends('site.layout.main')

@section('htmlClass', 'is-checkout-page')

@section('content')
    <div id="checkout-page">
        @include('site.checkout.info.breadcrumb-and-h1')
        @include('site.checkout.steps')
        @include('site.checkout.alert')

        @if ($controller->getCart())
            @include('site.checkout.info.form')
        @endif
    </div>
@stop
