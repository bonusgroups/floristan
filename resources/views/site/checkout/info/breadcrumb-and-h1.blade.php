<div class="container">
    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{ lang('bread.index') }}</a></li>
                <li><a href="{{ url('checkout') }}">Оформление заказа</a></li>
                <li>Контакты и доставка</li>
            </ul>
        </div>
    </div>

    <h1>Оформление заказа</h1>
</div>