<div class="container">
    {{ Form::open([
        'route' => ['site.checkout.submit', $controller->getCurrentFranchisee()->cityDomain],
        'method' => 'post',
        'class' => $controller->getFormId() . ' step-pay'
    ]) }}

    {{ Form::hidden('step', $controller->getStep()) }}

    <div class="pay-buttons">
        <button name="action" value="cash">
            <span>Оплата курьеру</span>
        </button>
        <button name="action" value="online">
            <span>Оплатить онлайн</span>
        </button>
    </div>

    {{ Form::close() }}
</div>