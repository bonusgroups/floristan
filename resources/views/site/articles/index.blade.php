@extends('site.layout.main')

@section('title', "Флористан - Статьи специалистов о выборе букетов")
@section('description', "Статьи компании \"Флристан\" о выборе букетов, значении цветов, вариантов доставке и методах сохранения буеетов в свежем виде.")

@section('content')
    @include('site.partials.categories', ['small' => true])

    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="/">{{lang("bread.index")}}</a></li>
                <li>{{lang("bread.articles")}}</li>
            </ul>
        </div>
    </div>


    <h1>{{lang("articles.title")}}</h1>


    <div id="articles-page">
        <div class="container">
            <div class="articles-list">
                @foreach($articles as $article)
                    <div class="item">
                        @if(!empty($article->image))
                            <div class="img">
                                <img src="{{$article->imageUrl}}" alt="">
                            </div>
                        @endif
                        <div class="info">
                            <div class="name">
                                <a href="{{routeWithCity('site.articles.show',[$article->slug])}}">{{field($article,"title")}}</a>
                            </div>
                            <div class="date">{{$article->created_at->format("d.m.Y")}}</div>
                            <div class="text">
                                {{field($article,"description")}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
@stop