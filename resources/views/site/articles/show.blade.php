@extends('site.layout.main')

@section('title', field($article, 'page_title'))
@section('description',  field($article, 'meta_description'))

@section('content')
    @include('site.partials.categories', ['small' => true])

    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="/">{{lang("bread.index")}}</a></li>
                <li><a href="{{routeWithCity('site.articles')}}">{{lang("bread.articles")}}</a></li>
                <li>{{field($article,"title")}}</li>
            </ul>
        </div>
    </div>


    <section class="choice">
        <div class="container">
            <h1 class="h1__main-choice">{{field($article,"title")}}</h1>
            <p class="choice__main_date">
                {{$article->created_at->format("d.m.Y")}}
            </p>
            <div class="choice__content">
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        @if(!empty($article->image))
                        <div class="super-image" style="background-image:url('{{$article->imageUrl}}')"></div>
                        @endif
                        <div class="choice__conten-left">
                            {!! field($article,"content") !!}
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                    <div class="choice__right">
                        <div class="col-md-3">
                            <p class="choice__right-title">
                                {{lang("articles.some")}}
                            </p>
                            @foreach($articles as $article)
                            <div class="choice__right-box">
                                @if(!empty($article->image))
                                <img src="{{$article->imageUrl}}" alt="" style="    max-width: 100%;">
                                @endif
                                <a href="{{routeWithCity('site.articles.show',[$article->slug])}}" class="choice__right-box1">
                                    {{field($article,"title")}}
                                </a>
                                <p class="choice__right-box2">
                                    {{$article->created_at->format("d.m.Y")}}
                                </p>
                                <p class="choice__right-box3">
                                    {{field($article,"description")}}
                                </p>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop