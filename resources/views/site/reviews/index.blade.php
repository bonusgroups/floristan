@extends('site.layout.main')

@section('title', "Флористан - Отзывы от наших Клиентов")
@section('description', "Отзывы о доставке цветов в городе {$franchisee->getCity()}. Фотографии доставленных букетов и довольных Клиентов. Всегда самые свежие цветы и самая быстрая доставка. ")
@section('keywords', "отзывы, Флористан, хороший, быстро, доставка, {$franchisee->getCity()}")

@section('content')
    <!--    @include('site.partials.categories', ['small' => true])
            -->
    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="/">{{lang('bread.index')}}</a></li>
                <li>{{lang('bread.feedbacks')}}</li>
            </ul>
        </div>
    </div>


    <h1>Отзывы о салоне</h1>

    <div id="reviews-page">
        <div class="container">

            <div class="add-review-link">
                <a href="#" id="openModalButton" class="button btn-target" data-toggle="modal"
                   data-target="#myModal">{{lang("feedbacks.addbutton")}}</a>
            </div>

            <div class="bouquet-reviews">
                <div class="container">
                    <div class="items">
                        @foreach($feedbacks as $item)
                            <div class="item">
                                <div class="inner">
                                    <div class="header flex">
                                        <div class="date">{{ $item->created_at->diffForHumans() }}</div>
                                        <div class="rating">
                                            @include('site.partials.stars', ['value' => $item->rating])
                                        </div>
                                    </div>
                                    <div class="text">«{{ trim($item->description) }}»</div>
                                    <div class="footer">
                                        <div class="name">{{ $item->name }}</div>

                                        @if($item->vk_link)
                                            <div class="vk-link">
                                                <a href="{{ url('redirect') }}?{{ http_build_query(['url' => $item->vk_link]) }}"
                                                   target="_blank">{{ $item->vk_link }}</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        {!! $feedbacks->render() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content bene5">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"></span> <img src="/img/site/close.png" alt=""></button>
                    <p class="modal-title bene1">{{lang("review.title")}}</p>
                </div>
                <div class="modal-body modal-body-order">
                    <div class="form__fone">
                        <form action="{{routeWithCity("site.reviews.send")}}" id="popupResult"
                              enctype="multipart/form-data" class="review-form">
                            <input type="text" name="name" required="" placeholder="{{lang("review.fio")}}"
                                   class="form-control form__field form__box">
                            <input type="text" name="phone" required="" placeholder="Телефон"
                                   class="form-control form__field form__box">

                            <input type="text" name="city" required="" placeholder="Город"
                                   class="form-control form__field form__box">

                            <input type="text" name="social_link" required="" placeholder="{{lang("review.city")}}"
                                   class="form-control form__field form__box">
                            <textarea rows="5" cols="45" required="" name="review"
                                      placeholder="{{lang("review.review")}}" class="text-main-form"></textarea>
                            <div class="btn_under-main">
                                <div>
                                    <div>
                                        <p>
                                            {{lang("review.photo")}}
                                        </p>
                                        <div class="slim" data-label="{{lang("review.photo")}}" data-ratio="1:1"
                                             data-button-cancel-label="отмена">
                                            <input type="file" name="photo" required/>
                                        </div>
                                    </div>
                                    {{--<div class="col-sm-6">--}}
                                    {{--<p>--}}
                                    {{--{{lang("review.user")}}--}}
                                    {{--</p>--}}
                                    {{--<div class="slim" data-label="{{lang("review.user")}}" data-ratio="1:1">--}}
                                    {{--<input type="file" name="avatar"/>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                            </div>

                            <p style="color:#FF0000; text-align: center; margin-top: 10px;display:none" id="errshow">Заполните обязательные поля</p>

                            <button class="btn-blue-order btn-bene8" type="submit"
                                    id="sendReviewButton">{{lang("review.send")}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalAfter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content bene5">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"></span> <img src="/img/site/close.png" alt=""></button>
                    <p class="modal-title bene1">{{lang("review.sended")}}</p>
                </div>
                <div class="modal-body modal-body-order">
                    <div class="form__fone">
                        {{lang("review.sended.desc")}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section("js")
    <script>
        $(function () {
            $("#openModalButton").on("click", function () {
                $("#myModal").fadeIn();
                return false;
            });
            $("#myModalAfter .close").on("click", function () {
                $("#myModalAfter").fadeOut();
            });

            $("#myModal .close").on("click", function () {
                $("#myModal").fadeOut();
                $("#myModal").find(".slim").each(function () {
                    Slim.find(this).remove();
                });
                $("#myModal").find("input").val("");
                $("#myModal").find("textarea").val("");
                $("#myModal .modal-dialog").removeClass("loading");
            });

            $("#sendReviewButton").on("click", function () {
                $("#errshow").hide();
                var data = new FormData($("#popupResult")[0]);
                $("#myModal .modal-dialog").addClass("loading");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: $("#popupResult").attr("action"),
                    data: data,
                    enctype: 'multipart/form-data',
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,
                    type: 'POST',
                    error: function () {
                        $("#myModal .modal-dialog").removeClass("loading");
                        $("#errshow").show();
                    },
                    success: function (data) {
                        $("#myModal .modal-dialog").removeClass("loading");

                        if (data.success) {
                            $("#myModal").fadeOut();
                            $("#myModal").find(".slim").each(function () {
                                Slim.find(this).remove();
                            });
                            $("#myModal").find("input").val("");
                            $("#myModal").find("textarea").val("");
                            $("#myModal .modal-dialog").removeClass("loading");
                            $("#myModalAfter").fadeIn();
                        }
                    }
                });
                return false;
            });
        });
    </script>
@endsection
