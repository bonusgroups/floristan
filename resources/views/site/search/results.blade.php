@extends('site.layout.main')

@section('title', "Флористан - Поиск по сайту")
@section('description', "")

@section('content')
<!--    @include('site.partials.categories', ['small' => true])
-->
    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{lang('bread.index')}}</a></li>
                <li><a href="{{ URL::route('site.search') }}">{{lang('bread.search')}}</a></li>
                <li>{{ $query }}</li>
            </ul>
            @include('site.layout.inc.search')
        </div>
    </div>

    <div id="catalog-page">
        <div class="container clearfix">

            <div class="only-tablet-or-mobile">
                <div class="filters-form-link">
                    <a href="#" class="closed">{{lang('catalog.filter.title')}}</a>
                </div>
            </div>

{{--            @include('site.catalog.filter')--}}

            <div data-filters-result>
                @include('site.catalog.products')
            </div>
        </div>
    </div>

    @include('site.catalog.delivery')
@endsection


@section('js')
    <script>
        window.route_add_to_cart = '{{ URL::route('site.products-cart-add') }}';
        window.route_del_from_cart = '{{ URL::route('site.products-cart-del') }}';
    </script>
    <script src="/js/site/cart.js"></script>
@stop
