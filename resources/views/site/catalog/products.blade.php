<div class="products-list">
    @foreach($bouquets as $bouquet)
        @include('site.catalog.item',[
            'bouquet' => $bouquet,
            'big' => true
        ])
    @endforeach
</div>

@if(method_exists($bouquets, 'links'))
    {{ $bouquets->links('site.catalog.pagination') }}
@endif

@if(!$bouquets->count())
    <p class="not-results">Нет товаров</p>
@endif