@php($currentVote = $bouquet->getRatingValue())

<div class="rating">
    @include('site.partials.stars', ['value' => $bouquet->getRatingAverage()])

    <div class="count">
        всего голосов: {{ $bouquet->getVotesCount() }}

        @if($currentVote)
            <div>ваша оценка: {{ $currentVote }}</div>
        @endif
    </div>
</div>