@extends('site.layout.main')

@section('title', "Букет “{$bouquet->getName()}” купить в {$franchiseeName} с доставкой")
@section('description', "Доставим букет “{$bouquet->getName()}” на дом или в офис в любую точку {$franchiseeName}. Международная служба доставки цветов Флористан. Свежие цветы и быстрая доставка по всей России.")

@section('htmlClass', 'is-bouquet-page')

@section('content')
    <!--    @include('site.partials.categories', ['small' => true])
            -->

    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{lang('bread.index')}}</a></li>
                <li>
                    @if ($bouquet->isBouquet())
                        <a href="{{ url('catalog/all') }}">Все букеты</a>
                    @else
                        <a href="{{ url('catalog/presents') }}">Подарки</a>
                    @endif
                </li>
                <li>{{field($bouquet,'name')}}</li>
            </ul>

            @include('site.layout.inc.search')
        </div>
    </div>



    <div id="product-page">
        <div class="container clearfix">
            <div class="only-mobile">
                @include('site.catalog.delivery-pay-composition')
            </div>

            <div class="left">
                <div class="baloons">
                    @if($bouquet->is_new)
                        <div class="new">{{lang('catalog.items.new')}}</div>
                    @endif
                    @if($bouquet->hit)
                        <div class="hit">{{lang('catalog.items.hit')}}</div>
                    @endif
                    @if($bouquet->discount)
                        <div class="discount">-{{ $bouquet->discount }}%</div>
                    @endif
                </div>

                <div class="slider">
                    <div id="product-slider" class="flexslider">
                        <ul class="slides">
                            @foreach($bouquet->images as $image)
                                <li>
                                    <a href="{{ $image->url }}" class="colorbox" rel="product-slider">
                                        <img src="{{ $image->url }}"/>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="thumbs-and-other flex flex-nowrap">
                        <div id="product-carousel" class="flexslider">
                            <ul class="slides">
                                @foreach($bouquet->images as $image)
                                    <li>
                                        <img src="{{$image->url}}"/>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="other">
                            <div class="view-may-be-different">* Внешний вид может отличаться</div>
                            <div class="flex">
                                <div class="like">
                                    @if(env('APP_ENV') == 'local')
                                        <script type="text/javascript"
                                                src="https://vk.com/js/api/openapi.js?160"></script>

                                        <script type="text/javascript">
                                            VK.init({apiId: 6915570, onlyWidgets: true});
                                        </script>

                                        <!-- Put this div tag to the place, where the Like block will be -->
                                        <div id="vk_like"></div>
                                        <script type="text/javascript">
                                            VK.Widgets.Like("vk_like", {type: "mini"});
                                        </script>
                                    @else
                                        <script type="text/javascript"
                                                src="https://vk.com/js/api/openapi.js?160"></script>

                                        <script type="text/javascript">
                                            VK.init({apiId: 6915572, onlyWidgets: true});
                                        </script>

                                        <!-- Put this div tag to the place, where the Like block will be -->
                                        <div id="vk_like"></div>
                                        <script type="text/javascript">
                                            VK.Widgets.Like("vk_like", {type: "mini"});
                                        </script>
                                    @endif
                                </div>

                                <div class="social">
                                    @php
                                        $franchisee = app(\App\Services\FranchiseeService::class)->getCurrentFranchisee();
                                    @endphp
                                    @if(!empty($franchisee->social_vk))
                                        <a href="{{$franchisee->social_vk}}" class="vk"></a>
                                    @endif
                                    @if(!empty($franchisee->social_fb))
                                        <a href="{{$franchisee->social_fb}}" class="fb"></a>
                                    @endif
                                    @if(!empty($franchisee->social_ig))
                                        <a href="{{$franchisee->social_ig}}" class="ig"></a>
                                    @endif
                                    @if(!empty($franchisee->social_wa))
                                        <a href="{{$franchisee->social_wa}}" class="wa"></a>
                                    @endif
                                    @if(!empty($franchisee->social_tg))
                                        <a href="{{$franchisee->social_tg}}" class="tg"></a>
                                    @endif
                                    @if(!empty($franchisee->social_vb))
                                        <a href="{{$franchisee->social_vb}}" class="vb"></a>
                                    @endif
                                </div>

                                <div data-bouquet-rating="{{ $bouquet->id }}">
                                    @include('site.catalog.rating')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="right">
                <div class="info">
                    <h1>{{field($bouquet,'name')}}</h1>

                    <form action="#" class="add-to-cart-form">
                        {{ Form::hidden('id', $bouquet->id) }}

                        <div class="form-group item-size {{ $bouquet->isAdditional() ? 'hide' : '' }}">
                            <div class="form-group radios">
                                @if($bouquet->priceWithDiscount(\App\Models\Bouquet::SIZE_SMALL) > 0)
                                    <div class="form-group">
                                        <input type="radio" id="add-to-cart-form-size-1" name="size"
                                               value="size_small">
                                        <label for="add-to-cart-form-size-1">{{lang("catalog.show.size.small")}}</label>
                                    </div>
                                @endif

                                @if ($bouquet->priceWithDiscount(\App\Models\Bouquet::SIZE_MIDDLE) > 0)
                                    <div class="form-group">
                                        <input type="radio" id="add-to-cart-form-size-2" name="size" checked="checked"
                                               value="size_middle">
                                        <label for="add-to-cart-form-size-2">{{lang("catalog.show.size.medium")}}</label>
                                    </div>
                                @endif

                                @if($bouquet->priceWithDiscount(\App\Models\Bouquet::SIZE_BIG)>0)
                                    <div class="form-group">
                                        <input type="radio" id="add-to-cart-form-size-3" name="size" value="size_big">
                                        <label for="add-to-cart-form-size-3">{{lang("catalog.show.size.big")}}</label>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="prices">
                            <div class="price"
                                 data-size="{{\App\Models\Bouquet::SIZE_SMALL}}">{{ $bouquet->priceWithDiscountWithFormat(\App\Models\Bouquet::SIZE_SMALL) }}
                                ₽
                            </div>
                            <div class="price"
                                 data-size="{{\App\Models\Bouquet::SIZE_MIDDLE}}">{{ $bouquet->priceWithDiscountWithFormat(\App\Models\Bouquet::SIZE_MIDDLE) }}
                                ₽
                            </div>
                            <div class="price"
                                 data-size="{{\App\Models\Bouquet::SIZE_BIG}}">{{ $bouquet->priceWithDiscountWithFormat(\App\Models\Bouquet::SIZE_BIG) }}
                                ₽
                            </div>
                            @if($bouquet->discount>0)
                                <div class="old-price"
                                     data-size="{{\App\Models\Bouquet::SIZE_MIDDLE}}">{{ $bouquet->priceWithFormat(\App\Models\Bouquet::SIZE_MIDDLE) }}
                                    ₽
                                </div>
                                <div class="old-price"
                                     data-size="{{\App\Models\Bouquet::SIZE_SMALL}}">{{ $bouquet->priceWithFormat(\App\Models\Bouquet::SIZE_SMALL) }}
                                    ₽
                                </div>
                                <div class="old-price"
                                     data-size="{{\App\Models\Bouquet::SIZE_BIG}}">{{ $bouquet->priceWithFormat(\App\Models\Bouquet::SIZE_BIG) }}
                                    ₽
                                </div>
                            @endif
                        </div>

                        <div class="form-group form-actions">
                            <button class="big white add-to-cart">Заказать</button>
                            <button class="big buy-one-click"
                                    data-id="{{ $bouquet->id }}">{{ lang("catalog.show.order.oneclick") }}</button>
                        </div>
                    </form>

                    {{--<div class="balls" data-size="{{\App\Models\Bouquet::SIZE_SMALL}}">--}}
                    {{--+{{$bouquet->ballsFromPrice(\App\Models\Bouquet::SIZE_SMALL)}} {{lang('catalog.show.bales')}}</div>--}}
                    {{--<div class="balls" data-size="{{\App\Models\Bouquet::SIZE_MIDDLE}}">--}}
                    {{--+{{$bouquet->ballsFromPrice(\App\Models\Bouquet::SIZE_MIDDLE)}} {{lang('catalog.show.bales')}}</div>--}}
                    {{--<div class="balls" data-size="{{\App\Models\Bouquet::SIZE_BIG}}">--}}
                    {{--+{{$bouquet->ballsFromPrice(\App\Models\Bouquet::SIZE_BIG)}} {{lang('catalog.show.bales')}}</div>--}}


                    <div class="only-desktop">
                        @include('site.catalog.delivery-pay-composition', ['open' => true])
                    </div>

                    <div class="guarantee">
                        <div class="title">ГАРАНТИЯ КАЧЕСТВА</div>
                        <div class="text">
                            Если получателю не понравится букет,<br>
                            и Вы сообщите об этом в течении 24 часов,<br>
                            мы бесплатно его поменяем
                        </div>
                    </div>

                </div>
            </div>



        </div>
    </div>

    @include('site.catalog.reviews')
    @include('site.catalog.similar')
    @include('site.catalog.about-bouquet')

@stop

@section('js')
    <script>
        window.route_add_to_cart = '{{ URL::route('site.products-cart-add') }}';
        window.route_del_from_cart = '{{ URL::route('site.products-cart-del') }}';
    </script>
    <script src="/js/site/cart.js"></script>
@stop
