<div class="delivery-pay-composition">
    <div class="item item-delivery {{ !empty($open) ? 'open' : null }}">
        <div class="name">ДОСТАВКА</div>
        <div class="text">
            <p>Доставка в г. {{ \App\Models\Franchisee::getCurrent()->city_ru }} - БЕСПЛАТНО в
                течение 3х часов</p>
            <p>
                с 8.00 до 21.00 - бесплатно<br>
                с 21.00 до 8.00 - 200 рублей<br>
                * стоимость в отдаленные районы уточняйте у операторов
            </p>
        </div>
    </div>
    <div class="item item-pay {{ !empty($open) ? 'open' : null }}">
        <div class="name">ОПЛАТА</div>
        <div class="text">
            <span class="label">Карта Visa/MasterCard</span> Сбербанк Онлайн<br>
            <span class="label">PayPal</span> Яндекс.Деньги<br>
            <span class="label">Альфа-Клик</span> QIWI Wallet<br>
            <span class="label">Оплата курьеру</span>
        </div>
    </div>
    <div class="item item-composition">
        <div class="name">СОСТАВ</div>
        <div class="text">
            @if (!$bouquet->isAdditional())
                @foreach($contains as $size => $contain)
                    <div class="composition" data-size="{{$size}}">
                        @foreach($contain as $flower)
                            <div>{{$flower->pivot->count}} {{field($flower,'name')}}</div>
                        @endforeach
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>