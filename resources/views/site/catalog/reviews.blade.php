<div class="bouquet-reviews">
    <div class="container">
        <div class="block-title">Отзывы о салоне</div>
        <div class="owl-carousel">
            @foreach($feedbacks as $item)
                <div class="item">
                    <div class="inner">
                        <div class="header flex">
                            <div class="date">{{ $item->created_at->diffForHumans() }}</div>
                            <div class="rating">
                                @include('site.partials.stars', ['value' => $item->rating])
                            </div>
                        </div>
                        <div class="text">«{{ trim($item->description) }}»</div>
                        <div class="footer">
                            <div class="name">{{ $item->name }}</div>

                            @if($item->vk_link)
                                <div class="vk-link">
                                    <a href="{{ url('redirect') }}?{{ http_build_query(['url' => $item->vk_link]) }}"
                                       target="_blank">{{ $item->vk_link }}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="show-all">
            <a href="{{ url('reviews') }}">Показать все отзывы</a>
        </div>
    </div>
</div>