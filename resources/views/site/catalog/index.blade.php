@extends('site.layout.main')

@php($page = request()->get('page') ?: 1)

@section('title', $title ?: "{$category->name_ru} - страница № {$page} - Флористан")
@section('description', $metaDescription ?: "Флористан - Заказ и доставка цветов в городе {$franchisee->getCity()}. Заказать доставку букетов цветов на дом, в офис.")

@section('content')

    {{--    @include('site.partials.categories', ['small' => true])--}}

    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{ lang('bread.index') }}</a></li>
                <li><a href="{{ url('catalog/all') }}">{{ lang('bread.catalog') }}</a></li>
                <li>{{ field($category,'name') }}</li>
            </ul>

            @include('site.layout.inc.search')
        </div>
    </div>

    <div id="catalog-page" class="category-{{ $category->slug }}">
        <div class="container clearfix">

            <h1>
                @if ($h1)
                    {{ $h1 }}
                @else
                    {{ $category->name_ru }}
                @endif
            </h1>

            <div class="only-tablet-or-mobile">
                <div class="filters-form-link">
                    <a href="#" class="closed">{{lang("catalog.filter.title")}}</a>
                </div>
            </div>

            @include('site.catalog.filter')

            <div data-filters-result>
                @include('site.catalog.products')
            </div>
        </div>
    </div>

    @if((int)request('page') <= 1)
        @include('site.catalog.delivery')
    @endif

@stop

@section('js')
    <script>
        window.route_add_to_cart = '{{ URL::route('site.products-cart-add') }}';
        window.route_del_from_cart = '{{ URL::route('site.products-cart-del') }}';
    </script>
    <script src="/js/site/cart.js"></script>
@stop
