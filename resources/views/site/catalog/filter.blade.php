@inject('cats', \App\Services\CatalogService)
@inject('filter', \App\Services\FilterService)

<div class="only-tablet-or-mobile">
    <div class="filters-form-overlay"></div>
</div>

<form action="#" class="filters-form">
    {{ Form::hidden('category', $filter->getCategory()) }}
    {{ Form::hidden('q', request('q')) }}

    <div class="boxes flex flex-nowrap">
        <div class="form-group box closed">
            <label for="#">{{lang("catalog.filter.price")}}</label>
            <div class="box-content">
                <div class="between">
                    <label for="filters-form-price-from">{{lang("catalog.filter.price.from")}}</label>
                    <input id="filters-form-price-from" type="text" name="f:min"
                           value="{{$filter->currentMinPrice()}}">
                    <label for="filters-form-price-to">{{lang("catalog.filter.price.to")}}</label>
                    <input id="filters-form-price-to" type="text" name="f:max"
                           value="{{$filter->currentMaxPrice()}}">
                </div>
                <div data-ui-slider
                     data-min="{{$filter->getMinPrice()}}"
                     data-max="{{$filter->getMaxPrice()}}"
                     data-input-from="#filters-form-price-from"
                     data-input-to="#filters-form-price-to"
                ></div>
            </div>
        </div>
        <div class="form-group box closed">
            <label for="#">{{lang("catalog.filter.usage")}}</label>
            <div class="box-content">
                <div class="form-group checkboxes">
                    @foreach($cats->getReasons() as $reason)
                        <div class="form-group">
                            <input type="checkbox" id="filters-form-cause-{{$reason->id}}" value="{{$reason->id}}"
                                   name="f:reasons[]" @if($filter->hasReason($reason)) checked @endif >
                            <label for="filters-form-cause-{{$reason->id}}">{{field($reason,'name')}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="form-group box closed">
            <label for="#">{{lang("catalog.filter.color")}}</label>
            <div class="box-content">
                <div class="form-group checkboxes">
                    @foreach($cats->getColors() as $color)
                        <div class="form-group">
                            <input type="checkbox"
                                   id="filters-form-color-{{$color->id}}"
                                   value="{{$color->id}}"
                                   name="f:colors[]" @if($filter->hasColor($color)) checked @endif
                            >
                            <label for="filters-form-color-{{$color->id}}">{{ $color->name }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="form-group box closed">
            <label for="#">{{lang("catalog.filter.bouquet")}}</label>
            <div class="box-content">
                <div class="form-group checkboxes">
                    @foreach($cats->getFlowers() as $flower)
                        <div class="form-group">
                            <input type="checkbox" id="filters-form-bouquet-{{$flower->id}}" value="{{$flower->id}}"
                                   name="f:flowers[]" @if($filter->hasFlower($flower)) checked @endif >
                            <label for="filters-form-bouquet-{{$flower->id}}">{{field($flower,'name')}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="form-group box box-action">
            <button>{{lang("catalog.filter.show")}}</button>
        </div>
    </div>
    {{--<div class="form-group form-actions">--}}
        {{--<button>{{lang("catalog.filter.show")}}</button>--}}
        {{--<button class="clear">{{lang("catalog.filter.reset")}}</button>--}}
    {{--</div>--}}
</form>

@section("js")
    @parent
    <script type="text/javascript">
        $("button.clear").on("click", function () {
            window.location = "{{routeWithCity('site.catalog')}}";
            return false;
        });
    </script>
@endsection