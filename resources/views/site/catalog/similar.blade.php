@if ($similarBouquets->count())
    <div id="similar-products">
        <div class="block-title">Похожие товары</div>
        <div class="container">
            <div class="products-list">
                @foreach($similarBouquets as $bouquet)
                    @include('site.catalog.item', ['bouquet'=>$bouquet] )
                @endforeach
            </div>
        </div>
    </div>
@endif