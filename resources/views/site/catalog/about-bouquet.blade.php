@if (field($bouquet, 'text'))
    <div class="abount-bouquet">
        <div class="block-title"><H2>Доставка букета "{{field($bouquet,'name')}}" в городе {{$franchisee->getCity()}}</H2></div>
        <div class="container">
            <div class="text">
	        {!! str_replace('[city]', $franchisee->getCity(), clean(field($bouquet, 'text'))) !!}
	    </div>
        </div>
    </div>
@endif
