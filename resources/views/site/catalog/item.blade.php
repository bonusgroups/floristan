<div class="item {{ !empty($big) ? 'item-big' : null }}">
    <div class="item-inner">
        <div class="img">
            <div class="inner">
                <a href="{{ url('catalog/product', $bouquet->id) }}">
                    <img src="{{ $bouquet->images->isNotEmpty() ? $bouquet->images->first()->url : \Faker\Factory::create()->imageUrl(210, 210) }}"
                         alt="{{ field($bouquet,'name') }}">
                </a>
            </div>
        </div>
        <div class="info">
            <div class="baloons">
                @if ($bouquet->is_new)
                    <div class="new">{{lang('catalog.items.new')}}</div>
                @endif

                @if ($bouquet->hit)
                    <div class="hit">{{lang('catalog.items.hit')}}</div>
                @endif

                @if ($bouquet->discount)
                    <div class="discount">-{{ $bouquet->discount }}%</div>
                @endif
            </div>
            <div class="name">
                @if ($bouquet->isAdditional())
                    <a href="{{ url('catalog/product', $bouquet->id) }}">{{ field($bouquet,'name') }}</a>
                @else
                    <a href="{{ url('catalog/product', $bouquet->id) }}">{{ lang('catalog.items.title', ['name' => field($bouquet,'name')]) }}</a>
                @endif
            </div>
            <div class="price">
                {{ $bouquet->priceWithDiscountWithFormat(\App\Models\Bouquet::SIZE_MIDDLE) }} ₽
            </div>

            @if($bouquet->discount)
                <div class="price">
                    <strike>{{ $bouquet->priceWithFormat(\App\Models\Bouquet::SIZE_MIDDLE) }} ₽</strike>
                </div>
            @endif

            <div class="buy clearfix">
                <a href="#"
                   class="add-to-cart"
                   data-id="{{ $bouquet->id }}"
                   data-size="{{ \App\Models\Bouquet::SIZE_MIDDLE }}"
                   rel="nofollow"
                >
                    <span>Заказать</span>
                </a>

                <a href="#"
                   class="buy-one-click"
                   data-id="{{ $bouquet->id }}"
                   rel="nofollow"
                >Купить в 1 клик</a>
            </div>

        </div>
    </div>
</div>
