<form action="{{ url('ajax/page-info/submit') }}"
      method="post"
      class="page-info-form"
>

    {{ Form::hidden('path', $path) }}
    {{ Form::hidden('id', $model->id) }}

    <div data-page-info-form-messages></div>

    <div class="form-group item-title">
        <label for="#">Title</label>
        {{ Form::text('title', $model->title) }}
    </div>

    <div class="form-group item-meta-keywords">
        <label for="#">Meta keywords</label>
        {{ Form::text('meta_keywords', $model->meta_keywords) }}
    </div>

    <div class="form-group item-meta-description">
        <label for="#">Meta description</label>
        {{ Form::textarea('meta_description', $model->meta_description, [
            'rows' => 4
        ]) }}
    </div>

    <div class="form-group item-actions">
        {{ Form::button('Сохранить', [
            'type' => 'submit'
        ]) }}
    </div>

</form>