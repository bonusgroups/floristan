@if($messages)
    @foreach($messages as $type => $items)
        <ul class="alert alert-{{ $type }}">
            @foreach($items as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    @endforeach
@endif