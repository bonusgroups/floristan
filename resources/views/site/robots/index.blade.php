User-agent: *
Allow: /*?page=
Allow: /css/
Allow: /js/
Disallow: /*?
Disallow: /order-status$
Disallow: /checkout$
Sitemap: https://{{ (\App\Models\Franchisee::getCurrent())->domain }}/sitemap.xml
