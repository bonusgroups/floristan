<p>Данные:</p>

<p>
    @if($name)
        Как к вам обращаться: {{ $name }} <br>
    @endif

    @if($phone)
        Ваш номер телефона: {{ $phone }} <br>
    @endif

    @if($holiday_1_name)
        Наименование 1-го праздника: {{ $holiday_1_name }} <br>
    @endif

    @if($holiday_1_date)
        Дата 1-го праздника: {{ $holiday_1_date }} <br>
    @endif

    @if($holiday_2_name)
        Наименование 2-го праздника: {{ $holiday_2_name }} <br>
    @endif

    @if($holiday_2_date)
        Дата 2-го праздника: {{ $holiday_2_date }} <br>
    @endif

    @if($holiday_3_name)
        Наименование 3-го праздника: {{ $holiday_3_name }} <br>
    @endif

    @if($holiday_3_date)
        Дата 3-го праздника: {{ $holiday_3_date }} <br>
    @endif
</p>