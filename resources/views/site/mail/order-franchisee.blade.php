<p>Добрый день.</p>
<p>Поступила новая заявка с сайта <a href="https://floristan.ru">Floristan.ru</a>. Информацию по заявке:</p>

<p>
    Номер заказа: {{ $order->id }}<br>

    @if ($order->postcard_text)
        Текст открытки: {{ $order->postcard_text }}<br>
    @endif

    {{--Вариант доставки: {{ $order->delivery ? field($order->delivery, 'name') : 'Самовывоз' }}<br>--}}

    @if ($order->delivery_type)
        Способ доставки: {{ $order->getDeliveryTypeName() }}<br>
    @endif

    @if ($order->delivery_date and $order->delivery_time)
        Дата и время
        доставки: {{ \Carbon\Carbon::parse($order->delivery_date)->format('d.m.Y') }} {{ $order->delivery_time }}<br>
    @endif

    @if ($order->recipient_name)
        Имя получателя: {{ $order->recipient_name  }}<br>
    @endif

    @if ($order->recipient_phone)
        Телефон получателя: {{ $order->recipient_phone  }}<br>
    @endif

    @if ($order->need_to_call)
        Необходимо уточнить адрес: Да<br>
    @endif

    @if ($order->recipient_address)
        Адрес получателя: {{ $order->recipient_address  }}<br>
    @endif

    Способ оплаты: {{ $order->getPayTypeName()  }}<br>
    Статус оплаты: {{ $order->getPayedName()  }}<br>

    @if ($order->client_name)
        Имя заказчика: {{ $order->client_name  }}<br>
    @endif

    @if ($order->client_phone)
        Телефон заказчика: {{ $order->client_phone  }}<br>
    @endif

    @if ($order->client_comment)
        Комментарий: {{ $order->client_comment  }}<br>
    @endif

    Состав заказа:<br>

    @foreach($order->bouquets as $item)
        <a href="{{ url('catalog/product', $item->id) }}">{{ field($item, 'name') }} ({{ $item->getSizeNameByCode($item->pivot->bouquet_size) }})</a>,
        {{ $item->pivot->count }} шт., {{ $item->priceWithDiscount($item->pivot->bouquet_size, $order->franchisee_id) }}
        <br>
    @endforeach

    Сумма заказа: {{ $order->total_value }} руб.

</p>