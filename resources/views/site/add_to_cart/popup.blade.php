<div data-ui-dialog="add-to-cart"
     data-ui-dialog-title="Добавлено в корзину"
     data-ui-dialog-width="500"
>
    <div class="add-to-cart-popup">
        <div class="text">
            Букет
            <a href="{{ url('catalog/product', $bouquet->id) }}">{{ field($bouquet, 'name') }}</a>
            добавлен в
            <a href="{{ url('checkout') }}">корзину</a>
        </div>
        <div class="actions">
            <a href="{{ url('checkout') }}" class="button">Перейти в корзину</a>
            <a href="#" class="button close">Продолжить покупки</a>
        </div>
    </div>
</div>