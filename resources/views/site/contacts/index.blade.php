@extends('site.layout.main')

@section('title', "Флористан - Контакты флористического салоны в городе {$franchisee->getCity()}")
@section('description', "Контакты компании \"Флористан\". Международная служба доставки цветов Флористан. Вежливые курьеры, свежие цветы, быстрая доставка по всей России.")
@section('keywords', "Флористан, {$franchisee->getCity()}, контакты, купить, цветы, доставка")

@section('content')
<!--    @include('site.partials.categories', ['small' => true])
-->

    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{lang('bread.index')}}</a></li>
                <li>{{lang('bread.contacts')}}</li>
            </ul>
        </div>
    </div>

    <h1>{{lang("contacts.title")}}</h1>

    <div id="contacts-page">
        <div class="container">
            <div class="info">
                <div class="inner">
                    <div class="items">
                        <div class="item address">
                            {{ $franchisee->city_ru }}, {{field($franchisee,'address')}}
                        </div>
                        <div class="item phone">
                            {{ $franchisee->getPhoneWithFormat() }}
                        </div>
                        <div class="item email">
                            <a href="mailto:info@floristan.ru">info@floristan.ru</a>
                        </div>
                    </div>
                    <div class="social">
                        <div class="label">{{lang("contacts.socials")}}</div>
                        <div class="links clearfix">
                            @if(!empty($franchisee->social_vk))
                                <a href="{{$franchisee->social_vk}}" class="vk"></a>
                            @endif
                            @if(!empty($franchisee->social_fb))
                                <a href="{{$franchisee->social_fb}}" class="fb"></a>
                            @endif
                            @if(!empty($franchisee->social_ig))
                                <a href="{{$franchisee->social_ig}}" class="ig"></a>
                            @endif
                            @if(!empty($franchisee->social_wa))
                                <a href="{{$franchisee->social_wa}}" class="wa"></a>
                            @endif
                            @if(!empty($franchisee->social_tg))
                                <a href="{{$franchisee->social_tg}}" class="tg"></a>
                            @endif
                            @if(!empty($franchisee->social_vb))
                                <a href="{{$franchisee->social_vb}}" class="vb"></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="map" id="mapplace">
            <!--<script type="text/javascript" charset="utf-8" async
                    src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A5c4538f450f4441feb873b7d3ba07212a391c65790cb5c69df5120222aea0b6b&amp;width=500&amp;height=400&amp;lang=ru_RU"></script>-->
        </div>
    </div>
@stop
@section("js")
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>
        ymaps.ready(initMap);
        var map = null;
        function initMap() {
            map = new ymaps.Map("mapplace", {
                center: [55.76, 37.64],
                zoom: 7,
                controls: ['geolocationControl']
            });
            map.margin.addArea({
                left: 0,
                bottom: 0,
                width: '50%',
                height: '100%'
            });

            var myGeocoder = ymaps.geocode("{{ $franchisee->city_ru }}, {{$franchisee->address_ru}}", {results: 1});
            myGeocoder.then(function (res) {
                var geo = res.geoObjects.get(0),
                        coords = geo.geometry.getCoordinates(),
                        bounds = geo.properties.get('boundedBy');
                map.geoObjects.add(geo);
                map.setBounds(bounds, {
                    // Проверяем наличие тайлов на данном масштабе.
                    checkZoomRange: true,
                    useMapMargin: true
                });

                //map.setCenter(res.geoObjects.geometry.Point);
                //  var placemark = new ymaps.Placemark([55.76, 37.64], { hintContent: 'Москва!', balloonContent: 'Столица России' });

            }, function (err) {
            });


        }
    </script>
@endsection
