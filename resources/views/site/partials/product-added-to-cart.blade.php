<?php $uniqid = uniqid() ?>

<div data-ui-dialog="product-added-to-cart"
     data-ui-dialog-title="Товар добавлен в корзину"
     data-ui-dialog-width="370"
     data-ui-dialog-class="without-title"
     id="product-added-to-cart-{{ $uniqid }}"
>
    <div class="product-added-to-cart">
        Товар успешно добавлен в корзину
    </div>

    <script>
        setTimeout(function () {
            var element = $('#product-added-to-cart-{{ $uniqid }}');

            if (element.length) {
                element.dialog('close');
            }
        }, 2000);
    </script>
</div>