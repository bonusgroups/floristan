@php($value = isset($value) ? $value : 0)

<div class="stars">
    @for ($i = 1; $i <= 5; $i++)
        <span class="star {{ $i <= $value ? 'yes': 'no' }}"
              data-value="{{ $i }}"
        >{{ $i }}</span>
    @endfor
</div>