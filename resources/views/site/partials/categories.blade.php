@inject('cats',\App\Services\CatalogService)

<div id="categories" @if(isset($small) and $small) class="small" @endif>
    <div class="container clearfix">
        @foreach($cats->getCatalogsSliced() as $catMain)
            @if($catMain instanceof \Illuminate\Support\Collection)
                <div class="col">
                    @foreach($catMain as $catSub)
                        <a href="{{ $catSub->getPageUrl() }}" class="item @if($cats->isActive($catSub->id)) active @endif">
                            <div class="name">{{field($catSub,'name')}}</div>
                            <span class="img">
                                <img src="/img/site/category-{{$catSub->id}}.png" alt="">
                            </span>
                        </a>
                    @endforeach
                </div>
            @else
                <div class="col">
                    <a href="{{ $catMain->getPageUrl() }}" class="item big  @if($cats->isActive($catMain->id)) active @endif">
                        <div class="name">{{field($catMain,'name')}}</div>
                        <span class="img">
                <img src="/img/site/category-{{$catMain->id}}.png" alt="">
            </span>
                    </a>
                </div>
            @endif
        @endforeach

    </div>
</div>