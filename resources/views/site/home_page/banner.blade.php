@if ($banners->count())
    <div class="#home-page-bnr">
        <div class="container">
            @foreach ($banners as $banner)
                <div class="item only-{{ $banner->type }}">
                    @if ($banner->image)
                        @if ($banner->url)
                            <a href="{{ $banner->url }}">
                                <img src="{{ $banner->image->getUrl() }}" alt="">
                            </a>
                        @else
                            <img src="{{ $banner->image->getUrl() }}" alt="">
                        @endif
                    @endif
                </div>
            @endforeach
        </div>
    </div>
@endif