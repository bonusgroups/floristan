@extends('site.layout.main')

@section('title', "Флористан - Доставка цветов в городе {$franchisee->getCity()}")
@section('description', "Флористан - Заказ и доставка цветов в {$franchisee->getCity()}. Заказать бесплатную доставку букетов цветов на дом или в офис по всей России.")
@section('keywords', "Доставка, цветы, {$franchisee->getCity()}, букеты, онлайн")

@section('htmlClass', 'is-home-page')

@section('content')

{{--    @include('site.partials.categories', ['small' => false])--}}


    <div id="breadcrumb">
        <div class="container">
            {{--<ul>--}}
                {{--<li><a href="{{ URL::route('site.home_page') }}">{{lang('bread.index')}}</a></li>--}}
            {{--</ul>--}}

            <div></div>

            @include('site.layout.inc.search')
        </div>
    </div>


    <div id="advantages">
        <div class="container">
            <div class="items">
                <div class="item item-1">{!! lang("main.middle.delivery") !!}</div>
                <div class="item item-2">{!! lang("main.middle.cardfree") !!}</div>
                <div class="item item-3">{!! lang("main.middle.balls") !!}</div>
                <div class="item item-4">{!! lang("main.middle.garanty") !!}</div>
                <div class="item item-5">{!! lang("main.middle.photobefore") !!}</div>
            </div>
        </div>
    </div>


    {{--@include('site.home_page.banner')--}}


    <div id="hits">
        <div class="container">
            <div class="block-title">{{lang("main.hits.title")}}</div>
            <div class="products-list">
                @foreach($bouquets as $bouquet)
                    @include('site.catalog.item',['bouquet'=>$bouquet, 'big' => true])
                @endforeach
            </div>
            <div class="more-products">
                <div class="title">{{lang("main.hits.more")}}</div>
                <a href="{{ url('catalog/all') }}" class="button">{{lang("main.hits.go")}}</a>
            </div>
        </div>
    </div>


    <div id="best-price">
        <div class="container">
            <div class="block-title">{{lang("main.new.title")}}</div>
            <div class="products-list">
                @foreach($bouquetsNew as $bouquet)
                    @include('site.catalog.item',['bouquet'=>$bouquet, 'big' => true])
                @endforeach
            </div>
            <div class="more-products">
                <div class="title">{{lang("main.hits.more")}}</div>
                <a href="{{ url('catalog/all') }}" class="button">{{lang("main.hits.go")}}</a>
            </div>
        </div>
    </div>


    @include('site.home_page.reviews')


    <div id="delivery-flowers">
        <div class="container clearfix">
            <div class="block-title"><H1>{{lang( "main.delivery.title")}}{{$franchisee->getCity()}}</H2></div>
            <div class="left">
                <H2>{{lang( "main.delivery.title_h2_1")}}</H2>
                {!! lang("main.delivery.content_1") !!}
                <H2>{{lang( "main.delivery.title_h2_2")}}</H2>
                {!! lang("main.delivery.content_2") !!}
            </div>
            <div class="right">
                <img src="/img/site/delivery-flowers2.jpg" alt="">
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
        window.route_add_to_cart = '{{ URL::route('site.products-cart-add') }}';
        window.route_del_from_cart = '{{ URL::route('site.products-cart-del') }}';
    </script>
    <script src="/js/site/cart.js"></script>
@stop
