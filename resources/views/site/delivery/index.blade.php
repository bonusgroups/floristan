@extends('site.layout.main')

@section('title', "Флористан - Условия доставки цветов в городе {$franchisee->getCity()}")
@section('description', "Условия доставки цветов в городе {$franchisee->getCity()}. Зона обслуживания, способы, время и стоимость доставки. Вежливые курьеры, свежие цветы.")
@section('keywords', "заказ, онлайн, букет, цветы, доставка, {$franchisee->getCity()}")

@section('content')
<!--    @include('site.partials.categories', ['small' => true])
-->
    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{lang("bread.index")}}</a></li>
                <li>{{lang("bread.delivery")}}</li>
            </ul>
        </div>
    </div>


    <div id="delivery-page">
        <div class="container">
            <div class="box clearfix">
                <div class="block-title"><H1>{!! str_replace('[city]', $franchisee->getCity(), lang("delivery.title")) !!}</H1></div>
                <div class="left">
                    <div class="text">
			<H2>{!! lang("delivery.delivery.title") !!}</H2>
                        {!! lang("delivery.delivery.content") !!}
                    </div>
                </div>
                <div class="right">
                    <img src="/img/site/flowers3.jpg" alt="">
                </div>
            </div>
            <div class="box clearfix">
                <div class="left">
                    <div class="text">
                        <H2>{!! lang("delivery.payment.title") !!}</H2>
                        {!! str_replace('[phone]', $currentFranchisee->getPhoneWithFormat(), lang("delivery.payment.content")) !!}
                        <img src="/img/site/payment-methods.png" alt="">
                    </div>
                </div>
                <div class="right">
                    <img src="/img/site/pay.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
@stop
