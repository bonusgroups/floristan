@extends('site.layout.main')


@section('content')
    @include('site.partials.categories', ['small' => true])

    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="/">{{lang('bread.index')}}</a></li>
                <li><a href="{{ url('checkout') }}">{{lang('bread.cart')}}</a></li>
                <li>{{lang('bread.order.success')}}</li>
            </ul>
        </div>
    </div>


    <h1>{{lang('cart.success')}}</h1>
    <div id="cart-page">
        <div class="container">
            <div class="clearfix">
                <div class="box" style="width: 100%;">
                    <div class="title" style="text-align: center;">{!! lang('cart.success.inside',['number'=>$orderNumber]) !!}</div>
                    @if($paymentForm)
                    <form method="POST" action="{{ $paymentForm->getPaymentUrl() }}">
                        @foreach ($paymentForm->toArray() as $k => $v)
                            <input type="hidden" name="{{ $k }}" value="{{ $v }}" />
                        @endforeach
                            <div class="form-group form-actions" style="text-align: center">
                                <button class="button big shadow">{{lang('cart.checkout.payment')}}</button>
                            </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop


