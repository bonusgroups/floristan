@extends('site.layout.main')

@section('title', 'Флористан - Оформление заказа. Заказ цветов онлайн в любой город России')
@section('description', 'Флористан - Оформление заказа. Заказ цветов онлайн в любой город России. Всегда свежие цветы и быстрая доставка.')

@section('content')
    @include('site.partials.categories', ['small' => true])


    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="/">{{lang('bread.index')}}</a></li>
                <li><a href="{{ url('checkout') }}">{{lang('bread.cart')}}</a></li>
                <li>{{lang('bread.order')}}</li>
            </ul>
        </div>
    </div>


    <h1>{{lang('cart.title')}}</h1>


    <div id="cart-page">
        <div class="container">

            <div class="clearfix">
                <div class="box">
                    <div class="title">{{lang('cart.inside')}}</div>
                    <div class="list">
                        @php
                            $balls = 0;
                        @endphp
                        @forelse($cart as $key => $cart_item)
                            @php
                                $bouquet = $products->where('id', $cart_item['id'])->first();
                            @endphp

                            <div id="item-{{$key}}" class="item">
                                <div class="img">
                                    <a href="{{ url('catalog/product', $bouquet->id) }}">
                                        <img src="{{ $bouquet->images->isNotEmpty() ? $bouquet->images->first()->url : \Faker\Factory::create()->imageUrl(210, 210) }}"
                                             alt="{{ field($bouquet,'name') }}">
                                    </a>
                                </div>
                                <div class="info">
                                    <div class="name">
                                        <a href="{{ url('catalog/product', $bouquet->id) }}">
                                            @if ($bouquet->isBouquet())
                                                {{lang('catalog.items.title', ['name'=>field($bouquet,'name')]) }}
                                            @else
                                                {{ field($bouquet, 'name') }}
                                            @endif
                                        </a>

                                        @if($cart_item['bouquet_size'] == \App\Models\Bouquet::SIZE_SMALL){{lang("catalog.show.size.small")}}@endif
                                        @if($cart_item['bouquet_size'] == \App\Models\Bouquet::SIZE_MIDDLE){{lang("catalog.show.size.medium")}}@endif
                                        @if($cart_item['bouquet_size'] == \App\Models\Bouquet::SIZE_BIG){{lang("catalog.show.size.big")}}@endif
                                    </div>
                                    <div class="price">{{ $bouquet->priceWithDiscountWithFormat($cart_item['bouquet_size']) }}
                                        ₽
                                    </div>

                                    @if ($bouquet->discount)
                                        <div class="price">
                                            <small>
                                                <strike>{{ $bouquet->priceWithFormat($cart_item['bouquet_size']) }} ₽</strike>
                                            </small>
                                        </div>
                                    @endif


                                    <br/>
                                    {{--<div class="balls">+{{$bouquet->ballsFromPrice($cart_item['bouquet_size'])}} {{lang('catalog.show.bales')}}</div>--}}
                                </div>
                                <div class="delete">
                                    <a href="javascript:cart_del('{{$key}}')">{{lang('cart.items.delete')}}</a>
                                </div>
                            </div>
                        @empty
                            <div class="item">{{lang('cart.items.empty')}}</div>
                        @endforelse
                    </div>
                </div>

                <div class="box right">
                    <div class="title">{{lang('cart.other-add')}}</div>
                    <div class="list">
                        @foreach($related_products as $bouquet)
                            <div class="item">
                                <div class="img">
                                    <a href="#">
                                        <img src="{{ $bouquet->images->isNotEmpty() ? $bouquet->images->first()->url : \Faker\Factory::create()->imageUrl(210, 210) }}"
                                             alt="{{ field($bouquet,'name') }}">
                                    </a>
                                </div>
                                <div class="info">
                                    <div class="name">
                                        <a href="{{ url('catalog/product', $bouquet->id) }}">{{ field($bouquet,'name')  }}</a>
                                    </div>
                                    <div class="price">{{ $bouquet->getMinimalPriceWithDiscountWithFormat() }} ₽</div>
                                    <div class="price"><strike>{{ $bouquet->getMinimalPriceWithFormat() }} ₽</strike>
                                    </div>
                                    <br/>
                                    {{--<div class="balls">+{{$bouquet->ballsFromPrice(\App\Models\Bouquet::SIZE_SMALL)}} {{lang('catalog.show.bales')}}</div>--}}
                                </div>
                                <div class="buy">
                                    <a href="javascript:cart_add_in_cart({{ $bouquet->id }}, '{{ $bouquet->getMinimalPriceSize() }}')">{{lang('cart.items.buy')}}</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            {{Form::open(['url'=>routeWithCity('site.cart.order.send'),'class'=>'checkout-form'])}}

            <div class="form-group">
                <label for="checkout-form-card">{{lang('cart.checkout.cart')}}</label>

                <div class="form-group form-inline item-postcard">
                    <div class="form-group">
                        {{ Form::radio('postcard', 0, 1, ['id' => 'checkout-form-postcard-0']) }}
                        {{ Form::label('checkout-form-postcard-0', 'Без открытки') }}
                    </div>
                    <div class="form-group">
                        {{ Form::radio('postcard', 1, 0, ['id' => 'checkout-form-postcard-1']) }}
                        {{ Form::label('checkout-form-postcard-1', 'Приложить открытку (бесплатно)') }}
                    </div>
                </div>

                <div class="form-group item-postcard-text hide">
                    <textarea id="checkout-form-card"
                              rows="5"
                              placeholder="{{lang('cart.checkout.cart.description')}}"
                              name="postcard_text"
                    ></textarea>
                    @if($errors->has('postcard_text'))
                        <span class="help-block">{{ $errors->first('postcard_text') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="#">{{lang('cart.checkout.delivery')}}</label>

                <div class="form-group form-inline radios">
                    <div class="form-group">
                        {{Form::radio("delivery_id", 0, true, ['id'=>'checkout-form-delivery-0'])}}
                        <label for="checkout-form-delivery-0">{{lang('cart.checkout.delivery.self')}}</label>
                    </div>

                    @php
                        $setFirstPayment = false;
                    @endphp


                    @foreach($deliveries as $delivery)

                        <div class="form-group">
                            {{Form::radio("delivery_id", $delivery->id, false,['id'=>'checkout-form-delivery-'.$delivery->id,'data-custom-delivery'])}}
                            <label for="checkout-form-delivery-{{$delivery->id}}">{{field($delivery,'name')}} @if($delivery->price)
                                    ({{price_format($delivery->price)}}
                                    ) @else {{lang('cart.delivery.free')}} @endif</label>
                        </div>
                    @endforeach

                    @if($errors->has('delivery_id'))
                        <span class="help-block">{{ $errors->first('delivery_id') }}</span>
                    @endif
                </div>
            </div>


            <div data-only-if-custom-delivery class="hide">
                <div class="form-group contacts clearfix">
                    <div class="form-group you-contacts">
                        <label for="#">{{lang('cart.checkout.delivery-time')}}</label>
                        <div class="row">
                            <div style="width: 48%;display: inline-block;">
                                <div class="form-group" data-current-day>
                                    {{Form::text("delivery_time",$fBegin,['placeholder'=>lang('cart.checkout.contacts.delivery_time'), 'class'=>'timepicker'])}}
                                    @if($errors->has('delivery_time'))
                                        <span class="help-block">{{ $errors->first('delivery_time') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div style="width: 48%;display: inline-block;margin-left: 2%;">
                                <div class="form-group">
                                    {{Form::text("delivery_date", $dDay,['placeholder'=>lang('cart.checkout.contacts.delivery_date'),'class'=>'datepicker'])}}
                                    @if($errors->has('delivery_date'))
                                        <span class="help-block">{{ $errors->first('delivery_date') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group contacts clearfix">
                <div class="form-group you-contacts">
                    <label for="#">{{lang('cart.checkout.contacts')}}</label>
                    <div class="form-group">
                        {{Form::text("client_name",null,['placeholder'=>lang('cart.checkout.contacts.name')])}}
                        @if($errors->has('client_name'))
                            <span class="help-block">{{ $errors->first('client_name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {{Form::text("client_phone",null,['placeholder'=>lang('cart.checkout.contacts.phone')])}}
                        @if($errors->has('client_phone'))
                            <span class="help-block">{{ $errors->first('client_phone') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {{Form::textarea("client_comment",null,['placeholder'=>lang('cart.checkout.comment')])}}
                        @if($errors->has('client_comment'))
                            <span class="help-block">{{ $errors->first('client_comment') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group recipient-contacts hide" data-only-if-custom-delivery>
                    <label for="#">{{lang('cart.checkout.our_contacts')}}</label>
                    <div class="form-group">
                        {{Form::text("recipient_name",null,['placeholder'=>lang('cart.checkout.our_contacts.name')])}}
                        @if($errors->has('recipient_name'))
                            <span class="help-block">{{ $errors->first('recipient_name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {{Form::text("recipient_phone",null,['placeholder'=>lang('cart.checkout.our_contacts.phone')])}}
                        @if($errors->has('recipient_phone'))
                            <span class="help-block">{{ $errors->first('recipient_phone') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {{Form::textarea("recipient_address",null,['placeholder'=>lang('cart.checkout.our_contacts.address')])}}
                        @if($errors->has('recipient_address'))
                            <span class="help-block">{{ $errors->first('recipient_address') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {{Form::checkbox("need_to_call",'1',false,['id'=>'checkout-form-d-now-address'])}}
                        <label for="checkout-form-d-now-address">{!! lang('cart.checkout.our_contacts.need_to_call') !!}</label>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label for="#">{{lang('cart.payment')}}</label>
                <div class="form-group form-inline radios">
                    <?php $firstKey = collect($payments)->keys()->first(); ?>

                    @foreach($payments as $pid=>$payment)
                        <div class="form-group">
                            {{Form::radio("pay_type",$pid, $pid == $firstKey,['id'=>'checkout-form-payment-'.$pid])}}
                            <label for="checkout-form-payment-{{$pid}}">{{lang($payment)}}</label>
                        </div>
                    @endforeach

                    @if($errors->has('pay_type'))
                        <span class="help-block">{{ $errors->first('pay_type') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group item-promocode">
                <label for="checkout-form-promocode">Использовать купон на скидку</label>
                <div class="form-group form-inline">
                    <div class="form-group">
                        {{ Form::text('promocode', null, [
                            'id' => 'checkout-form-promocode',
                            'placeholder' => 'Введите код купона, если есть'
                        ]) }}
                        <div class="help-block">
                            <span class="error">{{ $errors->first('promocode') }}</span>
                            <span class="success">{{ !empty($promocodeText) ? $promocodeText : null }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::button('Использовать', [
                            'type' => 'submit',
                            'class' => 'button shadow'
                        ]) }}
                    </div>
                </div>
            </div>

            {{--<div class="form-group balls">--}}
            {{--<label for="#">{{lang('cart.payment.balls')}}</label>--}}
            {{--<div class="info">--}}
            {{--<b>{{lang('cart.payment.balls.customer')}}</b> {{lang('cart.payment.balls.size',['balls'=>0])}}--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
            {{--<label for="checkout-form-balls">{{lang('cart.payment.usage')}}</label>--}}
            {{--<input type="text" value="0" id="checkout-form-balls" data-max-balls="0" name="use_balls">--}}
            {{--@if($errors->has('use_balls'))--}}
            {{--<span class="help-block">{{ $errors->first('use_balls') }}</span>--}}
            {{--@endif--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="total">
                {{--<div class="item">--}}
                {{--<div class="name">{{lang('cart.checkout.total')}}</div>--}}
                {{--<div class="value" data-cart-original="{{get_cart_size(false)}}">{{get_cart_size()}} ₽</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                {{--<div class="name">{{lang('cart.checkout.balls-per-order')}}</div>--}}
                {{--<div class="value">{{$balls}} ₽</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                {{--<div class="name">{{lang('cart.checkout.balls-payment')}}</div>--}}
                {{--<div class="value">0 ₽</div>--}}
                {{--</div>--}}
                <div class="item">
                    <div class="name">{{lang('cart.checkout.total')}}</div>
                    <div class="value cart_value">{{get_cart_size()}} ₽</div>
                </div>
            </div>

            <div class="form-group form-actions">
                <button class="button big shadow">{{lang('cart.checkout.send')}}</button>
            </div>
            </form>

        </div>
    </div>
@stop
@section('js')
    <script>
        window.route_add_to_cart = '{{ URL::route('site.products-cart-add') }}';
        window.route_del_from_cart = '{{ URL::route('site.products-cart-del') }}';
        window.franchisee_min_time = '{{$fBegin}}';
        window.franchisee_max_time = '{{$fEnd}}';
        window.franchisee_def_time = '{{$fBegin}}';
        window.franchisee_start_date = '+{{$startDay}}d';

        $(function () {

            $('.checkout-form input[name="postcard"]')
                    .on('states', function () {
                        var $this = $(this);
                        var checked = $this.prop('checked');

                        if (!checked) return;

                        var value = $this.val();
                        var needCard = (value == 1);
                        var element = $this.closest('form').find('.item-postcard-text');

                        needCard ? element.show() : element.hide();
                    })
                    .change(function () {
                        $(this).trigger('states');
                    })
                    .filter(':checked')
                    .trigger('states');


            $("#checkout-form-balls").on("change", function () {
                var max = parseInt($(this).attr("data-max-balls"));
                var val = parseInt($(this).val().replace(/[^0-9]+/gm, '0'));
                if (val > max) {
                    $(this).val(max);
                }
                if (val <= 0) {
                    $(this).val(0);
                }
            });

            $('.datepicker').datepicker(
                    {
                        format: 'dd.mm.yyyy',
                        startDate: window.franchisee_start_date,
                        language: 'ru',
                        'maxViewMode': 0,
                        todayHighlight: true, todayBtn: true
                    }
            ).on("change", function () {
                if ($(this).val() === '{{date("d.m.Y")}}') {
                    $('.timepicker').timepicker('destroy').timepicker({
                        timeFormat: 'HH:mm',
                        interval: 15,
                        minTime: window.franchisee_min_time,
                        maxTime: window.franchisee_max_time,
                        startTime: window.franchisee_min_time,
                        defaultTime: window.franchisee_min_time,
                        dynamic: false,
                        dropdown: true,
                        scrollbar: true
                    });
                } else {
                    $('.timepicker').timepicker('destroy').timepicker({
                        timeFormat: 'HH:mm',
                        interval: 15,
                        minTime: '{{$ofBegin}}',
                        maxTime: window.franchisee_max_time,
                        startTime: '{{$ofBegin}}',
                        defaultTime: '{{$ofBegin}}',
                        dynamic: false,
                        dropdown: true,
                        scrollbar: true
                    });
                }
            });

            if ('{{$dDay}}' === '{{date("d.m.Y")}}') {
                $('.timepicker').timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15,
                    minTime: window.franchisee_min_time,
                    maxTime: window.franchisee_max_time,
                    startTime: window.franchisee_min_time,
                    defaultTime: window.franchisee_min_time,
                    dynamic: false,
                    dropdown: true,
                    scrollbar: true
                });
            } else {
                $('.timepicker').timepicker({
                    timeFormat: 'HH:mm',
                    interval: 15,
                    minTime: '{{$ofBegin}}',
                    maxTime: window.franchisee_max_time,
                    startTime: '{{$ofBegin}}',
                    defaultTime: '{{$ofBegin}}',
                    dynamic: false,
                    dropdown: true,
                    scrollbar: true
                });
            }

        });
    </script>
    <script src="/js/site/cart.js"></script>
@stop


