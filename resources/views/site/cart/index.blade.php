@extends('site.layout.main')

@section('title', "Флористан - Корзина. Заказ цветов онлайн в любой город России")
@section('description', 'Флористан - Корзина. Заказ цветов онлайн в любой город России. Всегда свежие цветы и быстрая доставка.')

@section('content')
    @include('site.partials.categories', ['small' => true])


    <div id="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">{{lang('bread.index')}}</a></li>
                <li>{{lang('bread.cart')}}</li>
            </ul>
        </div>
    </div>


    <h1>{{lang('cart.title')}}</h1>


    <div id="cart-page">
        <div class="container">

            @if ($payUrl)
                <p>Переадресация на платежную систему...</p>

                <script>
                    window.onload = function () {
                        yaCounter49678651.reachGoal('yandexmoney');
                        top.location.href = "{{ $payUrl }}";
                    };
                </script>
            @else
                <div class="clearfix">
                    <div class="box">
                        <div class="title">{{lang('cart.inside')}}</div>
                        <div class="list">
                            @php
                                $balls = 0;
                            @endphp
                            @forelse($cart as $key => $cart_item)
                                @php
                                    $bouquet = $products->where('id', $cart_item['id'])->first();
                                    $balls += $bouquet->ballsFromPrice($cart_item['bouquet_size']);
                                @endphp

                                <div id="item-{{$key}}" class="item">
                                    <div class="img">
                                        <a href="{{ url('catalog/product', $bouquet->id) }}">
                                            <img src="{{ $bouquet->images->isNotEmpty() ? $bouquet->images->first()->url : \Faker\Factory::create()->imageUrl(210, 210) }}"
                                                 alt="{{ field($bouquet,'name') }}">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <div class="name">
                                            <a href="{{ url('catalog/product', $bouquet->id) }}">
                                                @if ($bouquet->isBouquet())
                                                    {{lang('catalog.items.title', ['name'=>field($bouquet,'name')]) }}
                                                @else
                                                    {{ field($bouquet, 'name') }}
                                                @endif
                                            </a>
                                            @if($cart_item['bouquet_size'] == \App\Models\Bouquet::SIZE_SMALL){{lang("catalog.show.size.small")}}@endif
                                            @if($cart_item['bouquet_size'] == \App\Models\Bouquet::SIZE_MIDDLE){{lang("catalog.show.size.medium")}}@endif
                                            @if($cart_item['bouquet_size'] == \App\Models\Bouquet::SIZE_BIG){{lang("catalog.show.size.big")}}@endif
                                        </div>
                                        <div class="price">{{ $bouquet->priceWithDiscountWithFormat($cart_item['bouquet_size']) }}
                                            ₽
                                        </div>

                                        @if ($bouquet->discount)
                                            <div class="price">
                                                <small>
                                                    <strike>{{ $bouquet->priceWithFormat($cart_item['bouquet_size']) }}
                                                        ₽</strike>
                                                </small>
                                            </div>
                                        @endif

                                        <br/>
                                        {{--<div class="balls">+{{$bouquet->ballsFromPrice($cart_item['bouquet_size'])}} {{lang('catalog.show.bales')}}</div>--}}
                                    </div>
                                    <div class="delete">
                                        <a href="javascript:cart_del('{{$key}}')">{{lang('cart.items.delete')}}</a>
                                    </div>
                                </div>
                            @empty
                                <div class="item">{{lang('cart.items.empty')}}</div>
                            @endforelse
                        </div>
                    </div>

                    <div class="box right">
                        <div class="title">{{lang('cart.other-add')}}</div>
                        <div class="list">
                            @foreach($related_products as $bouquet)
                                <div class="item">
                                    <div class="img">
                                        <a href="{{ url('catalog/product', $bouquet->id) }}">
                                            <img src="{{ $bouquet->images->isNotEmpty() ? $bouquet->images->first()->url : \Faker\Factory::create()->imageUrl(210, 210) }}"
                                                 alt="{{ field($bouquet,'name') }}">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <div class="name">
                                            <a href="{{ url('catalog/product', $bouquet->id) }}">{{ field($bouquet,'name')  }}</a>
                                        </div>
                                        <div class="price">{{ $bouquet->getMinimalPriceWithDiscountWithFormat() }}₽
                                        </div>
                                        <div class="price"><strike>{{ $bouquet->getMinimalPriceWithFormat() }}₽</strike>
                                        </div>
                                        <br/>
                                        {{--<div class="balls">+{{$bouquet->ballsFromPrice(\App\Models\Bouquet::SIZE_SMALL)}} {{lang('catalog.show.bales')}}</div>--}}
                                    </div>
                                    <div class="buy">
                                        <a href="javascript:cart_add_in_cart({{ $bouquet->id }}, '{{ $bouquet->getMinimalPriceSize() }}')">{{lang('cart.items.buy')}}</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif


            @if(sizeof($cart))
                <div class="total">
                    {{--<div class="item">--}}
                    {{--<div class="name">{{lang("cart.balls")}}</div>--}}
                    {{--<div class="value">0</div>--}}
                    {{--</div>--}}
                    <div class="item">
                        <div class="name">{{lang('cart.checkout.total')}}</div>
                        <div class="value cart_value">{{get_cart_size()}} ₽</div>
                    </div>
                    {{--<div class="item">--}}
                    {{--<div class="name">{{lang('cart.balls.to')}}</div>--}}
                    {{--<div class="value balls_value">{{$balls}}</div>--}}
                    {{--</div>--}}
                </div>

                <a href="{{routeWithCity('site.cart.order')}}" class="button big shadow">{{lang('cart.order')}}</a>
            @endif
        </div>
    </div>
@stop

@section('js')
    <script>
        window.route_add_to_cart = '{{ URL::route('site.products-cart-add') }}';
        window.route_del_from_cart = '{{ URL::route('site.products-cart-del') }}';
    </script>
    <script src="/js/site/cart.js"></script>
@stop


