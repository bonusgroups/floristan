<!DOCTYPE html>
<!--[if IE 9]>
<html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <title>ProUI - Responsive Bootstrap Admin Template</title>

  <meta name="description"
        content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
  <meta name="author" content="pixelcave">
  <meta name="robots" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

  <!-- Icons -->
  <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
  <link rel="shortcut icon" href="/img/admin/favicon.png">
  <link rel="apple-touch-icon" href="/img/admin/icon57.png" sizes="57x57">
  <link rel="apple-touch-icon" href="/img/admin/icon72.png" sizes="72x72">
  <link rel="apple-touch-icon" href="/img/admin/icon76.png" sizes="76x76">
  <link rel="apple-touch-icon" href="/img/admin/icon114.png" sizes="114x114">
  <link rel="apple-touch-icon" href="/img/admin/icon120.png" sizes="120x120">
  <link rel="apple-touch-icon" href="/img/admin/icon144.png" sizes="144x144">
  <link rel="apple-touch-icon" href="/img/admin/icon152.png" sizes="152x152">
  <link rel="apple-touch-icon" href="/img/admin/icon180.png" sizes="180x180">
  <!-- END Icons -->


  {{ Html::style(mix('css/admin/styles.css')) }}

  {{ Html::script('js/admin/modernizr.min.js') }}
</head>
<body>
<!-- Login Full Background -->
<!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
<img src="/img/admin/placeholders/backgrounds/login_full_bg.jpg" alt="Login Full Background"
     class="full-bg animation-pulseSlow">
<!-- END Login Full Background -->

<!-- Login Container -->
<div id="login-container" class="animation-fadeIn">
  <!-- Login Title -->
  <div class="login-title text-center">
    <h1><i class="gi gi-flower"></i> <strong>Floristan</strong><br>
      <small>{{ Session::has('status') ? Session::get('status') : 'Сброс пароля' }}</small>
    </h1>
  </div>
  <!-- END Login Title -->

  <!-- Login Block -->
  <div class="block push-bit">
    <!-- Login Form -->
    {{ Form::open(['route' => 'admin.password.reset.action', 'method' => 'post', 'class' => 'form-horizontal form-bordered form-control-borderless', 'id' => 'form-login']) }}
    {{ Form::hidden('token', $token) }}
    {{ Form::hidden('email', $email) }}

    <div class="form-group">
      <div class="col-xs-12">
        <div class="input-group">
          <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
          <input type="email" id="email" name="email" value="{{ $email }}" disabled="disabled"
                 class="form-control input-lg" placeholder="Email">
        </div>
        @if($errors->has('email'))
          <div id="login-email-error" class="help-block animation-slideDown">{{ $errors->first('email') }}</div>
        @endif
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-12">
        <div class="input-group">
          <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
          <input type="password" id="password" name="password" class="form-control input-lg"
                 placeholder="Пароль">
        </div>
        @if($errors->has('password'))
          <div id="login-password-error" class="help-block animation-slideDown">{{ $errors->first('password') }}</div>
        @endif
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-12">
        <div class="input-group">
          <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
          <input type="password" id="password_confirmation" name="password_confirmation" class="form-control input-lg"
                 placeholder="Подтверждение пароля">
        </div>
        @if($errors->has('password_confirmation'))
          <div id="login-password-error" class="help-block animation-slideDown">{{ $errors->first('password_confirmation') }}</div>
        @endif
      </div>
    </div>
    <div class="form-group form-actions">
      <div class="col-xs-12 text-right">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Сбросить
        </button>
      </div>
    </div>

  {{ Form::close() }}
  <!-- END Login Form -->
  </div>
  <!-- END Login Block -->
</div>
<!-- END Login Container -->


<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
{{ Html::script(mix('js/admin/scripts.js')) }}

<!-- Load and execute javascript code used only in this page -->
{{ Html::script(mix('js/admin/login.js')) }}
<script>$(function () {
    Login.init();
  });</script>
</body>
</html>