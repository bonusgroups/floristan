<!DOCTYPE html>
<!--[if IE 9]>
<html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <title>ProUI - Responsive Bootstrap Admin Template</title>

  <meta name="description"
        content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
  <meta name="author" content="pixelcave">
  <meta name="robots" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

  <!-- Icons -->
  <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
  <link rel="shortcut icon" href="/img/franchisee/favicon.png">
  <link rel="apple-touch-icon" href="/img/franchisee/icon57.png" sizes="57x57">
  <link rel="apple-touch-icon" href="/img/franchisee/icon72.png" sizes="72x72">
  <link rel="apple-touch-icon" href="/img/franchisee/icon76.png" sizes="76x76">
  <link rel="apple-touch-icon" href="/img/franchisee/icon114.png" sizes="114x114">
  <link rel="apple-touch-icon" href="/img/franchisee/icon120.png" sizes="120x120">
  <link rel="apple-touch-icon" href="/img/franchisee/icon144.png" sizes="144x144">
  <link rel="apple-touch-icon" href="/img/franchisee/icon152.png" sizes="152x152">
  <link rel="apple-touch-icon" href="/img/franchisee/icon180.png" sizes="180x180">
  <!-- END Icons -->


  {{ Html::style(mix('css/franchisee/styles.css')) }}
  {{ Html::script('js/franchisee/modernizr.min.js') }}
</head>
<body>
<!-- Login Full Background -->
<!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
<img src="/img/franchisee/placeholders/backgrounds/login_full_bg.jpg" alt="Login Full Background"
     class="full-bg animation-pulseSlow">
<!-- END Login Full Background -->

<!-- Login Container -->
<div id="login-container" class="animation-fadeIn">
  <!-- Login Title -->
  <div class="login-title text-center">
    <h1><i class="gi gi-flower"></i> <strong>Floristan</strong><br>
      <small>{{ Session::has('status') ? Session::get('status') : 'Франчайзи панель' }}</small>
    </h1>
  </div>
  <!-- END Login Title -->

  <!-- Login Block -->
  <div class="block push-bit">
    <!-- Login Form -->
    {{ Form::open(['route' => 'franchisee.login.action', 'method' => 'post', 'class' => 'form-horizontal form-bordered form-control-borderless', 'id' => 'form-login']) }}

    <div class="form-group">
      <div class="col-xs-12">
        <div class="input-group">
          <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
          <input type="text" id="login-email" name="email" class="form-control input-lg" placeholder="Email">
        </div>
        @if($errors->has('email'))
          <div id="login-email-error" class="help-block animation-slideDown">{{ $errors->first('email') }}</div>
        @endif
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-12">
        <div class="input-group">
          <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
          <input type="password" id="login-password" name="password" class="form-control input-lg"
                 placeholder="Пароль">
        </div>
        @if($errors->has('password'))
          <div id="login-password-error" class="help-block animation-slideDown">{{ $errors->first('password') }}</div>
        @endif
      </div>
    </div>
    <div class="form-group form-actions">
      <div class="col-xs-4">
        <label class="switch switch-primary" data-toggle="tooltip" title="Запомнить меня">
          <input type="checkbox" id="login-remember-me" name="remember" checked>
          <span></span>
        </label>
      </div>
      <div class="col-xs-8 text-right">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Войти
        </button>
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-12 text-center">
        <a href="javascript:void(0)" id="link-reminder-login">
          <small>Забыли пароль?</small>
        </a>
      </div>
    </div>

    {{ Form::close() }}
  <!-- END Login Form -->

    <!-- Reminder Form -->
    {{ Form::open(['route' => 'franchisee.password.email', 'method' => 'post', 'class' => 'form-horizontal form-bordered form-control-borderless display-none', 'id' => 'form-reminder']) }}

    <div class="form-group">
      <div class="col-xs-12">
        <div class="input-group">
          <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
          <input type="text" id="email" name="email" class="form-control input-lg" placeholder="Email">
        </div>
      </div>
    </div>
    <div class="form-group form-actions">
      <div class="col-xs-12 text-right">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Сбросить пароль</button>
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-12 text-center">
        <small>Вспомнили пароль?</small>
        <a href="javascript:void(0)" id="link-reminder">
          <small>Войти</small>
        </a>
      </div>
    </div>

  {{ Form::close() }}
  <!-- END Reminder Form -->

  </div>
  <!-- END Login Block -->
</div>
<!-- END Login Container -->


<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
{{ Html::script(mix('js/franchisee/scripts.js')) }}

<!-- Load and execute javascript code used only in this page -->
{{ Html::script(mix('js/franchisee/login.js')) }}
<script>$(function () {
    Login.init();
  });</script>
</body>
</html>