.@extends('franchisee.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-sm-3">
                        <label>Текущий баланс:</label>
                        {{price_format($franchisee->balance)}}
                    </div>
                    <div class="col-sm-3">
                        <label>Всего выведено:</label>
                        {{price_format($franchisee->getTotalPayment())}}
                    </div>
                    <div class="col-sm-3">
                        <label>Всего заработано:</label>
                        {{ price_format($franchisee->getTotalCash() )}}
                    </div>
                    <div class="col-sm-3">
                        <label>Дата последнего вывода денег:</label>
                        {{$franchisee->total_date?$franchisee->total_date->format("d.m.Y"):"-"}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Дата вывода</th>
                                    <th>Сумма вывода</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($logs as $log)
                                    <tr>
                                        <td>
                                            {{ $log->created_at->format("d.m.Y")}}
                                        </td>
                                        <td>
                                            {{ price_format($log->append)}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        {{$logs->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
