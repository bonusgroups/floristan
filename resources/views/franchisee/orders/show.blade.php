@extends('franchisee.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {{ Form::open(['route' => ['franchisee.orders.update',$order->id], 'method' => 'put', 'class' => 'form-horizontal']) }}
                        {{Form::setModel($order)}}

                        {{--<div class="form-group">--}}
                            {{--<label class="col-md-3 control-label" for="example-text-input">Доставка</label>--}}
                            {{--<div class="col-md-9">--}}
                                {{--{{ Form::select('delivery_id', $deliveries, null, ['disabled','class' => 'form-control order-filter', 'size' => 1,'data-force-select'=>$order->delivery_id]) }}--}}
                                {{--@if($errors->has('delivery_id'))--}}
                                    {{--<span class="help-block">{{ $errors->first('delivery_id') }}</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Дата доставки</label>
                            <div class="col-md-9">
                                {{ Form::date('delivery_date', null, ['disabled','class' => 'form-control'] )}}
                                @if($errors->has('delivery_date'))
                                    <span class="help-block">{{ $errors->first('delivery_date') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Время доставки</label>
                            <div class="col-md-9">
                                {{ Form::time('delivery_time', null, ['disabled','class' => 'form-control'] )}}
                                @if($errors->has('delivery_time'))
                                    <span class="help-block">{{ $errors->first('delivery_time') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Способ оплаты</label>
                            <div class="col-md-9">
                                {{ Form::select('pay_type', \App\Models\Order::getPayTypes(), null, ['disabled','class' => 'form-control order-filter', 'size' => 1]) }}
                                @if($errors->has('pay_type'))
                                    <span class="help-block">{{ $errors->first('pay_type') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Статус оплаты</label>
                            <div class="col-md-9">
                                {{ Form::select('pay_type', \App\Models\Order::getPayedOptions(), null, ['disabled', 'class' => 'form-control order-filter', 'size' => 1]) }}
                                @if($errors->has('pay_type'))
                                    <span class="help-block">{{ $errors->first('pay_type') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Статус заказа</label>
                            <div class="col-md-9">
                                {{ Form::select('status', \App\Models\Order::getStatuses(), null, ['class' => 'form-control order-filter', 'size' => 1]) }}
                                @if($errors->has('status'))
                                    <span class="help-block">{{ $errors->first('status') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="total_value">Стоимость</label>
                            <div class="col-md-3">
                                {{ Form::number('total_value', null, ['class' => 'form-control', 'placeholder' => 'Полная стоимость', 'disabled']) }}
                                @if($errors->has('total_value'))
                                    <span class="help-block">{{ $errors->first('total_value') }}</span>
                                @endif
                            </div>

                        </div>

                        {{--<div class="form-group">--}}
                        {{--<label class="col-md-3 control-label" for="our_value">Наша доля</label>--}}
                        {{--<div class="col-md-3">--}}
                        {{--{{ Form::number('our_value', null, ['class' => 'form-control', 'placeholder' => 'Наша доля', 'disabled']) }}--}}
                        {{--@if($errors->has('our_value'))--}}
                        {{--<span class="help-block">{{ $errors->first('our_value') }}</span>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="our_value">Доля партнера</label>
                            <div class="col-md-3">
                                {{ Form::number('partner_value', null, ['class' => 'form-control', 'placeholder' => 'Доля партнера', 'disabled']) }}
                                @if($errors->has('partner_value'))
                                    <span class="help-block">{{ $errors->first('partner_value') }}</span>
                                @endif
                            </div>
                        </div>

                        {{--<div class="form-group">--}}
                        {{--<label class="col-md-3 control-label" for="our_value">Оплачено баллами</label>--}}
                        {{--<div class="col-md-3">--}}
                        {{--{{ Form::number('points_value', null, ['disabled','class' => 'form-control', 'placeholder' => 'Оплачено баллами']) }}--}}
                        {{--@if($errors->has('points_value'))--}}
                        {{--<span class="help-block">{{ $errors->first('points_value') }}</span>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="our_value">Рабочий комментарий</label>
                            <div class="col-md-9">
                                {{ Form::text('comment', null, ['disabled','class' => 'form-control', 'placeholder' => 'Рабочий комментарий']) }}
                                @if($errors->has('comment'))
                                    <span class="help-block">{{ $errors->first('comment') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">

                            <div class="block">
                                <!-- Block Title -->
                                <div class="block-title">
                                    <h2>Информация о заказчике</h2>
                                </div>
                                <!-- END Block Title -->


                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="our_value">Имя</label>
                                    <div class="col-md-9">
                                        {{ Form::text('client_name', null, ['disabled','class' => 'form-control', 'placeholder' => 'Имя заказчика']) }}
                                        @if($errors->has('client_name'))
                                            <span class="help-block">{{ $errors->first('client_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="our_value">Телефон</label>
                                    <div class="col-md-6">
                                        {{ Form::text('client_phone', null, ['disabled','class' => 'form-control', 'placeholder' => 'Телефон']) }}
                                        @if($errors->has('client_phone'))
                                            <span class="help-block">{{ $errors->first('client_phone') }}</span>
                                        @endif
                                    </div>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label class="col-md-3 control-label" for="our_value">Комментарий заказчика</label>--}}
                                    {{--<div class="col-md-6">--}}
                                        {{--{{ Form::textarea('client_comment', null, ['disabled','class' => 'form-control', 'placeholder' => 'Комментарий заказчика']) }}--}}
                                        {{--@if($errors->has('client_comment'))--}}
                                            {{--<span class="help-block">{{ $errors->first('client_comment') }}</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="our_value">Имя получателя</label>
                                    <div class="col-md-9">
                                        {{ Form::text('recipient_name', null, ['disabled','class' => 'form-control', 'placeholder' => 'Имя получателя']) }}
                                        @if($errors->has('recipient_name'))
                                            <span class="help-block">{{ $errors->first('recipient_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="our_value">Телефон получателя</label>
                                    <div class="col-md-6">
                                        {{ Form::text('recipient_phone', null, ['disabled','class' => 'form-control', 'placeholder' => 'Телефон']) }}
                                        @if($errors->has('recipient_phone'))
                                            <span class="help-block">{{ $errors->first('recipient_phone') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="our_value">Адрес получателя</label>
                                    <div class="col-md-9">
                                        {{ Form::text('recipient_address', null, ['disabled','class' => 'form-control', 'placeholder' => 'Адрес']) }}
                                        @if($errors->has('recipient_address'))
                                            <span class="help-block">{{ $errors->first('recipient_address') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="need_to_call">Требуется уточнить
                                        адрес</label>
                                    <div class="col-md-9">
                                        <label class="switch switch-info">
                                            {{ Form::checkbox('need_to_call',true,null,['disabled']) }}<span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="block">
                                <!-- Block Title -->
                                <div class="block-title">
                                    <h2>Состав заказа. Букеты.</h2>
                                </div>
                                <!-- END Block Title -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-text-input">Текст
                                        открытки</label>
                                    <div class="col-md-9">
                                        {{ Form::textarea('postcard_text', null, ['disabled','class' => 'form-control', 'placeholder' => 'Текст открытки']) }}
                                        @if($errors->has('postcard_text'))
                                            <span class="help-block">{{ $errors->first('postcard_text') }}</span>
                                        @endif
                                    </div>
                                </div>
                                @foreach($order->bouquets as $item)
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th colspan="1">{{$item->name_ru}}
                                                ({{size_to_text($item->pivot->bouquet_size)}})
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Цветок</th>
                                            <th>Количество</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($item->flowers()->wherePivot("size",$item->pivot->bouquet_size)->get() as $flower)
                                            <tr>
                                                <td>{{$flower->name_ru}}</td>
                                                <td>{{$flower->pivot->count}} шт.</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <hr/>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i>
                                    Сохранить
                                </button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>
                                    Сбросить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push("scripts")
@endpush