@extends('franchisee.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                {{Form::setModel($filter)}}
                <form id="orders-filter" action="{{route('franchisee.orders.index')}}" method="get">
                    <div class="row">
                        <div class="col-md-2 from-group">
                            <label class="control-label" for="franchisee_id">Статус:</label>
                            {{ Form::select('status', $statuses, null, ['class' => 'form-control', 'size' => 1]) }}
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Дата заказа от:</label>
                            {{ Form::text('date_start',null,['class' => 'form-control', 'placeholder' => 'дд.мм.гггг'])}}
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Дата заказа до:</label>
                            {{ Form::text('date_end',null,['class' => 'form-control', 'placeholder' => 'дд.мм.гггг'])}}
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">&nbsp;</label>
                            <input type="submit" class="form-control" value="Применить">
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">&nbsp;</label>
                            <a href="{{route('franchisee.orders.index')}}" class="form-control"
                               style="display:flex;align-items: center;justify-content: center;">Сбросить</a>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Дата/Время</th>
                                    <th>Созадн</th>
                                    <th>Номер заказа</th>
                                    <th>Статус</th>
                                    <th>Состав заказа</th>
                                    <th>Стоимость</th>
                                    {{--<th>Сумма Floristan</th>--}}
                                    <th>Сумма партнера</th>
                                    <th>Комментарий</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($orders as $order)
                                    <tr class="animate">
                                        <td>{{$order->delivery_date}}/{{$order->delivery_time}}</td>
                                        <td>{{$order->created_at->format("d.m.Y")}}</td>
                                        <td><a href="{{route('franchisee.orders.show',$order)}}">{{$order->id}}</a></td>
                                        <td>
                                            {{ Form::select("status",$statuses,$order->status,["class"=>"form-control","data-id"=>$order->id]) }}
                                        </td>
                                        <td>{{$order->getBouquetsListText()}}</td>
                                        <td>{{$order->total_value}} р.</td>
                                        {{--<td>{{$order->our_value}} р.</td>--}}
                                        <td>{{$order->partner_value}} р.</td>
                                        <td>{{$order->comment}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">Список заказов пуст.</td>
                                    <tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        @if(count($orders)>0)
                            {{ $orders->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        tr.animate td {
            transition: all 0.5s;
        }

        tr.animate.active td {
            background: #b0da98 !important;
        }
    </style>
@stop

@push("scripts")
<script type="text/javascript">
    $(function () {
        $("body").on("change", "select[name=status]", function () {
            var sel = $(this);
            var currentValue = sel.val();
            console.log("set", sel.attr("data-id"), sel.val());
            $.ajax({
                url: '/franchisee/orders/' + sel.attr('data-id'),
                type: 'PUT',
                data: {
                    status: sel.val()
                },
                processData: true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (e) {
                    sel.closest("tr").addClass("active");
                    setTimeout(function () {
                        sel.closest("tr").removeClass("active");
                    }, 500);
                }.bind(this)
            });
        });
    });
</script>

<script>
    $(function () {
        $(document).ready(function () {
            $('input[name="date_start"], input[name="date_end"]').datepicker({
                format: 'dd.mm.yy',
                firstDay: 1
            });
        });
    });
</script>

@endpush