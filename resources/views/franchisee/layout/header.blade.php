<div class="content-header">
  <ul class="nav-horizontal text-center">
    <li class="@include('franchisee.layout.inc.active_class', ['routeName' => 'franchisee.orders.*'])">
      <a href="{{ URL::route('franchisee.orders.index') }}"><i class="fa fa-signal"></i> Заказы</a>
    </li>
    <li class="@include('franchisee.layout.inc.active_class', ['routeName' => 'franchisee.flowers.*'])">
      <a href="{{ URL::route('franchisee.flowers.index') }}"><i class="gi gi-flower"></i> Цветы</a>
    </li>
    <li class="@include('franchisee.layout.inc.active_class', ['routeName' => 'franchisee.products.*'])">
      <a href="{{ URL::route('franchisee.products.index') }}"><i class="fa fa-gift"></i> Доп. товары</a>
    </li>
    <li class="@include("franchisee.layout.inc.active_class",['routeName'=>'franchisee.finance.*'])">
      <a href="{{route("franchisee.finance.index")}}"><i class="fa fa-credit-card"></i> Исотрия выводов</a>

    </li>
  </ul>
</div>