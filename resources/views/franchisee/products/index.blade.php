@extends('franchisee.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                {{ Form::open(['route'=>'franchisee.products.store','method'=>"POST"]) }}
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Цена</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($flowers as $flower)
                                    <tr>
                                        <td>
                                            {{$flower->name_ru}}
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    {{ Form::number('prices['.$flower->id.']', $flower->franchise_price, ['placeholder' => 'Цена', 'class' => 'form-control']) }}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group form-actions">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-0">
                                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i> Сохранить</button>
                                    <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Сбросить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop