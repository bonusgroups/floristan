<div class="content-header">
    <ul class="nav-horizontal text-center">
        <li class="@include('admin.layout.inc.active_class', ['routeName' => 'admin.franchisee.*'])">
            <a href="{{ URL::route('admin.franchisee.index') }}"><i class="fa fa-shopping-bag"></i> Франчайзи</a>
        </li>
        <li class="@include('admin.layout.inc.active_class', ['routeName' => 'admin.bouquet_category.*'])">
            <a href="{{ URL::route('admin.bouquet_category.index') }}"><i class="gi gi-snowflake"></i> Категории</a>
        </li>
        <li class="@include('admin.layout.inc.active_class', ['routeName' => 'admin.bouquet.*'])">
            <a href="{{ URL::route('admin.bouquet.index') }}"><i class="gi gi-snowflake"></i> Букеты</a>
        </li>
        <li class="@include('admin.layout.inc.active_class', ['routeName' => 'admin.flower.*'])">
            <a href="{{ URL::route('admin.flower.index') }}"><i class="gi gi-flower"></i> Цветы</a>
        </li>
        <li class="@include('admin.layout.inc.active_class', ['routeName' => 'admin.additional_product.*'])">
            <a href="{{ URL::route('admin.additional_product.index') }}"><i class="fa fa-gift"></i> Доп. товары</a>
        </li>
        <li class="@include('admin.layout.inc.active_class', ['routeName' => 'admin.orders.*'])">
            <a href="{{ URL::route('admin.orders.index') }}"><i class="fa fa-signal"></i> Заказы</a>
        </li>
        <li class="@include('admin.layout.inc.active_class', ['routeName' => 'admin.finance.*'])">
            <a href="{{URL::route('admin.finance.index')}}"><i class="fa fa-credit-card"></i> Финансы</a>
        </li>
        <li class="@include('admin.layout.inc.active_class', ['routeName' => 'admin.feedbacks.*'])">
            <a href="{{ URL::route('admin.feedbacks.index') }}"><i class="fa fa-comments"></i> Отзывы</a>
        </li>
        <li class="@include('admin.layout.inc.active_class', ['routeName' => 'admin.articles.*'])">
            <a href="{{ URL::route('admin.articles.index') }}"><i class="fa fa-list-alt"></i> Статьи</a>
        </li>
        <li class="@include('admin.layout.inc.active_class', ['routeName' => 'admin.promocode.*'])">
            <a href="{{ URL::route('admin.promocode.index') }}"><i class="fa fa-certificate"></i> Купоны</a>
        </li>
        <li class="@if (Request::is(['admin/settings', 'admin/settings/*'])) active @endif">
            <a href="{{ URL::route('admin.settings.index') }}"><i class="fa fa-gear"></i> Настройки</a>
        </li>
    </ul>

    @if (Request::is(['admin/settings', 'admin/settings/*']))
        <ul class="nav nav-pills">
            <li class="{{ Request::is(['admin/settings']) ? 'active' : null }}">
                <a href="{{ url('admin/settings') }}">Основное</a>
            </li>
            <li class="{{ Request::is(['admin/settings/bouquet_reason', 'admin/settings/bouquet_reason/*']) ? 'active' : null }}">
                <a href="{{ url('admin/settings/bouquet_reason') }}">Поводы для букетов</a>
            </li>
            <li class="{{ Request::is(['admin/settings/flower_for_filters', 'admin/settings/flower_for_filters/*']) ? 'active' : null }}">
                <a href="{{ url('admin/settings/flower_for_filters') }}">Цветы для фильтров</a>
            </li>
            <li class="<?php echo e(Request::is(['admin/settings/banner', 'admin/settings/banner/*']) ? 'active' : null); ?>">
                <a href="<?php echo e(url('admin/settings/banner')); ?>">Баннеры</a>
            </li>
        </ul>
    @endif
</div>

<style>
    .nav-pills {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
        padding: 10px 0;
        background-color: #f2f2f2;
    }
    .nav-pills > li {
        float: none;
        margin: 0 5px;
    }
    .nav-pills > li a:hover {
        background: none;
    }
</style>