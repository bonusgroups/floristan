@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-3">
                        <a href="{{ URL::route('admin.franchisee.create') }}" class="btn btn-success">Добавить <i
                                    class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Город</th>
                                    <th>Домен</th>
                                    <th>Почта</th>
                                    <th>Телефон</th>
                                    <th>Адрес</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($franchisees as $franchisee)
                                    <tr>
                                        <td>
                                            <a href="{{ URL::route('admin.franchisee.edit', $franchisee) }}">{{ $franchisee->city_ru }}</a>
                                        </td>
                                        <td>
                                            <a href="{{ URL::to(URL::formatScheme(false) . $franchisee->domain) }}">
                                                {{ $franchisee->domain }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $franchisee->email }}
                                        </td>
                                        <td>
                                            {{ $franchisee->phone }}
                                        </td>
                                        <td>
                                            {{ $franchisee->address_ru }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        {{ $franchisees->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop