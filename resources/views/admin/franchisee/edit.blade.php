@extends('admin.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {{ Form::model($franchisee, ['route' => ['admin.franchisee.update', $franchisee], 'method' => 'put', 'class' => 'form-horizontal']) }}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Город (Ru) <span
                                        class="text-danger">*</span></label>

                            <div class="col-md-9">
                                {{ Form::text('city_ru', null, ['placeholder' => 'Город (Ru)', 'class' => 'form-control']) }}
                                @if($errors->has('city_ru'))
                                    <span class="help-block">{{ $errors->first('city_ru') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Город (En) <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('city_en', null, ['placeholder' => 'Город (En)', 'class' => 'form-control']) }}
                                @if($errors->has('city_en'))
                                    <span class="help-block">{{ $errors->first('city_en') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Название в предложном падеже</label>
                            <div class="col-md-9">
                                {{ Form::text('prepositional_name', null, [
                                    'placeholder' => 'Название в предложном падеже',
                                    'class' => 'form-control'
                                ]) }}

                                @if($errors->has('prepositional_name'))
                                    <span class="help-block">{{ $errors->first('prepositional_name') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Домен <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('domain', null, ['placeholder' => 'Домен', 'class' => 'form-control']) }}
                                @if($errors->has('domain'))
                                    <span class="help-block">{{ $errors->first('domain') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Почта <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('email', null, ['placeholder' => 'Почта', 'class' => 'form-control']) }}
                                @if($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Пароль </label>
                            <div class="col-md-9">
                                {{ Form::password('password', ['placeholder' => 'Пароль', 'class' => 'form-control']) }}
                                @if($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Повторите пароль </label>
                            <div class="col-md-9">
                                {{ Form::password('password_confirm', ['placeholder' => 'Повторите пароль', 'class' => 'form-control']) }}
                                @if($errors->has('password_confirm'))
                                    <span class="help-block">{{ $errors->first('password_confirm') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Телефон <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('phone', null, ['placeholder' => 'Телефон', 'class' => 'form-control']) }}
                                @if($errors->has('phone'))
                                    <span class="help-block">{{ $errors->first('phone') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Адрес (Ru) <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('address_ru', null, ['placeholder' => 'Адрес (Ru)', 'class' => 'form-control']) }}
                                @if($errors->has('address_ru'))
                                    <span class="help-block">{{ $errors->first('address_ru') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Адрес (En) <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('address_en', null, ['placeholder' => 'Адрес (En)', 'class' => 'form-control']) }}
                                @if($errors->has('address_en'))
                                    <span class="help-block">{{ $errors->first('address_en') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Время работы</label>
                            <div class="col-md-9">
                                {{ Form::text('opening_hours', null, ['placeholder' => 'Время работы', 'class' => 'form-control']) }}
                                @if($errors->has('opening_hours'))
                                    <span class="help-block">{{ $errors->first('opening_hours') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Комиссия за букеты, % <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('commission_for_bouquets', null, ['placeholder' => 'Комиссия за букеты, %', 'class' => 'form-control']) }}
                                @if($errors->has('commission_for_bouquets'))
                                    <span class="help-block">{{ $errors->first('commission_for_bouquets') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label" for="#">
                                Коэффициент стоимости доп. товаров (%) <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-9">
                                {{ Form::text('commission_for_additional', null, ['placeholder' => 'Коэффициент стоимости доп. товаров (%)', 'class' => 'form-control']) }}

                                @if($errors->has('commission_for_additional'))
                                    <span class="help-block">{{ $errors->first('commission_for_additional') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Реквизиты</label>
                            <div class="col-md-9">
                                {{ Form::textarea('requisites', null, ['placeholder' => 'Реквизиты', 'class' => 'form-control']) }}
                                @if($errors->has('requisites'))
                                    <span class="help-block">{{ $errors->first('requisites') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Ссылка Вконтакте</label>
                            <div class="col-md-9">
                                {{ Form::text('social_vk', null, ['placeholder' => 'Реквизиты', 'class' => 'form-control']) }}
                                @if($errors->has('social_vk'))
                                    <span class="help-block">{{ $errors->first('social_vk') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Ссылка Facebook</label>
                            <div class="col-md-9">
                                {{ Form::text('social_fb', null, ['placeholder' => 'Ссылка Facebook', 'class' => 'form-control']) }}
                                @if($errors->has('social_fb'))
                                    <span class="help-block">{{ $errors->first('social_fb') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Ссылка Instagram</label>
                            <div class="col-md-9">
                                {{ Form::text('social_ig', null, ['placeholder' => 'Ссылка Instagram', 'class' => 'form-control']) }}
                                @if($errors->has('social_ig'))
                                    <span class="help-block">{{ $errors->first('social_ig') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Ссылка WhatsApp</label>
                            <div class="col-md-9">
                                {{ Form::text('social_wa', null, ['placeholder' => 'Ссылка WhatsApp', 'class' => 'form-control']) }}
                                @if($errors->has('social_wa'))
                                    <span class="help-block">{{ $errors->first('social_wa') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Ссылка Telegram</label>
                            <div class="col-md-9">
                                {{ Form::text('social_tg', null, ['placeholder' => 'Ссылка Telegram', 'class' => 'form-control']) }}
                                @if($errors->has('social_tg'))
                                    <span class="help-block">{{ $errors->first('social_tg') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Ссылка Viber</label>
                            <div class="col-md-9">
                                {{ Form::text('social_vb', null, ['placeholder' => 'Ссылка Viber', 'class' => 'form-control']) }}
                                @if($errors->has('social_vb'))
                                    <span class="help-block">{{ $errors->first('social_vb') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Метатег yandex_verification</label>
                            <div class="col-md-9">
                                {{ Form::text('meta_yandex_verification', null, ['placeholder' => 'Метатег yandex_verification', 'class' => 'form-control']) }}
                                @if($errors->has('meta_yandex_verification'))
                                    <span class="help-block">{{ $errors->first('meta_yandex_verification') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Метатег google_verification</label>
                            <div class="col-md-9">
                                {{ Form::text('meta_google_verification', null, ['placeholder' => 'Метатег google_verification', 'class' => 'form-control']) }}
                                @if($errors->has('meta_google_verification'))
                                    <span class="help-block">{{ $errors->first('meta_google_verification') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i>
                                    Сохранить
                                </button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>
                                    Сбросить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}

                        {{ Form::open(['route' => ['admin.franchisee.destroy', $franchisee], 'method' => 'delete', 'class' => 'form-horizontal']) }}


                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-danger"
                                        onclick="return confirm('Удалить франчайзи?');">
                                    <i class="fa fa-times"></i> Удалить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="block">
                            <div class="block-title">
                                <h2>Доставка</h2>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-horizontal">
                                        @foreach($franchisee->deliveries as $delivery)
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"
                                                       for="example-text-input">{{ $delivery->name_ru }}</label>
                                                <div class="col-md-3">
                                                    {{ Form::number(null, $delivery->price, ['class' => 'form-control', 'disabled' => true]) }}
                                                </div>
                                                <div class="col-md-3">
                                                    {{ Form::open(['url' => URL::route('admin.delivery.destroy', [
                                                      'delivery' => $delivery,
                                                      'franchisee' => $franchisee,
                                                    ]), 'method' => 'delete']) }}
                                                    <button type="submit" class="btn btn-danger"
                                                            onclick="return confirm('Удалить тип доставки?');">
                                                        <i class="fa fa-times-circle"></i>
                                                    </button>
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    {{ Form::open(['url' => URL::route('admin.delivery.store', ['franchisee_id' => $franchisee]), 'action' => 'post', 'class' => 'form-horizontal']) }}

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-text-input">Название
                                            (Ru)</label>
                                        <div class="col-md-9">
                                            {{ Form::text('name_ru', null, ['class' => 'form-control', 'placeholder' => 'Название (Ru)']) }}
                                        </div>
                                        @if($errors->has('name_ru'))
                                            <span class="help-block">{{ $errors->first('name_ru') }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-text-input">Название
                                            (En)</label>
                                        <div class="col-md-9">
                                            {{ Form::text('name_en', null, ['class' => 'form-control', 'placeholder' => 'Название (En)']) }}
                                        </div>
                                        @if($errors->has('name_en'))
                                            <span class="help-block">{{ $errors->first('name_en') }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-text-input">Цена</label>
                                        <div class="col-md-9">
                                            {{ Form::number('price', null, ['class' => 'form-control', 'placeholder' => 'Цена']) }}
                                        </div>
                                    </div>

                                    <div class="form-group form-actions">
                                        <div class="col-md-9 col-md-offset-3">
                                            <button type="submit" class="btn btn-sm btn-success"><i
                                                        class="fa fa-angle-right"></i> Добавить
                                            </button>
                                        </div>
                                    </div>

                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop