<div class="form-group">
  <label class="col-md-3 control-label" for="franchisee_id">Франчайзи</label>
  <div class="col-md-3">
    {{ Form::select('franchisee_id', $franchisees->pluck('city_ru', 'id'), null, ['class' => 'form-control', 'size' => 1]) }}
  </div>
</div>