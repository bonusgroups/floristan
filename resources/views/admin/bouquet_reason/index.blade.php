@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-3">
                        <a href="{{ URL::route('admin.bouquet_reason.create') }}" class="btn btn-success">Добавить <i
                                    class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $model)
                                    <tr>
                                        <td>
                                            {{ Html::link(URL::route('admin.bouquet_reason.edit', $model), $model->name_ru) }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        {{ $items->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop