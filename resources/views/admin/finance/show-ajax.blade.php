<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-8">
                <label>Город</label>
            </div>
            <div class="col-sm-4">
                <label>{{$franchisee->city_ru}}</label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <label>Всего заработано</label>
            </div>
            <div class="col-sm-4">
                <label>{{price_format($franchisee->total_cash)}}</label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <label>Всего выведено</label>
            </div>
            <div class="col-sm-4">
                <label>{{price_format($franchisee->total_payment)}}</label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <label>Дата последнего вывода</label>
            </div>
            <div class="col-sm-4">
                <label>{{$franchisee->total_date?$franchisee->total_date->format("d.m.Y"):"нет"}}</label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <label>Текущий баланс</label>
            </div>
            <div class="col-sm-4">
                <label>{{price_format($franchisee->balance)}}</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-12">
                <div class="rows-scroll">
                    @if(!sizeof($logs))
                    <span>Выплат не было</span>
                    @endif
                    <table class="table">
                        @foreach($logs as $log)
                        <tr data-id="{{$log->id}}">
                            <td><label>{{$log->created_at->format("d.m.Y")}}</label></td>
                            <td><label>{{price_format($log->append)}}</label> </td>
                            <td width="48"><button class="btn btn-sm btn-link btn-remove"><i class="fa fa-remove"></i></button></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <input type="date" class="form-control" value="{{$today}}"/>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <input type="number" class="form-control" value="{{$franchisee->balance}}"/>
                </div>
            </div>
            <div class="col-sm-6">
                <button class="btn btn-primary full add" style="width:100%;" data-id="{{$franchisee->id}}"><i class="fa fa-plus"></i> Добавить</button>
            </div>
        </div>
    </div>
</div>