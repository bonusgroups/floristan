@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-sm-12"></div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Город</th>
                                    <th>Всего заработано</th>
                                    <th>Всего выведено</th>
                                    <th>Дата последнего вывода денег</th>
                                    <th>Текущий баланс</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($franchisees as $franchisee)
                                    <tr data-open="{{ URL::route('admin.finance.show', $franchisee) }}">
                                        <td>
                                            {{ $franchisee->city_ru }}
                                        </td>
                                        <td>
                                            {{$franchisee->total_cash}}
                                        </td>
                                        <td>
                                            {{$franchisee->total_payment}}
                                        </td>
                                        <td>
                                            {{ $franchisee->total_date ? $franchisee->total_date->format("d.m.Y") : "нет"}}
                                        </td>
                                        <td>
                                            {{ $franchisee->balance }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        {{$franchisees->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-align: center">Информация/История выводов франчайзи </h4>
                </div>
                <div class="modal-body">
                    <p>text place here</p>
                </div>
                <div class="modal-footer" style="text-align: center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <style>
        [data-open] {
            transition: all 0.3s;
        }

        [data-open]:hover {
            background:#ff6b6b!important;
            color:#FFF;
            cursor: pointer;
        }

        .rows-scroll {
            height:180px;
            overflow-y: scroll;
            overflow-x: hidden;
            border:1px solid #e8e8e8;
            border-radius: 4px;
            margin-bottom:10px;
            padding:10px;
        }

        .rows-scroll span {
            display:flex;
            align-items: center;
            height:100%;
            justify-content: center;
            color:#EAEAEA;
        }
    </style>
@stop

@push('scripts')
<script>
    function setLoading() {
        $("#myModal .modal-body").html("").addClass("loading");
    }

    function setLoaded() {
        $("#myModal .modal-body").removeClass("loading");
    }

    function loadUrl($url) {
        setLoading();
        $('#myModal').modal('show');
        $.get($url,{},function(response){
            $("#myModal .modal-body").html(response);
            setLoaded();
        });
    }

    var $surl="";

    $(function(){
        $('#myModal').modal({
            show: false
        });
        $("body").on("click","[data-open]",function(e) {
            e.preventDefault();
            $surl = $(this).attr("data-open");
            loadUrl($surl);
            return false;
        });

        $("body").on("click","#myModal button.add",function() {
            var sum = $("#myModal input[type=number]").val();
            var date = $("#myModal input[type=date]").val();
            var id = $(this).attr("data-id");
            $.ajax({
                url: '{{route('admin.finance.store')}}',
                type:'POST',
                data:{
                    sum:sum,
                    date:date,
                    franchisee_id:id
                },
                processData:true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(e) {
                    loadUrl($surl);
                }
            });
            return false;
        });

        $("body").on("click","#myModal .btn-remove",function() {
            var row = $(this).closest("[data-id]");
            var id = $(row).attr("data-id");
            $.ajax({
                url: '/admin/finance/'+id,
                type:'DELETE',
                data:{
                    franchisee_id:id
                },
                processData:true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(e) {
                    loadUrl($surl);
                }
            });
            return false;
        });
    });
</script>
@endpush