@extends('admin.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {{ Form::open(['route' => 'admin.bouquet_category.store', 'method' => 'post', 'class' => 'form-horizontal']) }}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Название (Ru) <span
                                        class="text-danger">*</span></label>

                            <div class="col-md-9">
                                {{ Form::text('name_ru', null, ['placeholder' => 'Название (Ru)', 'class' => 'form-control']) }}
                                @if($errors->has('name_ru'))
                                    <span class="help-block">{{ $errors->first('name_ru') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Название (En) <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('name_en', null, ['placeholder' => 'Название (En)', 'class' => 'form-control']) }}
                                @if($errors->has('name_en'))
                                    <span class="help-block">{{ $errors->first('name_en') }}</span>
                                @endif
                            </div>
                        </div>

                        @include('admin.form.text', [
                            'name' => 'title',
                            'label' => 'Title'
                        ])

                        @include('admin.form.textarea', [
                            'name' => 'description',
                            'label' => 'Meta description'
                        ])

                        @include('admin.form.text', [
                            'name' => 'h1',
                            'label' => 'H1'
                        ])

                        @include('admin.form.text', [
                            'name' => 'seo_title',
                            'label' => 'Заголовок сео-текста (внизу)'
                        ])

                        @include('admin.form.textarea', [
                            'name' => 'text',
                            'label' => 'Описание',
                            'editor' => true
                        ])

                        @include('admin.form.text', [
                            'name' => 'slug',
                            'label' => 'Slug (используется в URL)'
                        ])

                        @include('admin.form.checkbox', [
                            'name' => 'published',
                            'label' => 'Опубликовано'
                        ])

                        @include('admin.form.select', [
                            'name' => 'parent_id',
                            'label' => 'Родительская категория',
                            'options' => ['- не выбрано -'] + \App\Models\BouquetCategory::getParentOptions()
                        ])

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i>
                                    Сохранить
                                </button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>
                                    Сбросить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop