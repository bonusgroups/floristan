@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-3">
                        <a href="{{ URL::route('admin.bouquet_category.create') }}" class="btn btn-success">Добавить <i
                                    class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Опубликовано</th>
                                </tr>
                                </thead>
                                <tbody id="bouquet-categories-list">
                                @foreach($items as $model)
                                    <tr data-id="{{ $model->id }}">
                                        <td>
                                            <div class="sort-icon" data-handle>
                                                <img src="{{ url('img/admin/sort.svg') }}"/>
                                            </div>
                                            {{ Html::link(URL::route('admin.bouquet_category.edit', $model), $model->name_ru) }}
                                        </td>
                                        <td>{{ $model->published ? 'Да' : 'Нет' }}</td>
                                    </tr>

                                    @foreach($model->children as $children)
                                        <tr class="children" data-id="{{ $children->id }}">
                                            <td>
                                                <div class="sort-icon" data-handle>
                                                    <img src="{{ url('img/admin/sort.svg') }}"/>
                                                </div>
                                                {{ Html::link(URL::route('admin.bouquet_category.edit', $children), $children->name_ru) }}
                                            </td>
                                            <td>{{ $children->published ? 'Да' : 'Нет' }}</td>
                                        </tr>
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        {{ $items->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <style>
        .table > tbody > tr.children > td {
            padding-left: 30px;
        }

        .sort-icon {
            position: relative;
            top: -1px;
            display: inline-block;
            vertical-align: middle;
            margin-right: 5px;
            cursor: pointer;
        }

        .sort-icon img {
            width: 13px;
            height: 13px;
            display: block;
        }
    </style>
@stop

@section('js')
    <script>
        $('#bouquet-categories-list')
                .each(function () {
                    new Sortable(this, {
                        animation: 150,
                        handle: '[data-handle]',
                        onSort: function (event) {
                            var ids = [];

                            $(event.to).find('tr').each(function () {
                                ids.push($(this).attr('data-id'));
                            });

                            $.ajax('/admin/ajax/bouquet-category-sort', {
                                type: 'post',
                                data: {
                                    ids: ids
                                }
                            });
                        }
                    });
                });
    </script>
@stop
