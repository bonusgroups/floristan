@extends('admin.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {{ Form::open(['route' => 'admin.promocode.store', 'method' => 'post', 'class' => 'form-horizontal']) }}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Код<span class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('code', null, ['placeholder' => 'Код', 'class' => 'form-control']) }}

                                @if($errors->has('code'))
                                    <span class="help-block">{{ $errors->first('code') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Сумма, р.</label>
                            <div class="col-md-9">
                                {{ Form::text('discount', null, ['placeholder' => 'Сумма, р.', 'class' => 'form-control']) }}

                                @if($errors->has('discount'))
                                    <span class="help-block">{{ $errors->first('discount') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Максимальный %</label>
                            <div class="col-md-9">
                                {{ Form::text('percent', null, ['placeholder' => 'Максимальный %', 'class' => 'form-control']) }}

                                @if($errors->has('percent'))
                                    <span class="help-block">{{ $errors->first('percent') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Количество</label>
                            <div class="col-md-9">
                                {{ Form::text('count', null, ['placeholder' => 'Количество', 'class' => 'form-control']) }}

                                @if($errors->has('count'))
                                    <span class="help-block">{{ $errors->first('count') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Дата создания</label>
                            <div class="col-md-9">
                                {{ Form::text('created_at', null, ['placeholder' => 'Дата создания', 'class' => 'form-control']) }}

                                @if($errors->has('created_at'))
                                    <span class="help-block">{{ $errors->first('created_at') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Комментарий</label>
                            <div class="col-md-9">
                                {{ Form::textarea('comment', null, ['placeholder' => 'Комментарий', 'class' => 'form-control']) }}

                                @if($errors->has('comment'))
                                    <span class="help-block">{{ $errors->first('comment') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i>
                                    Сохранить
                                </button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>
                                    Сбросить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop