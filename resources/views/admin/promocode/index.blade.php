@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-3">
                        <a href="{{ URL::route('admin.promocode.create') }}" class="btn btn-success">Добавить <i
                                    class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Код</th>
                                    <th>Сумма, р.</th>
                                    <th>Максимальный %</th>
                                    <th>Комментарий</th>
                                    <th>Количество использований</th>
                                    <th>Дата создания купона</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($promocodes as $promocode)
                                    <tr>
                                        <td>
                                            <a href="{{ URL::route('admin.promocode.edit', $promocode) }}">{{ $promocode->code }}</a>
                                        </td>
                                        <td>{{ $promocode->discount }}</td>
                                        <td>{{ $promocode->percent }}</td>
                                        <td>{{ $promocode->comment }}</td>
                                        <td>{{ $promocode->count }}</td>
                                        <td>{{ $promocode->created_at->format('d.m.Y') }}</td>
                                        <td>{{ $promocode->code }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        {{ $promocodes->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')

<script>
    $(function () {
        $(document).ready(function () {
            $('input[name="created_at"]').datepicker({
                format: 'dd.mm.yy h:i',
                firstDay: 1
            });
        });
    });
</script>

@endpush