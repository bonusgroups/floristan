@php($uniqid = uniqid())

<div class="form-group">
    <label class="col-md-3 control-label" for="form-field-{{ $uniqid }}">
        {{ $label }}
    </label>

    @php($options = [
        'id' => 'form-field-' . $uniqid,
        'placeholder' => $label,
        'class' => 'form-control',
        'rows' => 4
    ])

    @if(!empty($editor))
        @php($options['data-enable-editor'] = 1)
    @endif

    <div class="col-md-9">
        {{ Form::textarea($name, null, $options) }}

        @if($errors->has($name))
            <span class="help-block">{{ $errors->first($name) }}</span>
        @endif
    </div>
</div>