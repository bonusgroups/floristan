@php($uniqid = uniqid())

<div class="form-group">
    <label class="col-md-3 control-label" for="form-field-{{ $uniqid }}">
        {{ $label }}
    </label>
    <div class="col-md-9">
        <label class="switch switch-success">
            {{ Form::checkbox($name) }}<span></span>
        </label>
    </div>
</div>