@php($uniqid = uniqid())

<div class="form-group">
    <label class="col-md-3 control-label" for="form-field-{{ $uniqid }}">
        {{ $label }}
    </label>

    <div class="col-md-9">
        {{ Form::select($name, $options, null, [
            'id' => 'form-field-' . $uniqid,
            'class' => 'form-control'
        ]) }}

        @if($errors->has($name))
            <span class="help-block">{{ $errors->first($name) }}</span>
        @endif
    </div>
</div>