@extends('admin.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {{ Form::model($feedback, [
                            'route' => ['admin.feedbacks.update', $feedback],
                            'method' => 'put',
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) }}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Имя</label>
                            <div class="col-md-9">
                                {{ Form::text('name', $feedback->name, ['class' => 'form-control']) }}

                                @if($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>

                        @include('admin.form.date', [
                            'name' => 'created_at',
                            'label' => 'Дата публикации'
                        ])

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="#">Оценка</label>
                            <div class="col-md-9">
                                {{ Form::select('rating', [
                                    0 => '- не выбрано -',
                                    1 => 1,
                                    2 => 2,
                                    3 => 3,
                                    4 => 4,
                                    5 => 5
                                ], null, ['class' => 'form-control']) }}

                                @if($errors->has('rating'))
                                    <span class="help-block">{{ $errors->first('rating') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Ссылка ВК</label>
                            <div class="col-md-9">
                                {{ Form::text('vk_link', $feedback->vk_link, ['class' => 'form-control']) }}

                                @if($errors->has('vk_link'))
                                    <span class="help-block">{{ $errors->first('vk_link') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Текст отзыва</label>

                            <div class="col-md-9">
                                {{ Form::textarea('description', $feedback->description, ['class' => 'form-control']) }}

                                @if($errors->has('description'))
                                    <span class="help-block">{{ $errors->first('description') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Фото букета</label>

                            <div class="col-md-9">
                                {{ Form::file('photo', ['class' => 'form-control']) }}

                                @if($errors->has('photo'))
                                    <span class="help-block">{{ $errors->first('photo') }}</span>
                                @endif

                                @if ($feedback->hasPhoto())
                                    <img src="{{$feedback->photo}}" style="max-width:80%;max-height:200px;"/>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Фото пользователя</label>

                            <div class="col-md-9">
                                {{ Form::file('avatar', ['class' => 'form-control']) }}

                                @if($errors->has('avatar'))
                                    <span class="help-block">{{ $errors->first('avatar') }}</span>
                                @endif

                                @if ($feedback->hasAvatar())
                                    <img src="{{$feedback->avatar}}" style="max-width:80%;max-height:200px;"/>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="is_active">Опубликован</label>
                            <div class="col-md-9">
                                <label class="switch switch-primary">
                                    {{ Form::checkbox('active') }}<span></span>

                                    @if($errors->has('active'))
                                        <span class="help-block">{{ $errors->first('active') }}</span>
                                    @endif
                                </label>
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i>
                                    Сохранить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}

                        {{ Form::open(['route' => ['admin.feedbacks.destroy', $feedback], 'method' => 'delete', 'class' => 'form-horizontal']) }}

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Удалить отзыв?');">
                                    <i class="fa fa-times"></i> Удалить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop