@extends('admin.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {{ Form::model($feedback, ['route' => ['admin.feedbacks.update', $feedback], 'method' => 'put', 'class' => 'form-horizontal']) }}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Имя</label>

                            <div class="col-md-9">
                                {{ Form::text('name', $feedback->name, ['placeholder' => 'Город', 'class' => 'form-control','disabled']) }}
                            </div>
                        </div>
                        @if(!empty($feedback->vk_link))
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Ссылка ВК</label>

                            <div class="col-md-9">
                                <a href="{{$feedback->vk_link}}" style="margin-top:7px;display:inline-block;" target="_blank">{{$feedback->vk_link}}</a>
                            </div>
                        </div>
                        @endif

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Текст отзыва</label>

                            <div class="col-md-9">
                                {{ Form::textarea('description', $feedback->description, ['placeholder' => 'Описание', 'class' => 'form-control','disabled']) }}
                            </div>
                        </div>
                        @if($feedback->hasPhoto())
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Фото букета</label>

                            <div class="col-md-9">
                                <img src="{{$feedback->photo}}" style="max-width:80%;max-height:200px;"/>
                            </div>
                        </div>
                        @endif

                        @if($feedback->hasAvatar())
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-text-input">Фото пользователя</label>

                                <div class="col-md-9">
                                    <img src="{{$feedback->avatar}}" style="max-width:80%;max-height:200px;"/>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="is_active">Опубликован</label>
                            <div class="col-md-9">
                                <label class="switch switch-primary">
                                    {{ Form::checkbox('active') }}<span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i> Сохранить </button>
                            </div>
                        </div>

                        {{ Form::close() }}

                        {{ Form::open(['route' => ['admin.feedbacks.destroy', $feedback], 'method' => 'delete', 'class' => 'form-horizontal']) }}


                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Удалить отзыв?');">
                                    <i class="fa fa-times"></i> Удалить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop