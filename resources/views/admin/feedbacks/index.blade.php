@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">

                <div class="row">
                    <div class="col-md-3">
                        <a href="{{ url('admin/feedbacks/create') }}" class="btn btn-success">Добавить <i
                                    class="fa fa-plus"></i></a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Имя</th>
                                    <th>Статус</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($feedbacks as $feedback)
                                    <tr>
                                        <td>
                                            <a href="{{ URL::route('admin.feedbacks.edit', $feedback) }}">{{ $feedback->name }}</a>
                                        </td>
                                        <td>
                                            {{ $feedback->active?'Активный':'Ждет модерации' }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        {{ $feedbacks->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop