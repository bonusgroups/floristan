@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-sm-12">
                        {{Form::open(["route"=>"admin.settings.store","method"=>"POST"])}}

                        {{Form::setModel($settings)}}

                        <div class="form-group row">
                            <label class="col-md-3 control-label" for="example-text-input">
                                Коэффициент стоимости букета (%)
                            </label>
                            <div class="col-md-9">
                                {{ Form::text('mul_price', null, ['placeholder' => 'Коэффициент стоимости букета (%)', 'class' => 'form-control']) }}
                                @if($errors->has('mul_price'))
                                    <span class="help-block">{{ $errors->first('mul_price') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 control-label" for="#">
                                Коэффициент стоимости доп. товаров (%)
                            </label>
                            <div class="col-md-9">
                                {{ Form::text('mul_price_additional', null, ['placeholder' => 'Коэффициент стоимости доп. товаров (%)', 'class' => 'form-control']) }}
                                @if($errors->has('mul_price_additional'))
                                    <span class="help-block">{{ $errors->first('mul_price_additional') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 control-label" for="#">
                                Api-ключ от sms.ru
                            </label>
                            <div class="col-md-9">
                                {{ Form::text('sms_api_key', null, ['placeholder' => 'Api-ключ от sms.ru', 'class' => 'form-control']) }}
                                @if($errors->has('sms_api_key'))
                                    <span class="help-block">{{ $errors->first('sms_api_key') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 control-label" for="#">E-mail</label>
                            <div class="col-md-9">
                                {{ Form::text('email', null, ['placeholder' => 'E-mail', 'class' => 'form-control']) }}
                                @if($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-actions row">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i>
                                    Сохранить
                                </button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>
                                    Сбросить
                                </button>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')

<script>


</script>

@endpush

