<div class="row">
  <div class="col-md-12">
    <div class="widget">
      <div class="widget-advanced widget-advanced-alt">
        <!-- Widget Main -->
        <div class="widget-main">
          {{ Form::open(['route' => 'admin.bouquet_image.store', 'method' => 'post', 'class' => 'dropzone', 'id' => 'bouquet_images']) }}
          {{ Form::hidden('bouquet_id', $bouquet->id) }}
          <div class="fallback">
            <input name="file" type="file" multiple/>
          </div>
          {{ Form::close() }}
          <div class="gallery gallery-widget" data-toggle="lightbox-gallery">
            <div class="row">
              @foreach($bouquet->images as $image)
                <div class="col-xs-6 col-sm-3">
                  <img src="{{ $image->url }}" alt="{{ $bouquet->name_ru }}">
                  <div>
                    {{ Form::open(['route' => ['admin.bouquet_image.destroy', $image], 'method' => 'delete']) }}
                    {{ Form::submit('Удалить', ['class' => 'btn btn-danger']) }}
                    {{ Form::close() }}
                  </div>
                </div>
              @endforeach
            </div>
          </div>
          <!-- END Widget Main -->
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')

  <script>
    Dropzone.options.bouquetImages = {
      uploadMultiple: true,
      success: function () {
        location.reload();
      }
    };
  </script>

@endpush