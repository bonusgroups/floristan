@include('admin.bouquet.inc.consist.consist_type_block', [
  'size_name' => 'Средний размер',
  'bouquet_size' => \App\Models\Bouquet::SIZE_MIDDLE
])

@include('admin.bouquet.inc.consist.consist_type_block', [
  'size_name' => 'Большой размер',
  'bouquet_size' => \App\Models\Bouquet::SIZE_BIG
])

@include('admin.bouquet.inc.consist.consist_type_block', [
  'size_name' => 'Малый размер',
  'bouquet_size' => \App\Models\Bouquet::SIZE_SMALL
])