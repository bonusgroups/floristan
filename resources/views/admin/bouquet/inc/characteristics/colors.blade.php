<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="block">
            <!-- Block Title -->
            <div class="block-title">
                <h2>Цвета</h2>
            </div>
            <!-- END Block Title -->

            <div class="row">
                @foreach($bouquet->colors as $color)
                    <div class="col-md-2">
                        {{ Form::open(['route' => ['admin.bouquet.detach_color', $bouquet], 'method' => 'post', 'style' => 'float: left']) }}

                        <button type="submit" name="color_id" value="{{ $color->id }}"
                                onclick="return confirm('Отвязать цвет?')"
                                class="btn" style="background-color: {{ $color->hex_value }}">
                            Отвязать
                        </button>

                        {{ Form::close() }}
                    </div>
                @endforeach
            </div>

            <br>

            <div class="row">
                {{ Form::open(['route' => 'admin.bouquet_color.store', 'class' => 'form-horizontal', 'method' => 'post']) }}
                {{ Form::hidden('bouquet_id', $bouquet->id) }}

                <div class="form-group">
                    <label class="col-md-4 control-label" for="hex_value">Цвет</label>
                    <div class="col-md-6">
                        <div class="input-group input-colorpicker">
                            <input type="text" id="hex_value" name="hex_value" class="form-control" value="#1bbae1">
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-sm btn-success">
                            <i class="fa fa-angle-right"></i> Добавить
                        </button>
                    </div>
                </div>

                <div class="form-group item-all-colors">
                    <div class="col-md-8 col-md-offset-4">
                        @foreach ($allColors as $color)
                            <button type="submit"
                                    name="color_id"
                                    value="{{ $color->id }}"
                                    class="btn btn-sm"
                                    style="background-color: {{ $color->hex_value }}; color: #fff; text-shadow: 1px 1px #333"
                            >
                                {{ $color->hex_value }}
                            </button>
                        @endforeach
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>