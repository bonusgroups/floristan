@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">

                <form action="#">
                    <table class="table table-vcenter table-striped">
                        <thead>
                        <tr>
                            <th scope="col" colspan="2">Показывать в фильтре</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                            <tr>
                                <td style="width: 100px;">
                                    <label class="switch switch-success">
                                        {{ Form::checkbox('flowers[]', $item->id, $item->show_in_filters) }}
                                        <span></span>
                                    </label>
                                </td>
                                <td>{{ field($item, 'name') }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </form>

            </div>
        </div>
    </div>
@stop


@push('scripts')

<script>
    (function ($) {

        $('input[name="flowers[]"]').change(function(){
            var $this = $(this);

            var id = $this.val();
            var checked = $this.prop('checked');

            $.ajax('/admin/settings/flower_for_filters/change', {
                type: 'post',
                data: {
                    id: id,
                    value: checked ? 1 : 0,
                    '_token': "{{ csrf_token() }}"
                }
            });
        });
    })(jQuery);
</script>

@endpush
