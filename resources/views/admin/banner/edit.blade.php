@extends('admin.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <div class="block">
                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        {{ Form::model($banner, [
                            'route' => ['admin.banner.update', $banner],
                            'method' => 'put',
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) }}

                        {{--<div class="form-group">--}}
                            {{--<label class="col-md-3 control-label" for="example-text-input">Тип</label>--}}
                            {{--<div class="col-md-9">--}}
                                {{--{{ Form::select('type', \App\Models\Banner::getTypeOptions(), null, [--}}
                                    {{--'class' => 'form-control'--}}
                                {{--]) }}--}}

                                {{--@if($errors->has('type'))--}}
                                    {{--<span class="help-block">{{ $errors->first('type') }}</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="#">Ссылка</label>
                            <div class="col-md-9">
                                {{ Form::text('url', null, [
                                    'class' => 'form-control'
                                ]) }}

                                @if ($errors->has('url'))
                                    <span class="help-block">{{ $errors->first('url') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="#">Файл</label>
                            <div class="col-md-9">
                                {{ Form::file('image') }}

                                @if($errors->has('image'))
                                    <span class="help-block">{{ $errors->first('image') }}</span>
                                @endif

                                @if ($image)
                                    <img style="max-width: 200px; height: auto; margin: 10px 0;" src="{{ $image->getUrl() }}" alt="">
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-angle-right"></i> Сохранить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop