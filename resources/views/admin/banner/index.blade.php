@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Версия сайта</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($banners as $banner)
                                    <tr>
                                        <td>
                                            <a href="{{ url('admin/settings/banner', [$banner->id, 'edit']) }}">
                                                {{ $banner->getTypeName() }}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        {{ $banners->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop