@extends('admin.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {{ Form::model($article, ['route' => ['admin.articles.update', $article], 'method' => 'put', 'class' => 'form-horizontal']) }}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Slug<span
                                        class="text-danger">*</span></label>

                            <div class="col-md-9">
                                {{ Form::text('slug', null, ['placeholder' => 'Название (Ru)', 'class' => 'form-control']) }}
                                @if($errors->has('slug'))
                                    <span class="help-block">{{ $errors->first('slug') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Название (Ru) <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('title_ru', null, ['placeholder' => 'Название (Ru)', 'class' => 'form-control']) }}
                                @if($errors->has('title_ru'))
                                    <span class="help-block">{{ $errors->first('title_ru') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Название (En) <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::text('title_en', null, ['placeholder' => 'Название (En)', 'class' => 'form-control']) }}
                                @if($errors->has('title_en'))
                                    <span class="help-block">{{ $errors->first('title_en') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Описание (Ru) <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::textarea('content_ru', null, [
                                    'placeholder' => 'Описание (Ru)',
                                    'class' => 'form-control',
                                    'id' => 'article-content-ru'
                                    ]) }}
                                @if($errors->has('content_ru'))
                                    <span class="help-block">{{ $errors->first('content_ru') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Описание (En) <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                {{ Form::textarea('content_en', null, [
                                    'placeholder' => 'Описание (En)',
                                    'class' => 'form-control',
                                    'id' => 'article-content-en'
                                ]) }}

                                @if($errors->has('content_en'))
                                    <span class="help-block">{{ $errors->first('content_en') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">
                                Title (Ru) <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-9">
                                {{ Form::text('page_title_ru', null, ['placeholder' => 'Title (Ru)', 'class' => 'form-control']) }}

                                @if($errors->has('page_title_ru'))
                                    <span class="help-block">{{ $errors->first('page_title_ru') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">
                                Title (En) <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-9">
                                {{ Form::text('page_title_en', null, ['placeholder' => 'Title (En)', 'class' => 'form-control']) }}

                                @if($errors->has('page_title_en'))
                                    <span class="help-block">{{ $errors->first('page_title_en') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">
                                Meta description (Ru) <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-9">
                                {{ Form::textarea('meta_description_ru', null, [
                                    'placeholder' => 'Title (Ru)',
                                    'class' => 'form-control',
                                    'rows' => 2
                                ]) }}

                                @if($errors->has('meta_description_ru'))
                                    <span class="help-block">{{ $errors->first('meta_description_ru') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">
                                Meta description (En) <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-9">
                                {{ Form::textarea('meta_description_en', null, [
                                    'placeholder' => 'Title (En)',
                                    'class' => 'form-control',
                                    'rows' => 2
                                ]) }}

                                @if($errors->has('meta_description_en'))
                                    <span class="help-block">{{ $errors->first('meta_description_en') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i>
                                    Сохранить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}

                        {{ Form::open(['route' => ['admin.articles.destroy', $article], 'method' => 'delete', 'class' => 'form-horizontal']) }}

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-danger"
                                        onclick="return confirm('Удалить статью?');">
                                    <i class="fa fa-times"></i> Удалить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        ClassicEditor
                .create(document.querySelector('#article-content-ru'));

        ClassicEditor
                .create(document.querySelector('#article-content-en'));
    </script>
@stop
