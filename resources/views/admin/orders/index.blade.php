@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                {{--<div class="row">--}}
                {{--<div class="col-md-2">--}}
                {{--<a href="{{ URL::route('admin.orders.create') }}" class="btn btn-success">Добавить <i--}}
                {{--class="fa fa-plus"></i></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="row">
                    <div height="20px">  &nbsp;</div>
                </div>

                <form id="orders-filter" action="{{route('admin.orders.index')}}" method="get">

                    <div class="row" align="center">
                        <label class="col-md-1 control-label" for="franchisee_id"
                               style="line-height: 31px;">Город</label>
                        <div class="col-md-2">
                            {{ Form::select('franchisee_id', $franchisees->pluck('city_ru', 'id'), null, ['class' => 'form-control order-filter', 'size' => 1]) }}
                        </div>
                        <label class="col-md-1 control-label" for="status" style="line-height: 31px;">Статус</label>
                        <div class="col-md-2">
                            {{ Form::select('status', array_merge([''=>'Любой'],\App\Models\Order::getStatuses()), null, ['class' => 'form-control order-filter', 'size' => 1]) }}
                        </div>
                        <div class="col-md-2">
                            {{ Form::text('date_start',null,['class' => 'order-filter form-control'])}}
                        </div>
                        <div class="col-md-2">
                            {{ Form::text('date_end',null,['class' => 'order-filter form-control'])}}
                        </div>
                        <div class="col-md-1">
                            <input type="submit" class="form-control btn-default" value="Применить">
                        </div>
                        <div class="col-md-1">
                            <a href="{{route('admin.orders.index')}}" class="form-control btn-default">Сбросить</a>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Дата и время<br>создания</th>
                                    <th>Дата доставки</th>
                                    <th>Номер заказа</th>
                                    <th>Статус</th>
                                    <th>Способ оплаты</th>
                                    <th>Стоимость</th>
                                    {{--<th>Сумма наша</th>--}}
                                    {{--<th>Сумма партнера</th>--}}
                                    <th>Комментарий</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($orders as $order)
                                    <tr>
                                        <td>{{$order->created_at->format('d.m.Y H:i')}}</td>
                                        <td>{{ $order->getDeliveryDateAndTimeWithFormat() }}</td>
                                        <td><a href="{{route('admin.orders.edit',$order->id)}}">{{$order->id}}</a></td>
                                        <td>{{$order->getStatusText()}}</td>
                                        <td>{{$order->getPayTypeName()}}</td>
                                        <td>{{$order->total_value}} р.</td>
                                        {{--<td>{{$order->our_value}} р.</td>--}}
                                        {{--<td>{{$order->partner_value}} р.</td>--}}
                                        <td>{{$order->comment}}</td>
                                        <td>
                                            {{ Form::open(['route' => ['admin.orders.delete', $order->id], 'method' => 'post']) }}
                                            {{ Form::button('Удалить', [
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger',
                                                'onclick' => "return confirm('Удалить заказ №{$order->id}')"
                                            ]) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="8">Список заказов пуст.</td>
                                    <tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        @if(count($orders)>0)
                            {{ $orders->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')

<script>
    $(function () {
        $(document).ready(function () {
            $('input[name="date_start"], input[name="date_end"]').datepicker({
                format: 'dd.mm.yy',
                firstDay: 1
            });
        });
    });
</script>

@endpush

