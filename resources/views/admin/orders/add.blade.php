@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {{ Form::open(['route' => 'admin.orders.store', 'method' => 'post', 'class' => 'form-horizontal']) }}


                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Город</label>
                            <div class="col-md-6">
                                {{ Form::select('franchisee_id', $franchisees, null, ['class' => 'form-control order-filter', 'size' => 1]) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Доставка</label>
                            <div class="col-md-9">
                                {{ Form::select('delivery_id', [], null, ['class' => 'form-control order-filter', 'size' => 1]) }}
                                @if($errors->has('delivery_id'))
                                    <span class="help-block">{{ $errors->first('delivery_id') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Дата доставки</label>
                            <div class="col-md-9">
                                {{ Form::date('delivery_date', null, ['class' => 'form-control'] )}}
                                @if($errors->has('delivery_date'))
                                    <span class="help-block">{{ $errors->first('delivery_date') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Способ оплаты</label>
                            <div class="col-md-9">
                                {{ Form::select('pay_type', \App\Models\Order::getPayTypes(), null, ['class' => 'form-control order-filter', 'size' => 1]) }}
                                @if($errors->has('pay_type'))
                                    <span class="help-block">{{ $errors->first('pay_type') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Статус</label>
                            <div class="col-md-9">
                                {{ Form::select('status', \App\Models\Order::getStatuses(), null, ['class' => 'form-control order-filter', 'size' => 1]) }}
                                @if($errors->has('status'))
                                    <span class="help-block">{{ $errors->first('status') }}</span>
                                @endif
                            </div>
                        </div>

                        {{Form::hidden('total_value')}}
                        {{Form::hidden('our_value')}}
                        {{Form::hidden('partner_value')}}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="total_value">Стоимость</label>
                            <div class="col-md-3">
                                {{ Form::number('total_value', null, ['class' => 'form-control', 'placeholder' => 'Полная стоимость', 'disabled']) }}
                                @if($errors->has('total_value'))
                                    <span class="help-block">{{ $errors->first('total_value') }}</span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="our_value">Наша доля</label>
                            <div class="col-md-3">
                                {{ Form::number('our_value', null, ['class' => 'form-control', 'placeholder' => 'Наша доля', 'disabled']) }}
                                @if($errors->has('our_value'))
                                    <span class="help-block">{{ $errors->first('our_value') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="our_value">Доля партнера</label>
                            <div class="col-md-3">
                                {{ Form::number('partner_value', null, ['class' => 'form-control', 'placeholder' => 'Доля партнера', 'disabled']) }}
                                @if($errors->has('partner_value'))
                                    <span class="help-block">{{ $errors->first('partner_value') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="our_value">Оплачено баллами</label>
                            <div class="col-md-3">
                                {{ Form::number('points_value', null, ['class' => 'form-control', 'placeholder' => 'Оплачено баллами']) }}
                                @if($errors->has('points_value'))
                                    <span class="help-block">{{ $errors->first('points_value') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="our_value">Рабочий комментарий</label>
                            <div class="col-md-9">
                                {{ Form::text('comment', null, ['class' => 'form-control', 'placeholder' => 'Рабочий комментарий']) }}
                                @if($errors->has('comment'))
                                    <span class="help-block">{{ $errors->first('comment') }}</span>
                                @endif
                            </div>
                        </div>

                        @include('admin.orders.inc.client')

                        @include('admin.orders.inc.bouquets',['bouquets'=>$bouquets])

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i>
                                    Сохранить
                                </button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>
                                    Сбросить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push("scripts")
<script>
    var franchisee_id = 0;
    function recalcPrice() {
        var items = []
        $(app.getRows()).each(function (i, item) {
            if (item.id && item.b_size) {
                items.push({id: item.id, size: item.b_size});
            }
        });

        $.ajax({
            url: '{{route('admin.orders.calculate')}}',
            type: 'POST',
            data: {
                franchisee_id: franchisee_id,
                items: items
            },
            processData: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (e) {
                $.each(e, function (key, value) {
                    if (key === "deliveries") {
                        return;
                    }
                    $("input[name=" + key + "]").val(value);
                });
                var cv = $("select[name=delivery_id] option:selected").attr("value");
                $("select[name=delivery_id] option").remove();
                var sel = $("select[name=delivery_id]");
                sel.append("<option value=\"0\">Самовывоз</option>");
                $.each(e.deliveries, function (key, value) {
                    sel.append($('<option></option>').attr("value", value.id).text(value.name_ru + " (" + value.price + " руб.) "))
                });
                sel.find("option[value=" + cv + "]").prop("selected", true);
            }.bind(this)
        });
    }
    $(function () {
        franchisee_id = $("select[name=franchisee_id]").on("change", function () {
            franchisee_id = $(this).val();
            recalcPrice();
        }).val();
        recalcPrice();
        $("body").on("change", "select[name=bouquets\\[\\]]", recalcPrice);
        $("body").on("change", "select[name=bouquets_size\\[\\]]", recalcPrice);
    });
</script>
@endpush