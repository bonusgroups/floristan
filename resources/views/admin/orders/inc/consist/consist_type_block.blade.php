<div class="col-md-4">
  <div class="block">
    <!-- Interactive Title -->
    <div class="block-title">
      <!-- Interactive block controls (initialized in js/app.js -> interactiveBlocks()) -->
      <div class="block-options pull-right">
        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-toggle-content"><i
              class="fa fa-arrows-v"></i></a>
        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-toggle-fullscreen"><i
              class="fa fa-desktop"></i></a>
      </div>
      <h2>{{ $size_name }}</h2>
    </div>
    <!-- END Interactive Title -->

    <!-- Interactive Content -->
    <!-- The content you will put inside div.block-content, will be toggled -->
    <div class="block-content">
      @foreach($bouquet->flowers->where('pivot.size', $bouquet_size) as $flower)
        @include('admin.bouquet.inc.consist.flower_item', [
          'bouquet' => $bouquet,
          'flower' => $flower,
          'bouquet_size' => $bouquet_size
        ])
      @endforeach
      <div class="row">
        {{ Form::open(['route' => ['admin.bouquet.add_to_consist', $bouquet], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('bouquet_size', $bouquet_size) }}

        <div class="form-group">
          <label class="col-md-3 control-label" for="flower_id">Цветок</label>
          <div class="col-md-6">
            {{ Form::select('flower_id', \App\Models\Flower::pluck('name_ru', 'id'), null, ['class' => 'form-control']) }}
            @if($errors->has('flower_id'))
              <span class="help-block">{{ $errors->first('flower_id') }}</span>
            @endif
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label" for="count">Количество</label>
          <div class="col-md-6">
            {{ Form::number('count_for_adding', null, ['class' => 'form-control']) }}
            @if($errors->has('count_for_adding'))
              <span class="help-block">{{ $errors->first('count_for_adding') }}</span>
            @endif
          </div>
        </div>

        <div class="form-group form-actions">
          <div class="col-md-9 col-md-offset-3">
            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i> Сохранить</button>
          </div>
        </div>

        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>