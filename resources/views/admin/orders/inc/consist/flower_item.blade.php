<div class="row">
  <div class="col-md-9">
    {{ Form::open(['route' => ['admin.bouquet.update_consist_count', $bouquet], 'method' => 'post', 'class' => 'form-horizontal']) }}
    {{ Form::hidden('flower_id', $flower->id) }}
    {{ Form::hidden('bouquet_size', $bouquet_size) }}
    <div class="form-group">
      <label class="col-md-3 control-label" for="example-text-input">{{ $flower->name_ru }}</label>

      <div class="col-md-6">
        {{ Form::number('count', $flower->pivot->count, ['placeholder' => 'Количество', 'class' => 'form-control']) }}
        @if($errors->has('count'))
          <span class="help-block">{{ $errors->first('count') }}</span>
        @endif
      </div>
      <div class="col-md-3">
        <button type="submit" class="btn btn-sm btn-default">
          <i class="fa fa-times"></i> Обновить
        </button>
      </div>
    </div>
    {{ Form::close() }}
  </div>
  <div class="col-md-3">
    {{ Form::open(['route' => ['admin.bouquet.delete_flower_from_consist', $bouquet], 'method' => 'delete']) }}
    {{ Form::hidden('flower_id', $flower->id) }}
    {{ Form::hidden('bouquet_size', $bouquet_size) }}
    <button type="submit" class="btn btn-danger" onclick="return confirm('Удалить цветок из состава?');">Удалить</button>
    {{ Form::close() }}
  </div>
</div>