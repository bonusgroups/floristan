<div class="row">

    <?php
    $bouqets = Request::old("bouquets", null);
    $bouquts_size = Request::old("bouquets_size", []);

    if (isset($order) && is_null($bouqets)) {
        $bouqets = [];
        $order->bouquets->map(function ($item) use (&$bouqets, &$bouquts_size) {
            $bouqets[] = $item->id;
            $bouquts_size[] = $item->pivot->bouquet_size;
        });
    }

    if (is_null($bouqets)) {
        $bouqets = [];
    }

    $promocode = $order->promocode;

    $json = [];

    $bouquetsNoAdditionalNoDiscountCount = $order->bouquets->where('is_additional', 0)->where('discount', 0)->count();
    $franchisee = \App\Models\Franchisee::find($order->franchisee_id);

    foreach ($bouqets as $i => $id) {
        $bouquet = $order->bouquets->where('id', $id)->first();
        $size = $bouquts_size[$i];

        $priceOriginal = $bouquet->price($size, $order->franchisee_id, false);
        $price = $bouquet->price($size, $franchisee->id);
        $priceWithDiscount = $bouquet->priceWithDiscount($size, $franchisee->id);

        $hasDiscount = $price > $priceWithDiscount;
        $promocodeDiscount = 0;

        if ($promocode and !$bouquet->discount) {
            if ($promocode->isFixed($order->base_total_value_without_discount_bouquets)) {
                $promocodeDiscount = $bouquetsNoAdditionalNoDiscountCount ? $promocode->discount / $bouquetsNoAdditionalNoDiscountCount : 0;
            } else {
                $percent = intval($promocode->percent);
                $promocodeDiscount = $percent ? intval($price * ($percent / 100)) : 0;
            }
        }

        $mul = $bouquet->getPriceMul();
        $franchiseeCommission = $franchisee->commission_for_bouquets;

        $partnerValue = ($priceWithDiscount - $priceOriginal) * ($franchiseeCommission / 100);
        $ourValue = $priceWithDiscount - $partnerValue;

        $json[] = [
                "id" => $id,
                "b_size" => $size,
                'priceOriginal' => (int)$priceOriginal,
                'price' => (int)$price,
                'discount' => [
                        'percent' => (int)$bouquet->discount,
                        'value' => (int)($price - $priceWithDiscount)
                ],
                'promocodeDiscount' => [
                        'isNotFixed' => $promocode ? !$promocode->isFixed($order->base_total_value_without_discount_bouquets) : false,
                        'percent' => $promocode ? (int)$promocode->percent : 0,
                        'value' => (int)$promocodeDiscount
                ],
                'promocodeCode' => $promocode ? $promocode->code : '-',
                'priceWithDiscount' => (int)($priceWithDiscount - $promocodeDiscount),
                'partnerValue' => (int)$partnerValue,
                'ourValue' => (int)$ourValue
        ];
    }
    ?>

    <div class="block">
        <!-- Block Title -->
        <div class="block-title">
            <h2>Состав заказа. Букеты.</h2>
        </div>
        <!-- END Block Title -->
        <div class="form-group">
            <label class="col-md-3 control-label" for="example-text-input">Текст открытки</label>
            <div class="col-md-9">
                {{ Form::textarea('postcard_text', null, ['class' => 'form-control', 'placeholder' => 'Текст открытки']) }}
                @if($errors->has('postcard_text'))
                    <span class="help-block">{{ $errors->first('postcard_text') }}</span>
                @endif
            </div>
        </div>
        <div id="app" class="form-group">

            <table class="table">
                <thead>
                <tr>
                    <td><strong>Букет</strong></td>
                    <td><strong>Размер букета</strong></td>
                    <td><strong>Себестоимость</strong></td>
                    <td><strong>Стоимость</strong></td>
                    <td><strong>Скидка</strong></td>
                    <td><strong>Скидка по купону</strong></td>
                    <td><strong>Номер купона</strong></td>
                    <td><strong>Стоимость с учетом скидки</strong></td>
                    <td><strong>Доля партнера</strong></td>
                    <td><strong>Доля наша</strong></td>

                    <td></td>
                </tr>
                </thead>
                <tbody>
                <tr v-for="row in rows">
                    <td>
                        <select name="bouquets[]" v-model="row.id">
                            <option v-for="bouquet in bouquets" :value="bouquet.id">@{{bouquet.name_ru}}</option>
                        </select>
                    </td>
                    <td>
                        <select name="bouquets_size[]" v-model="row.b_size">
                            <option v-for="bouquet_size in b_sizes"
                                    :value="bouquet_size.b_size">@{{bouquet_size.title}}</option>
                        </select>
                    </td>

                    <td>@{{row.priceOriginal}} р.</td>
                    <td>@{{row.price}} р.</td>
                    <td>
                        <span v-if="row.discount">
                            <span v-if="row.discount.value">@{{row.discount.percent}}% (@{{row.discount.value}} р.)</span>
                        </span>
                    </td>
                    <td>
                        <span v-if="row.promocodeDiscount">
                            <span v-if="row.promocodeDiscount.value">
                                <span v-if="row.promocodeDiscount.isNotFixed">@{{row.promocodeDiscount.percent}}
                                    % (@{{row.promocodeDiscount.value}} р.)</span>
                                <span v-else>@{{row.promocodeDiscount.value}} р.</span>
                            </span>
                        </span>
                    </td>
                    <td>@{{row.promocodeCode}}</td>
                    <td>@{{row.priceWithDiscount}} р.</td>
                    <td>@{{row.partnerValue}} р.</td>
                    <td>@{{row.ourValue}} р.</td>

                    <td><a v-on:click="removeElement(row);" style="cursor: pointer">Убрать</a></td>
                </tr>

                <?php $rows = collect($json) ?>

                <tr style="font-weight: bold; font-size: 15px;">
                    <td colspan="2">Итого</td>

                    <td>{{ $rows->sum('priceOriginal') }} р.</td>
                    <td>{{ $rows->sum('price') }} р.</td>
                    <td>{{ $rows->sum('discount.value') }} р.</td>
                    <td>{{ $rows->sum('promocodeDiscount.value') }}</td>
                    <td>-</td>
                    <td>{{ $rows->sum('priceWithDiscount') }} р.</td>
                    <td>{{ $rows->sum('partnerValue') }} р.</td>
                    <td>{{ $rows->sum('ourValue') }} р.</td>
                </tr>

                </tbody>
            </table>
            <div>
                <button class="button btn-primary" @click.prevent="addRow">Добавить букет</button>
            </div>
        </div>
    </div>

</div>

@push('scripts')
<script type="text/javascript">

    var app = new Vue({
        el: "#app",
        data: {
            rows: {!! json_encode($json) !!},
            bouquets: [
                    @foreach($bouquets as $bouquet)
                {
                    name_ru: "{{$bouquet->name_ru}}", id:{{$bouquet->id}} },
                @endforeach
            ],
            b_sizes: [
                    @foreach($bouquets_size as $title=>$b_size)
                {
                    title: "{{$title}}", b_size: "{{$b_size}}"
                },
                @endforeach
            ]
        },
        methods: {
            addRow: function () {

                this.rows.push({id: this.bouquets[0].id, title: this.bouquets[0].name_ru, b_size: 'size_small'});
                recalcPrice();
                return false;
            },
            removeElement: function (row) {
                var index = this.rows.indexOf(row);
                this.rows.splice(index, 1);
                recalcPrice();
            },
            getRows: function () {
                return this.rows;
            }

        }
    });

</script>
@endpush