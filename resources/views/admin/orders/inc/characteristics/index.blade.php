<div class="row">
    <div class="col-md-12">
        <div class="block">
            {{ Form::model($bouquet, ['route' => ['admin.bouquet.update', $bouquet], 'method' => 'put', 'class' => 'form-horizontal']) }}

            <div class="form-group">
                <label class="col-md-3 control-label" for="example-text-input">Название (Ru)</label>
                <div class="col-md-9">
                    {{ Form::text('name_ru', null, ['class' => 'form-control', 'placeholder' => 'Название (Ru)']) }}
                    @if($errors->has('name_ru'))
                        <span class="help-block">{{ $errors->first('name_ru') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="example-text-input">Название (En)</label>
                <div class="col-md-9">
                    {{ Form::text('name_en', null, ['class' => 'form-control', 'placeholder' => 'Название (En)']) }}
                    @if($errors->has('name_en'))
                        <span class="help-block">{{ $errors->first('name_en') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="example-text-input">Описание (Ru)</label>
                <div class="col-md-9">
                    {{ Form::textarea('description_ru', null, ['class' => 'form-control', 'placeholder' => 'Описание (Ru)','data-enable-editor'=>'']) }}
                    @if($errors->has('description_ru'))
                        <span class="help-block">{{ $errors->first('description_ru') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="example-text-input">Описание (En)</label>
                <div class="col-md-9">
                    {{ Form::textarea('description_en', null, ['class' => 'form-control', 'placeholder' => 'Описание (En)','data-enable-editor'=>'']) }}
                    @if($errors->has('description_en'))
                        <span class="help-block">{{ $errors->first('description_en') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="is_new">Новинка</label>
                <div class="col-md-9">
                    <label class="switch switch-info">
                        {{ Form::checkbox('is_new') }}<span></span>
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="hit">Хит</label>
                <div class="col-md-9">
                    <label class="switch switch-success">
                        {{ Form::checkbox('hit') }}<span></span>
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="is_active">Активный</label>
                <div class="col-md-9">
                    <label class="switch switch-primary">
                        {{ Form::checkbox('is_active') }}<span></span>
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="price">Цена (средний)</label>
                <div class="col-md-9">
                    {{ Form::number('price', $bouquet->price(\App\Models\Bouquet::SIZE_MIDDLE), ['class' => 'form-control', 'disabled']) }}
                    @if($errors->has('price'))
                        <span class="help-block">{{ $errors->first('price') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="price">Цена (большой)</label>
                <div class="col-md-9">
                    {{ Form::number('price', $bouquet->price(\App\Models\Bouquet::SIZE_BIG), ['class' => 'form-control', 'disabled']) }}
                    @if($errors->has('price'))
                        <span class="help-block">{{ $errors->first('price') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="price">Цена (малый)</label>
                <div class="col-md-9">
                    {{ Form::number('price', $bouquet->price(\App\Models\Bouquet::SIZE_SMALL), ['class' => 'form-control', 'disabled']) }}
                    @if($errors->has('price'))
                        <span class="help-block">{{ $errors->first('price') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="discount">Скидка</label>
                <div class="col-md-9">
                    {{ Form::number('discount', null, ['class' => 'form-control', 'placeholder' => 'Скидка']) }}
                    @if($errors->has('discount'))
                        <span class="help-block">{{ $errors->first('discount') }}</span>
                    @endif
                </div>
            </div>

            <div class="form-group form-actions">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i> Сохранить
                    </button>
                    <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Сбросить</button>
                </div>
            </div>

            {{ Form::close() }}

            {{ Form::open(['route' => ['admin.bouquet.destroy', $bouquet], 'method' => 'delete', 'class' => 'form-horizontal']) }}

            <div class="form-group form-actions">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Удалить букет?');">
                        <i class="fa fa-times"></i> Удалить
                    </button>
                </div>
            </div>

            {{ Form::close() }}

            @include('admin.bouquet.inc.characteristics.reasons_form')

            @include('admin.bouquet.inc.characteristics.colors')

            @include('admin.bouquet.inc.characteristics.categories')

        </div>
    </div>
</div>