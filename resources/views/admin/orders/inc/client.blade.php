<div class="row">

    <div class="block">
        <!-- Block Title -->
        <div class="block-title">
            <h2>Информация о заказчике</h2>
        </div>
        <!-- END Block Title -->


        <div class="form-group">
            <label class="col-md-3 control-label" for="our_value">Имя</label>
            <div class="col-md-9">
                {{ Form::text('client_name', null, ['class' => 'form-control', 'placeholder' => 'Имя заказчика']) }}
                @if($errors->has('client_name'))
                    <span class="help-block">{{ $errors->first('client_name') }}</span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="our_value">Телефон</label>
            <div class="col-md-6">
                {{ Form::text('client_phone', null, ['class' => 'form-control', 'placeholder' => 'Телефон']) }}
                @if($errors->has('client_phone'))
                    <span class="help-block">{{ $errors->first('client_phone') }}</span>
                @endif
            </div>
        </div>

        {{--<div class="form-group">--}}
            {{--<label class="col-md-3 control-label" for="our_value">Комментарий заказчика</label>--}}
            {{--<div class="col-md-6">--}}
                {{--{{ Form::textarea('client_comment', null, ['class' => 'form-control', 'placeholder' => 'Комментарий заказчика']) }}--}}
                {{--@if($errors->has('client_comment'))--}}
                    {{--<span class="help-block">{{ $errors->first('client_comment') }}</span>--}}
                {{--@endif--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="form-group">
            <label class="col-md-3 control-label" for="our_value">Имя получателя</label>
            <div class="col-md-9">
                {{ Form::text('recipient_name', null, ['class' => 'form-control', 'placeholder' => 'Имя получателя']) }}
                @if($errors->has('recipient_name'))
                    <span class="help-block">{{ $errors->first('recipient_name') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="our_value">Телефон получателя</label>
            <div class="col-md-6">
                {{ Form::text('recipient_phone', null, ['class' => 'form-control', 'placeholder' => 'Телефон']) }}
                @if($errors->has('recipient_phone'))
                    <span class="help-block">{{ $errors->first('recipient_phone') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="our_value">E-mail</label>
            <div class="col-md-6">
                {{ Form::text('client_email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) }}
                @if($errors->has('client_email'))
                    <span class="help-block">{{ $errors->first('client_email') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="our_value">Адрес получателя</label>
            <div class="col-md-9">
                {{ Form::text('recipient_address', null, ['class' => 'form-control', 'placeholder' => 'Адрес']) }}
                @if($errors->has('recipient_address'))
                    <span class="help-block">{{ $errors->first('recipient_address') }}</span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="need_to_call">Требуется уточнить адрес</label>
            <div class="col-md-9">
                <label class="switch switch-info">
                    {{ Form::checkbox('need_to_call') }}<span></span>
                </label>
            </div>
        </div>


    </div>

</div>