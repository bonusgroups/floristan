<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="block">
      <!-- Block Title -->
      <div class="block-title">
        <h2>Категории</h2>
      </div>
      <!-- END Block Title -->

      {{ Form::model($bouquet, ['route' => ['admin.additional_product.attach_category', $bouquet], 'class' => 'form-horizontal', 'method' => 'post']) }}

      <div class="form-group">
        <label class="col-md-4 control-label" for="example-chosen">Категория</label>
        <div class="col-md-6">
          {{ Form::select('category_id[]', \App\Models\BouquetCategory::all()->pluck('name_ru', 'id')->toArray(), $bouquet->categories->pluck('id'), ['class' => 'select-chosen', 'multiple']) }}
        </div>
      </div>

      <div class="form-group form-actions">
        <div class="col-md-8 col-md-offset-4">
          <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i> Сохранить
          </button>
        </div>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>