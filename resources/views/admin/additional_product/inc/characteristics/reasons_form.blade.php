<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="block">
      <!-- Block Title -->
      <div class="block-title">
        <h2>Поводы</h2>
      </div>
      <!-- END Block Title -->

      @foreach($bouquet->reasons as $reason)
        {{ Form::open(['route' => ['admin.additional_product.detach_reason', $bouquet], 'class' => 'form-horizontal', 'method' => 'post']) }}
        {{ Form::hidden('reason_id', $reason->id) }}

        <div class="form-group">
          <div class="col-md-6 col-md-offset-3">
            {{ Form::text(null, $reason->name_ru, ['class' => 'form-control', 'disabled']) }}
          </div>
          <div class="col-md-3">
            <button type="submit" onclick="return confirm('Отвязать повод?')" class="btn btn-danger">Удалить</button>
          </div>
        </div>

        {{ Form::close() }}
      @endforeach

      {{ Form::open(['route' => ['admin.additional_product.attach_reason', $bouquet], 'class' => 'form-horizontal', 'method' => 'post']) }}

      <div class="form-group">
        <label class="col-md-3 control-label" for="example-text-input">Повод</label>
        <div class="col-md-9">
          {{ Form::select('reason_id', \App\Models\BouquetReason::pluck('name_ru', 'id')->toArray(), null, ['class' => 'form-control']) }}
        </div>
        @if($errors->has('reason_id'))
          <span class="help-block">{{ $errors->first('reason_id') }}</span>
        @endif
      </div>

      <div class="form-group form-actions">
        <div class="col-md-9 col-md-offset-3">
          <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i> Добавить
          </button>
        </div>
      </div>

      {{ Form::close() }}

      {{ Form::open(['route' => 'admin.bouquet_reason.store', 'class' => 'form-horizontal', 'method' => 'post']) }}
      {{ Form::hidden('bouquet_id', $bouquet->id) }}

      <div class="form-group">
        <label class="col-md-3 control-label" for="example-text-input">Название (Ru)</label>
        <div class="col-md-9">
          {{ Form::text('name_ru', null, ['class' => 'form-control']) }}
          @if($errors->has('name_ru'))
            <span class="help-block">{{ $errors->first('name_ru') }}</span>
          @endif
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-3 control-label" for="example-text-input">Название (En)</label>
        <div class="col-md-9">
          {{ Form::text('name_en', null, ['class' => 'form-control']) }}
          @if($errors->has('name_en'))
            <span class="help-block">{{ $errors->first('name_en') }}</span>
          @endif
        </div>
      </div>

      <div class="form-group form-actions">
        <div class="col-md-9 col-md-offset-3">
          <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i> Добавить
          </button>
        </div>
      </div>

      {{ Form::close() }}
    </div>
  </div>
</div>