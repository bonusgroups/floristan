@extends('admin.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <div class="block full">
                <!-- Working Tabs Title -->
                <div class="block-title">
                    <h2>Работа с букетами</h2>
                </div>
                <!-- END Working Tabs Title -->

                <!-- Working Tabs Content -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- Default Tabs -->
                        <ul class="nav nav-tabs push" data-toggle="tabs">
                            <li class="@if(Session::get('active_tab', 'consist') == 'consist') active @endif">
                                <a href="#consist">Состав</a>
                            </li>
                            <li class="@if(Session::get('active_tab', 'consist') == 'images') active @endif">
                                <a href="#images">Изображения</a>
                            </li>
                            <li class="@if(Session::get('active_tab', 'consist') == 'characteristics') active @endif">
                                <a href="#characteristics">Характеристики</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane @if(Session::get('active_tab', 'consist') == 'consist') active @endif"
                                 id="consist">
                                @include('admin.additional_product.inc.consist.index')
                            </div>
                            <div class="tab-pane @if(Session::get('active_tab', 'consist') == 'images') active @endif"
                                 id="images">
                                @include('admin.additional_product.inc.images.index')
                            </div>
                            <div class="tab-pane @if(Session::get('active_tab', 'consist') == 'characteristics') active @endif"
                                 id="characteristics">
                                @include('admin.additional_product.inc.characteristics.index')
                            </div>
                        </div>
                        <!-- END Default Tabs -->
                    </div>
                </div>
                <!-- END Working Tabs Content -->
            </div>
        </div>
    </div>
@stop