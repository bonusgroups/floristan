@extends('admin.layout.main')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {{ Form::open(['route' => 'admin.additional_product.store', 'method' => 'post', 'class' => 'form-horizontal']) }}

                        {{ Form::hidden('is_additional', 1) }}

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Название (Ru)</label>
                            <div class="col-md-9">
                                {{ Form::text('name_ru', null, ['class' => 'form-control', 'placeholder' => 'Название (Ru)']) }}
                                @if($errors->has('name_ru'))
                                    <span class="help-block">{{ $errors->first('name_ru') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="example-text-input">Название (En)</label>
                            <div class="col-md-9">
                                {{ Form::text('name_en', null, ['class' => 'form-control', 'placeholder' => 'Название (En)']) }}
                                @if($errors->has('name_en'))
                                    <span class="help-block">{{ $errors->first('name_en') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="is_new">Новинка</label>
                            <div class="col-md-9">
                                <label class="switch switch-info">
                                    {{ Form::checkbox('is_new') }}<span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="hit">Хит</label>
                            <div class="col-md-9">
                                <label class="switch switch-success">
                                    {{ Form::checkbox('hit') }}<span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="is_active">Активный</label>
                            <div class="col-md-9">
                                <label class="switch switch-primary">
                                    {{ Form::checkbox('is_active') }}<span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="discount">Скидка</label>
                            <div class="col-md-9">
                                {{ Form::number('discount', null, ['class' => 'form-control', 'placeholder' => 'Скидка']) }}
                                @if($errors->has('discount'))
                                    <span class="help-block">{{ $errors->first('discount') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right"></i>
                                    Сохранить
                                </button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i>
                                    Сбросить
                                </button>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop