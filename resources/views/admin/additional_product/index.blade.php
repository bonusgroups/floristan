@extends('admin.layout.main')

@section('content')
    <div class="row full-width">
        <div class="col-md-8 col-md-offset-2">
            <div class="block">
                <div class="row">
                    <div class="col-md-3">
                        <a href="{{ URL::route('admin.additional_product.create') }}" class="btn btn-success">Добавить <i
                                    class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="row">
                    {{ Form::open(['route' => 'admin.additional_product.index', 'class' => 'form-horizontal', 'method' => 'get']) }}

                    @include('admin.partials.franchisee_select')

                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i>
                                Фильтровать
                            </button>
                        </div>
                    </div>

                    {{ Form::close() }}
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-vcenter table-striped">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Статус</th>
                                    <th>Состав средний</th>
                                    <th>Франчайзи</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bouquets as $bouquet)
                                    <tr>
                                        <td>
                                            {{ Html::link(URL::route('admin.additional_product.edit', $bouquet), $bouquet->name_ru) }}
                                        </td>
                                        <td>
                                            {{ $bouquet->is_active ? 'Активный' : 'Не активный' }}
                                        </td>
                                        <td>
                                            {{ $bouquet->consistAsString(\App\Models\Bouquet::SIZE_MIDDLE) }}
                                        </td>
                                        <td>
                                            {{ $bouquet->franchisee ? $bouquet->franchisee->city_ru : '-' }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-auto">
                        {{ $bouquets->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop