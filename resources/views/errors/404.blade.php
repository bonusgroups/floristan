@extends('site.layout.main')

@section('content')
    <div class="container">
        <h1>Ошибка 404</h1>
        <p style="text-align: center;">Данная страница не найдена</p>
    </div>
@endsection