/**
 * Created by zerg on 20.05.2018.
 */

function cart_add(id, redirectUrl) {
    //
    bouquet_size = $('input[name=size]:checked').val();

    $.get(window.route_add_to_cart, {id: id, bouquet_size: bouquet_size})
        .done(function (data) {
            $('.cart_value').html(data.cart_size + " ₽");
            $('.balls_value').html(data.balls_count);

            if (redirectUrl) {
                top.location.href = redirectUrl;
            }
        });

}

function cart_add_in_cart(id) {

    bouquet_size = $('input[name=size]:checked').val();
    console.log(bouquet_size);
    $.get(window.route_add_to_cart, {id: id, bouquet_size: bouquet_size})
        .done(function (data) {

            location.reload();
        });

}


function cart_del(id) {
    //
    $.get(window.route_del_from_cart, {id: id})
        .done(function (data) {

            $('#item-' + data.id).remove();
            $('.cart_value').html(data.cart_size + " ₽");
            $('.balls_value').html(data.balls_count);

        });

}

