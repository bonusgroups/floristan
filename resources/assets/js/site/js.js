(function ($) {

    document.cookie = "tzo=" + (-new Date().getTimezoneOffset());

    $("[data-set-lang]").on("click", function () {
        $("[data-set-lang]").removeClass('active');
        $(this).addClass('active');
        var lang = $(this).attr('data-set-lang');
        var date = new Date();
        date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
        var cookie = "language = " + lang + "; expires=" + date.toGMTString() + "; path=/;";
        console.log(cookie);
        document.cookie = cookie;

        window.location.reload();
        return false;
    });

    var cities = [];
    $('[data-filter-city]').each(function (i, item) {
        var el = $(item).parent()[0];
        el.cityname = $(item).attr('data-filter-city').toLowerCase().replace(/[ ]{2,}/gmu, '').split(' ');
        cities.push(el);
        $(el).hide();
    });

    $('[data-open-cities]')
        .on('click', function (e) {

            e.preventDefault();
            var $this = $('[data-ui-dialog-ex="cities-form"]');
            console.log(e, $this);

            var width = parseInt($this.attr('data-ui-dialog-width'));
            var title = $this.attr('data-ui-dialog-title');
            var dialogClass = $this.attr('data-ui-dialog-class');
            $this.find('input').val('');
            $(cities).show();

            $this.find('input').on('keyup', function () {
                var test = $this.find('input').val().toLowerCase();
                if (test.length === 0) {
                    $(cities).show();
                    return;
                }
                var regex = new RegExp(test, 'mgui');
                console.log(test, regex);
                $.each(cities, function (i, item) {
                    var show = false;
                    $.each(item.cityname, function (i, cityname) {
                        if (regex.test(cityname)) {
                            show = true;
                            console.log(cityname);
                        }
                    });
                    if (show) {
                        $(item).show();
                    } else {
                        $(item).hide();
                    }
                });
            });

            $this.dialog({
                modal: true,
                width: width,
                title: title,
                dialogClass: dialogClass,
                show: false
            });
            return false;
        });

    $('[data-ui-dialog-ex="call-me-form"] input').mask('+9 (999) 999-99-99', {placeholder: "_"});

    $('body').on("click", '[data-ui-dialog-ex="call-me-form"] button', function (e) {
        var formData = new FormData($(this).closest('form')[0]);
        $(this).closest('form').hide();

        $.ajax({
            url: $(this).closest('form').attr('action'),
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (e) {
                if (e.error != null) {
                    $(this).closest('form').show();
                } else {
                    yaCounter49678651.reachGoal('callback');
                }
            }.bind(this)
        });


        return false;
    });

    $(".call-me").on("click", function (e) {
        e.preventDefault();
        var $this = $('[data-ui-dialog-ex="call-me-form"]');

        var width = parseInt($this.attr('data-ui-dialog-width'));
        var title = $this.attr('data-ui-dialog-title');
        var dialogClass = $this.attr('data-ui-dialog-class');
        $this.find('input').val('');
        $this.find('form').show();

        $this.dialog({
            modal: true,
            width: width,
            title: title,
            dialogClass: dialogClass,
            show: false
        });
        return false;
    });


    function behaviors() {


        $('[data-ui-dialog]')
            .once('ui-dialog', function () {
                var $this = $(this);

                var code = $this.attr('data-ui-dialog');
                var width = parseInt($this.attr('data-ui-dialog-width'));
                var title = $this.attr('data-ui-dialog-title');
                var dialogClass = $this.attr('data-ui-dialog-class');

                $('[data-ui-dialog="' + code + '"]').not(this).remove();

                $this.find('input').val('');
                $(cities).show();

                $this.find('input').on('keyup', function () {
                    var test = $this.find('input').val().toLowerCase();
                    if (test.length === 0) {
                        $(cities).show();
                        return;
                    }
                    var regex = new RegExp(test, 'mgui');
                    console.log(test, regex);
                    $.each(cities, function (i, item) {
                        var show = false;
                        $.each(item.cityname, function (i, cityname) {
                            if (regex.test(cityname)) {
                                show = true;
                                console.log(cityname);
                            }
                        });
                        if (show) {
                            $(item).show();
                        } else {
                            $(item).hide();
                        }
                    });
                });


                $this.dialog({
                    modal: true,
                    width: width,
                    title: title,
                    dialogClass: dialogClass
                });
            });


        $('.register-completed a')
            .once()
            .click(function () {
                $(this)
                    .closest('[data-ui-dialog]')
                    .dialog('close');

                return false;
            });


        $('#reviews')
            .find('.owl-carousel')
            .owlCarousel({
                center: true,
                items: 3,
                loop: true,
                // autoHeight: true
            });


        $('.filters-form div[data-ui-slider]')
            .once('ui-slider', function () {
                var $this = $(this);

                var min = parseInt($this.attr('data-min'));
                var max = parseInt($this.attr('data-max'));

                var inputFrom = $($this.attr('data-input-from'));
                var inputTo = $($this.attr('data-input-to'));

                var valueFrom = parseInt(inputFrom.val());
                var valueTo = parseInt(inputTo.val());

                $this.slider({
                    min: min,
                    max: max,
                    values: [valueFrom, valueTo],
                    range: true,
                    step: 10,
                    slide: function (event, ui) {
                        var wrapper = $(ui.handle).closest('.box');
                        var slider = wrapper.find('[data-ui-slider]');

                        var inputFrom = $(slider.attr('data-input-from'));
                        var inputTo = $(slider.attr('data-input-to'));

                        inputFrom.val(ui.values[0]);
                        inputTo.val(ui.values[1]);
                    }
                });

                inputFrom.once('ui-slider').change(function () {
                    var $this = $(this);
                    var wrapper = $this.closest('.box');
                    var slider = wrapper.find('[data-ui-slider]');

                    slider.trigger('updateValues');
                });

                inputTo.once('ui-slider').change(function () {
                    var $this = $(this);
                    var wrapper = $this.closest('.box');
                    var slider = wrapper.find('[data-ui-slider]');

                    slider.trigger('updateValues');
                });
            })
            .on('updateValues', function () {
                var $this = $(this);

                var min = parseInt($this.attr('data-min'));
                var max = parseInt($this.attr('data-max'));

                var inputFrom = $($this.attr('data-input-from'));
                var inputTo = $($this.attr('data-input-to'));

                var valueFrom = parseInt(inputFrom.val());
                var valueTo = parseInt(inputTo.val());

                if (valueFrom < min) valueFrom = min;
                if (valueTo > max) valueTo = max;

                $this.slider('option', 'values', [valueFrom, valueTo]);
            });


        $('.filters-form .box > label')
            .once()
            .click(function () {
                $(this).closest('.box').toggleClass('closed');
            });


        $('#product-carousel')
            .once(function () {
                $(this).flexslider({
                    animation: "slide",
                    controlNav: false,
                    directionNav: false,
                    animationLoop: false,
                    slideshow: false,
                    itemWidth: 80,
                    itemMargin: 15,
                    asNavFor: '#product-slider'
                });
            });


        $('#product-slider')
            .once(function () {
                $(this).flexslider({
                    animation: "slide",
                    controlNav: false,
                    directionNav: false,
                    animationLoop: false,
                    slideshow: false,
                    sync: "#product-carousel"
                })
            });


        $('#header')
            .find('.menu-link, .popup .close')
            .once()
            .click(function () {
                $('#header').find('.popup, .popup-overlay').toggle();

                return false;
            });


        $('.filters-form-overlay')
            .once()
            .click(function () {
                $(this).hide();
                $('.filters-form').hide();
            });


        $('.filters-form-link a')
            .once()
            .click(function () {
                $('.filters-form, .filters-form-overlay').show();

                return false;
            });


        $('#certificates')
            .find('.owl-carousel')
            .once(function () {
                $(this).owlCarousel({
                    center: true,
                    items: 1,
                    nav: false,
                    margin: 20,
                    autoWidth: true,
                    startPosition: 1
                });
            });


        $(".add-to-cart-form input[type=radio][name=size]").on("change", function () {
            var currentSize = $(this).val();
            $("[data-size][data-size!=" + currentSize + "]").hide();
            $("[data-size][data-size=" + currentSize + "]").show();
        });


        var currentSize = $(".add-to-cart-form input[type=radio][name=size]:checked").length ? $(".add-to-cart-form input[type=radio][name=size]:checked").val() : $(".add-to-cart-form input[type=radio][name=size]:eq(0)").val()

        $("[data-size][data-size!=" + currentSize + "]").hide();


        $('#left')
            .find('.filters-form')
            .on('fixed', function () {
                var $this = $(this);
                var height = $this.height();
                var headerHeight = $('#header').find('.box-1').height();
                var limit = $('#delivery-flowers').offset().top - height;
                var windowHeight = $(window).height();

                $this.scrollToFixed({
                    marginTop: headerHeight,
                    top: 0,
                    limit: limit
                })
            })
            .once('fixed', function () {
                $(this).trigger('fixed');
            });


        $('.filters-form .box > label')
            .once('check-fixed')
            .click(function () {
                var $this = $(this);

                $this.closest('.filters-form')
                    .trigger('detach.ScrollToFixed')
                    .trigger('fixed');
            });


        $('[data-ui-dialog]')
            .once('ui-dialog', function () {
                var $this = $(this);

                var code = $this.attr('data-ui-dialog');
                var width = parseInt($this.attr('data-ui-dialog-width'));
                var title = $this.attr('data-ui-dialog-title');
                var dialogClass = $this.attr('data-ui-dialog-class');

                $('[data-ui-dialog="' + code + '"]').not(this).remove();

                $this.dialog({
                    modal: true,
                    width: width,
                    title: title,
                    dialogClass: dialogClass
                });
            });
    }


    $(document).ready(function () {
        behaviors();
    });


    $(document).ajaxComplete(function () {
        behaviors();
    });

})(jQuery);