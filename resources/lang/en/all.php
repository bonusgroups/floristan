<?php

return [
//head/foot
    "header.login"=>"Sign in",
    "header.register"=>"Sign up",
    "header.menu.index"=>"Index",
    "header.menu.catalog"=>"Catalog",
    "header.menu.delivery"=>"Delivery & payment",
    "header.menu.feedbacks"=>"Feedbacks",
    "header.menu.aboutus"=>"About us",
    "header.menu.contacts"=>"Contacts",
    "header.menu.articles"=>"Articles",
    "header.middle.city"=>"Your city:",
    "header.middle.search"=>"Search",
    "header.middle.callback"=>"Callback me",
    "header.middle.callback.message"=>"We callback you in 15 minutes",
    "bread.index"=>"Index",
    "bread.delivery"=>"Delivery & payment",
    "bread.feedbacks"=>"Feedbacks",
    "bread.aboutus"=>"About us",
    "bread.contacts"=>"Contacts",
    "bread.catalog"=>"Catalog",
    "bread.search"=>"Search",
    "bread.cart"=>"Cart",
    "bread.order"=>"Checkout",
    "bread.order.success"=>"Success order",
    "bread.articles"=>"Articles",

    //main
    "main.middle.delivery"=>"Free delivery",
    "main.middle.cardfree"=>"Free gift card",
    "main.middle.balls"=>"Points<br/>from all buy",
    "main.middle.garanty"=>"Quality guarantee",
    "main.middle.photobefore"=>"Photo of bouquet<br/>before delivery",
    "main.hits.title"=>"Hit sales",
    "main.new.title"=>"New",
    "main.hits.more"=>"More bouquets in our catalog",
    "main.hits.go"=>"Go to catalog",
    "main.feedbacks.title"=>"Feedback of us",
    "main.delivery.title"=>"Flower delivery",
    "main.delivery.content"=>"<p> If you are interested in the delivery of flowers at home with a preliminary call or without (in the second case it will turn out
           a real surprise for the gifted), look at the site of the salon FLORISTAN.RU. A wide range of,
           possibility to perform individual solutions of any complexity, adequate prices - a bouquet of flowers with
           delivery will be exactly as you need, and will not be late for a minute.
        </p><p>The salon is open daily from 8:00 to 21:00. Orders are accepted around the clock, delivery outside working hours
           salon are accepted in advance.
        </p><p>Attention! Free delivery of flowers is provided if the addressee resides in Kazan, and
           the order was issued through the website. A bouquet of roses to remote areas the courier will deliver, but it will cost you
           somewhat more expensive - the cost in remote areas (such as, Judino, Stolbishte, Usad, High Mountain,
           Zalesny, Osinovo, Verkhne-Uslonsky district, Nizhne-Uslonsky district, etc.), please call.
        </p>",
    //catalog
    //delivery
    "delivery.delivery.title"=>"Flower delivery",
    "delivery.delivery.content"=>"<p> If you are interested in the delivery of flowers at home with a preliminary call or without (in the second case it will turn out
           a real surprise for the gifted), look at the site of the salon FLORISTAN.RU. A wide range of,
           possibility to perform individual solutions of any complexity, adequate prices - a bouquet of flowers with
           delivery will be exactly as you need, and will not be late for a minute.
        </p><p>The salon is open daily from 8:00 to 21:00. Orders are accepted around the clock, delivery outside working hours
           salon are accepted in advance.
        </p><p>Attention! Free delivery of flowers is provided if the addressee resides in Kazan, and
           the order was issued through the website. A bouquet of roses to remote areas the courier will deliver, but it will cost you
           somewhat more expensive - the cost in remote areas (such as, Judino, Stolbishte, Usad, High Mountain,
           Zalesny, Osinovo, Verkhne-Uslonsky district, Nizhne-Uslonsky district, etc.), please call.
        </p>",
    "delivery.payment.title"=>"payment of an order",
    "delivery.payment.content"=>"<p>PAYMENT can be made using the ONLINE ROBOKASSA service.</p><p>
              Make payment by transferring money to the bank card of our director
               SBERBANK, ALFABANK, CONNECTED.</p><p>
              Payment by e-commerce: Yandex Money, QIWI WALLET, WEB MONEY
            </p><p>
              If you have any questions. you can order a call back or
               call free number
            </p><p>
              8 800 77-55-086 and our specialists will gladly advise you and help you make a purchase!
            </p>",
    //feedbacks
    "feedbacks.title"=>"Feedback from our customers",
    "feedbacks.addbutton"=>"Give feedback",
    //about us
    "aboutus.title"=>"About us",
    "aboutus.line1"=>"<p>
              <b>FLORISTAN.RU</b> - it's always high quality customer service and fresh compositions. Peace
               floristics
               constantly makes adjustments and new trends.
            </p><p>
             Experts of our online store follow the novelties in this area, so the proposed
               bouquets
               always attractive and modern. They are able to express your most intimate feelings and emotions.
            </p>",
    "aboutus.line2"=>"<p><b>Why should I order in our flower shop?</b></p><p>Company FLORISTAN Since 2011, is engaged in selling fresh cut flowers from different corners of the earth.
               Thanks to our company, you have the opportunity to remotely select the flowers you are interested in and
               get them on time when you need to make a present. If, for any reason, you do not have
               the opportunity to pick up or give flowers, then we can do it for you. Our couriers at their best
               deliver flowers to the recipient and convey your wishes in written or oral form.
            </p>",
    "aboutus.line3"=>"<p>We position ourselves in the market, as the choice of freshly cut flowers and delivery of flowers to the addressee. You
               you will always be sure that the flowers chosen by you will be brought from the warehouse where our flowers are stored
               properly (at a low temperature and in a dark room), and not as in the shops where
               they stand in the sunlight and not always at the right temperature (which shortens the life of flowers).</p>",
    "aboutus.line4"=>"<p>Frequently Asked Question:\"<b>And where is the guarantee that I will transfer you money and you deliver
                 bouquet?</b>\"</p><p>Answer: Pay attention to the activity of our group vkontakte and feedback from our customers. We have
               Small town. To achieve the location of customers, we do our best to keep you
               satisfied and turned to us again.
            </p>",
    "aboutus.line5"=>"
        Also, our company cooperates with corporate clients for which a special program operates
         \"TO CORPORATIVE CLIENTS\". Thanks to this program you can get a discount of up to 30%. Today
         our partners are such organizations as: TRC \"ETHER\", \"GRANDTOTEL\", IT-PARK, Political party
         \"FAIR RUSSIA\" and many others. We are also partners of the annual contest \"MISS JUSTICE\"
         RUSSIA\".
      ",
    //contacts
    "contacts.title"=>"Our contacts",
    "contacts.socials"=>"We are in soc. network:",
    //catalog
    "catalog.parts.delivery.title"=>"Flower delivery",
    "catalog.parts.delivery.content"=>"<p> If you are interested in the delivery of flowers at home with a preliminary call or without (in the second case it will turn out
                 a real surprise for the gifted), look at the site of the salon FLORISTAN.RU. A wide range of,
                 possibility to perform individual solutions of any complexity, adequate prices - a bouquet of flowers with
                 delivery will be exactly as you need, and will not be late for a minute.
            </p>
            <p>The salon is open daily from 8:00 to 21:00. Orders are accepted around the clock, delivery outside working hours
                 salon are accepted in advance.
            </p>
            <p>Attention! Free delivery of flowers is provided if the addressee resides in Kazan, and
                 the order was issued through the website. A bouquet of roses to remote areas the courier will deliver, but it will cost you
                 somewhat more expensive - the cost in remote areas (such as, Judino, Stolbishte, Usad, High Mountain,
                 Zalesny, Osinovo, Verkhne-Uslonsky district, Nizhne-Uslonsky district, etc.), please call.
            </p>",
    "catalog.filter.price"=>"Price",
    "catalog.filter.price.from"=>"From",
    "catalog.filter.price.to"=>"to",
    "catalog.filter.usage"=>"Occasion",
    "catalog.filter.color"=>"Color",
    "catalog.filter.bouquet"=>"Bouquet with",
    "catalog.filter.reset"=>"Reset",
    "catalog.filter.show"=>"Show",
    "catalog.filter.title"=>"Filters",
    "catalog.items.new"=>"New",
    "catalog.items.hit"=>"Hit",
    "catalog.items.title"=>"Bouquet «:name»",
    "catalog.items.oneclick"=>"order in 1 click",
    "catalog.items.addcart"=>"Add to cart",
    "catalog.items.delcart"=>"Del from cart",
    "catalog.items.incart"=>"Bouquet added to cart",
    "catalog.show.bales"=>"Points",
    "catalog.show.contains"=>"Composition:",
    "catalog.show.delivery"=>"Free shipping",
    "catalog.show.cardfree"=>"Postcard as a gift",
    "catalog.show.grant"=>"Quality assurance",
    "catalog.show.size"=>"Size of bouquet:",
    "catalog.show.size.small"=>"Little",
    "catalog.show.size.medium"=>"Average",
    "catalog.show.size.big"=>"Big",
    "catalog.show.order.oneclick"=>"Buy in 1 click",
    "catalog.show.order.cart"=>"Add to Basket",
    'search.noresults'=>"Nothing found on your request",

    //cart
    'cart.items.empty'=>"Empty",
    "cart.title"=>"Cart",
    "cart.inside"=>"In the basket:",
    "cart.other-add"=>"Also usually ordered:",
    "cart.items.delete"=>"Delete",
    "cart.items.buy"=>"Buy",
    "cart.checkout.cart"=>"Postcard:",
    "cart.checkout.cart.description"=>"Postcard text",
    "cart.checkout.delivery"=>"Choose a delivery option:",
    "cart.checkout.delivery.self"=>"Pickup",
    "cart.delivery.free"=>"free",
    "cart.checkout.contacts"=>"Your contacts:",
    "cart.checkout.contacts.phone"=>"Phone",
    "cart.checkout.contacts.name"=>"Name",
    "cart.checkout.comment"=>"Comment to the order",
    "cart.checkout.our_contacts"=>"Recipient Contacts:",
    "cart.checkout.our_contacts.phone"=>"Phone",
    "cart.checkout.our_contacts.name"=>"Name",
    "cart.checkout.our_contacts.address"=>"Address of beneficiaries",
    "cart.checkout.our_contacts.need_to_call"=>"I do not know the exact address.<br/>It is necessary to clarify by phone",
    "cart.payment"=>"Choose payment option:",
    "cart.pay.self"=>"Cash payment",
    "cart.pay.online"=>"Payment online",
    "cart.payment.balls"=>"Pay by points:",
    "cart.payment.balls.customer"=>"Total on the account:",
    "cart.payment.balls.size"=>":balls points",
    "cart.payment.usage"=>"Points for payment:",
    "cart.checkout.total"=>"Order price:",
    "cart.checkout.balls-per-order"=>"Accrued points for the order:",
    "cart.checkout.balls-payment"=>"Payment by points:",
    "cart.checkout.full-price"=>"Total:",
    "cart.checkout.send"=>"Checkout",
    "cart.balls"=>"Points on your account:",
    "cart.balls.to"=>"Points to be accrued:",
    "cart.order"=>"Proceed to Checkout",
    "cart.success"=>"Order is processed",
    "cart.success.inside"=>"Order №:number successfully issued",
    "cart.checkout.delivery-time"=>"Delivery date:",
    "cart.checkout.contacts.delivery_time"=>"Time",
    "cart.checkout.contacts.delivery_date"=>"Date",
    "articles.title"=>"Articles",
    "articles.some"=>"similar articles",
    "review.title"=>"Leave review",
    "review.fio"=>"First name",
    "review.city"=>"Link to VK",
    "review.review"=>"Review",
    "review.photo"=>"Photo bouquet",
    "review.user"=>"Avatar",
    "review.send"=>"send review",
    "review.cancel"=>"Cancel",
    "review.edit"=>"Edit",
    "review.confirm"=>"Confirm",
    "review.remove"=>"Remove",
    "review.sended"=>"Review send",
    "review.sended.desc"=>"You review will be sended and coming up after moderation"
];