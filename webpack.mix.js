let mix = require('laravel-mix');

function sitesAssets() {
  mix.styles([
    'resources/assets/css/site/reset.css',
    'resources/assets/css/site/owl.carousel.min.css',
      'resources/assets/css/site/flexslider.css',
    'resources/assets/css/site/style.css',
    'resources/assets/css/site/tablet.css',
    'resources/assets/css/site/mobile.css',
      'resources/assets/css/site/bootstrap-datepicker3.standalone.min.css',
      'resources/assets/css/site/jquery.timepicker.min.css',
      'resources/assets/css/site/slim.min.css'
  ], 'public/css/site/styles1.css');

  mix.scripts([

    'resources/assets/js/site/jquery-3.3.1.min.js',
      'resources/assets/js/site/slim.kickstart.min.js',
    'resources/assets/js/site/owl.carousel.min.js',
      'resources/assets/js/site/jquery.flexslider-min.js',
    'resources/assets/js/site/helpers.js',
    'resources/assets/js/site/jquery-ui.min.js',
    'resources/assets/js/site/jquery.maskedinput.min.js',
    'resources/assets/js/site/bootstrap-datepicker.min.js',
      'resources/assets/js/site/bootstrap-datepicker.ru.min.js',
      'resources/assets/js/site/jquery.timepicker.min.js',
    'resources/assets/js/site/js.js'
  ], 'public/js/site/scripts1.js');

    mix.scripts(['resources/assets/js/site/cart.js'],"public/js/site/cart.js");

  mix.copy('resources/assets/img/site/*', 'public/img/site/');
}

function adminAssets() {
  mix.styles([
    'resources/assets/css/admin/bootstrap.min.css',
    'resources/assets/css/admin/plugins.css',
    'resources/assets/css/admin/main.css',
    'resources/assets/css/admin/coral.css',
      'resources/assets/css/admin/summernote.css'
  ], 'public/css/admin/styles.css');

  mix.scripts([
    'resources/assets/js/admin/jquery.min.js',
    'resources/assets/js/admin/bootstrap.min.js',
    'resources/assets/js/admin/plugins.js',
      'resources/assets/js/admin/summernote.min.js',
      'resources/assets/js/admin/summernote-ru-RU.min.js',
    'resources/assets/js/admin/app.js',
  ], 'public/js/admin/scripts.js');

  mix.copy('resources/assets/js/admin/modernizr.min.js', 'public/js/admin/modernizr.min.js');
  mix.copy('resources/assets/js/admin/login.js', 'public/js/admin/login.js');

  mix.copyDirectory('resources/assets/fonts/admin', 'public/fonts/admin');

  mix.copyDirectory('resources/assets/img/admin', 'public/img/admin');
  mix.copyDirectory('resources/assets/css/admin/font', 'public/css/admin/font');
}

function franchiseeAssets() {
    mix.styles([
        'resources/assets/css/franchisee/bootstrap.min.css',
        'resources/assets/css/franchisee/plugins.css',
        'resources/assets/css/franchisee/main.css',
        'resources/assets/css/franchisee/coral.css'
    ], 'public/css/franchisee/styles.css');

    mix.scripts([
        'resources/assets/js/franchisee/jquery.min.js',
        'resources/assets/js/franchisee/bootstrap.min.js',
        'resources/assets/js/franchisee/plugins.js',
        'resources/assets/js/franchisee/app.js'
    ], 'public/js/franchisee/scripts.js');

    mix.copy('resources/assets/js/franchisee/modernizr.min.js', 'public/js/franchisee/modernizr.min.js');
    mix.copy('resources/assets/js/franchisee/login.js', 'public/js/franchisee/login.js');

    mix.copyDirectory('resources/assets/fonts/franchisee', 'public/fonts/franchisee');

    mix.copyDirectory('resources/assets/img/franchisee', 'public/img/franchisee');
}

sitesAssets();
adminAssets();
franchiseeAssets();