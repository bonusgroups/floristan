<?php

use Illuminate\Database\Seeder;

/**
 * Class BouquetCategorySeeder
 */
class BouquetCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Букеты по акции',
            'Букет из роз',
            'Свадебная флористика',
            'Букеты в коробках',
            'Композиции',
            'Подарки',
            'Все букеты',
        ];

        foreach ($names as $name) {
            \App\Models\BouquetCategory::create([
                'name_ru' => $name,
                'name_en' => $name,
            ]);
        }
    }
}
