<?php

use App\Models\Bouquet;
use App\Models\BouquetCategory;
use App\Models\BouquetColor;
use App\Models\BouquetImage;
use App\Models\BouquetReason;
use App\Models\Flower;
use Illuminate\Database\Seeder;

/**
 * Class BouquetWithAllRelationsSeeder
 */
class BouquetWithAllRelationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bouquet = factory(Bouquet::class)->create([
            'name_en' => 'Full bouquet',
            'name_ru' => 'Особенный букет',
        ]);

        $this->addImages($bouquet);
        $this->addColors($bouquet);
        $this->addReasons($bouquet);
        $this->addFlowers($bouquet);
        $this->addCategories($bouquet);
    }

    /**
     * @param Bouquet $bouquet
     */
    private function addImages(Bouquet $bouquet)
    {
        factory(BouquetImage::class, 3)->create([
            'bouquet_id' => $bouquet->getKey(),
        ]);
    }

    /**
     * @param Bouquet $bouquet
     */
    private function addColors(Bouquet $bouquet)
    {
        $colors = factory(BouquetColor::class, 5)->create();

        foreach ($colors as $color) {
            $bouquet->colors()->attach($color);
        }
    }

    /**
     * @param Bouquet $bouquet
     */
    private function addReasons(Bouquet $bouquet)
    {
        $reasons = factory(BouquetReason::class, 5)->create();

        foreach ($reasons as $reason) {
            $bouquet->reasons()->attach($reason);
        }
    }

    /**
     * @param Bouquet $bouquet
     */
    private function addFlowers(Bouquet $bouquet)
    {
        /** @var \Illuminate\Database\Eloquent\Collection $flowers */
        $flowers = factory(Flower::class, 15)->create()->split(3);
        $faker = \Faker\Factory::create();

        /** @var Flower $flower */
        foreach ($flowers as $flowersGroup) {
            foreach ($flowersGroup as $flower) {
                $flower->prices()->create([
                    'price' => $faker->biasedNumberBetween(0, 300),
                    'franchisee_id' => $bouquet->franchisee_id,
                ]);
            }
        }

        foreach ($flowers[0] as $flower) {
            $bouquet->flowers()->attach($flower, [
                'count' => $faker->biasedNumberBetween(1, 4),
                'size' => Bouquet::SIZE_BIG,
            ]);
        }

        foreach ($flowers[1] as $flower) {
            $bouquet->flowers()->attach($flower, [
                'count' => $faker->biasedNumberBetween(1, 4),
                'size' => Bouquet::SIZE_MIDDLE,
            ]);
        }

        foreach ($flowers[2] as $flower) {
            $bouquet->flowers()->attach($flower, [
                'count' => $faker->biasedNumberBetween(1, 4),
                'size' => Bouquet::SIZE_SMALL,
            ]);
        }
    }

    /**
     * @param Bouquet $bouquet
     */
    private function addCategories(Bouquet $bouquet)
    {
        $categories = BouquetCategory::inRandomOrder()->limit(3)->get();

        $bouquet->categories()->attach($categories);
    }
}
