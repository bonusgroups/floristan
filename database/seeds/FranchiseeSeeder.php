<?php

use Illuminate\Database\Seeder;

/**
 * Class FranchiseeSeeder
 */
class FranchiseeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Franchisee::class)->create([
            'city_ru' => 'Новосибирск',
            'city_en' => 'Novosibirsk',
            'domain' => 'novosibirsk.' . env('APP_BASE_DOMAIN'),
        ]);

        factory(\App\Models\Franchisee::class)->create([
            'city_ru' => 'Томск',
            'city_en' => 'Tomsk',
            'domain' => 'tomsk.' . env('APP_BASE_DOMAIN'),
        ]);
    }
}
