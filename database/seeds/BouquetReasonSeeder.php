<?php

use App\Models\BouquetReason;
use Illuminate\Database\Seeder;

/**
 * Class BouquetReasonSeeder
 */
class BouquetReasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(BouquetReason::class)->create();
    }
}
