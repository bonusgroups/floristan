<?php

use App\Models\AdminUser;
use Illuminate\Database\Seeder;

/**
 * Class DefaultAdminSeeder
 */
class DefaultAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminUser::create([
            'email' => 'bakautovalex@gmail.com',
            'password' => Hash::make('password'),
            'name' => 'Alexander',
        ]);
    }
}
