<?php

use App\Models\Flower;
use Illuminate\Database\Seeder;

/**
 * Class FlowerSeeder
 */
class FlowerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $realFlowerNames = [
            'Хризантема',
            'Роза',
            'Тюльпан',
            'Альстрамерия',
            'Астра',
        ];
        $flowers = factory(Flower::class, 10)->create();
        $faker = \Faker\Factory::create();

        /** @var Flower $flower */
        foreach ($flowers as $flower) {
            $flower->prices()->create([
                'price' => $faker->biasedNumberBetween(0, 300),
            ]);
        }

        foreach ($realFlowerNames as $realFlowerName) {
            /** @var Flower $flower */
            $flower = factory(Flower::class)->create(['name_ru' => $realFlowerName]);
            $flower->prices()->create([
                'price' => $faker->biasedNumberBetween(0, 300),
            ]);
        }
    }
}
