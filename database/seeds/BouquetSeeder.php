<?php

use Illuminate\Database\Seeder;

/**
 * Class BouquetSeeder
 */
class BouquetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Bouquet::class, 25)->create();
    }
}
