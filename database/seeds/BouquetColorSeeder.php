<?php

use App\Models\BouquetColor;
use Illuminate\Database\Seeder;

/**
 * Class BouquetColorSeeder
 */
class BouquetColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(BouquetColor::class)->create(10);
    }
}
