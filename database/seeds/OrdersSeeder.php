<?php

use Illuminate\Database\Seeder;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Order::class, 10)->create()->each(function ($order){

            $bouquets = \App\Models\Bouquet::all()->random(3)->pluck('id')->toArray();

            $order->bouquets()->sync($bouquets);

        });
    }
}
