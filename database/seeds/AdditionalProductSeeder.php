<?php

use App\Models\AdditionalProduct;
use Illuminate\Database\Seeder;

/**
 * Class AdditionalProductSeeder
 */
class AdditionalProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AdditionalProduct::class, 15)->create();
    }
}
