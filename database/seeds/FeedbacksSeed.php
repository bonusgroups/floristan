<?php

use Illuminate\Database\Seeder;

class FeedbacksSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $feedbacks = factory(\App\Models\Feedbacks::class,10)->create();
    }
}
