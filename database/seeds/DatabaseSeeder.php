<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BouquetCategorySeeder::class);

        if (App::environment() != 'production') {
            $this->call(FranchiseeSeeder::class);
            $this->call(DeliverySeeder::class);
            $this->call(DefaultAdminSeeder::class);
            $this->call(AdditionalProductSeeder::class);
            $this->call(FlowerSeeder::class);
            $this->call(BouquetWithAllRelationsSeeder::class);
            $this->call(BouquetSeeder::class);
            $this->call(FeedbacksSeed::class);
        }
    }
}
