<?php

use App\Models\BouquetImage;
use Illuminate\Database\Seeder;

/**
 * Class BouquetImageSeeder
 */
class BouquetImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(BouquetImage::class, 6)->create();
    }
}
