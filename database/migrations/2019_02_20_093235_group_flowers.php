<?php

use App\Models\Flower;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupFlowers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            'Гербера' => [
                'Гербера Мини'
            ],
            'Ирис белый' => [
                'Ирис синий'
            ],
            'Роза 40 см' => [
                'Роза 50 см',
                'Роза 60 см',
                'Роза Эквадор',
                'Роза Кения 40 см',
                'Роза кустовая',
                'Роза пионовидная',
                'Роза кустовая пионовидная',
                'Роза Cuppucino'
            ],
            'Хризантема Кустовая' => [
                'Хризантема Одноголовая',
                'Хризантема Сантини'
            ],
            'Шляпная коробка (маленькая)' => [
                'Шляпная коробка (средняя)',
                'Шляпная коробка (большая)'
            ],
            'Гвоздика' => [
                'Гвоздика Кустовая',
                'Гвоздика грин трик'
            ],
            'Корзина большая' => [
                'Корзина средняя',
                'Корзина маленькая'
            ],
            'Коробочка для композиции (большая)' => [
                'Коробочка для композиции (средняя)',
                'Коробочка для композиции (маленькая)'
            ],
            'Орхидея Цимбидиум' => [
                'Орхидея Ванда'
            ]
        ];

        foreach ($data as $name => $names) {
            $flower = Flower::where('name_ru', $name)->first();

            foreach ($names as $otherName) {
                $otherFlower = Flower::where('name_ru', $otherName)->first();

                if (!$otherFlower) continue;

                $otherFlower->prices()->delete();

                DB::table('bouquet_flower')
                    ->where('flower_id', $otherFlower->id)
                    ->update([
                        'flower_id' => $flower->id
                    ]);

                $otherFlower->delete();
            }
        }

        $renameData = [
            'Ирис белый' => 'Ирис',
            'Роза 40 см' => 'Роза',
            'Хризантема Кустовая' => 'Хризантема',
            'Шляпная коробка (маленькая)' => 'Шляпная коробка',
            'Корзина большая' => 'Корзина',
            'Коробочка для композиции (большая)' => 'Коробочка для композиции',
            'Орхидея Цимбидиум' => 'Орхидея'
        ];

        foreach ($renameData as $name => $newName) {
            $flower = Flower::where('name_ru', $name)->first();

            if (!$flower) continue;

            $flower->name_ru = $newName;
            $flower->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
