<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNameToBouquetColors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('bouquet_colors', function ($table) {
            $table->string('name');
            $table->dropColumn('hex_value');
        });

        DB::table('bouquet_colors')->delete();
        DB::table('bouquet_bouquet_color')->delete();

        foreach (['Яркий', 'Нежный'] as $name) {
            $model = new \App\Models\BouquetColor();
            $model->name = $name;
            $model->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
