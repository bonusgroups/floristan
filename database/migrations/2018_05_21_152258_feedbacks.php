<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Feedbacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("feedbacks",function(Blueprint $t) {
            $t->unsignedBigInteger("id",true);
            $t->integer("franchisee_id");
            $t->string("name");
            $t->string("vk_link")->default("");
            $t->text("description");
            $t->string("photo");
            $t->string("avatar");
            $t->boolean("active")->default(false);
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
