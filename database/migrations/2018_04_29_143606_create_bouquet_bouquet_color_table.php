<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBouquetBouquetColorTable
 */
class CreateBouquetBouquetColorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bouquet_bouquet_color', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bouquet_id')->index();
            $table->unsignedInteger('bouquet_color_id')->index();

            $table->foreign('bouquet_id')
                ->references('id')
                ->on('bouquets')
                ->onDelete('cascade');
            $table->foreign('bouquet_color_id')
                ->references('id')
                ->on('bouquet_colors')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bouquet_bouquet_color');
    }
}
