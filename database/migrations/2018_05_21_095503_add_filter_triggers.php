<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class AddFilterTriggers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $triggers = ['DROP TRIGGER IF EXISTS `fl_trigger_bouquet_flower_insert`',
        'DROP TRIGGER IF EXISTS `fl_trigger_bouquet_flower_update`',
        'DROP TRIGGER IF EXISTS `fl_trigger_bouquet_flower_delete`',
        'DROP TRIGGER IF EXISTS `fl_trigger_flower_prices_insert`',
        'DROP TRIGGER IF EXISTS `fl_trigger_flower_prices_update`',
        'DROP TRIGGER IF EXISTS `fl_trigger_flower_prices_delete`',
        'DROP TRIGGER IF EXISTS `fl_trigger_bouquets_insert`',
        'DROP TRIGGER IF EXISTS `fl_trigger_bouquets_update`',
        'DROP TRIGGER IF EXISTS `fl_trigger_bouquets_delete`',

 'CREATE TRIGGER `fl_trigger_bouquet_flower_insert` AFTER INSERT  ON `bouquet_flower` FOR EACH ROW 
        BEGIN
            CALL fl_calc_franchisee_price();
        END',
        
 'CREATE TRIGGER `fl_trigger_bouquet_flower_update` AFTER  UPDATE ON `bouquet_flower` FOR EACH ROW 
        BEGIN
            CALL fl_calc_franchisee_price();
        END',
        
 'CREATE TRIGGER `fl_trigger_bouquet_flower_delete` AFTER DELETE ON `bouquet_flower` FOR EACH ROW 
        BEGIN
            CALL fl_calc_franchisee_price();
        END',
        
         'CREATE TRIGGER `fl_trigger_flower_prices_insert` AFTER INSERT ON `flower_prices` FOR EACH ROW 
        BEGIN
            CALL fl_calc_franchisee_price();
        END',
        
        'CREATE TRIGGER `fl_trigger_flower_prices_update` AFTER UPDATE ON `flower_prices` FOR EACH ROW 
        BEGIN
            CALL fl_calc_franchisee_price();
        END',
        
        'CREATE TRIGGER `fl_trigger_flower_prices_delete` AFTER DELETE ON `flower_prices` FOR EACH ROW 
        BEGIN
            CALL fl_calc_franchisee_price();
        END',
        
        'CREATE TRIGGER `fl_trigger_bouquets_insert` AFTER INSERT ON `bouquets` FOR EACH ROW 
        BEGIN
            CALL fl_calc_franchisee_price();
        END',
        
        'CREATE TRIGGER `fl_trigger_bouquets_update` AFTER UPDATE ON `bouquets` FOR EACH ROW 
        BEGIN
            CALL fl_calc_franchisee_price();
        END',
        
        'CREATE TRIGGER `fl_trigger_bouquets_delete` AFTER DELETE ON `bouquets` FOR EACH ROW 
        BEGIN
            CALL fl_calc_franchisee_price();
        END'];

        foreach ($triggers as $trigger) {
            DB::unprepared($trigger);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
