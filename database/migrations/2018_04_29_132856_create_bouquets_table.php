<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBouquetsTable
 */
class CreateBouquetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bouquets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ru')->index();
            $table->string('name_en')->index();
            $table->boolean('is_active')->index()->default(false);
            $table->boolean('is_new')->default(false)->index();
            $table->boolean('hit')->index()->default(false);
            $table->unsignedInteger('discount')->nullable();
            $table->unsignedInteger('franchisee_id')->index()->nullable();
            $table->timestamps();

            $table->foreign('franchisee_id')
                ->references('id')
                ->on('franchisees')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bouquets');
    }
}
