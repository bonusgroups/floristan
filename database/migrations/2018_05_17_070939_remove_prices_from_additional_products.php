<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePricesFromAdditionalProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("additional_products",function(Blueprint $t) {
            $t->dropForeign("additional_products_franchisee_id_foreign");
            $t->dropIndex("additional_products_franchisee_id_index");

            $t->dropColumn("price");
            $t->dropColumn("franchisee_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
