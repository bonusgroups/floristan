<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAdditionalProductsTable
 */
class CreateAdditionalProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ru')->nullable(false)->index();
            $table->string('name_en')->nullable(false)->index();
            $table->unsignedInteger('price')->nullable(false);
            $table->unsignedInteger('franchisee_id')->nullable()->index();
            $table->timestamps();

            $table->foreign('franchisee_id')
                ->references('id')
                ->on('franchisees')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_products');
    }
}
