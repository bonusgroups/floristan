<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Articles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("articles",function(Blueprint $t) {
            $t->unsignedBigInteger("id",true);
            $t->string("slug")->default("");
            $t->string("title_ru");
            $t->string("keywords_ru");
            $t->string("description_ru");
            $t->text("content_ru");
            $t->string("title_en");
            $t->string("keywords_en");
            $t->string("description_en");
            $t->text("content_en");
            $t->index(['slug']);
            $t->unique(['slug']);
            $t->string("image")->default("");
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
