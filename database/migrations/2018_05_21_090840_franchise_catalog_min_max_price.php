<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FranchiseCatalogMinMaxPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("franchisee_prices",function(Blueprint $t) {
            $t->integer("id");
            $t->float("min");
            $t->float("max");
            $t->string('size');
            $t->primary(["id","size"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
