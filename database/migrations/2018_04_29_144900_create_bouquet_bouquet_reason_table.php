<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBouquetBouquetReasonTable
 */
class CreateBouquetBouquetReasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bouquet_bouquet_reason', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bouquet_id')->index();
            $table->unsignedInteger('bouquet_reason_id')->index();
            $table->foreign('bouquet_id')
                ->references('id')
                ->on('bouquets')
                ->onDelete('cascade');
            $table->foreign('bouquet_reason_id')
                ->references('id')
                ->on('bouquet_reasons')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bouquet_bouquet_reason');
    }
}
