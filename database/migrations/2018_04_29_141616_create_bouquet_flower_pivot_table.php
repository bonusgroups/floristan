<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBouquetFlowerPivotTable
 */
class CreateBouquetFlowerPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bouquet_flower', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bouquet_id')->index();
            $table->unsignedInteger('flower_id')->index();
            $table->unsignedInteger('count');
            $table->string('size')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bouquet_flower');
    }
}
