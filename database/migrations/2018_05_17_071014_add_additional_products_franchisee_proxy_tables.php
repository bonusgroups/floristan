<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalProductsFranchiseeProxyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("additional_product_prices",function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('additional_product_id')->index();
            $table->unsignedInteger('franchisee_id')->index()->nullable();
            $table->unsignedInteger('price');
            $table->foreign('additional_product_id')
                ->references('id')
                ->on('additional_products')
                ->onDelete('cascade');
            $table->foreign('franchisee_id')
                ->references('id')
                ->on('franchisees')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
