<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDeliveriesTable
 */
class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ru')->nullable(false);
            $table->string('name_en')->nullable(false);
            $table->unsignedInteger('price')->nullable(false);
            $table->unsignedInteger('franchisee_id')->index();

            $table->foreign('franchisee_id')
                ->references('id')
                ->on('franchisees')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
