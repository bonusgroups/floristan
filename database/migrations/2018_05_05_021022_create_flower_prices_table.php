<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateFlowerPricesTable
 */
class CreateFlowerPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flower_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('flower_id')->index();
            $table->unsignedInteger('franchisee_id')->index()->nullable();
            $table->unsignedInteger('price');
            $table->foreign('flower_id')
                ->references('id')
                ->on('flowers')
                ->onDelete('cascade');
            $table->foreign('franchisee_id')
                ->references('id')
                ->on('franchisees')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flower_prices');
    }
}
