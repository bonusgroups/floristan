<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = \App\Models\BouquetCategory::where('slug', 'roses')->first();

        if ($category) {
            $category->name_ru = 'Розы';
            $category->save();
        }

        $category = \App\Models\BouquetCategory::where('slug', 'sales')->first();

        if ($category) {
            $category->name_ru = 'Акции';
            $category->save();
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
