<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('franchisee_id')->nullable();
            $table->unsignedInteger('delivery_id')->nullable();
            $table->text('postcard_text')->nullable();
            $table->date('delivery_date');
            $table->smallInteger('status')->default(0);
            $table->float('total_value')->default(0);
            $table->float('our_value')->default(0);
            $table->float('partner_value')->default(0);
            $table->integer('points_value')->default(0);
            $table->string('comment')->nullable();
            $table->string('client_name',50);
            $table->string('client_phone',20);
            $table->string('client_comment')->nullable();
            $table->string('recipient_name',50);
            $table->string('recipient_phone',20);
            $table->string('recipient_address')->nullable();
            $table->boolean('need_to_call')->default(false);
            $table->smallInteger('pay_type')->default(0);



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
