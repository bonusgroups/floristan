<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateFranchiseesTable
 */
class CreateFranchiseesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('franchisees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city_ru', 50)->index()->nullable(false);
            $table->string('city_en', 50)->index()->nullable(false);
            $table->string('domain', 100)->index()->nullable(false);
            $table->string('email', 100)->index()->nullable(false);
            $table->string('phone', 100)->nullable(false);
            $table->string('address_ru', 100)->nullable(false);
            $table->string('address_en', 100)->nullable(false);
            $table->string('opening_hours', 30)->nullable();
            $table->unsignedInteger('commission_for_bouquets')->nullable(false);
            $table->text('requisites')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('franchisees');
    }
}
