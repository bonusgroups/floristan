<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBouquetVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bouquet_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('value')->nullable()->index();
            $table->integer('bouquet_id')->nullable()->index();
            $table->string('ip')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bouquet_votes');
    }
}
