<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FinanceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("franchisees",function(Blueprint $t) {
            $t->decimal("balance",15,2)->default(0);
        });

        Schema::create("franchisee_logs", function(Blueprint $t) {
            $t->unsignedBigInteger("id",true);
            $t->unsignedBigInteger("franchisee_id");
            $t->string("type")->default("in");
            $t->integer("payment_type")->default(0);
            $t->decimal("balance",15,2);
            $t->decimal("append",15,2);

            $t->timestamps();
            $t->index(["franchisee_id","type","payment_type"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
