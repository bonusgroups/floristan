<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BouquetsAggPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("bouquets_franchisee_prices",function(Blueprint $t) {
            $t->unsignedBigInteger("id");
            $t->integer("franchisee_id");
            $t->string("size");
            $t->float("price");

            $t->primary(['id','franchisee_id','size']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
