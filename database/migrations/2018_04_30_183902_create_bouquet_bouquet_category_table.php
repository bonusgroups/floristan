<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBouquetBouquetCategoryTable
 */
class CreateBouquetBouquetCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bouquet_bouquet_category', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bouquet_id');
            $table->unsignedInteger('bouquet_category_id');
            $table->index(['bouquet_id', 'bouquet_category_id']);
            $table->foreign('bouquet_id')
                ->references('id')
                ->on('bouquets')
                ->onDelete('cascade');
            $table->foreign('bouquet_category_id')
                ->references('id')
                ->on('bouquet_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bouquet_bouquet_category');
    }
}
