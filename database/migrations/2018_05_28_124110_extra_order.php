<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtraOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("orders",function(Blueprint $t) {
            $t->timestamp("complete")->nullable()->default(null);
        });

        Schema::table("franchisee_logs",function(Blueprint $t) {
            $t->dropColumn("payment_type");
            $t->unsignedBigInteger("order_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
