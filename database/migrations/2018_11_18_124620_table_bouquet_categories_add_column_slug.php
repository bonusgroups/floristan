<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableBouquetCategoriesAddColumnSlug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bouquet_categories', function ($table) {
            $table->string('slug', 100)->nullable()->index();
        });

        $data = [
            7 => 'all',
            6 => 'presents',
            5 => 'compositions',
            4 => 'boxes',
            3 => 'wedding',
            2 => 'roses',
            1 => 'sales'
        ];

        foreach ($data as $id => $slug) {
            \App\Models\BouquetCategory::where('id', $id)->update([
                'slug' => $slug
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
