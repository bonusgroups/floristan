<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFilterProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = '
        CREATE PROCEDURE `fl_calc_franchisee_price`()
        BEGIN
        INSERT INTO franchisee_prices(`id`,`min`,`max`,`size`) 
         SELECT `fse`.`aid`,min(`fse`.`price`) as min, max(`fse`.`price`) as max, `fse`.`size` FROM (SELECT `ff`.`id`,`ff`.`aid`,SUM(`ff`.`price`) as `price`,`ff`.`size` FROM (SELECT `b`.`id`, `fp`.`franchisee_id` as `aid`, `fp`.`price`*`bf`.`count` * ((100-`b`.discount)/100) as `price` , `bf`.`size` 
FROM `bouquets` as `b` 
JOIN `bouquet_flower` as `bf` ON (`bf`.`bouquet_id` = `b`.`id`) 
JOIN `flower_prices` as `fp` ON (`fp`.`flower_id`=`bf`.`flower_id`)
GROUP BY `b`.`id`,`fp`.`franchisee_id`, `bf`.`size`,`bf`.`flower_id`) as ff GROUP BY `ff`.`id`,`ff`.`aid`, `ff`.`size` ) as fse GROUP BY `fse`.`aid`,`fse`.`size`
         ON DUPLICATE KEY UPDATE `min`=VALUES(`min`), `max`=VALUES(`max`), `size`=VALUES(`size`);
        END
        ';

        DB::unprepared('DROP procedure IF EXISTS fl_calc_franchisee_price');
        DB::unprepared($procedure);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
