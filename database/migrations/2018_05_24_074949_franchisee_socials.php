<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FranchiseeSocials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("franchisees",function(Blueprint $t) {
            $t->string("social_vk",255)->default("")->nullable();
            $t->string("social_fb",255)->default("")->nullable();
            $t->string("social_ig",255)->default("")->nullable();
            $t->string("social_wa",255)->default("")->nullable();
            $t->string("social_tg",255)->default("")->nullable();
            $t->string("social_vb",255)->default("")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
