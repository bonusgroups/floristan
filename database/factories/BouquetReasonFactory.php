<?php

use App\Models\BouquetReason;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(BouquetReason::class, function (Faker $faker) {
    return [
        'name_ru' => $faker->word,
        'name_en' => $faker->word,
    ];
});
