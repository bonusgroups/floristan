<?php

use App\Models\Flower;
use App\Models\Franchisee;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Flower::class, function (Faker $faker) {
    return [
        'name_ru' => $faker->word,
        'name_en' => $faker->word,
    ];
});
