<?php

use App\Models\BouquetColor;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(BouquetColor::class, function (Faker $faker) {
    return [
        'hex_value' => $faker->hexColor,
    ];
});
