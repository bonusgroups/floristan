<?php

use App\Models\Franchisee;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Article::class, function (Faker $faker) {

    $name_ru = implode(" ",$faker->words(10));

    $photo = file_get_contents(\Faker\Factory::create()->imageUrl(640, 480,'nature'));
    $photoName = $faker->uuid.".jpg";
    \Storage::disk('feedback_images')->put($photoName,$photo);

    return [
        'title_ru' => $name_ru,
        'title_en' => implode(" ",$faker->words(10)),
        'slug' => \App\Models\Article::slugPrepare($name_ru.microtime()),
        'keywords_ru'=>implode(" ",$faker->words(10)),
        'keywords_en'=>implode(" ",$faker->words(10)),
        'description_ru'=>implode(" ",$faker->words(10)),
        'description_en'=>implode(" ",$faker->words(10)),
        'content_ru'=>$faker->paragraphs(10,true),
        'content_en'=>$faker->paragraphs(10,true),
        'image'=>$photoName
    ];
});
