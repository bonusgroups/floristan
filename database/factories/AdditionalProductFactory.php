<?php

use App\Models\AdditionalProduct;
use App\Models\Franchisee;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(AdditionalProduct::class, function (Faker $faker) {
    $franchiseeId = $faker->boolean ? Franchisee::inRandomOrder()->firstOrNew([])->getKey() : null;

    return [
        'name_ru' => $faker->word,
        'name_en' => $faker->word
    ];
});
