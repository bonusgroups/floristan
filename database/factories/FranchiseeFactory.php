<?php

use App\Models\Franchisee;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Franchisee::class, function (Faker $faker) {
    return [
        'city_ru' => $faker->city,
        'city_en' => $faker->city,
        'domain' => $faker->url,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'address_ru' => $faker->address,
        'address_en' => $faker->address,
        'opening_hours' => '10:00-20:00',
        'commission_for_bouquets' => $faker->biasedNumberBetween(0, 20),
        'requisites' => $faker->bankAccountNumber,
    ];
});
