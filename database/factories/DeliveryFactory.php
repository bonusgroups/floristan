<?php

use App\Models\Delivery;
use App\Models\Franchisee;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Delivery::class, function (Faker $faker) {
    return [
        'name_ru' => $faker->word,
        'name_en' => $faker->word,
        'price' => $faker->biasedNumberBetween(0, 350),
        'franchisee_id' => Franchisee::inRandomOrder()->firstOrCreate(factory(Franchisee::class)->make()->toArray()),
    ];
});
