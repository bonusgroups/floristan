<?php

use App\Models\Bouquet;
use App\Models\BouquetImage;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(BouquetImage::class, function (Faker $faker) {
    return [
        'file_name' => 'bouquet' . $faker->biasedNumberBetween(1, 9) . '.png',
        'bouquet_id' => Bouquet::inRandomOrder()->firstOrCreate(factory(Bouquet::class)->make()->toArray()),
    ];
});
