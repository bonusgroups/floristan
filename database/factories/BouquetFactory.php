<?php

use App\Models\Bouquet;
use App\Models\Franchisee;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Bouquet::class, function (Faker $faker) {
    $franchisee = Franchisee::inRandomOrder()->firstOrCreate(factory(Franchisee::class)->make()->toArray());

    return [
        'name_ru' => $faker->word,
        'name_en' => $faker->word,
        'is_active' => $faker->boolean,
        'is_new' => $faker->boolean,
        'hit' => $faker->boolean,
        'discount' => $faker->biasedNumberBetween(0, 30),
        'franchisee_id' => $franchisee->getKey(),
    ];
});
