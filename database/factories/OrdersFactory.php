<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Order::class, function (Faker $faker) {
    $franchisee = \App\Models\Franchisee::all()->random();
     if (count($franchisee->deliveries)>0){
         $delivery_id = $franchisee->deliveries->random();
     } else {
         $delivery_id = null;
     }

   // $total_value = $faker->biasedNumberBetween(100,20000);


    return [
        'franchisee_id'=>$franchisee->id,
        'delivery_id'=>$delivery_id,
        'postcard_text'=>$faker->text,
        'delivery_date'=>$faker->date(),
        'total_value' =>  $faker->biasedNumberBetween(100,20000),
        'our_value'=>$faker->biasedNumberBetween(50,15000),
        'partner_value'=>$faker->biasedNumberBetween(25,7000),
        'points_value'=>$faker->biasedNumberBetween(10,1000),
        'comment'=>$faker->text(20),
        'client_name'=>$faker->name(),
        'client_phone'=>$faker->phoneNumber,
        'client_comment'=>$faker->text(),
        'recipient_name'=>$faker->name(),
        'recipient_phone'=>$faker->phoneNumber,
        'recipient_address'=>$faker->address,
        'need_to_call'=>$faker->boolean(30),
        'status'=>$faker->numberBetween(0,3),
        'pay_type'=>$faker->numberBetween(0,3)

    ];
});
