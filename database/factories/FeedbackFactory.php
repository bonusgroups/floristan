<?php
/**
 * Created by PhpStorm.
 * User: jonic
 * Date: 21.05.2018
 * Time: 19:06
 */

use Faker\Generator as Faker;

$factory->define(App\Models\Feedbacks::class, function (Faker $faker) {
    $photo = file_get_contents(\Faker\Factory::create()->imageUrl(640, 480,'nature'));
    $avatar = file_get_contents(\Faker\Factory::create()->imageUrl(640, 640,'people'));
    $photoName = $faker->uuid.".jpg";
    $avatarName = $faker->uuid.".jpg";
    \Storage::disk('feedback_images')->put($photoName,$photo);
    \Storage::disk('feedback_images')->put($avatarName,$avatar);

    $franchisee = \App\Models\Franchisee::orderByRaw("rand()")->first();

    return [
        'franchisee_id'=>$franchisee->id,
        'name' => $faker->name,
        'description'=>$faker->text,
        'vk_link'=> "https://vk.com/".$faker->name,
        'photo' => $photoName,
        'avatar'=>$avatarName
    ];
});